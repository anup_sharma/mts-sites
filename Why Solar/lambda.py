@app.route('/addcrmleadappdev', methods=['POST'])
def addcrmleadappdev():
    token = crmtoken()
    request_body = app.current_request.json_body
    print(request_body)
    print("Referral")
    fname = request_body['fname']
    lname = request_body['lname']
    address = request_body['full_address']
    phone = request_body['phone']
    email = request_body['email']
    product = "Solar"
    canvasser = "website@momentumsolar.com"
    utmcamp = request_body['utmcamp']
    utmsource = request_body['utmsource']
    utmchannel = request_body['utmchannel']
    clickid = request_body['clickid']
    subid = request_body['subid']
    comments = request_body['comments']
    optinurl = request_body['optinurl']
    repemail = "Website@momentumsolar.com"
    name = "Website Referral Program {} {}".format(fname, lname)
    crmurl = 'https://procustomsolarllcdev.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    crmbody['mts_email'] = email
    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    if 'demo_date_time' in request_body:
        demodatetime = request_body['demo_date_time']
        crmbody['mts_appointmenttimeind'] = demodatetime

    if 'cb_date_time' in request_body:
        demodatetime = request_body['cb_date_time']
        crmbody['mts_appointmentstarttime'] = demodatetime
    if 'city' in request_body:
        city = request_body['city']
        crmbody['mts_city'] = city
    if 'state' in request_body:
        state = request_body['state']
        crmbody['mts_state'] = state
    if "zip" in request_body:
        crmbody['mts_zipcode'] = request_body['zip']
    crmbody['mts_firstname'] = fname
    crmbody['mts_lastname'] = lname
    crmbody['mts_leadvendortrackingcampaign'] = utmcamp
    crmbody['mts_leadvendorsource'] = utmsource
    crmbody['mts_leadvendorchannel'] = utmchannel
    crmbody['mts_leadvendorclickid'] = clickid
    crmbody['mts_leadvendorsubid'] = subid
    crmbody['mts_leadvendoroptinurl'] = optinurl
    crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(fb5f949b-a664-eb11-a812-000d3a3ba9b8)"
    crmbody['mts_returnleadsource'] = 'Website Referral'
    crmbody['mts_leadadditionalcomments'] = comments
    crmbody['mts_demoappointmenttype'] = 962080000

    if len(name) < 1:
        return {'success': 'failed'}

    if len(phone) < 1:
        return {'success': 'failed'}

    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverifydevb(repemail, phone)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}