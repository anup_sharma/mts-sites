<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Momentum Solar - Why Solar</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
  
  <link href="css/custom-style.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
<style>
 
 span.ui-helper-hidden-accessible {
    display: none;
}
.ui-menu-item {
    font-size: 10px !important;
}
ul#ui-id-1 {
    height: 40% !important;
    overflow-y: scroll;
}
ul#ui-id-2 {
    height: 40% !important;
    overflow-y: scroll;
}
ul#ui-id-3 {
    height: 40% !important;
    overflow-y: scroll;
}
  </style>
  <script>
     
    jQuery(document).ready(function ($) {
      
      
      document.getElementById("loading-image").style.display = 'none';
      $('#phone').mask('(000)-000-0000');
      $('#opprtunity').mask('OP-000000');
        $(function () {
  
          $('form#leadformContainer').on('submit', function (e) {
           
            document.getElementById("loading-image").style.display = 'block';
            e.preventDefault();
            //alert("you are submitting" + $(this).serialize());
            $.ajax({
              type: 'post',
              url: 'ajax.php',
              cache: false,
             contentType: false, 
              processData: false, 
              data: new FormData(this),
              success: function (data) {
               // alert (data);
                document.getElementById("results").style.display = 'block';
                document.getElementById("leadformContainer").style.display = 'none'
                document.getElementById("loading-image").style.display = 'none';
              }
            });
  
          });
  
      });
      
    });
  
    $(document).ready(function() { 
      
      document.getElementById("results").style.display = 'none !important';
      $("#fileToUpload").change(function() {
          filename = this.files[0].name;
          $('#fileLabel')[0].lastChild.textContent = filename;
          //console.log(filename);
        });
    });
      </script>	
<style>
.celebratingSolar {
    display: block;
    position: relative;
    margin-top: 8%;
    margin-left: auto;
    margin-right: auto;
}
</style>
</head>

<body>



  <!-- Page Header -->
  <header class="masthead">
   
    <div class="container">

    
      <div class="row">
   
      
   <div class="row match-height-bootstrap-row">
          <!-- 2nd Column-->
          <div class="col-md-6 col-xs-12 secondColumn">
          <a href="https://www.momentumsolar.com/Why-Solar/"><img style="margin-top: 8%; display:block; margin-left:auto; margin-right:auto;" class="mslogo" src="img/MS_logo_top.png"> </a>
           <!--  Text on banner -->
             <img src="img/celebratingSolar.png" class="img-fluid celebratingSolar" style="margin-left:4%;"/>
             <a href="https://momentumsolar.com/what-are-the-benefits-of-going-solar/"><img src="img/low.png" class="img-fluid celebratingSolar"/></a>
             <a href="https://blog.resourcewatch.org/2019/05/02/which-countries-use-the-most-fossil-fuels/"> <img src="img/reduced.png" class="img-fluid celebratingSolar"/></a>
             <a href="https://www.energy.gov/eere/solar/articles/solar-homes-sell-premium-0"> <img src="img/increased.png" class="img-fluid celebratingSolar"/></a>

          </div>
        <!-- second column-->
          <div class="col-md-6 col-xs-12 firstColumn">
            <div class="col-3" style="height: 750px; margin-top:7%; max-width:100%;">
              <!-- logo -->
              
              
              
             <div id="results" class="topform" style="display:none;">
              <br><br><img src="img/thankyou.png" class="img-fluid celebratingSolar thankyou"/>
             
           </div>   
                <form class="form-form" method="POST" id="leadformContainer" style="height: 750px;" enctype="multipart/form-data">
              
                  <div class="form">
                    
                          <p class="title2">Name</p>
                          <div class="form-error" style="display:none">Please fix the errors below.</div>
                          <div class="form-group form-group2">
                              <input type="text" id="fname" name="fname" class="required" value="" placeholder="First Name*" autocomplete="off" aria-required="true" required="">
                              <input type="text" id="lname" name="lname" class="required" value="" placeholder="Last Name*" autocomplete="off" aria-required="true" required="">
                              <table class="table">
                                <tbody>
                                  <tr>
                                    <td>
                                       <span class="title2">Phone</span>
                                       <input type="text" id="phone" name="phone" class="required" value="" placeholder="(XXX)XXX-XXXX" autocomplete="off" aria-required="true" required="" maxlength="14">
                                   </td>
                                   <td>
                                        <span class="title2" >Email</span>
                                        
                                        <input type="text" id="email" name="email" cvalue="" placeholder="Email Address" autocomplete="off" class="required" required="">
                                  </td>
                                </tr>
                              </tbody>
                             </table>
                             <table class="table">
                                <tbody>
                                  <tr>
                                    <td>
                                      <span class="title2">Solar Specialist</span>
                                     <input type="text" id="solarspecialist" name="solarspecialist"  value="" placeholder="Enter your solar specialist’s first and last name" autocomplete="off" required="" class="required" >
                                    </td>
                                    <td>
                                        <span class="title2" >Opportunity #</span>
                                       <input type="text" id="opprtunity" name="opprtunity" cvalue="" placeholder="Enter your opportunity number" autocomplete="off" class="Enter your opportunity number" required="" class="required" >
                                       </td>
                                </tr>
                              </tbody>
                             </table>
                              <span class="title2">Why did you go solar?</span>
                              <input type="text" id="reason1" name="reason1" cvalue="" placeholder="Reason #1" autocomplete="off" class="required" required="" >
                              <input type="text" id="reason2" name="reason2" cvalue="" placeholder="Reason #2" autocomplete="off" class="required" required="" >
                              <input type="text" id="reason3" name="reason3" cvalue="" placeholder="Reason #3" autocomplete="off"  class="required" required="" >
                              <input type="text" id="reason4" name="reason4" cvalue="" placeholder="Reason #4" autocomplete="off" >
                              <input type="text" id="reason5" name="reason5" cvalue="" placeholder="Reason #5" autocomplete="off" >
                              <span class="title2">Anything else you would like us to know?</span>
                              <input type="text" id="additionalcomments" name="additionalcomments" cvalue="" placeholder="Enter additional comments here" autocomplete="off" >
                              
                             
                           <button type="submit" id="submitbtn"><img src="img/submit.png" /></button>
                           <div class="col-12 text-center" style="margin-left:40%; margin-top:2%;">
    
                            <img src="img/ajax-loader.gif" id="loading-image" style="display:none;" alt="Please Wait">
                          </div>

                              <!-- get utm paramters -->
                              <input type="hidden" name="utm_source" id="utm_source" value="<?php echo $_GET['utm_source'];?>">
                              <input type="hidden" name="utm_medium" id="utm_medium" value="<?php echo $_GET['utm_medium'];?>">
                              <input type="hidden" name="utm_campaign" id="utm_campaign" value="<?php echo $_GET['utm_campaign'];?>">
                              <input type="hidden" name="utm_content" id="utm_content" value="<?php echo $_GET['utm_content'];?>">
                               <!-- get optin url-->
                               <input type="hidden" name="optinurl" id="optinurl" value="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;?>" />
                              </div>
                

            </div></form>
          </div> <!-- column-->
          <!-- Form Content -->
          
        </div>
 
    


      </div>
    


      </div>
    </div>
  </header>



  <!-- Footer -->

  <footer class="footer">
      
        <center>
            <p id="footertext">Copyright © 2021 Momentum Solar All Rights Reserved. D|B|A Momentum Solar *Subject to applicable terms and conditions. Eligibility and savings is determined by municipal utility, roof condition and roof space, azimuth, tree location, shading and other factors. HIC # – CA: 1026366; CT: HIC.0646323; FL: CVC57036; MA: 192204; NJ: 13VH05539000; NYC: 2042828-DCA; PA: PA131435; TX: 31915; MA: 192204; AZ: 237781. EL License # - CA: 1026366; CO: ME.3000298; CT: HIC.0646323; DE: T100006310; FL: EC13008217; IL: SE7545; MA: ME: 10024-ELMR; ME: MS60021311; NH: 14277M: NJ: 34EB01591300; NYC: 12943; RI: B-014803; TX: 409620; UT: 11242273-5502; VA: 2710069151; VT: EM-06808</p>
        </center>
      </footer>


  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
  <script src="js/custom-js.js"></script>
  <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>

        jQuery(document).ready(function ($) {     
      
            var availableTags = ["solar", 
"solar energy", 
"solar energy benefits", 
"solar benefits", 
"green energy", 
"solar battery", 
"solar storage", 
"solar panels", 
"solar panel energy", 
"solar panel system", 
"how to go solar", 
"solar power", 
"solar power benefits", 
"solar power storage", 
"clean energy", 
"clean power", 
"price-protected", 
"price protected", 
"homeowners", 
"solar homeowner", 
"solar home", 
"solar panel home", 
"utility", 
"utility bills", 
"low utility bills", 
"lower utility bills", 
"grid", 
"free grid", 
"solar system", 
"solar panel system", 
"solar panel energy", 
"sun", 
"solar sun energy", 
"solar panel energy system", 
"renewable", 
"renewable energy", 
"renewable energy system", 
"grid free", 
"battery", 
"battery storage", 
"battery storage system", 
"switch to solar", 
"switch solar", 
"solar switch", 
"green solar energy", 
"solar green energy", 
"what is solar energy", 
"solar energy definition", 
"solar energy advantages", 
"advantages of solar energy", 
"solar energy is used for", 
"uses of solar energy", 
"solar energy disadvantages", 
"solar energy uses", 
"solar energy users", 
"solar energy usage", 
"how does solar energy work", 
"solar energy information", 
"solar energy companies", 
"benefits of solar energy", 
"solar energy benefits", 
"solar energy system", 
"solar energy facts", 
"solar energy systems", 
"solar energy corporation of India", 
"what is solar energy definition", 
"definition of solar energy", 
"is solar energy renewable", 
"application of solar energy", 
"solar energy projects", 
"solar energy project", 
"solar energy materials and solar cells", 
"examples of solar energy", 
"solar energy equipment supplier", 
"how solar energy works", 
"is solar energy important", 
"how solar energy is produced", 
"solar energy the future", 
"history of solar energy", 
"future of solar energy", 
"solar energy history", 
"solar energy storage", 
"solar energy types", 
"solar energy future", 
"about solar energy", 
"what solar energy is used for", 
"pros of solar energy", 
"source of solar energy", 
"solar energy at home", 
"solar and wind energy", 
"solar energy industries association", 
"solar energy for home", 
"solar energy for homes", 
"solar energy for kids", 
"solar energy home", 
"solar energy international", 
"solar energy power", 
"solar energy sources", 
"solar energy source", 
"passive solar energy", 
"solar power energy", 
"solar energy pros", 
"solar energy use", 
"solar energy how it works", 
"solar energy environmental impact", 
"solar energy environmental impacts", 
"solar energy quotes", 
"solar energy panel", 
"solar energy panels", 
"solar energy meaning", 
"solar energy calculator", 
"solar energy calculation", 
"solar energy quote", 
"solar energy united states", 
"solar energy cost per kwh", 
"is solar energy efficient", 
"solar energy USA", 
"solar energy model", 
"solar energy models", 
"solar energy investments", 
"u.s. solar energy", 
"solar energy solutions", 
"solar energy efficiency", 
"solar energy in the us", 
"solar energy in USA", 
"solar energy us", 
"where does solar energy come from", 
"where do solar energy come from", 
"why solar energy is good", 
"solar energy fun facts", 
"solar energy kits", 
"solar energy kit", 
"solar energy price", 
"solar energy prices", 
"solar energy problems", 
"solar energy slogans", 
"solar energy origin", 
"solar energy poster", 
"solar energy photovoltaic", 
"photovoltaic solar energy", 
"solar energy is", 
"solar energy", 
"solar energy benefits to the environment", 
"how solar energy is converted into electricity", 
"solar energy with battery storage", 
"5 disadvantages of solar energy", 
"how solar energy is stored", 
"solar energy storage batteries", 
"solar energy storage battery", 
"solar energy in Florida", 
"solar energy on houses", 
"solar energy for house", 
"solar energy technology", 
"solar energy technologies", 
"solar panel energy", 
"solar energy plant", 
"solar energy plants", 
"solar energy generation", 
"solar energy generator", 
"solar energy Germany", 
"solar energy Florida", 
"solar energy courses", 
"solar energy conversion", 
"solar energy batteries", 
"solar energy battery", 
"solar energy car", 
"solar energy cars", 
"solar energy houses", 
"solar energy house", 
"solar energy new your", 
"solar energy Georgia", 
"solar energy California", 
"solar energy Texas", 
"solar energy Nevada", 
"solar energy Arizona", 
"solar energy Massachusetts", 
"solar energy Connecticut", 
"solar energy new jersey", 
"solar energy renewable", 
"solar energy uses in daily life", 
"uses of solar energy in daily life", 
"solar energy system for home", 
"where solar energy is used", 
"how solar energy is generated", 
"solar energy storage systems", 
"solar energy home system", 
"solar energy to electricity", 
"solar energy stocks India", 
"solar energy is sustainable", 
"is solar energy sustainable", 
"passive solar energy definition", 
"solar energy as electricity", 
"solar energy installation", 
"solar energy installers", 
"solar energy requirements", 
"solar energy for ac", 
"solar energy solar energy", 
"solar energy 101", 
"solar energy ac", 
"solar energy fact", 
"solar energy usage in the us", 
"solar energy use in the united states", 
"what is solar energy made of", 
"why solar energy is bad for the environment", 
"where solar energy can be used", 
"solar energy system for home in Pakistan", 
"solar energy is what type of resource", 
"compare passive and active solar energy", 
"natural gas vs solar energy cost analysis", 
"can solar energy replace fossil fuels", 
"solar energy and how it works", 
"does solar energy work at night", 
"is solar energy a nonrenewable resource", 
"solar energy as a renewable source of energy", 
"solar energy how much does it cost", 
"solar energy and climate change", 
"what is solar energy pdf", 
"solar energy and solar cells", 
"solar energy benefits and costs", 
"solar energy and solar power", 
"solar energy costs and benefits", 
"solar energy cost per watt", 
"solar energy tax credit irs", 
"when did solar energy start", 
"how solar energy works", 
"solar energy vs solar power", 
"sell solar energy to grid", 
"sun and solar energy", 
"solar pv energy solutions", 
"solar energy at night", 
"solar energy advantages Wikipedia", 
"united solar energy reviews", 
"solar energy light bulb", 
"solar energy efficiency facts", 
"solar energy management system", 
"solar energy history facts", 
"solar energy system information", 
"solar energy solar panels", 
"solar energy use in the us", 
"solar energy greenhouse", 
"affordable solar energy", 
"solar home energy", 
"solar energy exchange", 
"solar energy loans", 
"solar energy phone", 
"is solar energy"];
            $("#reason1").autocomplete({
                source: function(requestObj, responseFunc) {
                    var matchArry = availableTags.slice(); // Copy the array
                    var srchTerms = $.trim(requestObj.term).split(/\s+/);
                    // For each search term, remove non-matches.
                    $.each(srchTerms, function(J, term) {
                        var regX = new RegExp(term, "i");
                        matchArry = $.map(matchArry, function(item) {
                            return regX.test(item) ? item : null;
                        });
                    });
                    // Return the match results.
                    responseFunc(matchArry);
                },
                open: function(event, ui) {
                    var resultsList = $("ul.ui-autocomplete > li.ui-menu-item > a");
                    var srchTerm = $.trim($("#reason1").val()).split(/\s+/).join('|');
                    // Loop through the results list and highlight the terms.
                    resultsList.each(function() {
                        var jThis = $(this);
                        var regX = new RegExp('(' + srchTerm + ')', "ig");
                        var oldTxt = jThis.text();
                        jThis.html(oldTxt.replace(regX, '<span class="srchHilite">$1</span>'));
                    });
                }
            });

            $("#reason2").autocomplete({
                source: function(requestObj, responseFunc) {
                    var matchArry = availableTags.slice(); // Copy the array
                    var srchTerms = $.trim(requestObj.term).split(/\s+/);
                    // For each search term, remove non-matches.
                    $.each(srchTerms, function(J, term) {
                        var regX = new RegExp(term, "i");
                        matchArry = $.map(matchArry, function(item) {
                            return regX.test(item) ? item : null;
                        });
                    });
                    // Return the match results.
                    responseFunc(matchArry);
                },
                open: function(event, ui) {
                    var resultsList = $("ul.ui-autocomplete > li.ui-menu-item > a");
                    var srchTerm = $.trim($("#reason1").val()).split(/\s+/).join('|');
                    // Loop through the results list and highlight the terms.
                    resultsList.each(function() {
                        var jThis = $(this);
                        var regX = new RegExp('(' + srchTerm + ')', "ig");
                        var oldTxt = jThis.text();
                        jThis.html(oldTxt.replace(regX, '<span class="srchHilite">$1</span>'));
                    });
                }
            });

            $("#reason3").autocomplete({
                source: function(requestObj, responseFunc) {
                    var matchArry = availableTags.slice(); // Copy the array
                    var srchTerms = $.trim(requestObj.term).split(/\s+/);
                    // For each search term, remove non-matches.
                    $.each(srchTerms, function(J, term) {
                        var regX = new RegExp(term, "i");
                        matchArry = $.map(matchArry, function(item) {
                            return regX.test(item) ? item : null;
                        });
                    });
                    // Return the match results.
                    responseFunc(matchArry);
                },
                open: function(event, ui) {
                    // This function provides no hooks to the results list, so we have to trust the selector, for now.
                    var resultsList = $("ul.ui-autocomplete > li.ui-menu-item > a");
                    var srchTerm = $.trim($("#reason1").val()).split(/\s+/).join('|');
                    // Loop through the results list and highlight the terms.
                    resultsList.each(function() {
                        var jThis = $(this);
                        var regX = new RegExp('(' + srchTerm + ')', "ig");
                        var oldTxt = jThis.text();
                        jThis.html(oldTxt.replace(regX, '<span class="srchHilite">$1</span>'));
                    });
                }
            });
        
      });  
  </script>
</body>

</html>
