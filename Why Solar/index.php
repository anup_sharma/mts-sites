<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Momentum Solar - Why Solar</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
  
  <link href="css/custom-style.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
<style>
 
 span.ui-helper-hidden-accessible {
    display: none;
}
.ui-menu-item {
    font-size: 10px !important;
}
ul#ui-id-1 {
    height: 40% !important;
    overflow-y: scroll;
}
ul#ui-id-2 {
    height: 40% !important;
    overflow-y: scroll;
}
ul#ui-id-3 {
    height: 40% !important;
    overflow-y: scroll;
}
  </style>
  <script>
     
    jQuery(document).ready(function ($) {
      
      
      document.getElementById("loading-image").style.display = 'none';
      $('#phone').mask('(000)-000-0000');
      $('#opprtunity').mask('OP-000000');
        $(function () {
  
          $('form#leadformContainer').on('submit', function (e) {
           
            document.getElementById("loading-image").style.display = 'block';
            e.preventDefault();
            //alert("you are submitting" + $(this).serialize());
            $.ajax({
              type: 'post',
              url: 'ajax.php',
              cache: false,
             contentType: false, 
              processData: false, 
              data: new FormData(this),
              success: function (data) {
               // alert (data);
                document.getElementById("results").style.display = 'block';
                document.getElementById("leadformContainer").style.display = 'none'
                document.getElementById("loading-image").style.display = 'none';
              }
            });
  
          });
  
      });
      
    });
  
    $(document).ready(function() { 
      
      document.getElementById("results").style.display = 'none !important';
      $("#fileToUpload").change(function() {
          filename = this.files[0].name;
          $('#fileLabel')[0].lastChild.textContent = filename;
          //console.log(filename);
        });
    });
      </script>	
<style>
.celebratingSolar {
    display: block;
    position: relative;
    margin-top: 8%;
    margin-left: auto;
    margin-right: auto;
}
</style>
</head>

<body>



  <!-- Page Header -->
  <header class="masthead">
   
    <div class="container">

    
      <div class="row">
   
      
   <div class="row match-height-bootstrap-row">
          <!-- 2nd Column-->
          <div class="col-md-6 col-xs-12 secondColumn">
          <a href="https://www.momentumsolar.com/Why-Solar/"><img style="margin-top: 8%; display:block; margin-left:auto; margin-right:auto;" class="mslogo" src="img/MS_logo_top.png"> </a>
           <!--  Text on banner -->
             <img src="img/celebratingSolar.png" class="img-fluid celebratingSolar" style="margin-left:4%;"/>
             <a href="https://momentumsolar.com/what-are-the-benefits-of-going-solar/"><img src="img/low.png" class="img-fluid celebratingSolar phoneresize"/></a>
             <a href="https://blog.resourcewatch.org/2019/05/02/which-countries-use-the-most-fossil-fuels/"> <img src="img/reduced.png" class="img-fluid celebratingSolar"/></a>
             <a href="https://www.energy.gov/eere/solar/articles/solar-homes-sell-premium-0"> <img src="img/increased.png" class="img-fluid celebratingSolar phoneresize2"/></a>

          </div>
        <!-- second column-->
          <div class="col-md-6 col-xs-12 firstColumn">
            <div class="col-3" style="height: 750px; margin-top:7%; max-width:100%;">
              <!-- logo -->
              
              
              
             <div id="results" class="topform" style="display:none;">
              <br><br><img src="img/thankyou.png" class="img-fluid celebratingSolar thankyou"/>
             
           </div>   
                <form class="form-form" method="POST" id="leadformContainer" style="height: 750px;" enctype="multipart/form-data">
              
                  <div class="form">
                    
                          <p class="title2">Name</p>
                          <div class="form-error" style="display:none">Please fix the errors below.</div>
                          <div class="form-group form-group2">
                              <input type="text" id="fname" name="fname" class="required" value="" placeholder="First Name*" autocomplete="off" aria-required="true" required="">
                              <input type="text" id="lname" name="lname" class="required" value="" placeholder="Last Name*" autocomplete="off" aria-required="true" required="">
                              <table class="table">
                                <tbody>
                                  <tr>
                                  <td>
                                        <span class="title2" >Email</span>
                                        
                                        <input type="text" id="email" name="email" cvalue="" placeholder="Email Address" autocomplete="off" class="required" required="">
                                  </td>
                                    <td style="padding-left:5px !important;">
                                       <span class="title2">Phone</span>
                                       <input type="text" id="phone" name="phone" class="required" value="" placeholder="(XXX)XXX-XXXX" autocomplete="off" aria-required="true" required="" maxlength="14">
                                   </td>
                                   <td>
                                        <span class="title2" >Opportunity #</span>
                                       <input type="text" id="opprtunity" name="opprtunity" cvalue="" placeholder="Enter your opportunity #" autocomplete="off" class="Enter your opportunity number" required="" class="required" >
                                       </td>
                                 
                                </tr>
                              </tbody>
                             </table>
                             <table class="table">
                                <tbody>
                                  <tr>
                                    <td>
                                      <span class="title2">Solar Specialist</span>
                                     <input type="text" id="solarspecialist" name="solarspecialist"  value="" placeholder="Enter your solar specialist’s first and last name" autocomplete="off" required="" class="required" >
                                    </td>
                                    <td>
                                        <span class="title2" >Solar Specialist Email </span>
                                       <input type="text" id="solarspecemail" name="solarspecemail" cvalue="" placeholder="Enter your solar specialist email" autocomplete="off"  required="" class="required" >
                                       </td>
                                </tr>
                              </tbody>
                             </table>
                              <span class="title2">Why did you go solar with Momentum?</span>
                              <input type="text" id="reason1" name="reason1" cvalue="" placeholder="Reason #1" autocomplete="off" class="required" required="" >
                              <input type="text" id="reason2" name="reason2" cvalue="" placeholder="Reason #2" autocomplete="off" class="required" required="" >
                              <input type="text" id="reason3" name="reason3" cvalue="" placeholder="Reason #3" autocomplete="off"  class="required" required="" >
                              <input type="text" id="reason4" name="reason4" cvalue="" placeholder="Reason #4" autocomplete="off" >
                              <input type="text" id="reason5" name="reason5" cvalue="" placeholder="Reason #5" autocomplete="off" >
                              <span class="title2">Anything else you would like us to know?</span>
                              <input type="text" id="additionalcomments" name="additionalcomments" cvalue="" placeholder="Enter additional comments here" autocomplete="off" >
                              
                             
                           <button type="submit" id="submitbtn"><img src="img/submit.png" /></button>
                           <div class="col-12 text-center" style="margin-left:40%; margin-top:2%;">
    
                            <img src="img/ajax-loader.gif" id="loading-image" style="display:none;" alt="Please Wait">
                          </div>

                              <!-- get utm paramters -->
                              <input type="hidden" name="utm_source" id="utm_source" value="<?php echo $_GET['utm_source'];?>">
                              <input type="hidden" name="utm_medium" id="utm_medium" value="<?php echo $_GET['utm_medium'];?>">
                              <input type="hidden" name="utm_campaign" id="utm_campaign" value="<?php echo $_GET['utm_campaign'];?>">
                              <input type="hidden" name="utm_content" id="utm_content" value="<?php echo $_GET['utm_content'];?>">
                               <!-- get optin url-->
                               <input type="hidden" name="optinurl" id="optinurl" value="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;?>" />
                              </div>
                

            </div></form>
          </div> <!-- column-->
          <!-- Form Content -->
          
        </div>
 
    


      </div>
    


      </div>
    </div>
  </header>



  <!-- Footer -->

  <footer class="footer">
      
        <center>
            <p id="footertext">Copyright © 2021 Momentum Solar All Rights Reserved. D|B|A Momentum Solar *Subject to applicable terms and conditions. Eligibility and savings is determined by municipal utility, roof condition and roof space, azimuth, tree location, shading and other factors. HIC # – CA: 1026366; CT: HIC.0646323; FL: CVC57036; MA: 192204; NJ: 13VH05539000; NYC: 2042828-DCA; PA: PA131435; TX: 31915; MA: 192204; AZ: 237781. EL License # - CA: 1026366; CO: ME.3000298; CT: HIC.0646323; DE: T100006310; FL: EC13008217; IL: SE7545; MA: ME: 10024-ELMR; ME: MS60021311; NH: 14277M: NJ: 34EB01591300; NYC: 12943; RI: B-014803; TX: 409620; UT: 11242273-5502; VA: 2710069151; VT: EM-06808</p>
        </center>
      </footer>


  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
  <script src="js/custom-js.js"></script>
  <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>

        jQuery(document).ready(function ($) {     
      
            var availableTags = ["I feel proud producing green energy.", 
"I'm excited to harness the benefits of battery storage.", 
"I'm helping the Earth by generating clean energy.", 
"I love benefitting from price-protected power.", 
"I want to increase my home's value.", 
"I love the way panels look on my home.", 
"I want to lower my utility bills.", 
"I want to gain independence from the grid.", 
"I want to feel proud about using renewable energy.", 
"Going solar is also helpfng future generatons.", 
"Being able to produce energy at home s ncredble!", 
" Solar energy will replace fossil fuels in my home.", 
"Solar energy for is creating a better world for my family and kids.", 
"Solar energy's source, the Sun, is never ending! ", 
"I like how the process of solar energy generation flows from the panels to my outlets.", 
"Solar energy's positive environmental impact is wonderful.", 
"Solar energy's price protection makes me feel comfortable.", 
"Solar energy can help make my electricity bills more affordable.", 
"I want to help transition the United States to clean energy.", 
"I want to be the envy of my neighborhood. ", 
"Solar energy is a step in the fight against climate change. ", 
"There are tax credits available for some homeowners looking to go solar. ", 
"I can use energy generated by my solar panels to power my day to day life. ", 
"Solar energy generation can help combat global warming. "
            ];
            $("#reason1").autocomplete({
                source: function(requestObj, responseFunc) {
                    var matchArry = availableTags.slice(); // Copy the array
                    var srchTerms = $.trim(requestObj.term).split(/\s+/);
                    // For each search term, remove non-matches.
                    $.each(srchTerms, function(J, term) {
                        var regX = new RegExp(term, "i");
                        matchArry = $.map(matchArry, function(item) {
                            return regX.test(item) ? item : null;
                        });
                    });
                    // Return the match results.
                    responseFunc(matchArry);
                },
                open: function(event, ui) {
                    var resultsList = $("ul.ui-autocomplete > li.ui-menu-item > a");
                    //var srchTerm = $.trim($("#reason1").val()).split(/\s+/).join('|');
                    var srchTerm = ""
                    if (srchTerm.toLowerCase() == "i" || srchTerm.toLowerCase()  == "feel" || srchTerm.toLowerCase()  == "want"|| srchTerm.toLowerCase()  == "solar" || srchTerm.toLowerCase()  == "home" || srchTerm.toLowerCase()  == "homeowner" || srchTerm.toLowerCase()  == "homeowners" || srchTerm.toLowerCase()  == "energy" || srchTerm.toLowerCase()  == "benefits" || srchTerm.toLowerCase()  == "love" || srchTerm.toLowerCase()  == "can" || srchTerm.toLowerCase()  == "help" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "the" || srchTerm.toLowerCase()  == "an" || srchTerm.toLowerCase()  == "and" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "of" || srchTerm.toLowerCase()  == "for"  || srchTerm.toLowerCase()  == "be" || srchTerm.toLowerCase()  == "to" || srchTerm.toLowerCase()  == "will" || srchTerm.toLowerCase()  == "in" || srchTerm.toLowerCase()  == "my" ){
                      
                      srchTerm = "";
                      
                  }
                  else {
                    srchTerm = $.trim($("#reason1").val()).split(/\s+/).join('|');
                    
                  }
                    // Loop through the results list and highlight the terms.
                    resultsList.each(function() {

                          if (srchTerm.toLowerCase() == "i" || srchTerm.toLowerCase()  == "feel" || srchTerm.toLowerCase()  == "want"|| srchTerm.toLowerCase()  == "solar" || srchTerm.toLowerCase()  == "home" || srchTerm.toLowerCase()  == "homeowner" || srchTerm.toLowerCase()  == "homeowners" || srchTerm.toLowerCase()  == "energy" || srchTerm.toLowerCase()  == "benefits" || srchTerm.toLowerCase()  == "love" || srchTerm.toLowerCase()  == "can" || srchTerm.toLowerCase()  == "help" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "the" || srchTerm.toLowerCase()  == "an" || srchTerm.toLowerCase()  == "and" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "of" || srchTerm.toLowerCase()  == "for"  || srchTerm.toLowerCase()  == "be" || srchTerm.toLowerCase()  == "to" || srchTerm.toLowerCase()  == "will" || srchTerm.toLowerCase()  == "in" || srchTerm.toLowerCase()  == "my" ){
                            document.getElementById("ui-id-1").style.display = "none !important";
                            document.getElementById("ui-id-1").style.visibility = "hidden";

                            var oldTxt = "";
                            
                          }
                          else {
                            document.getElementById("ui-id-1").style.display = "block !important";
                            document.getElementById("ui-id-1").style.visibility = "visible";
                            var jThis = $(this);
                            var regX = new RegExp('(' + srchTerm + ')', "ig");
                            var oldTxt = jThis.text();
                            jThis.html(oldTxt.replace(regX, '<span class="srchHilite">$1</span>'));
                          }
                         
                        
                    });
                }
            });

            $("#reason2").autocomplete({
                source: function(requestObj, responseFunc) {
                    var matchArry = availableTags.slice(); // Copy the array
                    var srchTerms = $.trim(requestObj.term).split(/\s+/);
                    // For each search term, remove non-matches.
                    $.each(srchTerms, function(J, term) {
                        var regX = new RegExp(term, "i");
                        matchArry = $.map(matchArry, function(item) {
                            return regX.test(item) ? item : null;
                        });
                    });
                    // Return the match results.
                    responseFunc(matchArry);
                },
                open: function(event, ui) {
                    var resultsList = $("ul.ui-autocomplete > li.ui-menu-item > a");
                    //var srchTerm = $.trim($("#reason1").val()).split(/\s+/).join('|');
                    var srchTerm = ""
                    if (srchTerm.toLowerCase() == "i" || srchTerm.toLowerCase()  == "feel" || srchTerm.toLowerCase()  == "want"|| srchTerm.toLowerCase()  == "solar" || srchTerm.toLowerCase()  == "home" || srchTerm.toLowerCase()  == "homeowner" || srchTerm.toLowerCase()  == "homeowners" || srchTerm.toLowerCase()  == "energy" || srchTerm.toLowerCase()  == "benefits" || srchTerm.toLowerCase()  == "love" || srchTerm.toLowerCase()  == "can" || srchTerm.toLowerCase()  == "help" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "the" || srchTerm.toLowerCase()  == "an" || srchTerm.toLowerCase()  == "and" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "of" || srchTerm.toLowerCase()  == "for"  || srchTerm.toLowerCase()  == "be" || srchTerm.toLowerCase()  == "to" || srchTerm.toLowerCase()  == "will" || srchTerm.toLowerCase()  == "in" || srchTerm.toLowerCase()  == "my" ){
                      
                      srchTerm = "";
                      
                  }
                  else {
                    srchTerm = $.trim($("#reason2").val()).split(/\s+/).join('|');
                    
                  }
                    // Loop through the results list and highlight the terms.
                    resultsList.each(function() {

                          if (srchTerm.toLowerCase() == "i" || srchTerm.toLowerCase()  == "feel" || srchTerm.toLowerCase()  == "want"|| srchTerm.toLowerCase()  == "solar" || srchTerm.toLowerCase()  == "home" || srchTerm.toLowerCase()  == "homeowner" || srchTerm.toLowerCase()  == "homeowners" || srchTerm.toLowerCase()  == "energy" || srchTerm.toLowerCase()  == "benefits" || srchTerm.toLowerCase()  == "love" || srchTerm.toLowerCase()  == "can" || srchTerm.toLowerCase()  == "help" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "the" || srchTerm.toLowerCase()  == "an" || srchTerm.toLowerCase()  == "and" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "of" || srchTerm.toLowerCase()  == "for"  || srchTerm.toLowerCase()  == "be" || srchTerm.toLowerCase()  == "to" || srchTerm.toLowerCase()  == "will" || srchTerm.toLowerCase()  == "in" || srchTerm.toLowerCase()  == "my" ){
                            document.getElementById("ui-id-2").style.visibility = "hidden";

                            var oldTxt = "";
                            
                          }
                          else {
                            document.getElementById("ui-id-2").style.visibility = "visible";
                            var jThis = $(this);
                            var regX = new RegExp('(' + srchTerm + ')', "ig");
                            var oldTxt = jThis.text();
                            jThis.html(oldTxt.replace(regX, '<span class="srchHilite">$1</span>'));
                          }
                         
                        
                    });
                }
            });


            $("#reason3").autocomplete({
                source: function(requestObj, responseFunc) {
                    var matchArry = availableTags.slice(); // Copy the array
                    var srchTerms = $.trim(requestObj.term).split(/\s+/);
                    // For each search term, remove non-matches.
                    $.each(srchTerms, function(J, term) {
                        var regX = new RegExp(term, "i");
                        matchArry = $.map(matchArry, function(item) {
                            return regX.test(item) ? item : null;
                        });
                    });
                    // Return the match results.
                    responseFunc(matchArry);
                },
                open: function(event, ui) {
                    var resultsList = $("ul.ui-autocomplete > li.ui-menu-item > a");
                    //var srchTerm = $.trim($("#reason1").val()).split(/\s+/).join('|');
                    var srchTerm = ""
                    if (srchTerm.toLowerCase() == "i" || srchTerm.toLowerCase()  == "feel" || srchTerm.toLowerCase()  == "want"|| srchTerm.toLowerCase()  == "solar" || srchTerm.toLowerCase()  == "home" || srchTerm.toLowerCase()  == "homeowner" || srchTerm.toLowerCase()  == "homeowners" || srchTerm.toLowerCase()  == "energy" || srchTerm.toLowerCase()  == "benefits" || srchTerm.toLowerCase()  == "love" || srchTerm.toLowerCase()  == "can" || srchTerm.toLowerCase()  == "help" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "the" || srchTerm.toLowerCase()  == "an" || srchTerm.toLowerCase()  == "and" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "of" || srchTerm.toLowerCase()  == "for"  || srchTerm.toLowerCase()  == "be" || srchTerm.toLowerCase()  == "to" || srchTerm.toLowerCase()  == "will" || srchTerm.toLowerCase()  == "in" || srchTerm.toLowerCase()  == "my" ){
                      
                      srchTerm = "";
                      
                  }
                  else {
                    srchTerm = $.trim($("#reason3").val()).split(/\s+/).join('|');
                    
                  }
                    // Loop through the results list and highlight the terms.
                    resultsList.each(function() {

                          if (srchTerm.toLowerCase() == "i" || srchTerm.toLowerCase()  == "feel" || srchTerm.toLowerCase()  == "want"|| srchTerm.toLowerCase()  == "solar" || srchTerm.toLowerCase()  == "home" || srchTerm.toLowerCase()  == "homeowner" || srchTerm.toLowerCase()  == "homeowners" || srchTerm.toLowerCase()  == "energy" || srchTerm.toLowerCase()  == "benefits" || srchTerm.toLowerCase()  == "love" || srchTerm.toLowerCase()  == "can" || srchTerm.toLowerCase()  == "help" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "the" || srchTerm.toLowerCase()  == "an" || srchTerm.toLowerCase()  == "and" || srchTerm.toLowerCase()  == "is" || srchTerm.toLowerCase()  == "of" || srchTerm.toLowerCase()  == "for"  || srchTerm.toLowerCase()  == "be" || srchTerm.toLowerCase()  == "to" || srchTerm.toLowerCase()  == "will" || srchTerm.toLowerCase()  == "in" || srchTerm.toLowerCase()  == "my" ){
                            document.getElementById("ui-id-3").style.visibility = "hidden";

                            var oldTxt = "";
                            
                          }
                          else {
                            document.getElementById("ui-id-3").style.visibility = "visible";
                            var jThis = $(this);
                            var regX = new RegExp('(' + srchTerm + ')', "ig");
                            var oldTxt = jThis.text();
                            jThis.html(oldTxt.replace(regX, '<span class="srchHilite">$1</span>'));
                          }
                         
                        
                    });
                }
            });




      });  
  </script>
</body>

</html>
