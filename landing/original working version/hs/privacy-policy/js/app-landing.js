jQuery(document).ready(function($){
	   
	jQuery("a.start1").on("click", function(){
		if(jQuery("#ziptop").val() != ""){
			jQuery("form#step-1 #zip").val(jQuery('#ziptop').val());
			$(".step-1").hide();
			$(".step-2").fadeIn( 200 );
			$('#myModal').modal('show');
			$(".complete").css("width", "28%");
			$("#percents").css("left", "28%");
			$("#percents").html("28%");
			var data = jQuery('#step-1').serializeArray();
			request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})

		} else {
			alert("Please enter zip code");
		}
		return false;
	})
	jQuery("a.start2").on("click", function(){
		if(jQuery(".zipban").val() != ""){
			jQuery("form#step-1 #zip").val(jQuery('.zipban').val());
			$('#myModal').modal('show');
			$(".step-1").hide();
			$(".step-2").fadeIn( 200 );
			$(".complete").css("width", "28%");
			$("#percents").css("left", "28%");
			$("#percents").html("28%");
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})

		} else {
			alert("Please enter zip code");
		}
		return false;
	})
   jQuery('#ziptop').keypress(function(e) {
	   var key = e.which;
	   if (key == 13){
			if(jQuery("#ziptop").val() != ""){
				jQuery("form#step-1 #zip").val(jQuery('#ziptop').val());
				$(".step-1").hide();
				$(".step-2").fadeIn( 200 );
				$('#myModal').modal('show');
				$(".complete").css("width", "28%");
				$("#percents").css("left", "28%");
				$("#percents").html("28%");
				var data = jQuery('#step-1').serializeArray();
				request = jQuery.ajax({
					method: "POST",
					url: "ajax-landing.php",
					data: data
				})

			} else {
				alert("Please enter zip code");
			}
		   return false;
	   }
	})
	jQuery('.zipban').keypress(function(e) {
	   var key = e.which;
	   if (key == 13){
		if(jQuery(".zipban").val() != ""){
			jQuery("form#step-1 #zip").val(jQuery('.zipban').val());
			$('#myModal').modal('show');
			$(".step-1").hide();
			$(".step-2").fadeIn( 200 );
			$(".complete").css("width", "28%");
			$("#percents").css("left", "28%");
			$("#percents").html("28%");
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})

		} else {
			alert("Please enter zip code");
		}
		return false;
	   }
	})
   
    
   jQuery('#step-1').validate({			
	   rules: {
				zip: {required: true},
			},
			messages: {
				zip: "Please enter zipcode.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			/*ga('set', 'page', '/quote/bill/');
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');*/
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "15%");
				 $("#percents").css("left", "15%");
				 $("#percents").html("15%");
				 $(".step-1").hide();
				 $(".step-2").fadeIn( 200 );
				  
			});	
			return false; 
		}
	});
	
	
	
	jQuery('#step-2').validate({
			rules: {
				customRadioInline1: {required: true},
			},
			messages: {
				customRadioInline1: "Please Select Electic Bill Range.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			//ga('set', 'page', '/article1/solarprogram/address/');
			//window.history.pushState('', '', '/quote/address/');
			//ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			//fbq('track', 'Started Quote');
			var data = jQuery('#step-2').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "40%");
				 $("#percents").css("left", "40%");
				 $("#percents").html("40%");
				 $(".step-2").hide();
				 $(".step-3").fadeIn( 200 );
				  
			});	
			return false; 
		}
	});
	
	
	jQuery(".custom-radio").on('click',function(){
		$(".custom-control").css({'background':'#FFFFFF','color':'#000000'});
		var checkedid = $(this).attr('id');
		$('#customRadioInline'+checkedid).prop('checked', true);
		$("#"+checkedid).css({'background':'#4a8522','color':'#FFFFFF'});
	})
	
	 
	
	
	jQuery('#step-3').validate({
			rules: {
				full_address: {required: true},
				/*street_name: {required: true},*/
			},
			messages: {
				full_address: "Please enter address.",
				/*street_name: "Please select address from dropdown list.",*/
			},
		submitHandler: function(form) {
			//ga('send', 'pageview', '/quote/address/');
			//ga('set', 'page', '/article1/solarprogram/shadow/');
			//window.history.pushState('', '', '/quote/shadow/')
			//ga('send', 'pageview');
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#step-3').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "60%");
				 $("#percents").css("left", "60%");
				 $("#percents").html("60%");
				 $(".step-3").hide();
				 $(".step-4").fadeIn( 200 );
				  
			});	
			return false; 
		}
	});
	
	
	jQuery(".shade").on('click', function() {
		var divid = jQuery(this).attr('id');
		$(".shade-image").css('border-color', 'transparent'); //removes blue color when out of focus
		$(".shade-image").removeClass('joomdev');
		$(".step-4 H5 .fas").css('color','#414141');
		// $(".shade-image:after").css('background', 'none');
		
		$("#"+divid+" .shade-image").css('border-color','#4a8522'); //add blue color when in focus
		$("#"+divid+" .shade-image").css('background','rgba(0,0,0,0.2)');
		$("#"+divid+" .shade-image").css('background','rgba(0,0,0,0.2)');
		$("#"+divid+" H5 .fas").css('color','#4a8522');
		// $("#"+divid+" .shade-image").css('border-radius', '60px');
		
		$("."+divid).prop('checked', true);
	 });
	 
	// jQuery(".shade").on('click', function() {
	// 		var divid = jQuery(this).attr('id');
	// 		$("#"+divid+" .shade-image").css('border-radius', '60px');
	// 	}).on('focusout', function(){
	// 		var divid = jQuery(this).attr('id');
	// 		$("#"+divid+" .shade-image").css('border-radius', '10px');
	// });
	jQuery(".custom-radio1").on('click',function(){
		$(".custom-control-shade").css({'background':'#FFFFFF','color':'#000000'});
		var checkedid = $(this).attr('id');
		$('#customRadioInline'+checkedid).prop('checked', true);
		$("#"+checkedid).css({'background':'#4a8522','color':'#FFFFFF'});
	})
	
	
	
	jQuery('#step-4').validate({
			rules: {
				shade: {required: true},
			},
			messages: {
				shade: "Please Select Home shade.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			//ga('set', 'page', '/article1/solarprogram/contact/');
			//window.history.pushState('', '', '/quote/contact/')
			//ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			//fbq('track', 'Started Quote');
			var data = jQuery('#step-4').serializeArray();
						console.log('256');
				console.log(data);
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "80%");
				 $("#percents").css("left", "80%");
				 $("#percents").html("80%");
				 $(".step-4").hide();
				 $(".step-5").fadeIn( 200 );
				  $target = $('#scrolltoelement');
				  $('.modal-dialog').animate({
					scrollTop: 0
				  }, 'show');
						  
			});	
			return false; 
		}
	});
	
	
	 jQuery('#step-5').validate({
			rules: {
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true,email: true},
				phone: {required: true, phoneUS: true},
				
			},
			messages: {
				first_name: "Please enter first name.",
				last_name: "Please enter last name.",
				email: "Please enter valid email.",
				phone: "Please enter valid phone.",
			},
			 
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			$(".final_submit").hide();
			$(".loader_image").show();
			setTimeout(function(){ 
				 $(".loader_image").hide();
				 $(".step-5").hide();
				 $(".step-6").show();
				 $(".complete").css("width", "100%");
				 $("#percents").css("left", "100%");
				 $("#percents").html("100%");
			}, 1500);
			
			var data = jQuery('#step-5').serializeArray();
			fbq('track', 'Lead');
			gtag('event', 'Lead', { 'event_category' : 'Leads',  'event_label' : 'Lead'});
			
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-landing.php",
				data: data
			})
			request.done(function( response ) {
				
				 //window.history.pushState('', '', '/index.php');
				  
			});	
			return false; 
		}
	});
	 
	jQuery("a.open-modal2").on("click", function(){
		$('#myModal2').modal('show');
		return false;
	})

	 jQuery('#ccpa-form').validate({
			rules: {
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true,email: true},
				phone: {required: true},
				addres: {required: true},
				city: {required: true},
				state: {required: true},
				zip: {required: true},
				'type_request[]': {required: true}
				
			},
			messages: {
				first_name: "Please enter first name.",
				last_name: "Please enter last name.",
				email: "Please enter valid email.",
				phone: "Please enter valid phone.",
				'type_request[]': "Please select at least one"
			},
			 
		submitHandler: function(form) {
			
			
			var data = jQuery('#ccpa-form').serializeArray();
			
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-ccpa.php",
				data: data
			})
			request.done(function( response ) {
				
				 jQuery(".havesend").html("Your request has been sent.");
				  
			});	
	
			return false; 
		}
	});
	 
	
})
function gotoStep(step){
	
	var current = step + 1;
	
	if(step == 5 )
		 $(".modal-content").css('margin-top','70% !important');
	
	$(".step-"+current).hide();
	$(".step-"+step).show();
	percent = step*20 - 20;

	$(".complete").css("width", percent+"%");
	$("#percents").css("left", percent+"%");
	$("#percents").html(percent+"%");
	
}
