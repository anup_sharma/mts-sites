<!DOCTYPE html>



<html lang="en">



<head>



	<meta charset="utf-8">



	<meta name="robots" content="noindex" />



	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



	<meta name="description" content="">



	<meta name="author" content="">



	<title>Momentum Solar</title>



	<!-- Bootstrap core CSS -->



	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">



	<link href="css/bootstrap-modal-ios.css" rel="stylesheet">



	<!--Font Awsome-->



	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"



	 crossorigin="anonymous">



	<!-- Custom styles for this template -->



	<link href="css/new.css" rel="stylesheet">



	<!-- Fonts -->



	<link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600" rel="stylesheet">



	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,600" rel="stylesheet">

<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-NLXNWPG');</script>

<!-- End Google Tag Manager -->

</head>



<body id="page-top">

<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLXNWPG"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->

<div class="covid19" style="background: #00aeef;text-align: center;padding: 10px 5px;color: #fff;width: 100%;flex: 1; top: 0px;position: fixed; height: 51px; vertical-align: middle; z-index: 1030;">

Momentum Solar &amp; COVID 19 Safety Information <a href="/covid19/" style="color: #fff; text-decoration: underline;">Learn More</a>

</div>

<style>

	.fixed-top{

			top: 51px !important;

		}

	.banner{

		padding-top: 200px;

	}

	@media (max-width: 991.98px){

		.phonetop{

			    top: 43px !important;

		}

		.covid19{

			height: 36px !important;

			font-size: 11px;

			line-height: 12px;

		}

		.fixed-top{

			top: 36px !important;

		}

		.banner{

			padding-top: 80px;

		}

		

	}

</style>

<div class="fixed-bottom d-block d-flex justify-content-between d-sm-none px-5">



        <a class="btn btn-primary rounded-0 my-3 text-white pull-right" type="submit" data-toggle="modal" data-target="#myModal">Get Free Quote</a>



</div>



	<!-- Navigation -->



	<nav class="navbar navbar-expand-lg bg-white navbar-light fixed-top p-0" id="mainNav">



		<div class="logo-container">



			<a class="navbar-brand js-scroll-trigger pl-lg-5 pl-0 py-3 py-lg-0 mr-0">



				<img src="images/logo.png" alt="logo" class="text-center sitelogo">



				<button class="btn btn-primary rounded-0 btn-sm d-none text-center my-3 top-cta" type="submit" data-toggle="modal"



				 data-target="#myModal">Free Quote</button>



			</a>



		</div>



		<div class="container-fluid p-0 d-flex justify-content-center">



			<button class="navbar-toggler mr-3 hamburger-icon" type="button" data-toggle="collapse" data-target="#navbarResponsive"



			 aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">



				<span class="navbar-toggler-icon"></span>



			</button>



			<div class="collapse navbar-collapse" id="navbarResponsive">



				<ul class="navbar-nav ml-auto">



					<li class="nav-item">



						<a class="nav-link js-scroll-trigger how-it-woks-nav" href="#howitwork">How It Works</a>



					</li>



					<li class="nav-item">



						<a class="nav-link js-scroll-trigger why-choose-us-nav" href="#whychooseus">Why Choose Us</a>



					</li>



					<li class="nav-item">



						<a class="nav-link js-scroll-trigger faq-nav" href="#faq">FAQ</a>



					</li>



					<li class="nav-item free-quote">



						<a class="nav-link js-scroll-trigger text-white px-5 free-quote-text" OnClick="startQuote()" style="cursor:pointer" data-toggle="modal"



						 data-target="#myModal">Free Quote</a>



					</li>



				</ul>



			</div>



		</div>



	</nav>



	<?php 



	  function generateRandomString($length = 15) {



			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';



			$charactersLength = strlen($characters);



			$randomString = '';



			for ($i = 0; $i < $length; $i++) {



				$randomString .= $characters[rand(0, $charactersLength - 1)];



			}



			return $randomString;



	}



	  



	  $token = generateRandomString().str_replace('.',generateRandomString(),microtime(true));



	if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {



		$protocol = 'https://';



		}



		else {



		$protocol = 'http://';



		}



		$current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];



		 



	  ?>



	<section class="banner">



		<div class="container text-center">



			<h6 class="title-top">How much will switching to solar save you?</h6>



			<h3 class="title-mid my-5">Momentum Solar will provide you with a detailed quote which includes tax credits, rebates and financial programs you qualify for!</h3>



			<!--<p class="title-bot">See if your home qualifies.</p>-->



			<form id="step-1-form" class="needs-validation mx-auto" novalidate>



				<div class="startquoteform">



					<input type="text" name="zip2" class="form-control rounded-0" id="zip2" placeholder="Enter Zip Code" required>



					<button class="btn btn-primary rounded-0 button-go-text btn-sm" style="margin-top: 1px; margin-bottom: 1px;" type="submit">Go</button>



				</div> 



				<input type="hidden" id="utms" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">



				<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">



				<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">



				<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">	



				<input type="hidden" name="sub_id" value="<?php echo $_REQUEST['sub_id']?>">



				<input type="hidden" name="cost" value="<?php echo $_REQUEST['cost']?>">



				<input type="hidden" name="utm_sub_id" value="<?php echo $_REQUEST['utm_sub_id']?>">



				<input type="hidden" name="url" value="<?php echo $current_link?>">



				<input type="hidden" name="step" class="step" value="step1">



				<input type="hidden" class="customformtoken" name="token" value="<?php echo $token;?>">



				



			</form>



			<!-- <a class="btn btn-primary rounded-2 button-get-started py-2 mb-5" style="cursor:pointer" data-toggle="modal" OnClick="startQuote()" data-target="#myModal">Get Free Quote</a> -->



		</div>



		<script>



			var yourclickid = "<?php echo $_REQUEST['utm_content']?>";



		</script>



	</section>



	<section id="section-2" class="py-5 my-3">



		<div class="container">



			<div class="row align-items-center">



				<div class="col-12 col-md-4">



					<h2 class="text-uppercase center-on-small">GET A <span class="font-weight-bold" style="color:#00aeef">NO-OBLIGATION</span>



						QUOTE & SAVE <span style="color:#ff9504; letter-spacing:9px;">IMMEDIATELY.</span></h2>



				</div>



				<div class="col-12 col-md-8">



					<ul class="pb-3">



						<li>$0 to enroll</li>



						<li>No hidden fees</li>



						<li>Upfront pricing with no obligation</li>



					</ul>



					<p class="font-italic pr-md-5">Momentum Solar is fully transparent when it comes to solar quotes and will not encourage homeowners to go solar if the savings do not offset the costs. <strong>In fact, we disqualify 30% of homeowners who approach us for solar installation.</strong></p>



				</div>



			</div>



		</div>



	</section>



	<section id="section-3" class="bg-light my-3 py-4 featured-in-section">



		<div class="container">



			<div class="row">



				<div class="col">



					<ul class="d-lg-flex align-items-center justify-content-between text-center">



						<li class="m-0">



							<h5 class="mb-4 border-dark bt-sm bb-sm" style="letter-spacing:1px;">Featured In</h5>



						</li>



						<li>



							<img src="images/client-1.png" alt="forbes" class="my-2">



						</li>



						<li>



							<img src="images/client-2.png" alt="Inc" class="my-2">



						</li>



						<li>



							<img src="images/client-3.png" alt="Angie's List" class="my-2">



						</li>



						<li>



							<img src="images/client-4.png" alt="accredited business" class="my-2">



						</li>



						<li>



							<img src="images/client-5.png" alt="Deloitte" class="my-2">



						</li>



					</ul>



				</div>



			</div>



		</div>



	</section>



	<!-- How it works section -->



	<div id="howitwork">



		<div class="container">



			<div class="row">



				<div class="col-sm-12 text-center my-5">



					<h3 class="how-it-works-text">HOW IT WORKS</h3>



				</div>



			</div>



			<div class="row">



				<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">



					<div class="step1-img text-center">



						<img src="images/step-icon-1.jpg" alt="Step1-Icon" class="py-4">



					</div>



					<div class="step1-body">



						<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 1</h5>



						<P><span class="font-weight-bold text-dark">Pre-screening</span> - Fill out the short survey to see if you quality for federal, state and utility incentives towards your solar panel system.</p>



					<div class="center-on-small text-center">



					<button type="button" class="btn btn-primary rounded-0 button-get-started py-2 mb-5" data-toggle="modal"



					 data-target="#myModal" OnClick="startQuote()">Get Free Quote



					 </button>



				</div>



					</div>



					



				</div>



				<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">



					<div class="step1-img text-center">



						<img src="images/step-icon-2.jpg" alt="Step1-Icon" class="py-4">



					</div>



					<div class="step1-body">



						<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 2</h5>



						<P><span class="font-weight-bold text-dark">Initial Consultation</span> - If you qualify for a solar panel system, our team will reach out to you by email or phone to discuss next steps.</p>



					</div>



				</div>



				<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">



					<div class="step1-img text-center">



						<img src="images/step-icon-3.jpg" alt="Step1-Icon" class="py-4">



					</div>



					<div class="step1-body">



						<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 3</h5>



						<P><span class="font-weight-bold text-dark">On-site Assessment</span> - Our installers will visit your home to conduct an assessment and provide a completely FREE <span class="font-weight-bold text-dark">no-obligation</span>



							quote.</p>



					</div>



				</div>



				<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">



					<div class="step1-img text-center">



						<img src="images/step-icon-4.jpg" alt="Step1-Icon" class="py-4">



					</div>



					<div class="step1-body">



						<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 4</h5>



						<P><span class="font-weight-bold text-dark">If approved</span>, our team will handle all of the designing,



							engineering, and installation of your brand new Momentum solar panel system.</p>



					</div>



				</div>



			</div>



			<div class="row d-none">



			</div>



		</div>



	</div>



	</div>



	<section id="whychooseus" class="why-momentum-solar-container">



		<div class="container">



			<div class="row">



				<div class="col-md-6 offset-md-6 why-momentum-solar py-5">



					<h2 class="pb-4">WHY MOMENTUM SOLAR?</h2>



					<p><span class="font-weight-bold text-dark">Flexible Payment Options</span> - We offer customized payment plans



						that can include $0 upfront cost so you can save <span class="font-weight-bold text-dark">20-30% OFF your



							electric bill</span> immediately.</p>



					<p><span class="font-weight-bold text-dark">Long Term Price Protection</span> - We provide upfront rates for your electricity for the next 25 years and can guarantee that our prices will be lower than your electric company based on the program you choose.</p>



					<p><span class="font-weight-bold text-dark">Worry-Free Maintenance & Monitoring </span>- We provide complimentary system monitoring and maintenance to ensure your system is always functioning optimally as part of many of our programs.</p>



				</div>



			</div>



		</div>



	</section>



	<!-- FAQ Section -->



	<section id="faq">



		<div class="container">



			<div class="row">



				<div class="col-sm-12 text-center">



					<h2 class="frequentlyaq-text pt-5 pb-4">FREQUENTLY ASKED QUESTIONS</h2>



				</div>



			</div>



			<div class="row">



				<div class="col-sm-12">



					<div class="accordion w-100" id="accordionExample">



						<div class="card">



							<div class="card-header" id="headingOne">



								<h2 class="mb-0">



									<button class="btn btn-link w-100" type="button" data-toggle="collapse" data-target="#collapseOne"



									 aria-expanded="true" aria-controls="collapseOne">



										How much will my utility bill be?



									</button>



								</h2>



							</div>



							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">



								<div class="card-body">



									<p>Your solar provider cannot guarantee what your bill from the utility company will be after going solar. The



										bill will depend on the amount of energy you use from the grid after your solar production is subtracted,



										which was presented to you as an estimate based on your usage from the previous year. It does not factor in



										any adjustments in future energy usage, like installing a pool or a hot tub on your property.</p>



								</div>



							</div>



						</div>



						<div class="card">



							<div class="card-header" id="headingTwo">



								<h2 class="mb-0">



									<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseTwo"



									 aria-expanded="false" aria-controls="collapseTwo">



										Will I receive a bill from both the utility company and my solar provider?



									</button>



								</h2>



							</div>



							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">



								<div class="card-body">



									<p>Yes. You will continue receiving a bill from your utility company for basic service and any power needs not



										supplied by your solar system. You will receive a separate bill for your solar service agreement.</p>



								</div>



							</div>



						</div>



						<div class="card">



							<div class="card-header" id="headingThree">



								<h2 class="mb-0">



									<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseThree"



									 aria-expanded="false" aria-controls="collapseThree">



										What happens to my system generation at night or when it's cloudy?



									</button>



								</h2>



							</div>



							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">



								<div class="card-body">



									<p>When the sun goes down, any extra energy you produce during the day is pushed back into the grid,



										offsetting



										your energy use at night or when it’s cloudy. During the sunny summer months your system will produce the



										most



										energy, so the credits you’ve earned during those days of overproduction will be helpful during winter months



										when the days are shorter.</p>



								</div>



							</div>



						</div>



						<div class="card">



							<div class="card-header" id="headingFour">



								<h2 class="mb-0">



									<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseFour"



									 aria-expanded="false" aria-controls="collapseFour">



										How will I know if my system is working properly?



									</button>



								</h2>



							</div>



							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">



								<div class="card-body">



									<p>We offer complimentary maintenance and monitoring so that we can identify issues and fix them accordingly is provided as part of many of our programs. We also offer real-time monitoring software to all of our customers that will allow you to track your energy usage from your smartphone or computer, so you’ll know if you are not producing as much as you should be.</p>



								</div>



							</div>



						</div>



						<div class="card">



							<div class="card-header" id="headingFive">



								<h2 class="mb-0">



									<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseFive"



									 aria-expanded="false" aria-controls="collapseFive">



										What does the warranty cover?



									</button>



								</h2>



							</div>



							<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">



								<div class="card-body">



									<p>A warranty that covers the solar generating equipment (panels and inverters) for the life of the service contract is provided as part of many of our programs.  These programs provide “worry-free” maintenance because it’s taken care of at no cost to you. </p>



								</div>



							</div>



						</div>



						<div class="card">



							<div class="card-header" id="headingSix">



								<h2 class="mb-0">



									<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseSix"



									 aria-expanded="false" aria-controls="collapseSix">



										What happens if the solar service agreement outlives me?



									</button>



								</h2>



							</div>



							<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">



								<div class="card-body">



									<p>In the event of your passing, your heirs will be able to experience solar savings by simply continuing to



										make your monthly payments. If they sell the home, they can transfer the solar agreement to the home buyer.</p>



								</div>



							</div>



						</div>



						<div class="card">



							<div class="card-header" id="headingSeven">



								<h2 class="mb-0">



									<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseSeven"



									 aria-expanded="false" aria-controls="collapseSeven">



										What if I sell my home prior to the end of the solar service agreement?



									</button>



								</h2>



							</div>



							<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">



								<div class="card-body">



									<p>If you sell your home, you can transfer your solar agreement to the new homeowner pursuant to the terms of your solar service agreement. The process is very easy and offers electricity savings, which is a great selling point when your home is on the market. The Momentum Solar Customer Experience team will walk you through the entire process, but it’s important that you contact us by email at <a href="mailto:myinstall@momentumsolar.com">MyInstall@momentumsolar.com</a> or by phone at <a href="tel:732-902-6224">732.902.6224</a> as soon as you decide to sell your home.</p>



								</div>



							</div>



						</div>



						<div class="card">



							<div class="card-header" id="headingEight">



								<h2 class="mb-0">



									<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseEight"



									 aria-expanded="false" aria-controls="collapseEight">



										Why are agreements 20-25 years in duration?



									</button>



								</h2>



							</div>



							<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">



								<div class="card-body">



									<p>The reason for a 20 or 25-year term is to keep your rate per kWh as low as possible. Your solar savings are



										either locked in or predictable so that you can enjoy peace of mind for years to come.</p>



								</div>



							</div>



						</div>



					</div>



				</div>



			</div>



		</div>



		</div>



	</section>



	<!-- Testimonial Section -->



	<section class="testimonial py-5">



		<div class="container">



			<div class="row">



				<div class="col-lg-4">



					<div class="row">



						<p class="quote-heading center-on-small"><span>WHAT OUR CUSTOMERS SAY</span></p>



					</div>



				</div>



				<div class="col-lg-8">



					<div id="myCarousel" class="carousel slide" data-ride="carousel">



						<!-- Carousel indicators -->



						<ol class="carousel-indicators">



							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>



							<li data-target="#myCarousel" data-slide-to="1"></li>



							<li data-target="#myCarousel" data-slide-to="2"></li>



							<li data-target="#myCarousel" data-slide-to="3"></li>



							<li data-target="#myCarousel" data-slide-to="4"></li>



							<li data-target="#myCarousel" data-slide-to="5"></li>



							<li data-target="#myCarousel" data-slide-to="6"></li>



							<li data-target="#myCarousel" data-slide-to="7"></li>



						</ol>



						<!-- Wrapper for carousel items -->



						<div class="carousel-inner">



							<div class="item carousel-item active">



								<p class="testimonial">We’re growing more trees because of us going solar. I’m really happy because we’re



									saving more money and helping the environment by helping more trees grow.</p>



								<p class="person-name">- The Veiga Family from New Jersey</p>



							</div>



							<div class="item carousel-item">



								<p class="testimonial">Coming from a background in construction, I wanted every wire to be perfect, and



									everything squared, conduit straight. Momentum nailed every facet of that.</p>



								<p class="person-name">- Zach from Texas</p>



							</div>



							<div class="item carousel-item">



								<p class="testimonial">We wanted to save money and do something that’s good for the environment. I went with



									Momentum because of the products and services they offer, the pricing and ultimately our salesman.</p>



								<p class="person-name">- Michael from Florida</p>



							</div>



							<div class="item carousel-item">



								<p class="testimonial">Thanks to Momentum and the savings I’m getting, I can spend more money on things I’d put



									off for later. I don’t know exactly what but I know it’ll be good things for my house.</p>



								<p class="person-name">- Austria from New Jersey</p>



							</div>



							<div class="item carousel-item">



								<p class="testimonial">We just get our first bill and it was only $30. That’s your money or your children’s



									money to spend as you wish. We really can’t tell you how happy we are.</p>



								<p class="person-name">- The Wdowiaks from Florida</p>



							</div>



							<div class="item carousel-item">



								<p class="testimonial">Customer experience I had at Momentum Solar was exceptional. Everyone I’ve talked to has



									been very helpful and if I had a question, they would seek to answer my question and I was very happy with



									them!</p>



								<p class="person-name">- John from New Jersey</p>



							</div>



							<div class="item carousel-item">



								<p class="testimonial">The reputation of the company had a lot to do with it, the 25 yr warranty with service



									included was really important to me because I didn’t want to have to start paying for labor down the road.</p>



								<p class="person-name">- Bob from Florida</p>



							</div>



							<div class="item carousel-item">



								<p class="testimonial">I’ll be saving about $70 per month. Well, $70 a month gives me the opportunity to really



									spoil my grandchildren!</p>



								<p class="person-name">- Tom from New Jersey</p>



							</div>



						</div>



					</div>



				</div>



			</div>



		</div>



	</section>



	<!-- Footer -->



	<footer class="footer-section">



		<div class="container-fluid">



			<div class="row no-gutters">



				<div class="col-lg-3 align-self-center text-center col-md-12">



					<img src="images/logo-footer.jpg" alt="Footer Logo">



					<p><a href="/contractor-information/">State Contractor License Information</a></p>



				</div>



				<div class="col-lg-6 col-md-9 align-self-center py-4">



					<p class="m-0">© 2019 Momentum Solar. All Rights Reserved by Pro Custom Solar LLC D|B|A Momentum Solar | <a href="https://momentumsolar.com/privacy-policy" target="_blank">Privacy Policy</a></p>



				</div>



				<div class="col-lg-3 col-md-3 text-md-right align-self-center text-center">



					<div class="social-icons">



						<a href="https://twitter.com/momentumsolar" class="social-icon" target="_blank">



							<i class="fab fa-twitter twitter-icon"></i>



						</a>



						<a href="https://www.facebook.com/momentumsolar" class="social-icon" target="_blank">



							<i class="fab fa-facebook-f facebook-icon"></i>



						</a>



						<a href="https://www.instagram.com/momentumsolar" class="social-icon" target="_blank">



							<i class="fab fa-instagram insta-icon"></i>



						</a>



						<a href="https://www.linkedin.com/company/momentum-solar/" class="social-icon" target="_blank">



							<i class="fab fa-linkedin-in linked-icon"></i>



						</a>



					</div>



				</div>



			</div>



		</div>



		<!-- /.container -->



	</footer>



	<div id="myModal" class="modal fade custom-modal" role="dialog">



		<div class="modal-dialog modal-xl">



			<!-- Modal content-->



			<div class="modal-content">



				<div class="modal-body">



					<div id="scrolltoelement"></div>



					<button type="button" class="close" data-dismiss="modal">&times;</button>



					<div class="container">



						<div class="row">



							<div class="col d-flex text-center align-items-center">



								<!-- Step 1 -->



								<div class="col form py-1 mx-auto step-1">



									<h3 class="font-weight-light mb-5">What is your zip code?</h3>



									<p class="mb-5 mt-4 custom-width">Your zip code will allow Momentum Solar to calculate state and utility



										incentives to maximize your tax benefits.</p>



									<form id="step-1" class="needs-validation mx-auto" novalidate>



										<input id="zip" class="form-control rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip"



										 type="text">



										<div class="invalid-feedback mb-4">



											Please provide a valid zip.



										</div>



										<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">



										<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">



										<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">



										<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">																				



										<input type="hidden" name="sub_id" value="<?php echo $_REQUEST['sub_id']?>">



										<input type="hidden" name="cost" value="<?php echo $_REQUEST['cost']?>">



										<input type="hidden" name="utm_sub_id" value="<?php echo $_REQUEST['utm_sub_id']?>">



										<input type="hidden" name="url" value="<?php echo $current_link?>">



										<input type="hidden" name="step" class="step" value="step1">



										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">



										<div class="center-block mt-5">



											<button class="btn btn-primary rounded-0 btn-block" type="submit">Next</button>



										</div>



									</form>



								</div>



								<!-- Step 2 -->



								<div class="col form py-1 mx-auto step-2" style="display:none;">



									<h3 class="font-weight-light mb-5">Are you a homeowner?</h3>



									<form id="step-2" class="needs-validation mx-auto" novalidate>



										<div class="icons-holder"> 



											<div class="icon-residential"> 



												<label> 



													<input type="checkbox" name="input_1_1" id="choice_1_1" value="Residential"> 



													<img src="images/icon_residential-f9b85e53.png" id="residential" alt="Icon residential"> 







												</label>



												<br> Yes 



											</div> 



											<div class="icon-commercial"> 



												<label> 



													<input type="checkbox" name="input_1_2" id="choice_1_2" value="Commercial"> 



													<img src="images/icon_commercial-9a2c3cdc.png" id="commercial" alt="Icon commercial">



												</label>



												<br> No 



											</div> 



										</div>



									</form>



								</div>



								<!-- Step 2 -->



								<div class="col form pt-0 pt-lg-5 px-0 pb-0 mx-auto step-3" style="display:none;">



									<h3 class="font-weight-light mb-5">What is your credit score?</h3>



									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">We will use this information to see which solar incentives you qualify for.</p>



									<form id="step-3_3" class="needs-validation mx-auto bill-box" novalidate>



										<div id="11" class="custom-control2 custom-radio2 custom-control-inline checkbox">



											<input type="radio" value="Less than 551" id="customRadioInline12" name="customRadioInline2" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline12">Less than 551</label>



										</div>



										<div id="22" class="custom-control2 custom-radio2 custom-control-inline checkbox">



											<input type="radio" value="551-600" id="customRadioInline22" name="customRadioInline2" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline22">551-600</label>



										</div>



										<div id="33" class="custom-control2 custom-radio2 custom-control-inline checkbox">



											<input type="radio" value="601-650" id="customRadioInline33" name="customRadioInline2" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline33">601-650</label>



										</div>



										<div id="44" class="custom-control2 custom-radio2 custom-control-inline checkbox">



											<input type="radio" value="651 - 700" id="customRadioInline44" name="customRadioInline2" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline44">651 - 700</label>



										</div>



										<div id="55" class="custom-control2 custom-radio2 custom-control-inline checkbox">



											<input type="radio" value="701-750" id="customRadioInline55" name="customRadioInline2" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline55">701-750</label>



										</div>



										<div id="66" class="custom-control2 custom-radio2 custom-control-inline checkbox">



											<input type="radio" value="701-750" id="customRadioInline66" name="customRadioInline2" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline66">750 or more</label>



										</div>



										<input type="hidden" name="step" class="step" value="step22">



										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">



										<div class="row">



											<div class="col-6 text-left">



												<button class="btn btn-default text-left btn-link rounded-0 btn-sm" onclick="gotoStep(2)" type="button">



													<i class="fas fa-chevron-left"></i>Back</button>



											</div>



											<div class="col-6 text-right">



												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>



											</div>



										</div>



									</form>



								</div>



								<!-- Step 2 -->



								<div class="col form pt-0 pt-lg-5 px-0 pb-0 mx-auto step-4" style="display:none;">



									<h3 class="font-weight-light mb-5">What is your average monthly electric bill?</h3>



									<form id="step-4" class="needs-validation mx-auto bill-box" novalidate>



										<div id="1" class="custom-control custom-radio custom-control-inline checkbox">



											<input type="radio" value="Under $50" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline1">Under $50</label>



										</div>



										<div id="2" class="custom-control custom-radio custom-control-inline checkbox">



											<input type="radio" value="$50 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline2">$50 - $150</label>



										</div>



										<div id="3" class="custom-control custom-radio custom-control-inline checkbox">



											<input type="radio" value="$150 - $350" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline3">$150 - $350</label>



										</div>



										<div id="4" class="custom-control custom-radio custom-control-inline checkbox">



											<input type="radio" value="$300 - $450" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline4">$300 - $450</label>



										</div>



										<div id="5" class="custom-control custom-radio custom-control-inline checkbox">



											<input type="radio" value="Over $450" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">



											<label class="custom-control-label" for="customRadioInline5">Over $450</label>



										</div>



										<input type="hidden" name="step" class="step" value="step2">



										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">



										<div class="row">



											<div class="col-6 text-left">



												<button class="btn btn-default text-left btn-link rounded-0 btn-sm" onclick="gotoStep(3)" type="button">



													<i class="fas fa-chevron-left"></i>Back</button>



											</div>



											<div class="col-6 text-right">



												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>



											</div>



										</div>



									</form>



								</div>



								<!-- Step 3 -->



								<div class="col form pt-0 pt-lg-5 mx-auto step-5" style="display:none;">



									<h3 class="font-weight-light mb-5">Please enter your home address.</h3>



									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">We will use this information to estimate how much energy your roof can produce.</p>



									<form id="step-5" class="needs-validation mx-auto" novalidate>



										<input type="text" name="full_address" class="form-control rounded-0 cw-75" id="full_address" placeholder="Address">



										



										<input type="text" id="street_number" name="street_number" value="" style="height: 0px; border: none; margin: 0; padding: 0;">



										<input type="text" id="route" name="street_name" value="" style="height: 0px; border: none; margin: 0; padding: 0;">



										<input type="text" id="locality" name="city" value="" style="height: 0px; border: none; margin: 0; padding: 0;">



										<input type="text" id="postal_code" name="postal_code" value="" style="height: 0px; border: none; margin: 0; padding: 0;">



										<input type="text" id="administrative_area_level_1" name="state_abbr" value="" style="height: 0px; border: none; margin: 0; padding: 0;">



										<input type="text" id="country" name="country" value="" style="height: 0px; border: none; margin: 0; padding: 0;">



										<div class="invalid-feedback">



											Please provide a valid Address.



										</div>



										<input type="hidden" name="step" class="step" value="step3">



										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">



										<div class="row mt-5">



											<div class="text-left col-6 ">



												<button class="btn btn-default btn-link rounded-0 btn-sm text-left" onclick="gotoStep(4)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>



											<div class="col-6 text-right">



												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>



											</div>



										</div>



									</form>



								</div>



								<!-- Step 4 -->



								<div class="col form pt-0 pt-md-3 mx-auto step-6" style="display: none;">



									<h3 class="font-weight-light mb-5">How much shade does your roof get from 9 AM - 3 PM?</h3>



									<form id="step-6">



										<div class="step4mobile">



											<div id="6" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">



												<input type="radio" value="No Shade" id="customRadioInline6" name="shade" class="custom-control-input shade_radio">



												<label class="custom-control-label" for="customRadioInline6">No Shade</label>



											</div>



											<div id="7" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">



												<input type="radio" value="Some Shade" id="customRadioInline7" name="shade" class="custom-control-input shade_radio">



												<label class="custom-control-label" for="customRadioInline7">Some Shade</label>



											</div>



											<div id="8" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">



												<input type="radio" value="Severe Shade" id="customRadioInline8" name="shade" class="custom-control-input shade_radio">



												<label class="custom-control-label" for="customRadioInline8">Severe Shade</label>



											</div>



										</div>



										<div class="step4desk">



											<div class="row mb-5">



												<div class="col-12 col-md-6 shade " id="no-shade">



													<div class="shade-image d-none d-sm-block">



														<img src="images/roof-1.png">



													</div>



													<h5 class="mt-3 "><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> No Shade</h5>



													<p class="d-none d-sm-block">Entire roof is exposed to sunlight.</p>



													<input type="radio" class="no-shade" name="shade" value="No Shade" style="display:none">



												</div>



												<div class="col-12 col-md-6 shade" id="some-shade">



													<div class="shade-image d-none d-sm-block">



														<img src="images/roof-2.png">



													</div>



													<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Some Shade</h5>



													<p class="d-none d-sm-block">Parts of the roof are covered at certain times/all times.</p>



													<input type="radio" class="some-shade" name="shade" value="Some Shade" style="display:none">



												</div>



												<div class="col-12 col-md-6 shade " id="severe-shade">



													<div class="shade-image d-none d-sm-block">	



														<img src="images/roof-3.png">



													</div>



													<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Severe Shade</h5>



													<p class="d-none d-sm-block">Most or all of the roof is covered at certain times/all times.</p>



													<input type="radio" class="severe-shade" name="shade" value="Severe Shade" style="display:none">



													<input type="hidden" name="step" class="step" value="step4">



													<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">



												</div>



											</div>



										</div>



										<div class="row">



											<div class="text-left col-6">



												<button class="btn text-left btn-default rounded-0 btn-link btn-sm" onclick="gotoStep(5)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>



											<div class="col-6 text-right">



												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>



											</div>



										</div>



									</form>



								</div>



								<!-- Step 5 -->



								<div class="col form pt-0 pt-sm-8 mx-auto step-7" style="display:none;">



								



									<h3 class="font-weight-light mb-0 mb-md-5">Please enter your contact information to receive a <b>no-obligation</b>



										detailed quote from our solar professionals.</h3>



									<form id="step-7" class="needs-validation row" novalidate>



										<div class="col-12">



										<div class="row m-md-auto">



											<div class="col-12 col-md-12 col-lg-6">



												<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">



												<div class="invalid-feedback">



													Please provide first name.



												</div>



											</div>



											<div class="col-12 col-md-12 col-lg-6">



												<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">



												<div class="invalid-feedback">



													Please provide last name.



												</div>



											</div>



											<div class="col-12 col-md-12 col-lg-6">



												<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">



												<div class="invalid-feedback">



													Please provide a valid email.



												</div>



											</div>



											<div class="col-12 col-md-12 col-lg-6">



												<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04"



													placeholder="Phone">



												<div class="invalid-feedback">



													Please provide a valid phone.



												</div>



											</div>



										</div>



										<div class="row">



											<div class="col-12 text-center my-3">



												<button class="btn btn-primary rounded-0 btn-sm final_submit" type="submit">Submit</button>



												<img src="/landing/images/loading_bubble.svg" style="display:none" class="loader_image">



											</div>



										</div>



										<div class="row">



											<div class="col-12">



												<p class="my-3 small w-75 ml-auto mr-auto">*By hitting submit you authorize Momentum Solar to email, call and send you text messages on the phone number your submitted above using an automated dialer regarding solar products. This will override any federal or state Do Not Call lists. Consent is not a requirement of purchase. Data and text message rates will apply. <a href="/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="/ccpa/" target="_blank">Do Not Sell</a></p>



											</div>



										</div>



										<div class="row">



											<div class="col-12">



												<input type="hidden" name="step" class="step" value="step5">



												<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">



												<div class="row">



													<div class="text-left">



														<button class="btn btn-default rounded-0 btn-sm btn-link" onclick="gotoStep(6)" type="button">



															<i class="fas fa-chevron-left"></i>Back</button>



													</div>



												</div>



											</div>



										</div>



									</div>



									</form>



									<div class="bot-sec mt-5" style="display:none;">



										<div class="d-flex col- mx-o justify-content-between">



											<p class="back d-flex align-items-center"><span><i class="fas fa-chevron-left"></i></span>Back</p>



											<button class="btn btn-primary rounded-0" type="submit">Next</button>



										</div>



									</div>



								</div>



								<div class="col form py-5 mx-auto step-8" style="display:none;">



								<h3 class="text-success text-center">



									<span class="d-block text-center">



									<i class="fas fa-check-circle mb-3 fa-2x"></i></span>Thank you for your interest in Momentum Solar. Our team will get back to you within 24-48 hours.



								</h3>



								<div id="iframehere"></div><div id="iframe3here"></div>



								</div>



								<div class="col form py-5 mx-auto step-9" style="display:none;">



									<h3 class="text-primary text-center">



										Sorry,  you do not qualify for solar



									</h3>



									<div class="row">



										<div class="text-left">



											<button class="btn btn-default rounded-0 btn-sm btn-link" onclick="gotoStep(9)" type="button">



												<i class="fas fa-chevron-left"></i>Back</button>



										</div>



									</div>



								</div>



								<div class="close" style="display:none;">



									<i class="fas fa-times"></i>



								</div>



							</div>



						</div>



					</div>



				</div>



			</div>



		</div>



	</div>



	<!-- Bootstrap core JavaScript -->



	<script src="vendor/jquery/jquery.min.js"></script>



	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>



	<!-- Plugin JavaScript -->



	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>



	<!-- Custom JavaScript for this theme -->



	<script src="js/scrolling-nav.js"></script>



	<script src="js/app.js"></script>



	<script src="js/jquery.validate.js"></script>



	<script src="js/additional-methods.min.js"></script>



	<script src="js/jquery.mask.js"></script>



	<script>



		$('.phone_us').mask('(000) 000-0000');



		var placeSearch, autocomplete;



		var componentForm = {



			street_number: 'short_name',



			route: 'long_name',



			locality: 'long_name',



			administrative_area_level_1: 'short_name',



			country: 'long_name',



			postal_code: 'short_name'



		};



		function initAutocomplete() {



			// Create the autocomplete object, restricting the search to geographical



			// location types.



			autocomplete = new google.maps.places.Autocomplete(



				(document.getElementById('full_address')), {



					types: ['geocode']



				});



			// When the user selects an address from the dropdown, populate the address



			// fields in the form.



			autocomplete.addListener('place_changed', fillInAddress);



		}



		function fillInAddress() {



			// Get the place details from the autocomplete object.



			var place = autocomplete.getPlace();



			for (var component in componentForm) {



				document.getElementById(component).value = '';



				document.getElementById(component).disabled = false;



			}



			// Get each component of the address from the place details



			// and fill the corresponding field on the form.



			for (var i = 0; i < place.address_components.length; i++) {



				var addressType = place.address_components[i].types[0];



				if (componentForm[addressType]) {



					var val = place.address_components[i][componentForm[addressType]];



					document.getElementById(addressType).value = val;



				}



			}



		}



		// Bias the autocomplete object to the user's geographical location,



		// as supplied by the browser's 'navigator.geolocation' object.



		function geolocate() {



			if (navigator.geolocation) {



				navigator.geolocation.getCurrentPosition(function (position) {



					var geolocation = {



						lat: position.coords.latitude,



						lng: position.coords.longitude



					};



					var circle = new google.maps.Circle({



						center: geolocation,



						radius: position.coords.accuracy



					});



					autocomplete.setBounds(circle.getBounds());



				});



			}



		}



	</script>



    <!--Fading out "Get Free Quote" button in mobile-->



    <script>



        var windowsize = $(window).width();



        if(windowsize <= 425){



            $('.fixed-bottom').css('position','unset');



            $('.banner ').css('margin-top','0');



            var preScroll = 0;



            $(window).scroll(function(){



                var currentScroll = $(this).scrollTop();



                if (currentScroll > 350){



                    $('.fixed-bottom').fadeIn(500).css('position','fixed');



//                    $('.banner ').css('margin-top','48');



                } else {



                    $('.fixed-bottom').fadeOut(500).css('position','unset');



                }



                preScroll = currentScroll;



            });



        }



    </script>



	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVx6biCSBFRVQhpvxAbS7kRX9vSypJfXg&libraries=places&callback=initAutocomplete"



	 async defer></script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75829764-1"></script>



	<script>



		window.dataLayer = window.dataLayer || [];



		function gtag() {



			dataLayer.push(arguments);



		}



		gtag('js', new Date());



		gtag('config', 'UA-75829764-1');



		gtag('config', 'AW-922148957');



	</script>



	<script>



		(function (i, s, o, g, r, a, m) {



			i['GoogleAnalyticsObject'] = r;



			i[r] = i[r] || function () {



				(i[r].q = i[r].q || []).push(arguments)



			}, i[r].l = 1 * new Date();



			a = s.createElement(o),



				m = s.getElementsByTagName(o)[0];



			a.async = 1;



			a.src = g;



			m.parentNode.insertBefore(a, m)



		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');



		ga('create', 'UA-75829764-1', 'momentumsolar.com');



		ga('set', 'page', '/landing/cs/');



		ga('send', 'pageview');



	</script>



<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"26038347"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>










<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10081644'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>



<script type="text/javascript">

    adroll_adv_id = "6GSI7DKO2ZAUFHVY3S2HXR";

    adroll_pix_id = "KXJUCWTBX5EYDF7NKT2JUM";



    (function () {

        var _onload = function(){

            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}

            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}

            var scr = document.createElement("script");

            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");

            scr.setAttribute('async', 'true');

            scr.type = "text/javascript";

            scr.src = host + "/j/roundtrip.js";

            ((document.getElementsByTagName('head') || [null])[0] ||

                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);

        };

        if (window.addEventListener) {window.addEventListener('load', _onload, false);}

        else {window.attachEvent('onload', _onload)}

    }());

</script>

<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"> </script> <script type="text/javascript"> window.criteo_q = window.criteo_q || []; var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d"; window.criteo_q.push( { event: "setAccount", account: 71272 }, { event: "setSiteType", type: deviceType }, { event: "viewItem", item: "1" } ); </script>

<script type="text/javascript">

  (function() {

      var field = 'xxTrustedFormCertUrl';

      var provideReferrer = false;

      var invertFieldSensitivity = false;

      var tf = document.createElement('script');

      tf.type = 'text/javascript'; tf.async = true;

      tf.src = 'http' + ('https:' == document.location.protocol ? 's' : '') +

        '://api.trustedform.com/trustedform.js?provide_referrer=' + escape(provideReferrer) + '&field=' + escape(field) + '&l='+new Date().getTime()+Math.random() + '&invert_field_sensitivity=' + invertFieldSensitivity;

      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tf, s); }

  )();

</script>

<noscript>

    <img src="http://api.trustedform.com/ns.gif" />

</noscript>


</body>



</html>