function startQuote() {
		ga('set', 'page', '/landing/full/zip/');
		ga('send', 'pageview');
		fbq('track', 'Started Quote');
}
jQuery(document).ready(function($){
   /* $("#myCarousel").swiperight(function() {
      $(this).carousel('prev');
    });
   $("#myCarousel").swipeleft(function() {
      $(this).carousel('next');
   }); */
   
   
  // $(".hidden_link").fancybox();
   //$(".customformtoken").val($(".freetoken").val()); 
   
   jQuery("form#step-01").validate({
		submitHandler: function(form) {
			ga('set', 'page', '/landing/full/zip/');
			ga('send', 'pageview');
			fbq('track', 'Started Quote');
			swiper.slideNext();
			return false; 
		}
	});
	jQuery("#residential").click(function(){
		ga('set', 'page', '/landing/full/zip/');
		ga('send', 'pageview');
		fbq('track', 'Started Quote');
		jQuery("input#property_ownership-OWN").val("Yes");
		swiper.slideNext();
	})
	jQuery("#commercial").click(function(){
		ga('set', 'page', '/landing/full/zip/');
		ga('send', 'pageview');
		fbq('track', 'Started Quote');
		jQuery("input#property_ownership-OWN").val("No");
		swiper.slideNext();
	})
   
   jQuery('#step-03').validate({
			rules: {
				zip2: {required: true, digits: true, maxlength: 5, minlength: 5, min: 1},
			},
			messages: {
				zip2: "Please enter zipcode.",
			},
			submitHandler: function(form) {
				ga('set', 'page', '/landing/full/bill/');
				ga('send', 'pageview');
				fbq('track', 'Started Quote');
				
				var data = jQuery('#step-03').serializeArray();
				request = jQuery.ajax({
					method: "POST",
					url: "/landing/full/ajax.php",
					data: data
				})
				request.done(function( response ) {
					swiper.slideNext();  
				});	
				return false; 
				 
		}
	});
   jQuery('#step-04').validate({
		rules: {
				full_address: {required: true},
				street_name: {required: true},
			},
			messages: {
				full_address: "Please enter address.",
				street_name: "Please select address from dropdown list.",
			},
		submitHandler: function(form) {
			ga('set', 'page', '/landing/full/bill/');
			ga('send', 'pageview');
			var data = jQuery('#step-04').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "/landing/full/ajax.php",
				data: data
			})
			request.done(function( response ) {
					swiper.slideNext();  
				});	
			return false;
		}
	});
   
	jQuery('#step-05').validate({
		submitHandler: function(form) {
			ga('set', 'page', '/landing/full/email/');
			ga('send', 'pageview');
			fbq('track', 'Started Quote');
			swiper.slideNext();
			return false; 
		}
	});
	jQuery('#step-06').validate({
			rules: {
				email: {required: true,email: true},
				
			},
			messages: {
				email: "Please enter valid email.",
			},
			 
			submitHandler: function(form) {
				ga('set', 'page', '/landing/full/contact/');
				ga('send', 'pageview');
				fbq('track', 'Started Quote');
				
				var data = jQuery('#step-06').serializeArray();
				request = jQuery.ajax({
					method: "POST",
					url: "/landing/full/ajax.php",
					data: data
				})
				request.done(function( response ) {
					swiper.slideNext();  
				});	
				return false; 
			}
	});
	jQuery('#step-07').validate({
			rules: {
				first_name: {required: true},
				last_name: {required: true},
				
			},
			messages: {
				first_name: "Please enter first name.",
				last_name: "Please enter last name.",
			},
			 
			submitHandler: function(form) {
				ga('set', 'page', '/landing/full/phone/');
				ga('send', 'pageview');
				fbq('track', 'Started Quote');
				var data = jQuery('#step-07').serializeArray();
				request = jQuery.ajax({
					method: "POST",
					url: "/landing/full/ajax.php",
					data: data
				})
				request.done(function( response ) {
					swiper.slideNext();  
				});	
			 
			}
	});
	jQuery('#step-08').validate({
			rules: {
				phone: {required: true, phoneUS: true},
			},
			messages: {
				phone: "Please enter valid phone.",
			},
			submitHandler: function(form) {
				ga('set', 'page', '/landing/full/thanks/');
				ga('send', 'pageview');
				gtag('event', 'conversion', {'send_to': 'AW-922148957/0agnCJjG9JMBEN3A27cD'});
				twttr.conversion.trackPid('o3495', { tw_sale_amount: 0, tw_order_quantity: 0 });
				fbq('track', 'Lead');
				obApi('track', 'Lead');

				window.uetq = window.uetq || []; 
				window.uetq.push('event', 'Lead', {});  
				window.uetq.push({ 'ec': 'Lead', 'ea': 'Lead', 'el': 'Lead', 'ev': 3 });
				
				swiper.slideNext();
				
				var data = jQuery('#step-08').serializeArray();
				window.criteo_q = window.criteo_q || []; 
			var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d";
			window.criteo_q.push( { event: "setAccount", account: 71272 }, { event: "setSiteType", type: deviceType }, { event: "setEmail", email: jQuery('#step-06 input[type="email"]').val() }, { event: "trackTransaction", id: jQuery('#step-08 input.freetoken').val(), item: [ { id: "1", price: "0", quantity: 1 } ]} );
				
				request = jQuery.ajax({
					method: "POST",
					url: "/landing/full/ajax.php",
					data: data
				})
				request.done(function( response ) {
					//swiper.slideNext();  
				});	
				
				return false; 
			}
	});
	
	
})
function gotoStep(step){
	
	var current = step + 1;
	swiper.slidePrev();
}