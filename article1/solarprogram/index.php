<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">

	<meta name="robots" content="noindex" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="">

	<meta name="author" content="">

	<title>Momentum Solar</title>

	<!-- Bootstrap core CSS -->

	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<link href="css/bootstrap-modal-ios.css" rel="stylesheet">

	<!--Font Awsome-->

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"

	 crossorigin="anonymous">

	<!-- Custom styles for this template -->

	<link href="css/new.css" rel="stylesheet">

	<link href="css/rangeslider.css" rel="stylesheet">

	<!-- Fonts -->

	<link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,600" rel="stylesheet">

<link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

  <!-- Demo styles -->

  <style>

    html, body {

      position: relative;

      height: 100%;

    }

    body {

      background: #eee;

      font-family: Helvetica Neue, Helvetica, Arial, sans-serif;

      font-size: 14px;

      color:#000;

      margin: 0;

      padding: 0;

    }

    .swiper-container {

      width: 100%;

      height: 100%;

    }

    .swiper-slide {

      text-align: center;

      font-size: 18px;

      /*background: #fff;*/

      /* Center slide text vertically */

      display: -webkit-box;

      display: -ms-flexbox;

      display: -webkit-flex;

      display: flex;

      -webkit-box-pack: center;

      -ms-flex-pack: center;

      -webkit-justify-content: center;

      justify-content: center;

      -webkit-box-align: center;

      -ms-flex-align: center;

      -webkit-align-items: center;

      align-items: center;

    }

	  .contractor{

		  position: absolute;

		  bottom: 10px;

		  text-align: center;

		  width: 100%;

		  font-size: 14px;

	  }

	  .contractor a{

		  color: #fff;

	  }

  </style>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLXNWPG');</script>
<!-- End Google Tag Manager -->
</head>

<body id="page-top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLXNWPG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<?php 

	  function generateRandomString($length = 10) {

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

			$charactersLength = strlen($characters);

			$randomString = '';

			for ($i = 0; $i < $length; $i++) {

				$randomString .= $characters[rand(0, $charactersLength - 1)];

			}

			return $randomString;

	}

	  

	$token = generateRandomString();

	if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {

		$protocol = 'https://';

		}

		else {

		$protocol = 'http://';

		}

		$current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

		 

	  ?>

<div class="swiper-container">

<div class="swiper-wrapper">

	<div class="swiper-slide slidebg"> 
	<div class="covid19" style="background: #00aeef;text-align: center;padding: 10px 5px;color: #fff;width: 100%; position: absolute; top: 0px; font-size: 14px; line-height: 16px;">
Momentum Solar &amp; COVID 19 Safety Information <a href="/covid19/" style="color: #fff; text-decoration: underline;">Learn More</a>
</div>
	<img src="images/MS_Logo_web_menu-new.png" alt="Momentum Solar" style="position: absolute; top: 70px;" />

	<section class="banner">

		<div class="container text-center">

			<h1>See If Your Home Qualifies For $0 Upfront Solar</h1>

			<h4 class="my-5">Momentum Solar will help you maximize savings by checking for solar programs in your area.<br>Find out which solar programs your home qualifies for!</h4>

			<form id="step-01" novalidate="novalidate">

				<label> 

					<input name="input_0" id="choice_0" value="Start">

				</label>

				<button class="btn btn-primary rounded-pill btn-sm" type="submit">Calculate Your Savings</button>

			</form>

		</div>

	</section>

	<div class="contractor"><a href="https://momentumsolar.com/contractor-information/">State Contractor License Information</a></div>

	</div>

	<div class="swiper-slide"> 

		<section class="banner">

			<div class="position-vertical-center"> 

			<div class="progress-holder"> <div class="progress-number">1</div> </div> 

			<h3 class="my-5">Do you own your home?</h3> 

			<form id="step-02" novalidate="novalidate">

			<div class="icons-holder"> 

				<div class="icon-residential"> 

					<label> 

						<input name="input_1_1" id="choice_1_1" value="Residential"> 

						<img src="images/icon_residential-f9b85e53.png" id="residential" alt="Icon residential"> 

						

					</label>

					<br> Yes 

				</div> 

				<div class="icon-commercial"> 

					<label> 

						<input type="checkbox" name="input_1_2" id="choice_1_2" value="Commercial"> 

						<img src="images/icon_commercial-9a2c3cdc.png" id="commercial" alt="Icon commercial">

					</label>

					<br> No 

				</div> 

			</div>

			</form> 

			</div>

		</section>

	</div>

	<div class="swiper-slide">

	<section class="banner">

		<div class="container text-center">

			<div class="position-vertical-center">

			<div class="progress-holder"> <div class="progress-number">2</div></div> 

			<h3 class="my-5">What is your zip code?</h3>

			<div class="map-pin"></div>

			<form id="step-03" class="needs-validation mx-auto" novalidate>

				<div class="startquoteform">

					<input type="text" name="zip2" class="form-control rounded-0" id="zip2" placeholder="Enter Zip Code" required>

				</div>

				<button class="btn btn-primary rounded-0 button-go-text my-5 btn-sm" style="margin-top: 1px; margin-bottom: 1px;" type="submit">Next</button>

				<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">

				<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">

				<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">

				<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">

				<input type="hidden" name="url" value="<?php echo $current_link?>">

				<input type="hidden" value="OWN" name="property_ownership" id="property_ownership-OWN" checked=""> 

				<input type="hidden" name="step" class="step" value="step3">

				<input type="hidden" class="customformtoken" name="token" value="<?php echo $token;?>">

				

			</form>

			</div>

		</div>

	</section>

	</div>

	<div class="swiper-slide">

	<section class="banner">

		<div class="container text-center">

			<div class="position-vertical-center">

			<div class="progress-holder"> <div class="progress-number">3</div></div>

			<h3 class="my-5">What is your address?</h3>

			<div class="map-pin"></div> 

			<form id="step-04" class="needs-validation mx-auto" novalidate style="max-width: 360px;">

				<div class="startquoteform" style="display: block;">

						<input type="text" name="full_address" class="form-control rounded-0" id="full_address" placeholder="Address">

						<input type="text" id="street_number" name="street_number" value="" style="height: 0px; border: none; margin: 0; padding: 0; min-height: 0px; float: left;">

						<input type="text" id="route" name="street_name" value="" style="height: 0px; border: none; margin: 0; padding: 0; min-height: 0px; float: left;">

						<input type="text" id="locality" name="city" value="" style="height: 0px; border: none; margin: 0; padding: 0; min-height: 0px; float: left;">

						<input type="text" id="postal_code" name="postal_code" value="" style="height: 0px; border: none; margin: 0; padding: 0; min-height: 0px; float: left;">

						<input type="text" id="administrative_area_level_1" name="state_abbr" value="" style="height: 0px; border: none; margin: 0; padding: 0; min-height: 0px; float: left;">

						<input type="text" id="country" name="country" value="" style="height: 0px; border: none; margin: 0; padding: 0; min-height: 0px;">

						<div class="invalid-feedback">

							Please provide a valid Address.

						</div>

				</div>

				<input type="hidden" name="step" class="step" value="step4">

 				<input type="hidden" class="customformtoken" name="token" value="<?php echo $token;?>">

				<button class="btn btn-primary rounded-0 button-go-text btn-sm" style="margin-top: 1px; margin-bottom: 1px;" type="submit">Next</button>

				

			</form>

			</div>

		</div>

	</section>

	</div>

	<div class="swiper-slide">

	<section class="banner">

		<div class="container text-center">

			<div class="position-vertical-center">

			<div class="progress-holder"> <div class="progress-number">4</div></div>

			<div class="my-5">

			<h3>What is Your Average Electric Bill?</h3>

			<h4>Move the slider to indicate your monthly average electricity bill amount</h4>

			</div> 

			<form style="max-width: 650px;" id="step-05" class="mx-auto" novalidate>

				<div class="startquoteform">

					<output id="js-output"></output>

					<input type="range" min="50" max="800" step="10" value="300" data-rangeslider> 

					<div class="sliderLegend"> <p class="sliderLegendItem--start">$50</p> <p class="sliderLegendItem--end">$800</p> </div>

				</div>

				<button class="btn btn-primary rounded-0 button-go-text my-5 btn-sm" style="margin-top: 1px; margin-bottom: 1px;" type="submit">Next</button>

				

			</form>

			</div>

		</div>

	</section>

	</div>

	<div class="swiper-slide">

	<section class="banner">

		<div class="container text-center">

			<div class="position-vertical-center">

			<div class="progress-holder"> <div class="progress-number">5</div></div>

			<h3 class="my-5">Where Can We Send Your Free Solar Quote?</h3>

			<div class="icon-email"></div> 

			<form id="step-06" class="needs-validation mx-auto" novalidate>

				<div class="startquoteform my-5">

					<input type="text" name="email" class="form-control rounded-0" id="validationCustom03" placeholder="Email">

					<div class="invalid-feedback">

							Please provide a valid email.

						</div>

				</div>

				<input type="hidden" name="step" class="step" value="step5">

 				<input type="hidden" class="customformtoken" name="token" value="<?php echo $token;?>">

				<input type="hidden" name="customRadioInline1" id="customRadioInline1" value="100">

				<button class="btn btn-primary rounded-0 button-go-text btn-sm" style="margin-top: 1px; margin-bottom: 1px;" type="submit">Next</button>

				

			</form>

			</div>

		</div>

	</section>

	</div>

	<div class="swiper-slide">

	<section class="banner">

		<div class="container text-center">

			<div class="position-vertical-center">

			<div class="progress-holder"> <div class="progress-number">6</div></div>

			<h3 class="my-5">Who Is This Quote For?</h3>

			<h4>Please enter your first and last name</h4>

			<form id="step-07" class="needs-validation mx-auto" novalidate>

				<div class="my-4">

				<div class="startquoteform">

					<input type="text" name="first_name" class="form-control rounded-0" id="validationCustom01" placeholder="First name">

					<div class="invalid-feedback">

							Please provide first name.

						</div>

				</div>

				<div class="startquoteform" style="margin-top: 35px;">

					<input type="text" name="last_name" class="form-control rounded-0" id="validationCustom02" placeholder="Last name">

					<div class="invalid-feedback">

							Please provide last name.

						</div>

				</div>

				</div>

				<input type="hidden" name="step" class="step" value="step6">

 				<input type="hidden" class="customformtoken" name="token" value="<?php echo $token;?>">

				<button class="btn btn-primary rounded-0 button-go-text btn-sm" style="margin-top: 1px; margin-bottom: 1px;" type="submit">Next</button>

				

			</form>

			</div>

		</div>

	</section>

	</div>

	<div class="swiper-slide">

	<section class="banner">

		<div class="container text-center">

			<div class="position-vertical-center">

			<div class="progress-holder"> <div class="progress-number">7</div></div>

			<h3 class="my-5">Phone Number</h3>

			<div class="icon-phone"></div>

			<form id="step-08" class="needs-validation mx-auto" novalidate>

				<div class="startquoteform">

					<input type="text" name="phone" class="form-control rounded-0 phone_us" maxlength="14" id="validationCustom04"

							placeholder="Phone">

					<div class="invalid-feedback">

							Please provide a valid phone.

						</div>

				</div>

				<input type="hidden" name="step" class="step" value="step7">

 				<input type="hidden" class="customformtoken" name="token" value="<?php echo $token;?>">

				<button class="btn my-5 btn-primary rounded-0 btn-sm final_submit" type="submit">Submit</button>

				<p style="font-size: 12px; line-height: 16px; font-weight: 400; text-shadow: none;">*By hitting submit you authorize Momentum Solar to email, call and send you text messages on the phone number your submitted above using an automated dialer regarding solar products. This will override any federal or state Do Not Call lists. Consent is not a requirement of purchase. Data and text message rates will apply. <a href="/privacy-policy/" target="_blank">Privacy Policy</a> | <a href="/ccpa/" target="_blank">Do Not Sell</a></p>

			</form>

			</div>

		</div>

	</section>

	</div>

	<div class="swiper-slide slidewhite">

		<div class="step-08">

			<h3 class="text-success text-center" style="max-width: 600px;">

				<span class="d-block text-center">

				<i class="fas fa-check-circle mb-3 fa-2x"></i></span>Thank you for your interest in Momentum Solar. Our team will get back to you within 24-48 hours.

			</h3>

		</div>

	</div>

</div>

</div>

	<!-- Bootstrap core JavaScript -->

	<script src="vendor/jquery/jquery.min.js"></script>

	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->

	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom JavaScript for this theme -->

	<script src="js/scrolling-nav.js"></script>

	

	<script src="js/jquery.validate.js"></script>

	<script src="js/jquery.mask.js"></script>

	<script src="js/additional-methods.min.js"></script>

	<script src="vendor/swiper/js/swiper.min.js"></script>

   <script src="js/app.js"></script>

   <script src="js/rangeslider.js"></script>

<script> $(function() { var $document = $(document); var selector = '[data-rangeslider]'; var $inputRange = $(selector); 

/** * Example functionality to demonstrate a value feedback * and change the output's value. */ 

function valueOutput(element) { 

var value = element.value;

 var output = element.parentNode.getElementsByTagName('output')[0]; 

 output.innerHTML = value; 

 } 

 /** * Initial value output */ 

 for (var i = $inputRange.length - 1; i >= 0; i--) { 

	valueOutput($inputRange[i]); 

	

 }; /** * Update value output */ 

 $document.on('input', selector, function(e) { 

	

	

	valueOutput(e.target); 

 

 });

 /** * Initialize the elements */ 

 $inputRange.rangeslider({ polyfill: false ,

		onSlideEnd: function(position, value) {

			console.log(value);

			jQuery("#customRadioInline1").val('$'+value);

		},

 }); 

/** * Example functionality to demonstrate programmatic value changes */ 

$document.on('click', '#js-example-change-value button', function(e) { var $inputRange = $('[data-rangeslider]', e.target.parentNode); 

var value = $('input[type="number"]', e.target.parentNode)[0].value; 

$inputRange .val(value) .change(); 

}); /** * Example functionality to demonstrate programmatic attribute changes */ $document.on('click', '#js-example-change-attributes button', function(e) { var $inputRange = $('[data-rangeslider]', e.target.parentNode); var attributes = { min: $('input[name="min"]', e.target.parentNode)[0].value, max: $('input[name="max"]', e.target.parentNode)[0].value, step: $('input[name="step"]', e.target.parentNode)[0].value }; $inputRange .attr(attributes) .rangeslider('update', true); }); /** * Example functionality to demonstrate destroy functionality */ $document .on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) { $('input[type="range"]', e.target.parentNode).rangeslider('destroy'); }) .on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) { $('input[type="range"]', e.target.parentNode).rangeslider({ polyfill: false }); }); });</script>

 

  <!-- Initialize Swiper -->

  <script>

    var swiper = new Swiper('.swiper-container', {

      direction: 'vertical',

	  allowTouchMove: false,

      pagination: {

        el: '.swiper-pagination',

        clickable: true,

      },

    });

  </script>

	<script>

	(function(){

		$('.phone_us').mask('(000) 000-0000');

	})(jQuery)

		var placeSearch, autocomplete;

		var componentForm = {

			street_number: 'short_name',

			route: 'long_name',

			locality: 'long_name',

			administrative_area_level_1: 'short_name',

			country: 'long_name',

			postal_code: 'short_name'

		};

		function initAutocomplete() {

			// Create the autocomplete object, restricting the search to geographical

			// location types.

			autocomplete = new google.maps.places.Autocomplete(

				(document.getElementById('full_address')), {

					types: ['geocode']

				});

			// When the user selects an address from the dropdown, populate the address

			// fields in the form.

			autocomplete.addListener('place_changed', fillInAddress);

		}

		function fillInAddress() {

			// Get the place details from the autocomplete object.

			var place = autocomplete.getPlace();

			for (var component in componentForm) {

				document.getElementById(component).value = '';

				document.getElementById(component).disabled = false;

			}

			// Get each component of the address from the place details

			// and fill the corresponding field on the form.

			for (var i = 0; i < place.address_components.length; i++) {

				var addressType = place.address_components[i].types[0];

				if (componentForm[addressType]) {

					var val = place.address_components[i][componentForm[addressType]];

					document.getElementById(addressType).value = val;

				}

			}

		}

		// Bias the autocomplete object to the user's geographical location,

		// as supplied by the browser's 'navigator.geolocation' object.

		function geolocate() {

			if (navigator.geolocation) {

				navigator.geolocation.getCurrentPosition(function (position) {

					var geolocation = {

						lat: position.coords.latitude,

						lng: position.coords.longitude

					};

					var circle = new google.maps.Circle({

						center: geolocation,

						radius: position.coords.accuracy

					});

					autocomplete.setBounds(circle.getBounds());

				});

			}

		}

	</script>

   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete"

	 async defer></script>

    <!--Fading out "Get Free Quote" button in mobile-->

    <script>

        var windowsize = $(window).width();

        if(windowsize <= 425){

            $('.fixed-bottom').css('position','unset');

            $('.banner ').css('margin-top','0');

            var preScroll = 0;

            $(window).scroll(function(){

                var currentScroll = $(this).scrollTop();

                if (currentScroll > 350){

                    $('.fixed-bottom').fadeIn(500).css('position','fixed');

//                    $('.banner ').css('margin-top','48');

                } else {

                    $('.fixed-bottom').fadeOut(500).css('position','unset');

                }

                preScroll = currentScroll;

            });

        }

    </script>

	

	<!-- Facebook Pixel Code -->

	<script>

		! function (f, b, e, v, n, t, s) {

			if (f.fbq) return;

			n = f.fbq = function () {

				n.callMethod ?

					n.callMethod.apply(n, arguments) : n.queue.push(arguments)

			};

			if (!f._fbq) f._fbq = n;

			n.push = n;

			n.loaded = !0;

			n.version = '2.0';

			n.queue = [];

			t = b.createElement(e);

			t.async = !0;

			t.src = v;

			s = b.getElementsByTagName(e)[0];

			s.parentNode.insertBefore(t, s)

		}(window, document, 'script',

			'https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '252884438399772');

		fbq('track', 'PageView');

	</script>

	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=696369663874492&ev=PageView&noscript=1" />

	</noscript>

	<!-- End Facebook Pixel Code -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75829764-1"></script>

	<script>

		window.dataLayer = window.dataLayer || [];

		function gtag() {

			dataLayer.push(arguments);

		}

		gtag('js', new Date());

		gtag('config', 'UA-75829764-1');

		gtag('config', 'AW-922148957');

	</script>

	<script>

		(function (i, s, o, g, r, a, m) {

			i['GoogleAnalyticsObject'] = r;

			i[r] = i[r] || function () {

				(i[r].q = i[r].q || []).push(arguments)

			}, i[r].l = 1 * new Date();

			a = s.createElement(o),

				m = s.getElementsByTagName(o)[0];

			a.async = 1;

			a.src = g;

			m.parentNode.insertBefore(a, m)

		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-75829764-1', 'momentumsolar.com');

		ga('set', 'page', '/landing/full/');

		ga('send', 'pageview');

	</script>

 

<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"26038347"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>



<!-- Taboola Pixel Code -->

<script type='text/javascript'>

window._tfa = window._tfa || [];

window._tfa.push({notify: 'event', name: 'page_view', id: 1145705});

!function (t, f, a, x) {

if (!document.getElementById(x)) {

t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);

}

}(document.createElement('script'),

document.getElementsByTagName('script')[0],

'//cdn.taboola.com/libtrc/unip/1145705/tfa.js',

'tb_tfa_script');

</script>

<noscript>

<img src='//trc.taboola.com/1145705/log/3/unip?en=page_view'

width='0' height='0' style='display:none'/>

</noscript>

<!-- End of Taboola Pixel Code -->



<script data-obct type="text/javascript">
  /** DO NOT MODIFY THIS CODE**/
  !function(_window, _document) {
    var OB_ADV_ID='00395e22ee73d289704490cb74b7c9b50d';
    if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
    var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
obApi('track', 'PAGE_VIEW');
</script>



<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','o2rlz');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>

<script type="text/javascript">
    adroll_adv_id = "6GSI7DKO2ZAUFHVY3S2HXR";
    adroll_pix_id = "KXJUCWTBX5EYDF7NKT2JUM";

    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"> </script> <script type="text/javascript"> window.criteo_q = window.criteo_q || []; var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d"; window.criteo_q.push( { event: "setAccount", account: 71272 }, { event: "setSiteType", type: deviceType }, { event: "viewItem", item: "1" } ); </script>
<script type="text/javascript">
  (function() {
      var field = 'xxTrustedFormCertUrl';
      var provideReferrer = false;
      var invertFieldSensitivity = false;
      var tf = document.createElement('script');
      tf.type = 'text/javascript'; tf.async = true;
      tf.src = 'http' + ('https:' == document.location.protocol ? 's' : '') +
        '://api.trustedform.com/trustedform.js?provide_referrer=' + escape(provideReferrer) + '&field=' + escape(field) + '&l='+new Date().getTime()+Math.random() + '&invert_field_sensitivity=' + invertFieldSensitivity;
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tf, s); }
  )();
</script>
<noscript>
    <img src="http://api.trustedform.com/ns.gif" />
</noscript>

<script>
(function(document, tag) {
    var script = document.createElement(tag);
    var element = document.getElementsByTagName('body')[0];
    script.src = 'https://acsbap.com/apps/app/assets/js/acsb.js'; script.async = true; script.defer = true;
    (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script);
    script.onload = function() {
        acsbJS.init({
            statementLink     : '',
            feedbackLink      : '',
            footerHtml        : '',
            hideMobile        : false,
            hideTrigger       : false,
            language          : 'en',
            position          : 'left',
            leadColor         : '#146ff8',
            triggerColor      : '#146ff8',
            triggerRadius     : '50%',
            triggerPositionX  : 'left',
            triggerPositionY  : 'bottom',
            triggerIcon       : 'default',
            triggerSize       : 'medium',
            triggerOffsetX    : 20,
            triggerOffsetY    : 20,
            mobile            : {
                triggerSize       : 'small',
                triggerPositionX  : 'left',
                triggerPositionY  : 'center',
                triggerOffsetX    : 0,
                triggerOffsetY    : 0,
                triggerRadius     : '0'
            }
        });
    };
}(document, 'script'));
</script>
</body>

</html>