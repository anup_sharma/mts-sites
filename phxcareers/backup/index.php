<!DOCTYPE html>
<html lang="en" > 
<head>
      <title>Momentum Spring 21 Promo</title>
    <!-- Meta -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content=""> 

      <!-- Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
      <style type="text/css">
                    #footer{
            position:fixed;
            bottom:0;
            left:0;
            }
                #fileToUpload{
              display:none;
            }
        
            .promo {
            
                margin-top: 2%;
                margin-bottom: 2%;
                margin-left: -36%;

            }
            .error {
              color: red;
              font-size: 19px;
              font-weight: 600;
          }
          @media only screen and (max-width: 770px) {
          
          .promo {
           
              margin-left: -4%;
          }
        }
      </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script>
	jQuery(document).ready(function ($) {
    
    document.getElementById("results").style.display = 'none';
    document.getElementById("loading-image").style.display = 'none';
      $(function () {
        //submit handler on button click
        $('form#spring21Form').on('submit', function (e) {
          document.getElementById("loading-image").style.display = 'block';
          e.preventDefault();

          $.ajax({
            type: 'post',
            url: 'ajax.php',
            cache: false,
           contentType: false, 
            processData: false, 
            data: new FormData(this),
            success: function (data) {
             // alert (data);
				      document.getElementById("results").style.display = 'block';
              document.getElementById("spring21Form").style.display = 'none'
              document.getElementById("loading-image").style.display = 'none';
            }
          });

        });

	  });

    // when the button is clicked
	  $("#newBtn").click(function(){    
      $("#spring21Form").valid();    //valdate the form
      if ($('#spring21Form').valid()) {
        $("#spring21Form").submit(); // Submit the form
      }
    });

	});

    </script>	

</head>

  <body>
      <img src="header1x@2x.png" height="100%" width="100%" style="">
    
    <div class="container-fluid text-center">
<img src="https://tinyimg.io/i/wogqvFX.png" height="60%" width="60%">
<div id="results" class="topform">
						<br/><br/><h2 class="font-weight-light mb-3">Thank You For Your Request.</h2>
						<h4 class="mb-3">We’ll be reaching out to you shortly.</h4>
	</div>   

<form class="row g-3"  method="post" action="" id ="spring21Form" name ="spring21Form" method="POST" enctype="multipart/form-data">
  <div class="col-md-6">
    <label for="inputEmail4" class="form-label"></label>
    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required>
    <div class="invalid-feedback">
        Please enter your first name.
      </div>
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label"></label>
    <input type="text" class="form-control" id="lname" name="lname"  placeholder="Last Name" required>
  </div>
 
  <div class="col-12">
    <label for="inputAddress2" class="form-label"> </label>
    <input type="text" class="form-control" id="address" name="address" placeholder="Street Address" required>
  </div>
  <div class="col-md-6">
    <label for="inputCity" class="form-label"></label>
    <input type="City" class="form-control" id="city" name="city"  placeholder="City" required>
  </div>
  <div class="col-md-4">
    <label for="inputState" class="form-label"></label>
    <select id="inputState" class="form-select" name="inputState" required>
      <option selected>State</option>
      <option value="AL">Alabama</option>
  <option value="AK">Alaska</option>
  <option value="AZ">Arizona</option>
  <option value="AR">Arkansas</option>
  <option value="CA">California</option>
  <option value="CO">Colorado</option>
  <option value="CT">Connecticut</option>
  <option value="DE">Delaware</option>
  <option value="DC">District Of Columbia</option>
  <option value="FL">Florida</option>
  <option value="GA">Georgia</option>
  <option value="HI">Hawaii</option>
  <option value="ID">Idaho</option>
  <option value="IL">Illinois</option>
  <option value="IN">Indiana</option>
  <option value="IA">Iowa</option>
  <option value="KS">Kansas</option>
  <option value="KY">Kentucky</option>
  <option value="LA">Louisiana</option>
  <option value="ME">Maine</option>
  <option value="MD">Maryland</option>
  <option value="MA">Massachusetts</option>
  <option value="MI">Michigan</option>
  <option value="MN">Minnesota</option>
  <option value="MS">Mississippi</option>
  <option value="MO">Missouri</option>
  <option value="MT">Montana</option>
  <option value="NE">Nebraska</option>
  <option value="NV">Nevada</option>
  <option value="NH">New Hampshire</option>
  <option value="NJ">New Jersey</option>
  <option value="NM">New Mexico</option>
  <option value="NY">New York</option>
  <option value="NC">North Carolina</option>
  <option value="ND">North Dakota</option>
  <option value="OH">Ohio</option>
  <option value="OK">Oklahoma</option>
  <option value="OR">Oregon</option>
  <option value="PA">Pennsylvania</option>
  <option value="RI">Rhode Island</option>
  <option value="SC">South Carolina</option>
  <option value="SD">South Dakota</option>
  <option value="TN">Tennessee</option>
  <option value="TX">Texas</option>
  <option value="UT">Utah</option>
  <option value="VT">Vermont</option>
  <option value="VA">Virginia</option>
  <option value="WA">Washington</option>
  <option value="WV">West Virginia</option>
  <option value="WI">Wisconsin</option>
  <option value="WY">Wyoming</option>
</select>       
  </div>
  <div class="col-md-2">
    <label for="inputZip" class="form-label"></label>
    <input type="text" class="form-control" id="zip" placeholder="Zip Code" name="zip"  required>
  </div>

  <div class="col-12">
    <label for="phone" class="form-label"></label>
    <input type="number" class="form-control" id="phone" placeholder="Phone Number" name="phone" required >
  </div>
    <div class="col-12">
    <label for="email" class="form-label"></label>
    <input type="text" class="form-control" id="email" placeholder="Email Address" name="email" required>
  </div>
  <div class="col-12">
    <label for="appt_time" class="form-label"></label>
    <input type="datetime-local" class="form-control" id="appt_time" name="appt_time"  placeholder="Preferred Appointment Time">
  </div>
  <div class="col-12">
    <label for="promocode2" class="form-label"></label>
    <input type="text" class="form-control" id="promocode2" name="promocode2"  placeholder="ROOFSPRING21" disabled >
    <input id="promocode" name="promocode" type="hidden" value="ROOFSPRING21">
  </div>

    <div class="col-12">
    <label for="utility" class="form-label"></label>
    <input type="text" class="form-control" id="utility" placeholder="Utility Company" name="utility" >
  </div>
 <div class="col-12">
  
<label style="width:98%;" >
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="hidden" name="fileToUpload" value="">
  <!-- <img src="https://tinyimg.io/i/fnWlEXe.png" class="img-fluid"> -->
  <img src="utilitybill.png" class="img-fluid" style="width:95%;" > 
</label>
</div>
<div class="col-12">
  <img src="promo.png" class="img-fluid promo">
  </div>
  <div class="col-12">
    <a type="submit" id="newBtn"><img src="submitBtn.png" class="img-fluid"/></a>
    
  </div>
  <div class="col-12 text-center" style="margin-left:40%;">
    
    <img src="images/ajax-loader.gif" id="loading-image" style="display:none;" alt="Please Wait" style="margin-left:40%;"/>
  </div>
</form>


    </div>
    

      <footer class="footer">
        <img src="newfoot.png" height="100%" width="100%" style="">
        <img src="newFootCopyright.png" height="100%" width="100%" style="margin-top:1%;">
      </footer>
</body>

  </html>