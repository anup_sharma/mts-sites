<!DOCTYPE html>
<html lang="en" > 
<head>
      <title>Momentum Recruiting Page</title>
    <!-- Meta -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content=""> 

      <!-- Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
      <style type="text/css">
          #footer{
            position:fixed;
            bottom:0;
            left:0;
            }
                #fileToUpload{
              display:none;
            }
        
            .resume{
            
              width: 91%;
             margin-left: 9%;

            }
            .error {
              color: red;
              font-size: 19px;
              font-weight: 600;
          }
         
        }
      </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script>
	jQuery(document).ready(function ($) {
    
    document.getElementById("results").style.display = 'none';
    document.getElementById("loading-image").style.display = 'none';
      $(function () {
        //submit handler on button click
        $('form#recruitingForm').on('submit', function (e) {
          document.getElementById("loading-image").style.display = 'block';
          e.preventDefault();

          $.ajax({
            type: 'post',
            url: 'ajax.php',
            cache: false,
           contentType: false, 
            processData: false, 
            data: new FormData(this),
            success: function (data) {
           //alert (data);
				      document.getElementById("results").style.display = 'block';
              document.getElementById("recruitingForm").style.display = 'none'
              document.getElementById("loading-image").style.display = 'none';
            }
          });

        });

	  });

    // when the button is clicked
	  $("#newBtn").click(function(){    
      $("#recruitingForm").valid();    //valdate the form
      if ($('#recruitingForm').valid()) {
        $("#recruitingForm").submit(); // Submit the form
      }
    });

	});

    </script>	

</head>

  <body>
      <img src="header1x@2x.png" height="100%" width="100%" style="">
    
    <div class="container-fluid text-center">

<div id="results" class="topform">
						<br/><br/><h2 class="font-weight-light mb-3">Thank You For Your Request.</h2>
						<h4 class="mb-3">We’ll be reaching out to you shortly.</h4>
	</div>   

<form class="row g-3"  method="post" action="" id ="recruitingForm" name ="recruitingForm" method="POST" enctype="multipart/form-data">
  <div class="col-md-6">
    <label for="inputEmail4" class="form-label"></label>
    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required>
    <div class="invalid-feedback">
        Please enter your first name.
      </div>
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label"></label>
    <input type="text" class="form-control" id="lname" name="lname"  placeholder="Last Name" required>
  </div>
 

  <div class="col-12">
    <label for="phone" class="form-label"></label>
    <input type="number" class="form-control" id="phone" placeholder="Phone Number" name="phone" required >
  </div>
    <div class="col-12">
    <label for="email" class="form-label"></label>
    <input type="text" class="form-control" id="email" placeholder="Email Address" name="email" required>
  </div>

    <div class="col-12">
    <label for="utility" class="form-label"></label>
    <input type="text" class="form-control" id="interest" placeholder="Department Interested in..." name="interest" >
  </div>
 <div class="col-12">
  
<label style="width:98%;" >
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="hidden" name="fileToUpload" value="">
 
  <img src="resumeUpload.png" class="img-fluid" style="width:95%;" > 
</label>
</div>
<div class="col-12">
<a href="#">
  <img src="resume.png" class="img-fluid resume"/>
  </a>
  </div>
  <div class="col-12">
    <a type="submit" id="newBtn"><img src="submitBtn.png" class="img-fluid"/></a>
    
  </div>
  <div class="col-12 text-center" style="margin-left:40%;">
    
    <img src="images/ajax-loader.gif" id="loading-image" style="display:none;" alt="Please Wait" style="margin-left:52%;"/>
  </div>
</form>


    </div>
    

      <footer class="footer">
        <img src="newfoot.png" height="100%" width="100%" >
        <div class="col-12 text-center" style="display:flex; " >
          <div class="col-4 text-center" >
        

          </div>
          <div class="col-4 text-center" >
          <a href="https://www.facebook.com/momentumsolar/" target="blank" style="text-decoration:none;">
          <svg xmlns="http://www.w3.org/2000/svg" width="8.352" height="16.189" viewBox="0 0 8.352 16.189">
            <path id="Path_2364" data-name="Path 2364" d="M1423.738,1176.459h-2.929c-.011-.153-.029-.287-.029-.421q-.014-3.228-.026-6.456c0-.15-.013-.3-.022-.492h-2.43v-2.874c.792-.062,1.593,0,2.415-.042.011-.135.029-.246.029-.357,0-.589-.006-1.178,0-1.768a4.741,4.741,0,0,1,.262-1.584,3.106,3.106,0,0,1,2.518-2.111,6.454,6.454,0,0,1,1.261-.083c.6.007,1.2.056,1.833.088.059.87.025,1.708.038,2.565a2.786,2.786,0,0,1-.289.036c-.371,0-.743,0-1.114,0a3.783,3.783,0,0,0-.611.048.98.98,0,0,0-.891.957c-.041.484-.023.972-.028,1.459,0,.24,0,.479,0,.772h2.846c-.126.994-.245,1.928-.368,2.895h-2.454c-.032.517-.009,1-.011,1.481s0,.974,0,1.46,0,.974,0,1.46,0,.974,0,1.461S1423.738,1175.928,1423.738,1176.459Z" transform="translate(-1418.303 -1160.27)" fill="#369"/>
          </svg>
          </a>
          <a href="https://twitter.com/momentumsolar?lang=en" target="blank" style="text-decoration:none;">
          <svg xmlns="http://www.w3.org/2000/svg" width="15.518" height="12.862" viewBox="0 0 15.518 12.862">
              <path id="Path_2368" data-name="Path 2368" d="M1536.73,1182.116a6.763,6.763,0,0,0,4.5-1.356c-1.644-.37-2.758-1.266-2.86-2.292h1.131l.043-.076a3.321,3.321,0,0,1-2.391-3.188c.255.1.451.182.656.24a2.2,2.2,0,0,0,.606.075,3.467,3.467,0,0,1-1.237-2.643,2.818,2.818,0,0,1,.448-1.607,9.67,9.67,0,0,0,2.963,2.374,8.854,8.854,0,0,0,3.666.981c-.01-.154-.016-.265-.025-.375a3.243,3.243,0,0,1,5.409-2.743.425.425,0,0,0,.481.11c.455-.171.911-.338,1.365-.51.1-.039.2-.092.372-.169a3.271,3.271,0,0,1-1.14,1.62,1.074,1.074,0,0,0,.743-.07c.242-.073.482-.156.784-.255a1.2,1.2,0,0,1-.131.256c-.368.391-.728.792-1.12,1.156a.679.679,0,0,0-.27.547,9.372,9.372,0,0,1-2.278,6.218,8.646,8.646,0,0,1-5.927,3.057,9.29,9.29,0,0,1-4.591-.672A5.282,5.282,0,0,1,1536.73,1182.116Z" transform="translate(-1536.73 -1170.655)" fill="#369"/>
            </svg>
            </a>
            <a href="https://www.linkedin.com/company/momentum-solar" target="blank" style="text-decoration:none;">
              <svg id="Group_4" data-name="Group 4" xmlns="http://www.w3.org/2000/svg" width="14.538" height="14.463" viewBox="0 0 14.538 14.463">
              <path id="Path_2365" data-name="Path 2365" d="M1741.5,1203.9h-2.993c-.009-.142-.025-.278-.026-.413-.006-1.435,0-2.87-.019-4.305a7.12,7.12,0,0,0-.113-1.335,1.308,1.308,0,0,0-1.362-1.164,1.523,1.523,0,0,0-1.666,1.018,3.946,3.946,0,0,0-.187,1.13c-.022,1.537-.012,3.075-.014,4.613v.474c-1.005.023-1.982.021-2.991.005v-9.592a16.568,16.568,0,0,1,2.84-.031c.012.364.025.737.04,1.176a2.74,2.74,0,0,0,.215-.22,3.073,3.073,0,0,1,2.12-1.193,4.307,4.307,0,0,1,2.3.279,2.511,2.511,0,0,1,1.258,1.137,4.476,4.476,0,0,1,.518,1.8c.044.51.07,1.023.073,1.535.01,1.55.006,3.1.007,4.652C1741.5,1203.6,1741.5,1203.739,1741.5,1203.9Z" transform="translate(-1726.963 -1189.474)" fill="#369"/>
              <path id="Path_2366" data-name="Path 2366" d="M1704.49,1205.366h-2.963v-9.606h2.919a.288.288,0,0,1,.024.022c.008.009.02.019.022.03a.841.841,0,0,1,.022.151q0,4.612.006,9.223A1.25,1.25,0,0,1,1704.49,1205.366Z" transform="translate(-1701.263 -1190.937)" fill="#369"/>
              <path id="Path_2367" data-name="Path 2367" d="M1701.591,1169.137a1.741,1.741,0,1,1,1.777-1.693A1.733,1.733,0,0,1,1701.591,1169.137Z" transform="translate(-1699.881 -1165.656)" fill="#369"/>
            </svg>
          </a>
          <a href="https://www.instagram.com/momentumsolar/?hl=en" target="blank" style="text-decoration:none;">
          <svg id="Group_5" data-name="Group 5" xmlns="http://www.w3.org/2000/svg" width="15.291" height="15.265" viewBox="0 0 15.291 15.265">
              <path id="Path_2370" data-name="Path 2370" d="M2038.214,1163.161c1,0,2-.01,3,0a5.941,5.941,0,0,1,1.779.268,3.813,3.813,0,0,1,2.7,2.937,8.407,8.407,0,0,1,.189,1.791c.013,2,.006,4-.034,5.995a4.314,4.314,0,0,1-.811,2.522,3.77,3.77,0,0,1-2.627,1.578,23.289,23.289,0,0,1-2.719.158c-1.46.016-2.921-.01-4.381-.03a5.58,5.58,0,0,1-2.143-.387,3.924,3.924,0,0,1-2.452-3.366,20.657,20.657,0,0,1-.115-2.339q-.012-2.248.04-4.5a6.811,6.811,0,0,1,.191-1.521,3.817,3.817,0,0,1,3.025-2.937,7.245,7.245,0,0,1,1.446-.171c.973-.026,1.947-.008,2.92-.008Zm.047,1.321v.027c-.589,0-1.18-.016-1.768,0a18.323,18.323,0,0,0-2.107.134,2.567,2.567,0,0,0-2.257,2.158,4.078,4.078,0,0,0-.1.8c-.015,1.961-.028,3.921-.017,5.882a6.414,6.414,0,0,0,.164,1.408,2.4,2.4,0,0,0,1.741,1.876,5.078,5.078,0,0,0,1.322.209c1.9.03,3.793.035,5.689.027a6.755,6.755,0,0,0,1.373-.146,2.417,2.417,0,0,0,1.93-1.683,4.744,4.744,0,0,0,.235-1.316c.037-1.768.044-3.536.041-5.3a11.271,11.271,0,0,0-.118-1.608,2.57,2.57,0,0,0-1.282-1.955,3.47,3.47,0,0,0-1.657-.433Z" transform="translate(-2030.59 -1163.154)" fill="#369"/>
              <path id="Path_2371" data-name="Path 2371" d="M2099.971,1180.5a.884.884,0,0,1-.934.9.914.914,0,1,1,.024-1.827A.884.884,0,0,1,2099.971,1180.5Z" transform="translate(-2087.313 -1176.948)" fill="#369"/>
              <path id="Path_2372" data-name="Path 2372" d="M2057.756,1186.183a3.93,3.93,0,1,0,3.943,3.963A3.914,3.914,0,0,0,2057.756,1186.183Zm0,6.473a2.54,2.54,0,0,1-.016-5.079,2.54,2.54,0,1,1,.016,5.079Z" transform="translate(-2050.104 -1182.493)" fill="#369"/>
            </svg>
            </a>
          </div>
          <div class="col-4 text-center" >
        

          </div>
         
        </div>
        <img src="newFootCopyright.png" height="100%" width="97%" style="margin-top:1%;">
      </footer>
</body>

  </html>