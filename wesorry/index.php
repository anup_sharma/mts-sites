<!DOCTYPE html>
<html lang="en" > 
<head>
      <title>We're Sorry!</title>
    <!-- Meta -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content=""> 



      <!-- Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
      <style type="text/css">
        #footer{
 position:fixed;
 bottom:0;
 left:0;
}
    #fileToUpload{
  display:none;
}

#submitBtn {
    font-size: 0;
    background: url(images/submitBtn.png) no-repeat center center;
    width: 100%;
    height: 83px;
    margin-top: 2%;
}
body{
  background-image: url(images/bg.png);
    background-size: cover;
}
.form-control {

    color: #222222;
    border:none;
    background-image: linear-gradient(#f8f8f8, #e2e2e2);
}
.form-select {
    display: block;
    width: 100%;
    padding: .375rem 1.75rem .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    vertical-align: middle;
    background-color: #fff;
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e);
    background-repeat: no-repeat;
    background-position: right .75rem center;
    background-size: 16px 12px;
    border: none;
    border-radius: .25rem;
    -webkit-appearance: none;
    -moz-appearance: none;
    background: -webkit-gradient(linear, left top, left 35, from(#f8f8f8), color-stop(11%, #f8f8f8), to(#e2e2e2)) !important;
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M2 5l6 6 6-6'/%3e%3c/svg%3e);

    /* appearance: none; */
}

::placeholder {
    color: #222222 !important;
}
.phoneNumber {
  text-align: center;
  margin-top: 3%;
}
@media only screen and (max-width: 440px){
  img.phoneNumber {
      width: 66%;
      margin-top: 9%;
  }
}
      </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<?php 

if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  $protocol = 'https://';
  }
  else {
  $protocol = 'http://';
  }
  $current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
   
?>

<script>
	jQuery(document).ready(function ($) {
    document.getElementById("loading-image").style.display = 'none';
      $(function () {

        $('form#ccmcForm').on('submit', function (e) {
          document.getElementById("loading-image").style.display = 'block';
          e.preventDefault();

          $.ajax({
            type: 'post',
            url: 'ajax.php',
            cache: false,
           contentType: false, 
            processData: false, 
            data: new FormData(this),
            success: function (data) {
              //alert (data);
				      document.getElementById("results").style.display = 'block';
              document.getElementById("ccmcForm").style.display = 'none'
              document.getElementById("loading-image").style.display = 'none';
            }
          });

        });

	  });
	  
	});

	$(document).ready(function() { 
		
		document.getElementById("results").style.display = 'none';

	});
    </script>	

</head>

  <body>
      <img src="header1x@2x.png" height="100%" width="100%" style="">
    
    <div class="container-fluid text-center">

<div id="results" class="topform">
						<br/><br/><h2 class="font-weight-light mb-3">Thank You For Your Request.</h2>
						<h4 class="mb-3">We’ll be reaching out to you shortly.</h4>
	</div>   

<form class="row g-3"  method="post" action="" id ="ccmcForm" name ="ccmcForm" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
						<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
						<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
						<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
						<input type="hidden" name="url" value="<?php echo $current_link?>">
            
  <div class="col-md-6">
    <label for="inputEmail4" class="form-label"></label>
    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required>
    <div class="invalid-feedback">
        Please enter your first name.
      </div>
  </div>
  <div class="col-md-6">
    <label for="inputPassword4" class="form-label"></label>
    <input type="text" class="form-control" id="lname" name="lname"  placeholder="Last Name" required>
  </div>

  <div class="col-12">
    <label for="inputAddress2" class="form-label"> </label>
    <input type="text" class="form-control" id="address" name="address" placeholder="Street Address" required>
  </div>
  <div class="col-md-6">
    <label for="inputCity" class="form-label"></label>
    <input type="City" class="form-control" id="city" name="city"  placeholder="City" required>
  </div>
  <div class="col-md-4">
    <label for="inputState" class="form-label"></label>
    <select id="inputState" class="form-select" name="inputState" required>
      <option selected>State</option>
      <option value="AL">Alabama</option>
  <option value="AK">Alaska</option>
  <option value="AZ">Arizona</option>
  <option value="AR">Arkansas</option>
  <option value="CA">California</option>
  <option value="CO">Colorado</option>
  <option value="CT">Connecticut</option>
  <option value="DE">Delaware</option>
  <option value="DC">District Of Columbia</option>
  <option value="FL">Florida</option>
  <option value="GA">Georgia</option>
  <option value="HI">Hawaii</option>
  <option value="ID">Idaho</option>
  <option value="IL">Illinois</option>
  <option value="IN">Indiana</option>
  <option value="IA">Iowa</option>
  <option value="KS">Kansas</option>
  <option value="KY">Kentucky</option>
  <option value="LA">Louisiana</option>
  <option value="ME">Maine</option>
  <option value="MD">Maryland</option>
  <option value="MA">Massachusetts</option>
  <option value="MI">Michigan</option>
  <option value="MN">Minnesota</option>
  <option value="MS">Mississippi</option>
  <option value="MO">Missouri</option>
  <option value="MT">Montana</option>
  <option value="NE">Nebraska</option>
  <option value="NV">Nevada</option>
  <option value="NH">New Hampshire</option>
  <option value="NJ">New Jersey</option>
  <option value="NM">New Mexico</option>
  <option value="NY">New York</option>
  <option value="NC">North Carolina</option>
  <option value="ND">North Dakota</option>
  <option value="OH">Ohio</option>
  <option value="OK">Oklahoma</option>
  <option value="OR">Oregon</option>
  <option value="PA">Pennsylvania</option>
  <option value="RI">Rhode Island</option>
  <option value="SC">South Carolina</option>
  <option value="SD">South Dakota</option>
  <option value="TN">Tennessee</option>
  <option value="TX">Texas</option>
  <option value="UT">Utah</option>
  <option value="VT">Vermont</option>
  <option value="VA">Virginia</option>
  <option value="WA">Washington</option>
  <option value="WV">West Virginia</option>
  <option value="WI">Wisconsin</option>
  <option value="WY">Wyoming</option>
</select>       
  </div>
  <div class="col-md-2">
    <label for="inputZip" class="form-label"></label>
    <input type="text" class="form-control" id="zip" placeholder="Zip Code" name="zip"  required>
  </div>

  <div class="col-12">
    <label for="phone" class="form-label"></label>
    <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone" required >
  </div>
  
    <div class="col-12">
    <label for="utility" class="form-label"></label>
    <input type="text" id="apptDateTime" name="apptDateTime" cvalue="" placeholder="Preferred Appointment Time" autocomplete="off" class="form-control" required>
  </div>

  <div class="col-12">
    <button type="submit" class="btn" id="submitBtn">Submit</button>
    <img src="images/phone.png" class="phoneNumber" class="img-fluid">
    
  </div>
  <div class="col-12 text-center" style="margin-left:40%;">
    
    <img src="images/ajax-loader.gif" id="loading-image" style="display:none;" alt="Please Wait" style="margin-left:40%;"/>
  </div>
</form>


    </div>
    

      <footer class="footer" style="text-align:center; margin-top: 4%;">
        <img src="newfoot.png" height="100%" width="88%" style="">
      </footer>
</body>

<script>
    $(document).ready(function(){
        $("#apptDateTime").focus( function() {
          $(this).attr({type: 'datetime-local'});
          });
    });
    </script>
  </html>