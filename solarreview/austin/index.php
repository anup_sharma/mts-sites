﻿<?php
$phone = "(833) 381-7588";
if($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_NJ") {
    $phone = "(833) 381-7588";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_PA") {
    $phone = "(844) 592-8056";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_CT") {
    $phone = "(855) 338-0690";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_FL") {
    $phone = "(866) 426-9323";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_TX") {
    $phone = "(844) 731-0129";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_NY") {
    $phone = "(855) 997-3041";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Remarketing") {
    $phone = "(844) 249-0102";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_NJ") {
    $phone = "(833) 582-9932";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_PA") {
    $phone = "(855) 997-7495";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_CT") {
    $phone = "(855) 782-0757";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_FL") {
    $phone = "(855) 965-1765";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_TX") {
    $phone = "(844) 790-2124";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_NY") {
    $phone = "(844) 247-3551";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_NJ") {
    $phone = "(844) 332-1902";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_PA") {
    $phone = "(844) 263-0006";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_FL") {
    $phone = "(844) 249-0568";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_CT") {
    $phone = "(844) 259-1553";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_TX") {
    $phone = "(844) 264-4824";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_NY") {
    $phone = "(844) 261-9076";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_CA") {
    $phone = "(833) 841-5030";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_CA") {
    $phone = "(833) 844-6405";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_CA") {
    $phone = "(833) 842-0531";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Momentum Solar</title>
	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-modal-ios.css" rel="stylesheet">
	<!--Font Awsome-->
	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	 crossorigin="anonymous">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.css">
	<!-- Custom styles for this template -->
	<link href="css/new.css" rel="stylesheet">
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,600" rel="stylesheet">
</head>
<body id="page-top">
	<textarea id="hidencopy" style="position: absolute; left: -2000px; width: 600px; opacity: 0;"></textarea>
<div class="phonetop">
	<a href="tel:<?php echo $phone; ?>"><img src="images/phone-black.png" alt="<?php echo $phone; ?>"></a>
</div>
<div class="fixed-bottom d-block d-flex justify-content-between d-sm-none px-5">
    <a class="btn btn-primary rounded-0 my-3 text-white pull-left" type="submit" href="tel:<?php echo $phone; ?>">Call Us</a>
</div>
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg bg-white navbar-light fixed-top p-0" id="mainNav">
		<div class="logo-container">
			<a class="navbar-brand js-scroll-trigger pl-lg-5 pl-0 py-3 py-lg-0 mr-0">
				<img src="images/logo.png" alt="logo" class="text-center sitelogo">
			</a>
		</div>
		<div class="container-fluid p-0 d-flex justify-content-center">
			<button class="navbar-toggler mr-3 hamburger-icon" type="button" data-toggle="collapse" data-target="#navbarResponsive"
			 aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item phonenav">
						<a class="nav-link js-scroll-trigger phonenumber" href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone; ?></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<?php 
	  function generateRandomString($length = 15) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
	}
	  
	  $token = generateRandomString().str_replace('.',generateRandomString(),microtime(true));
	if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
		$protocol = 'https://';
		}
		else {
		$protocol = 'http://';
		}
		$current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		 
	  ?>
	<section id="faq">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="frequentlyaq-text pt-5 pb-4">Write a Review</h2>
				</div>
			</div>
			<div class="row">
				<form id="reviewform" class="needs-validation mx-auto" novalidate>
				<input type="number" name="rating" id="rating-input" min="1" max="5" />
				<div class="rating" role="optgroup">
				  <!-- in Rails just use 1.upto(5) -->  
				  <i class="fa fa-star-o fa-2x rating-star" id="rating-1" data-rating="1" tabindex="0" aria-label="Rate as one out of 5 stars" role="radio"></i>
				  <i class="fa fa-star-o fa-2x rating-star" id="rating-2" data-rating="2" tabindex="0" aria-label="Rate as two out of 5 stars" role="radio"></i>
				  <i class="fa fa-star-o fa-2x rating-star" id="rating-3" data-rating="3" tabindex="0" aria-label="Rate as three out of 5 stars" role="radio"></i>
				  <i class="fa fa-star-o fa-2x rating-star" id="rating-4" data-rating="4" tabindex="0" aria-label="Rate as four out of 5 stars" role="radio"></i>
				  <i class="fa fa-star-o fa-2x rating-star" id="rating-5" data-rating="5" tabindex="0" aria-label="Rate as five out of 5 stars" role="radio"></i>
				</div>
			<input type="hidden" name="state" value="Austin" />
				<div class="row">
				<div class="col-12 col-md-12 col-lg-6">
					<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">
					<div class="invalid-feedback">
						Please provide first name.
					</div>
				</div>
				<div class="col-12 col-md-12 col-lg-6">
					<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">
					<div class="invalid-feedback">
						Please provide last name.
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-12 col-md-12 col-lg-6">
					<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">
					<div class="invalid-feedback">
						Please provide a valid email.
					</div>
				</div>
				<div class="col-12 col-md-12 col-lg-6">
					<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04"
						placeholder="Phone">
					<div class="invalid-feedback">
						Please provide a valid phone.
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-12 col-md-12">
					<textarea name="review" class="form-control mt-3 rounded-0" id="reviewtext" placeholder="Review"></textarea>
				</div>
				</div>
				<div class="row justify-content-end">
				<div class="center-block mt-3 col-5">
					<button class="btn btn-primary rounded-0 btn-block" type="submit">Submit</button>
				</div>
				</div>
				</form>
			</div>
		</div>
	</section>
	<section class="share">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="frequentlyaq-text pt-5 pb-4">Thank you for your feedback</h2>
					<p>We're so happy to welcome you to the Momentum Family. Help us spread the word by sharing your review in 2 easy steps:</p>
					<p>1. Click the "Copy" button to save your review.<br><button type="button" class="btn btn-primary rounded-0 button-get-started py-2 mb-2" onclick="copyToClipboard('#reviewtext')" style="margin-top: 1.2rem">Copy</button></p>
					<p>2. Click the link below to get redirected to our profile page and paste (CTRL+V) your review!</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4 text-center" style="margin: 10px auto;">
					<a href="https://search.google.com/local/writereview?placeid=ChIJr8Kz1n_JRIYRnzTfl39Tqlc" target="_blank"><img src="images/reviews-google.jpg" alt="" /></a>
				</div>
			</div>
		</div>
	</section>
	<section class="thankyou">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="frequentlyaq-text pt-5 pb-4">Thank you!</h2>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section -->
	<section class="testimonial py-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="row">
						<p class="quote-heading center-on-small"><span>WHAT OUR CUSTOMERS SAY</span></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
							<li data-target="#myCarousel" data-slide-to="4"></li>
							<li data-target="#myCarousel" data-slide-to="5"></li>
							<li data-target="#myCarousel" data-slide-to="6"></li>
							<li data-target="#myCarousel" data-slide-to="7"></li>
						</ol>
						<!-- Wrapper for carousel items -->
						<div class="carousel-inner">
							<div class="item carousel-item active">
								<p class="testimonial">We’re growing more trees because of us going solar. I’m really happy because we’re
									saving more money and helping the environment by helping more trees grow.</p>
								<p class="person-name">- The Veiga Family from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Coming from a background in construction, I wanted every wire to be perfect, and
									everything squared, conduit straight. Momentum nailed every facet of that.</p>
								<p class="person-name">- Zach from Texas</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">We wanted to save money and do something that’s good for the environment. I went with
									Momentum because of the products and services they offer, the pricing and ultimately our salesman.</p>
								<p class="person-name">- Michael from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Thanks to Momentum and the savings I’m getting, I can spend more money on things I’d put
									off for later. I don’t know exactly what but I know it’ll be good things for my house.</p>
								<p class="person-name">- Austria from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">We just get our first bill and it was only $30. That’s your money or your children’s
									money to spend as you wish. We really can’t tell you how happy we are.</p>
								<p class="person-name">- The Wdowiaks from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Customer experience I had at Momentum Solar was exceptional. Everyone I’ve talked to has
									been very helpful and if I had a question, they would seek to answer my question and I was very happy with
									them!</p>
								<p class="person-name">- John from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">The reputation of the company had a lot to do with it, the 25 yr warranty with service
									included was really important to me because I didn’t want to have to start paying for labor down the road.</p>
								<p class="person-name">- Bob from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">I’ll be saving about $70 per month. Well, $70 a month gives me the opportunity to really
									spoil my grandchildren!</p>
								<p class="person-name">- Tom from New Jersey</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer -->
	<footer class="footer-section">
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-lg-3 align-self-center text-center col-md-12">
					<img src="images/logo-footer.jpg" alt="Footer Logo">
					<p><a href="/contractor-information/">State Contractor License Information</a></p>
				</div>
				<div class="col-lg-6 col-md-9 align-self-center py-4">
					<p class="m-0">© 2019 Momentum Solar. All Rights Reserved by Pro Custom Solar LLC D|B|A Momentum Solar | <a href="https://momentumsolar.com/privacy-policy" target="_blank">Privacy Policy</a></p>
				</div>
				<div class="col-lg-3 col-md-3 text-md-right align-self-center text-center">
					<div class="social-icons">
						<a href="https://twitter.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-twitter twitter-icon"></i>
						</a>
						<a href="https://www.facebook.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-facebook-f facebook-icon"></i>
						</a>
						<a href="https://www.instagram.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-instagram insta-icon"></i>
						</a>
						<a href="https://www.linkedin.com/company/momentum-solar/" class="social-icon" target="_blank">
							<i class="fab fa-linkedin-in linked-icon"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</footer>
	<div id="myModal" class="modal fade custom-modal" role="dialog">
		<div class="modal-dialog modal-xl">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="scrolltoelement"></div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="container">
						<div class="row">
							<div class="col d-flex text-center align-items-center">
								<!-- Step 1 -->
								<div class="col form py-1 mx-auto step-1">
									<h3 class="font-weight-light mb-5">What is your zip code?</h3>
									<p class="mb-5 mt-4 custom-width">Your zip code will allow Momentum Solar to calculate state and utility
										incentives to maximize your tax benefits.</p>
									<form id="step-1" class="needs-validation mx-auto" novalidate>
										<input id="zip" class="form-control rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip"
										 type="text">
										<div class="invalid-feedback mb-4">
											Please provide a valid zip.
										</div>
										<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
										<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
										<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
										<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
										<input type="hidden" name="url" value="<?php echo $current_link?>">
										<input type="hidden" name="step" class="step" value="step1">
										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
										<div class="center-block mt-5">
											<button class="btn btn-primary rounded-0 btn-block" type="submit">Next</button>
										</div>
									</form>
								</div>
								<!-- Step 2 -->
								<div class="col form pt-0 pt-lg-5 px-0 pb-0 mx-auto step-2" style="display:none;">
									<h3 class="font-weight-light mb-5">What is your average monthly electric bill?</h3>
									<form id="step-2" class="needs-validation mx-auto bill-box" novalidate>
										<div id="1" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="Under $50" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline1">Under $50</label>
										</div>
										<div id="2" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="$50 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline2">$50 - $150</label>
										</div>
										<div id="3" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="$150 - $350" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline3">$150 - $350</label>
										</div>
										<div id="4" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="$300 - $450" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline4">$300 - $450</label>
										</div>
										<div id="5" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="Over $450" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline5">Over $450</label>
										</div>
										<input type="hidden" name="step" class="step" value="step2">
										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
										<div class="row">
											<div class="col-6 text-left">
												<button class="btn btn-default text-left btn-link rounded-0 btn-sm" onclick="gotoStep(1)" type="button">
													<i class="fas fa-chevron-left"></i>Back</button>
											</div>
											<div class="col-6 text-right">
												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
											</div>
										</div>
									</form>
								</div>
								<!-- Step 3 -->
								<div class="col form pt-0 pt-lg-5 mx-auto step-3" style="display:none;">
									<h3 class="font-weight-light mb-5">Please enter your home address.</h3>
									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">We will use this information to estimate how much energy your roof can produce.</p>
									<form id="step-3" class="needs-validation mx-auto" novalidate>
										<input type="text" name="full_address" class="form-control rounded-0 cw-75" id="full_address" placeholder="Address">
										<input type="text" id="street_number" name="street_number" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
										<input type="text" id="route" name="street_name" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
										<input type="text" id="locality" name="city" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
										<input type="text" id="postal_code" name="postal_code" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
										<input type="text" id="administrative_area_level_1" name="state_abbr" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
										<input type="text" id="country" name="country" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
										<div class="invalid-feedback">
											Please provide a valid Address.
										</div>
										<input type="hidden" name="step" class="step" value="step3">
										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
										<div class="row mt-5">
											<div class="text-left col-6 ">
												<button class="btn btn-default btn-link rounded-0 btn-sm text-left" onclick="gotoStep(2)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>
											<div class="col-6 text-right">
												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
											</div>
										</div>
									</form>
								</div>
								<!-- Step 4 -->
								<div class="col form pt-0 pt-md-3 mx-auto step-4" style="display: none;">
									<h3 class="font-weight-light mb-5">How much shade does your roof get from 9 AM - 3 PM?</h3>
									<form id="step-4">
										<div class="step4mobile">
											<div id="6" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">
												<input type="radio" value="No Shade" id="customRadioInline6" name="shade" class="custom-control-input shade_radio">
												<label class="custom-control-label" for="customRadioInline6">No Shade</label>
											</div>
											<div id="7" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">
												<input type="radio" value="Some Shade" id="customRadioInline7" name="shade" class="custom-control-input shade_radio">
												<label class="custom-control-label" for="customRadioInline7">Some Shade</label>
											</div>
											<div id="8" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">
												<input type="radio" value="Severe Shade" id="customRadioInline8" name="shade" class="custom-control-input shade_radio">
												<label class="custom-control-label" for="customRadioInline8">Severe Shade</label>
											</div>
										</div>
										<div class="step4desk">
											<div class="row mb-5">
												<div class="col-12 col-md-6 shade " id="no-shade">
													<div class="shade-image d-none d-sm-block">
														<img src="images/roof-1.png">
													</div>
													<h5 class="mt-3 "><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> No Shade</h5>
													<p class="d-none d-sm-block">Entire roof is exposed to sunlight.</p>
													<input type="radio" class="no-shade" name="shade" value="No Shade" style="display:none">
												</div>
												<div class="col-12 col-md-6 shade" id="some-shade">
													<div class="shade-image d-none d-sm-block">
														<img src="images/roof-2.png">
													</div>
													<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Some Shade</h5>
													<p class="d-none d-sm-block">Parts of the roof are covered at certain times/all times.</p>
													<input type="radio" class="some-shade" name="shade" value="Some Shade" style="display:none">
												</div>
												<div class="col-12 col-md-6 shade " id="severe-shade">
													<div class="shade-image d-none d-sm-block">	
														<img src="images/roof-3.png">
													</div>
													<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Severe Shade</h5>
													<p class="d-none d-sm-block">Most or all of the roof is covered at certain times/all times.</p>
													<input type="radio" class="severe-shade" name="shade" value="Severe Shade" style="display:none">
													<input type="hidden" name="step" class="step" value="step4">
													<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="text-left col-6">
												<button class="btn text-left btn-default rounded-0 btn-link btn-sm" onclick="gotoStep(3)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>
											<div class="col-6 text-right">
												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
											</div>
										</div>
									</form>
								</div>
								<!-- Step 5 -->
								<div class="col form pt-0 pt-sm-8 mx-auto step-5" style="display:none;">
								
									<h3 class="font-weight-light mb-0 mb-md-5">Please enter your contact information to receive a <b>no-obligation</b>
										detailed quote from our solar professionals.</h3>
									<form id="step-5" class="needs-validation row" novalidate>
										<div class="col-12">
										<div class="row m-md-auto">
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">
												<div class="invalid-feedback">
													Please provide first name.
												</div>
											</div>
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">
												<div class="invalid-feedback">
													Please provide last name.
												</div>
											</div>
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">
												<div class="invalid-feedback">
													Please provide a valid email.
												</div>
											</div>
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04"
													placeholder="Phone">
												<div class="invalid-feedback">
													Please provide a valid phone.
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12 text-center my-3">
												<button class="btn btn-primary rounded-0 btn-sm final_submit" type="submit">Submit</button>
												<img src="/landing/images/loading_bubble.svg" style="display:none" class="loader_image">
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<p class="my-3 small w-75 ml-auto mr-auto">*By clicking the "Submit" button you agree to our Terms and Privacy Policy and
													authorize Momentum Solar to reach out to you with a no-obligation quote or to ask for additional
													information in order to provide an accurate quote.</p>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<input type="hidden" name="step" class="step" value="step5">
												<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
												<div class="row">
													<div class="text-left">
														<button class="btn btn-default rounded-0 btn-sm btn-link" onclick="gotoStep(4)" type="button">
															<i class="fas fa-chevron-left"></i>Back</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									</form>
									<div class="bot-sec mt-5" style="display:none;">
										<div class="d-flex col- mx-o justify-content-between">
											<p class="back d-flex align-items-center"><span><i class="fas fa-chevron-left"></i></span>Back</p>
											<button class="btn btn-primary rounded-0" type="submit">Next</button>
										</div>
									</div>
								</div>
								<div class="col form py-5 mx-auto step-6" style="display:none;">
								<h3 class="text-success text-center">
									<span class="d-block text-center">
									<i class="fas fa-check-circle mb-3 fa-2x"></i></span>Thank you for your interest in Momentum Solar. Our team will get back to you within 24-48 hours.
								</h3>
								</div>
								<div class="close" style="display:none;">
									<i class="fas fa-times"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Plugin JavaScript -->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<!-- Custom JavaScript for this theme -->
	<script src="js/scrolling-nav.js"></script>
	<script src="js/app.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script>
function copyToClipboard(element) {
	temp = jQuery("#reviewtext").val();
	jQuery('#hidencopy').text(temp);
	temp = "";
var copyText = document.getElementById("hidencopy");
  copyText.select();
  document.execCommand("copy");
}
		
		
		$('.phone_us').mask('(000) 000-0000');
		var placeSearch, autocomplete;
		var componentForm = {
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'short_name',
			country: 'long_name',
			postal_code: 'short_name'
		};
		function initAutocomplete() {
			// Create the autocomplete object, restricting the search to geographical
			// location types.
			autocomplete = new google.maps.places.Autocomplete(
				(document.getElementById('full_address')), {
					types: ['geocode']
				});
			// When the user selects an address from the dropdown, populate the address
			// fields in the form.
			autocomplete.addListener('place_changed', fillInAddress);
		}
		function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();
			for (var component in componentForm) {
				document.getElementById(component).value = '';
				document.getElementById(component).disabled = false;
			}
			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			for (var i = 0; i < place.address_components.length; i++) {
				var addressType = place.address_components[i].types[0];
				if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]];
					document.getElementById(addressType).value = val;
				}
			}
		}
		// Bias the autocomplete object to the user's geographical location,
		// as supplied by the browser's 'navigator.geolocation' object.
		function geolocate() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function (position) {
					var geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					var circle = new google.maps.Circle({
						center: geolocation,
						radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
				});
			}
		}
	</script>
    <!--Fading out "Get Free Quote" button in mobile-->
    <script>
        var windowsize = $(window).width();
        if(windowsize <= 425){
            $('.fixed-bottom').css('position','unset');
            $('.banner ').css('margin-top','0');
            var preScroll = 0;
            $(window).scroll(function(){
                var currentScroll = $(this).scrollTop();
                if (currentScroll > 350){
                    $('.fixed-bottom').fadeIn(500).css('position','fixed');
//                    $('.banner ').css('margin-top','48');
                } else {
                    $('.fixed-bottom').fadeOut(500).css('position','unset');
                }
                preScroll = currentScroll;
            });
        }
    </script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete"
	 async defer></script>
	<!-- Facebook Pixel Code -->
	<script>
		! function (f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '252884438399772');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=696369663874492&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75829764-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-75829764-1');
		gtag('config', 'AW-922148957');
	</script>
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-75829764-1', 'momentumsolar.com');
		ga('set', 'page', '/landing/reviews/austin/');
		ga('send', 'pageview');
	</script>
<script>
	  gtag('config', 'AW-922148957/sK1kCKaBr5QBEN3A27cD', {
    //'phone_conversion_number': '1 (833)381-7588'
	// Instructions for Jonathan's programmer, When you use Dynamic variables. Please make sure you replace them everywhere the phone number (variable is) used.
    'phone_conversion_number': '<?php echo $phone;?>'
  });
</script>
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"26038347"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
<!-- Hotjar Tracking Code for https://momentumsolar.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1275020,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<script async type="text/javascript" src="//static.klaviyo.com/onsite/js/klaviyo.js?company_id=QGKQnL"></script>

<!-- Snap Pixel Code --> <script type='text/javascript'> (function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function() {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)}; a.queue=[];var s='script';r=t.createElement(s);r.async=!0; r.src=n;var u=t.getElementsByTagName(s)[0]; u.parentNode.insertBefore(r,u);})(window,document, 'https://sc-static.net/scevent.min.js'); snaptr('init', 'aef0a08a-ac06-4142-b18d-531729c6cfa6', { 'user_email': '__INSERT_USER_EMAIL__' }); snaptr('track', 'PAGE_VIEW'); </script> <!-- End Snap Pixel Code -->

<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10081644'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
<!-- Reddit Conversion Pixel -->
<script>
!function(w,d){if(!w.rdt){var p=w.rdt=function(){p.sendEvent?p.sendEvent.apply(p,arguments):p.callQueue.push(arguments)};p.callQueue=[];var t=d.createElement("script");t.src="https://www.redditstatic.com/ads/pixel.js",t.async=!0;var s=d.getElementsByTagName("script")[0];s.parentNode.insertBefore(t,s)}}(window,document);rdt('init','t2_41bcncpx');
</script>
<!-- DO NOT MODIFY -->
<!-- End Reddit Conversion Pixel -->
</body>
</html>