<?php
$conn = new mysqli('localhost','momemt_nsite','nJBxoUoRoV7R','momemt_nsite');
$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='NJ'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_nj = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='NJ'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_nj){
	$av_nj = $sum / $count_nj;
	$av_nj = round($av_nj, 2);
} else {
	$av_nj = "no reviews";
}


$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='NY'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_ny = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='NY'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_ny){
	$av_ny = $sum / $count_ny;
	$av_ny = round($av_ny, 2);
} else {
	$av_ny = "no reviews";
}


$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='Tampa'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_tampa = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='Tampa'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_tampa){
	$av_tampa = $sum / $count_tampa;
	$av_tampa = round($av_tampa, 2);
} else {
	$av_tampa = "no reviews";
}


$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='Orlando'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_orlando = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='Orlando'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_orlando){
	$av_orlando = $sum / $count_orlando;
	$av_orlando = round($av_orlando, 2);
} else {
	$av_orlando = "no reviews";
}


$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='Austin'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_austin = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='Austin'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_austin){
	$av_austin = $sum / $count_austin;
	$av_austin = round($av_austin, 2);
} else {
	$av_austin = "no reviews";
}


$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='San Antonio'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_antonio = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='San Antonio'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_antonio){
	$av_antonio = $sum / $count_antonio;
	$av_antonio = round($av_antonio, 2);
} else {
	$av_antonio = "no reviews";
}

$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='Cherry Hill'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_ch = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='Cherry Hill'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_ch){
	$av_ch = $sum / $count_ch;
	$av_ch = round($av_ch, 2);
} else {
	$av_ch = "no reviews";
}

$select_sql = "SELECT COUNT(id) AS CountReviews FROM `reviews` WHERE `state`='Orange County'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$count_oc = $row['CountReviews'];

$select_sql = "SELECT SUM(rate) AS Rating FROM `reviews` WHERE `state`='Orange County'";
$result = $conn->query($select_sql);
$row = mysqli_fetch_assoc($result);
$sum = $row['Rating'];
if($count_oc){
	$av_oc = $sum / $count_oc;
	$av_oc = round($av_oc, 2);
} else {
	$av_oc = "no reviews";
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Material Design Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.min.css" rel="stylesheet">
  <style>

    .map-container{
overflow:hidden;
padding-bottom:56.25%;
position:relative;
height:0;
}
.map-container iframe{
left:0;
top:0;
height:100%;
width:100%;
position:absolute;
}
  </style>
</head>

<body class="grey lighten-3">

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Brand -->
        <a class="navbar-brand waves-effect" href="index.php" target="_blank">
          <strong class="blue-text">Momenum Solar Reviews</strong>
        </a>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link waves-effect" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="reviews.php" target="_blank">Reviews</a>
            </li>
          </ul>

          <!-- Right -->

        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed">

      <a class="logo-wrapper waves-effect">
        <img src="https://www.momentumsolar.com/landing/reviews/ch/images/logo.png" class="img-fluid" alt="">
      </a>

      <div class="list-group list-group-flush">
        <a href="index.php" class="list-group-item active waves-effect">
          <i class="fas fa-chart-pie mr-3"></i>Dashboard
        </a>
        <a href="reviews.php" class="list-group-item list-group-item-action waves-effect">
          <i class="fas fa-table mr-3"></i>Reviews</a>
      </div>

    </div>
    <!-- Sidebar -->

  </header>
  <!--Main Navigation-->

  <!--Main layout-->
  <main class="pt-5 mx-lg-5">
    <div class="container-fluid mt-5">

      <!-- Heading -->
      <div class="card mb-4 wow fadeIn">

        <!--Card content-->
        <div class="card-body d-sm-flex justify-content-between">

          <h4 class="mb-2 mb-sm-0 pt-1">
            <span>Dashboard</span>
          </h4>

        </div>

      </div>
      <!-- Heading -->

      <!--Grid row-->
      <div class="row wow fadeIn">

        <!--Grid column-->
        <div class="col-md-9 mb-4">

          <!--Card-->
          <div class="card">

            <!--Card content-->
            <div class="card-body">

              <canvas id="myChart"></canvas>

            </div>

          </div>
          <!--/.Card-->

        </div>
        <!--Grid column-->
		<!--Grid column-->
        <div class="col-md-3 mb-4">

          
          <!--Card-->
          <div class="card mb-4">
			<div class="card-header text-center">
              Average Rating
            </div>
            <!--Card content-->
            <div class="card-body">

              <!-- List group links -->
              <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action waves-effect">New Jersey
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_nj; ?>
                  </span>
                </a>
                <a class="list-group-item list-group-item-action waves-effect">New York
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_ny; ?>
                  </span>
                </a>
                <a class="list-group-item list-group-item-action waves-effect">Tampa
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_tampa; ?>
                  </span>
                </a>
                <a class="list-group-item list-group-item-action waves-effect">Orlando
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_orlando; ?>
                  </span>
                </a>
                <a class="list-group-item list-group-item-action waves-effect">Austin
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_austin; ?>
                  </span>
                </a>
                <a class="list-group-item list-group-item-action waves-effect">San Antonio
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_antonio; ?>
                  </span>
                </a>
                <a class="list-group-item list-group-item-action waves-effect">Cherry Hill
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_ch; ?>
                  </span>
                </a>
                <a class="list-group-item list-group-item-action waves-effect">Orange County
                  <span class="badge badge-primary badge-pill pull-right"><?php echo $av_oc; ?>
                  </span>
                </a>

              </div>
              <!-- List group links -->

            </div>

          </div>
          <!--/.Card-->

        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4 wow fadeIn">




    <!--Copyright-->
    <div class="footer-copyright py-3">
      © 2019 Copyright:
      <a href="/" target="_blank"> Momentum Solar. </a>
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

  </script>

  <!-- Charts -->
  <script>
    // Line
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ["New Jersey", "New York", "Tampa", "Orlando", "Austin", "San Antonio", "Cherry Hill", "Orange County"],
        datasets: [{
          label: '# of Reviews',
          data: [<?php echo $count_nj; ?>, <?php echo $count_ny; ?>, <?php echo $count_tampa; ?>, <?php echo $count_orlando; ?>, <?php echo $count_austin; ?>, <?php echo $count_antonio; ?>, <?php echo $count_ch; ?>, <?php echo $count_oc; ?>],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(202, 202, 202, 0.2)',
            'rgba(135, 139, 26, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(202, 202, 202, 1)',
            'rgba(135, 139, 26, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    
  </script>

  <!--Google Maps-->
  <script src="https://maps.google.com/maps/api/js"></script>
  
</body>

</html>
