   function startQuote() {
	  		ga('set', 'page', '/quote/zip/');
			ga('send', 'pageview');
			fbq('track', 'Started Quote');
	}
jQuery(document).ready(function($){
   /* $("#myCarousel").swiperight(function() {
      $(this).carousel('prev');
    });
   $("#myCarousel").swipeleft(function() {
      $(this).carousel('next');
   }); */
   
   
  // $(".hidden_link").fancybox();
   $(".customformtoken").val($(".freetoken").val()); 
   
	
	jQuery('#reviewform').validate({
			rules: {
				rating: {required: true},
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true},
			},
			messages: {
				rating: "Please rate us!",
				first_name: "Please enter First Name.",
				last_name: "Please enter Last Name.",
				email: "Please enter Email.",
			},
		submitHandler: function(form) {
			var data = jQuery('#reviewform').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 if(response == "4" || response == "5"){
				 	jQuery("#faq").slideUp(300);
				 	jQuery("section.share").slideDown(300);
				 } else {
					jQuery("#faq").slideUp(300);
				 	jQuery("section.thankyou").slideDown(300); 
				 }
			});	
			return false; 
		}
	});
	
   
   jQuery('#step-1-form').validate({
			rules: {
				zip2: {required: true},
			},
			messages: {
				zip2: "Please enter zipcode.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			//ga('set', 'page', '/quote/zip/');
			//ga('send', 'pageview');
			ga('send', 'pageview', '/quote/zip/');
			//window.history.pushState('', '', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-1-form').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			$("#myModal .step-2").hide();
			$("#myModal .step-3").hide();
			$("#myModal .step-4").hide();
			$("#myModal .step-5").hide();
			$("#myModal .step-6").hide();
			
			jQuery("form#step-1 #zip").val(jQuery('#step-1-form #zip2').val());
			request.done(function( response ) {
				$('#myModal').modal('show');
			     //$("#inline" ).trigger( "click" );
				// $('#myModal').fancybox();
				 //jQuery.fancybox.open({content:$("#myModal").html()});
				 // $("#hidden_link").fancybox().trigger('click');
				 $(".complete").css("width", "20%");
				 $("#percents").css("left", "20%");
				 $("#percents").html("20%");
				 $("#myModal .step-1").hide();
				 $("#myModal .step-2").fadeIn( 200 );
				 $(".freetoken").val($(".customformtoken").val()); 
				  
			});	
			return false; 
		}
	});
   
   
	jQuery('#step-1').validate({
			rules: {
				zip: {required: true},
			},
			messages: {
				zip: "Please enter zipcode.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			ga('set', 'page', '/quote/bill/');
			//window.history.pushState('', '', '/quote/bill/');
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "20%");
				 $("#percents").css("left", "20%");
				 $("#percents").html("20%");
				 $(".step-1").hide();
				 $(".step-2").fadeIn( 200 );
				  
			});	
			return false; 
		}
	});
	
	
	
	jQuery('#step-2').validate({
			rules: {
				customRadioInline1: {required: true},
			},
			messages: {
				customRadioInline1: "Please Select Electic Bill Range.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			ga('set', 'page', '/quote/address/');
			//window.history.pushState('', '', '/quote/address/');
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-2').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "40%");
				 $("#percents").css("left", "40%");
				 $("#percents").html("40%");
				 $(".step-2").hide();
				 $(".step-3").fadeIn( 200 );
				  
			});	
			return false; 
		}
	});
	
	
	jQuery(".custom-radio").on('click',function(){
		$(".custom-control").css({'background':'#FFFFFF','color':'#000000'});
		var checkedid = $(this).attr('id');
		$('#customRadioInline'+checkedid).prop('checked', true);
		$("#"+checkedid).css({'background':'#009fdf','color':'#FFFFFF'});
	})
	
	 
	
	
	jQuery('#step-3').validate({
			rules: {
				full_address: {required: true},
				street_name: {required: true},
			},
			messages: {
				full_address: "Please enter address.",
				street_name: "Please select address from dropdown list.",
			},
		submitHandler: function(form) {
			//ga('send', 'pageview', '/quote/address/');
			ga('set', 'page', '/quote/shadow/');
			//window.history.pushState('', '', '/quote/shadow/')
			ga('send', 'pageview');
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#step-3').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "60%");
				 $("#percents").css("left", "60%");
				 $("#percents").html("60%");
				 $(".step-3").hide();
				 $(".step-4").fadeIn( 200 );
				  
			});	
			return false; 
		}
	});
	
	
	jQuery(".shade").on('click', function() {
		var divid = jQuery(this).attr('id');
		$(".shade-image").css('border-color', 'transparent'); //removes blue color when out of focus
		$(".shade-image").removeClass('joomdev');
		$(".step-4 H5 .fas").css('color','#414141');
		// $(".shade-image:after").css('background', 'none');
		
		$("#"+divid+" .shade-image").css('border-color','#00aeef'); //add blue color when in focus
		$("#"+divid+" .shade-image").css('background','rgba(0,0,0,0.2)');
		$("#"+divid+" .shade-image").css('background','rgba(0,0,0,0.2)');
		$("#"+divid+" H5 .fas").css('color','#00aeef');
		// $("#"+divid+" .shade-image").css('border-radius', '60px');
		
		$("."+divid).prop('checked', true);
	 });
	 
	// jQuery(".shade").on('click', function() {
	// 		var divid = jQuery(this).attr('id');
	// 		$("#"+divid+" .shade-image").css('border-radius', '60px');
	// 	}).on('focusout', function(){
	// 		var divid = jQuery(this).attr('id');
	// 		$("#"+divid+" .shade-image").css('border-radius', '10px');
	// });
	jQuery(".custom-radio1").on('click',function(){
		$(".custom-control-shade").css({'background':'#FFFFFF','color':'#000000'});
		var checkedid = $(this).attr('id');
		$('#customRadioInline'+checkedid).prop('checked', true);
		$("#"+checkedid).css({'background':'#009fdf','color':'#FFFFFF'});
	})
	
	
	
	jQuery('#step-4').validate({
			rules: {
				shade: {required: true},
			},
			messages: {
				shade: "Please Select Home shade.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			ga('set', 'page', '/quote/contact/');
			//window.history.pushState('', '', '/quote/contact/')
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-4').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".complete").css("width", "80%");
				 $("#percents").css("left", "80%");
				 $("#percents").html("80%");
				 $(".step-4").hide();
				 $(".step-5").fadeIn( 200 );
				  $target = $('#scrolltoelement');
				  $('.modal-dialog').animate({
					scrollTop: 0
				  }, 'show');
						  
			});	
			return false; 
		}
	});
	
	
	 jQuery('#step-5').validate({
			rules: {
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true,email: true},
				//phone: {required: true},
				phone: {required: true, phoneUS: true}
				
			},
			messages: {
				first_name: "Please enter first name.",
				last_name: "Please enter last name.",
				email: "Please enter valid email.",
				phone: "Please enter valid phone.",
			},
			 
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			$(".final_submit").hide();
			$(".loader_image").show();
			setTimeout(function(){ 
				 $(".loader_image").hide();
				 $(".step-5").hide();
				 $(".step-6").show();
				 $(".complete").css("width", "100%");
				 $("#percents").css("left", "100%");
				 $("#percents").html("100%");
			}, 1500);
			
			var data = jQuery('#step-5').serializeArray();
			ga('set', 'page', '/quote/thanks/');
			//window.history.pushState('', '', '/quote/thanks/')
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/thanks/');
			gtag('event', 'conversion', {'send_to': 'AW-922148957/0agnCJjG9JMBEN3A27cD'});
			fbq('track', 'Lead');
			rdt('track', 'Lead');
			//_tfa.push({notify: 'event', name: 'page_view',"item-url":'/quote/thanks/'});
			//obApi('track', 'Lead');
			snaptr('track','SIGN_UP');
			window.uetq = window.uetq || []; 
			window.uetq.push('event', 'Lead', {});  
			window.uetq.push({ 'ec': 'Lead', 'ea': 'Lead', 'el': 'Lead', 'ev': 3 });
			window.dotq = window.dotq || [];
			window.dotq.push(
			{
			  'projectId': '10000',
			  'properties': {
				'pixelId': '10081644',
				'qstrings': {
				  'et': 'custom',
				  'ec': 'Form',
				  'ea': 'Form Lead',
				  'el': 'Form Lead',
				  'ev': 'Lead',
				  'gv': '1'
				}
			} } );
			
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				
				 //window.history.pushState('', '', '/index.php');
				  
			});	
			return false; 
		}
	});
	 
	 
	
	 
	
})
function gotoStep(step){
	
	var current = step + 1;
	
	if(step == 5 )
		 $(".modal-content").css('margin-top','70% !important');
	
	$(".step-"+current).hide();
	$(".step-"+step).show();
	percent = step*20 - 20;
	$(".complete").css("width", percent+"%");
	$("#percents").css("left", percent+"%");
	$("#percents").html(percent+"%");
	
}
$(document).ready(function () {
  
  function setRating(rating) {
    $('#rating-input').val(rating);
    // fill all the stars assigning the '.selected' class
    $('.rating-star').removeClass('fa-star-o').addClass('selected');
    // empty all the stars to the right of the mouse
    $('.rating-star#rating-' + rating + ' ~ .rating-star').removeClass('selected').addClass('fa-star-o');
  }
  
  $('.rating-star')
  .on('mouseover', function(e) {
    var rating = $(e.target).data('rating');
    // fill all the stars
    $('.rating-star').removeClass('fa-star-o').addClass('fa-star');
    // empty all the stars to the right of the mouse
    $('.rating-star#rating-' + rating + ' ~ .rating-star').removeClass('fa-star').addClass('fa-star-o');
  })
  .on('mouseleave', function (e) {
    // empty all the stars except those with class .selected
    $('.rating-star').removeClass('fa-star').addClass('fa-star-o');
  })
  .on('click', function(e) {
    var rating = $(e.target).data('rating');
    setRating(rating);
  })
  .on('keyup', function(e){
    // if spacebar is pressed while selecting a star
    if (e.keyCode === 32) {
      // set rating (same as clicking on the star)
      var rating = $(e.target).data('rating');
      setRating(rating);
    }
  });
});

