<!DOCTYPE html>
<html lang="en">

<head>
	<meta name="robots" content="noindex" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Momentum Solar</title>
	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!--Font Awsome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	 crossorigin="anonymous">
	<!-- Custom styles for this template -->
	<link href="css/custom.css" rel="stylesheet">
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600" rel="stylesheet">
</head>

<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg bg-white navbar-light fixed-top p-0" id="mainNav">
		<div class="logo-container">
		<a class="navbar-brand js-scroll-trigger pl-md-5">
				<img src="images/logo.png" alt="logo" class="text-center logo-lg-screen">
				<img src="images/logo-top-small.png" alt="logo-small" class="text-center logo-sm-screen">
				<button class="btn btn-primary rounded-0 btn-sm d-block text-center my-3" type="submit"  data-toggle="modal" data-target="#myModal">Free Quote</button>
		</a>
		</div>
		<div class="container-fluid p-0 d-flex justify-content-center">
			<button class="navbar-toggler mr-3 mt-3 hamburger-icon" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
			 aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger how-it-woks-nav" href="#howitwork">How It Works</a>
					</li>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger why-choose-us-nav" href="#whychooseus">Why Choose Us</a>
					</li>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger faq-nav" href="#faq">FAQ</a>
					</li>
					<li class="nav-item free-quote">
						<a class="nav-link js-scroll-trigger text-white px-5 free-quote-text" style="cursor:pointer" data-toggle="modal" data-target="#myModal">Free Quote</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
<?php 
	  function generateRandomString($length = 10) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
	}
	  
	  $token = generateRandomString();
	  
	  ?>
	<section class="banner">
		<div class="container text-center">
			<h6 class="title-top">Take advantage of the new <span class="font-weight-bold">New Jersey tax credit</span> for
				solar panels.</h6>
			<h3 class="title-mid my-5">Momentum Solar customers receive an average of $9,000 towards their home solar system.</h3>
			<p class="title-bot">See if your home qualifies.</p>
			<form id="step-1-form" class="needs-validation mx-auto d-flex" novalidate>
				<input type="text" name="zip" class="form-control rounded-0" id="validationCustom015" placeholder="Enter Zip Code" required>
				<input type="hidden" name="step" class="step" value="step1">
            <input type="hidden" class="customformtoken" name="token" value="<?php echo $token;?>">
				<button class="btn btn-primary rounded-0 button-go-text btn-sm" style="margin-top: 1px; margin-bottom: 1px;" type="submit">Go</button>
			</form>
		</div>
	</section>

	<section id="section-2" class="py-5 my-3">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-md-4">
					<h2 class="text-uppercase center-on-small">Save up to <span class="font-weight-bold" style="color:#00aeef">30% OFF</span> your electric bill <span style="color:#ff9504; letter-spacing:9px;">immediately.</span></h2>
				</div>
				<div class="col-12 col-md-8">
					<ul class="pb-3">
						<li>$0 to enroll</li>
						<li>No hidden fees</li>
						<li>Upfront pricing with no obligation</li>
					</ul>
					<p class="font-italic pr-md-5">Momentum Solar has installed more solar panel systems in New Jersey than any other solar company.</p>
				</div>
			</div>
		</div>
	</section>

	<section id="section-3" class="bg-light my-3 py-4 featured-in-section">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="d-lg-flex align-items-center justify-content-between text-center">
						<li class="m-0">
							<h5 class="mb-4 border-dark bt-sm bb-sm" style="letter-spacing:1px;">Featured In</h5>
						</li>
						<li>
							<img src="images/client-1.png" alt="forbes" class="my-2">
						</li>
						<li>
							<img src="images/client-2.png" alt="Inc" class="my-2">
						</li>
						<li>
							<img src="images/client-3.png" alt="Angie's List" class="my-2">
						</li>
						<li>
							<img src="images/client-4.png" alt="accredited business" class="my-2">
						</li>
						<li>
							<img src="images/client-5.png" alt="Deloitte" class="my-2">
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- How it works section -->
	<div id="howitwork">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center my-5">
					<h3 class="how-it-works-text">HOW IT WORKS</h3>
				</div>
			</div>
				<div class="row">
					<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">
						<div class="step1-img text-center">
							<img src="images/step-icon-1.jpg" alt="Step1-Icon" class="py-4">
						</div>
						<div class="step1-body">
							<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 1</h5>
							<P><span class="font-weight-bold text-dark">Pre-screening</span> - Fill out the short survey to see if you
								quality for a NJ tax rebate towards your solar panel system.</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">
						<div class="step1-img text-center">
							<img src="images/step-icon-2.jpg" alt="Step1-Icon" class="py-4">
						</div>
						<div class="step1-body">
							<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 2</h5>
							<P><span class="font-weight-bold text-dark">Initial Consultation</span> - Our professional installers will reach
								out to you by phone and answer any questions that you may have.</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">
						<div class="step1-img text-center">
							<img src="images/step-icon-3.jpg" alt="Step1-Icon" class="py-4">
						</div>
						<div class="step1-body">
							<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 3</h5>
							<P><span class="font-weight-bold text-dark">On-site Assessment</span> - If you qualify, our installers will
								visit your home to conduct an assessment and provide a <span class="font-weight-bold text-dark">no-obligation</span>
								quote.</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-12 center-on-small">
						<div class="step1-img text-center">
							<img src="images/step-icon-4.jpg" alt="Step1-Icon" class="py-4">
						</div>
						<div class="step1-body">
							<h5 class="step-text py-3 font-weight-bold mb-0 text-center">STEP 4</h5>
							<P><span class="font-weight-bold text-dark">Pre-screening</span>, our team will handle all of the designing,
								engineering, and installation of your brand new Momentum solar panel system.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 center-on-small">
						<button type="button" class="btn btn-primary rounded-0 button-get-started py-2 mb-5" data-toggle="modal" data-target="#myModal">Get Started</button>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<section id="whychooseus" class="why-momentum-solar-container">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-6 why-momentum-solar p-5">
					<h2 class="pb-4">WHY MOMENTUM SOLAR?</h2>
					<p><span class="font-weight-bold text-dark">Flexible Payment Options</span> - We offer customized payment plans
						that can include $0 upfront cost so you can save <span class="font-weight-bold text-dark">20-30% OFF your
							electric bill</span> immediately.</p>
					<p><span class="font-weight-bold text-dark">Long Term Price Protection</span> - We provide upfront rates for your
						electricity for the next 25 years and guarantee that our prices will be lower than your electric company.</p>
					<p><span class="font-weight-bold text-dark">Worry-Free Maintenance & Monitoring </span>- We provide complimentary system monitoring and maintenance to ensure your
						system is always functioning optimally.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- FAQ Section -->
	<section id="faq">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="frequentlyaq-text pt-5 pb-4">FREQUENTLY ASKED QUESTIONS</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
				<div class="accordion w-100" id="accordionExample">
					<div class="card">
						<div class="card-header" id="headingOne">
							<h2 class="mb-0">
								<button class="btn btn-link w-100" type="button" data-toggle="collapse" data-target="#collapseOne"
								 aria-expanded="true" aria-controls="collapseOne">
									How much will my utility bill be?
								</button>
							</h2>
						</div>
						<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
							<div class="card-body">
								<p>Your solar provider cannot guarantee what your bill from the utility company will be after going solar. The
									bill will depend on the amount of energy you use from the grid after your solar production is subtracted,
									which was presented to you as an estimate based on your usage from the previous year. It does not factor in
									any adjustments in future energy usage, like installing a pool or a hot tub on your property.</p>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingTwo">
							<h2 class="mb-0">
								<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseTwo"
								 aria-expanded="false" aria-controls="collapseTwo">
									Will I receive a bill from both the utility company and my solar provider?
								</button>
							</h2>
						</div>
						<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
							<div class="card-body">
								<p>Yes. You will continue receiving a bill from your utility company for basic service and any power needs not
									supplied by your solar system. You will receive a separate bill for your solar service agreement.</p>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingThree">
							<h2 class="mb-0">
								<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseThree"
								 aria-expanded="false" aria-controls="collapseThree">
									What happens to my system generation at night or when it's cloudy?
								</button>
							</h2>
						</div>
						<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
							<div class="card-body">
								<p>When the sun goes down, any extra energy you produce during the day is pushed back into the grid, offsetting
									your energy use at night or when it’s cloudy. During the sunny summer months your system will produce the most
									energy, so the credits you’ve earned during those days of overproduction will be helpful during winter months
									when the days are shorter.</p>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingFour">
							<h2 class="mb-0">
								<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseFour"
								 aria-expanded="false" aria-controls="collapseFour">
									How will I know if my system is working properly?
								</button>
							</h2>
						</div>
						<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
							<div class="card-body">
								<p>Your solar agreement includes complimentary maintenance and monitoring so that we can identify issues and
									fix them accordingly. We also offer real-time monitoring software to all of our customers that will allow you
									to track your energy usage from your smartphone or computer, so you’ll know if you are not producing as much
									as you should be.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="headingFive">
							<h2 class="mb-0">
								<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseFive"
								 aria-expanded="false" aria-controls="collapseFive">
									What does the warranty cover?
								</button>
							</h2>
						</div>
						<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
							<div class="card-body">
								<p>Your warranty covers all system maintenance, and repairs or replacements, for the lifetime of your solar
									agreement. We call it “worry-free” maintenance because it’s taken care of at no cost to you.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="headingSix">
							<h2 class="mb-0">
								<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseSix"
								 aria-expanded="false" aria-controls="collapseSix">
									What happens if the solar service agreement outlives me?
								</button>
							</h2>
						</div>
						<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
							<div class="card-body">
								<p>In the event of your passing, your heirs will be able to experience solar savings by simply continuing to
									make your monthly payments. If they sell the home, they can transfer the solar agreement to the home buyer.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="headingSeven">
							<h2 class="mb-0">
								<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseSeven"
								 aria-expanded="false" aria-controls="collapseSeven">
									What if I sell my home prior to the end of the solar service agreement?
								</button>
							</h2>
						</div>
						<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
							<div class="card-body">
								<p>If you sell your home, you can transfer your solar agreement to the new homeowner. It’s easy and offers
									electricity savings, which is a great selling point when your home is on the market. The Momentum Solar
									Customer Experience team will walk you through the entire process, but it’s important that you contact us by
									email at <a href="mailto:myinstall@momentumsolar.com">MyInstall@momentumsolar.com</a> or by phone at <a href="tel:1-732-902-6224">732.902.6224</a>
									as soon as you decide to sell your home. We have a 100% success rate with our system transfers.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="headingEight">
							<h2 class="mb-0">
								<button class="btn btn-link collapsed w-100" type="button" data-toggle="collapse" data-target="#collapseEight"
								 aria-expanded="false" aria-controls="collapseEight">
									Why are agreements 20-25 years in duration?
								</button>
							</h2>
						</div>
						<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
							<div class="card-body">
								<p>The reason for a 20 or 25-year term is to keep your rate per kWh as low as possible. Your solar savings are
									either locked in or predictable so that you can enjoy peace of mind for years to come.</p>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
</div>
		</div>
	</section>
	<!-- Testimonial Section -->
	<section class="testimonial py-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="row">
						<p class="quote-heading"><span>WHAT YOUR CUSTOMERS SAY</span></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
							<li data-target="#myCarousel" data-slide-to="4"></li>
							<li data-target="#myCarousel" data-slide-to="5"></li>
							<li data-target="#myCarousel" data-slide-to="6"></li>
							<li data-target="#myCarousel" data-slide-to="7"></li>
						</ol>
						<!-- Wrapper for carousel items -->
						<div class="carousel-inner">
							<div class="item carousel-item active">
								<p class="testimonial">We’re growing more trees because of us going solar. I’m really happy because we’re saving more money and helping the environment by helping more trees grow.</p>
								<p class="person-name">- The Veiga Family from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Coming from a background in construction, I wanted every wire to be perfect, and everything squared, conduit straight. Momentum nailed every facet of that.</p>
								<p class="person-name">- Zach from Texas</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">We wanted to save money and do something that’s good for the environment. I went with Momentum because of the products and services they offer, the pricing and ultimately our salesman.</p>
								<p class="person-name">- Michael from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Thanks to Momentum and the savings I’m getting, I can spend more money on things I’d put off for later. I don’t know exactly what but I know it’ll be good things for my house.</p>
								<p class="person-name">- Austria from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">We just get our first bill and it was only $30. That’s your money or your children’s money to spend as you wish. We really can’t tell you how happy we are.</p>
								<p class="person-name">- The Wdowiaks from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Customer experience I had at Momentum Solar was exceptional. Everyone I’ve talked to has been very helpful and if I had a question, they would seek to answer my question and I was very happy with them!</p>
								<p class="person-name">- John from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">The reputation of the company had a lot to do with it, the 25 yr warranty with service included was really important to me because I didn’t want to have to start paying for labor down the road.</p>
								<p class="person-name">- Bob from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">I’ll be saving about $70 per month. Well, $70 a month gives me the opportunity to really spoil my grandchildren!</p>
								<p class="person-name">- Tom from New Jersey</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Contact Section -->
	<section class="call-us">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center contact-number py-6">
					<p class="phone-number m-0"><img src="images/phone.png"> CALL US AT <a class="text-white" href="tel:1-888-666-36886">(888) MOMENTUM</a></p>
					<p class="phone-number my-3">OR</p>
					<button type="button" class="btn btn-primary btn-lg gradient-btn rounded-pill contact-button" data-toggle="modal" data-target="#myModal">GET FREE QUOTE ONLINE</button>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer -->
	<footer class="footer-section">
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-lg-3 align-self-center text-center col-md-12 py-4">
					<img src="images/logo-footer.jpg" alt="Footer Logo">
				</div>
				<div class="col-lg-6 col-md-12 align-self-center">
					<p class="m-0">© 2019 Momentum Solar. All Rights Reserved by Pro Custom Solar LLC D|B|A Momentum Solar </p>
				</div>
				<div class="col-lg-3 text-md-right align-self-center text-center col-md-12 py-4">
					<div class="social-icons">
						<a href="https://twitter.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-twitter twitter-icon"></i>
						</a>
						<a href="https://www.facebook.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-facebook-f facebook-icon"></i>
						</a>
						<a href="https://www.instagram.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-instagram insta-icon"></i>
						</a>
						<a href="https://www.linkedin.com/company/momentum-solar/" class="social-icon" target="_blank">
							<i class="fab fa-linkedin-in linked-icon"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /.container -->
	</footer>
	
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
	  <button type="button" class="close" data-dismiss="modal">&times;</button>
        <iframe id="frame" frameBorder="0" src="popup.php" width="100%" height="500">
		</iframe>
      </div>
    </div>
  </div>
</div>
<style>

.modal-dialog, .modal-xl {
    max-width: 1000px;
}

#frame .container {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    padding-top: 59px !important;
}
 
</style>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom JavaScript for this theme -->
	<script src="js/scrolling-nav.js"></script>
	<script src="js/validation.js"></script>
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '252884438399772');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=696369663874492&ev=PageView&noscript=1"
	/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75829764-1"></script>
	<script>  window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(	arguments);}  
		gtag('js', new Date());
		//gtag('config', 'UA-75829764-1');
		gtag('config', 'AW-922148957');
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-75829764-1', 'momentumsolar.com');
	  ga('set', 'page', '/quote/');
	  ga('send', 'pageview');
</script>
</body>

</html>