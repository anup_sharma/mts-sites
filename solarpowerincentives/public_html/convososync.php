<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
define('DB_HOST', 'localhost');         
define('DB_USER', 'solarpowerincent');    
define('DB_PASS', 'nJBxoUoRoV7R');    
define('DB_NAME', 'solarpowerincentives_forms');   

$dns = 'mysql:host=localhost;dbname=solarpowerincentives_forms';

try {

    $pdo = new PDO($dns, 'solarpowerincent', 'nJBxoUoRoV7R');
    if ($pdo) {
        $sql = "Select * from funnel_form where `phone` != '' and ip !='111.125.141.94' and sendtovelocify = 0 limit 0,2";
        $query = $pdo->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_OBJ);
        if (count($results) > 0) {
            foreach ($results as $row) {
                $url = 'https://api.convoso.com/v1/leads/insert?auth_token=6mtuqtxafj3qr5d0p94lexwulvosjcpf&list_id=509&check_dup=2&check_dup_archive=0&check_dnc=1&check_wireless=0%20&hopper=1&hopper_priority=99&hopper_expires_in=0&phone_code=1&check_wireless=0';
                $postdata = (array)$row;
                $response = sendconvoso($url, $postdata);

                $save =  array();
                $save['post'] = $postdata;
                $save['response'] = $response;
                $save['url'] = $url;
                $savedata = json_encode($save);
                $datapostfile = getcwd().'/postdata.php';
                $current = file_get_contents($datapostfile);
                $current .= date("Y-m-d H:i:s").'-- '.$savedata."\n\n";
                file_put_contents($datapostfile, $current);
                echo strval($response);
                $book = json_decode($response, true);
                $strvalid = strval($book['data']['lead_id']);
                $updateQuery = "Update funnel_form set sendtovelocify =1,velocify_id ='".$strvalid."' where id=".$row->id."";
                $updateQuery = $pdo->prepare($updateQuery);
                $updateQuery->execute();

            }
        }
    }

} catch (Exception $e) {
    echo $e->getMessage();
}


function sendconvoso($url, $requestdata)
{
    $data['lead_type'] = 'Solar';
    if (isset($requestdata['email']) && !empty($requestdata['email'])) {
        $data['email'] = $requestdata['email'];
    }

    if (isset($requestdata['fname']) && !empty($requestdata['fname'])) {
        $data['first_name'] = $requestdata['fname'];
    }

    if (isset($requestdata['lname']) && !empty($requestdata['lname'])) {
        $data['last_name'] = $requestdata['lname'];
    }


    if (isset($requestdata['phone']) && !empty($requestdata['phone'])) {
        $data['phone_number'] = $requestdata['phone'];
    }

    if (isset($requestdata['full_address']) && !empty($requestdata['full_address'])) {
        $data['address1'] = $requestdata['street_number'] . ' ' . $requestdata['street_name'];
    }

    if (isset($requestdata['zip']) && !empty($requestdata['zip'])) {
        $data['postal_code'] = $requestdata['zip'];
    }

    if (isset($requestdata['city']) && !empty($requestdata['city'])) {
        $data['city'] = $requestdata['city'];
    }
    if (isset($requestdata['state_abbr']) && !empty($requestdata['state_abbr'])) {
        $data['state'] = $requestdata['state_abbr'];
    }

    if (isset($requestdata['utility_cost']) && !empty($requestdata['utility_cost'])) {
        $data['monthly_electric_bill'] = $requestdata['utility_cost'];
    }

    if (isset($requestdata['roof_shading']) && !empty($requestdata['roof_shading'])) {
        $data['roof_shade'] = $requestdata['roof_shading'];
    }

    if (isset($requestdata['utm_source']) && !empty($requestdata['utm_source'])) {
        $data['source'] = $requestdata['utm_source'];
        $data['lead_source'] = "Website - Momentum Solar";
    }
    if (isset($requestdata['utm_medium']) && !empty($requestdata['utm_medium'])) {
        $data['channel'] = $requestdata['utm_medium'];
    }

    if (isset($requestdata['utm_campaign']) && !empty($requestdata['utm_campaign'])) {
        $data['tracking_campaign'] = $requestdata['utm_campaign'];
    }

    if (isset($requestdata['utm_content']) && !empty($requestdata['utm_content'])) {
        $data['sub_group'] = $requestdata['utm_content'];
    }

    if (isset($requestdata['trusted_form']) && !empty($requestdata['trusted_form'])) {
        $data['trusted_form_url'] = $requestdata['trusted_form'];
    }

    if (isset($requestdata['url']) && !empty($requestdata['url'])) {
        $data['url'] = $requestdata['url'];
         $data['opt_in_url'] = $requestdata['url'];
        //Code Added for mapping of paid ad click id's
        $queryString = parse_url($requestdata['url'], PHP_URL_QUERY);
        parse_str($queryString, $output);

        if (isset($output['fbclid']) && !empty($output['fbclid'])) {
            $data['custom_id'] = $output['fbclid'];
        }

        if (isset($output['gclid']) && !empty($output['gclid'])) {
            $data['click_id'] = $output['gclid'];
        }

        if (isset($output['msclkid']) && !empty($output['msclkid'])) {
            $data['ref_id'] = $output['msclkid'];
        }
    }

    if (isset($requestdata['home_owner']) && !empty($requestdata['home_owner'])) {
        $data['comments'] = ', Is home owner :' . $requestdata['home_owner'];
    } else {
        $data['comments'] = ', Is home owner : No';
    }

    $url = $url;
    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($data),
            'timeout' => 30,
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;

}