<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WSKTGNB');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176059540-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176059540-4');
</script>
	<meta charset="UTF-8">
<meta name="facebook-domain-verification" content="5er1eeciqyss2f12w10lyobhb51d0p" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Solar Programs For US Homeowners</title>
	<meta name="robots" content="noindex,nofollow">
	<meta name="description" content=""/>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--styles-->
	<link rel="stylesheet" href="assets/css/style-top.css">
	

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MQZ6MGQ');</script>
<!-- End Google Tag Manager -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1409545});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'), document.getElementsByTagName('script')[0], '//cdn.taboola.com/libtrc/unip/1409545/tfa.js', 'tb_tfa_script');
</script>
<!-- End of Taboola Pixel Code -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WSKTGNB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQZ6MGQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '336901150769341');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=336901150769341&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
	<?php 
      function generateRandomString($length = 25) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
    }
      
      $token = generateRandomString();
    if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $protocol = 'https://';
        }
        else {
        $protocol = 'http://';
        }
        $current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         
      ?>
<header class="fixed-top">
	<div class="container">
		<div class="logo">
			<a href="/"><img class="logo" src="images/logo.jpg" alt="logo"></a>
		</div>
		<div class="startquote">
		</div>
	</div>
</header>
	<section class="topbanner">
		<div class="container">
			<div class="row headerrow">
				<div class="col-12 col-md-7 text-center">
					<h1>Take Advantage of Federal $0 Down Solar Programs</h1>
					<div class="top-image">
						<img src="images/banner-solar.jpg" alt="">
					</div>
					<p>It only takes 30 seconds to see if you qualify.</p>
				</div>
				<div class="col-12 col-md-5">
					<form id="step-1" class="needs-validation mx-auto" onsubmit = "return fireTaboola();" novalidate>
						<div class="topform">
						<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
						<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
						<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
						<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
						<input type="hidden" name="url" value="<?php echo $current_link?>">
						<input type="hidden" name="step" class="step" value="step1">
						<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
							<div class="row m-md-auto">
								<div class="col-12 col-md-12 col-lg-6">
									<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">
								</div>
								<div class="col-12 col-md-12 col-lg-12">
									<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<input class="form-control mt-3 rounded-0" placeholder="Zip Code" autocomplete="off" name="zip" type="text">
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04" placeholder="Phone">
								</div>
								<div class="col-12 col-md-12 col-lg-12">
										<select class="form-control mt-3 rounded-0" name="customRadioInline1">
											<option value="">What is your monthly electric bill?</option>
											<option value="Under $75">Under $75</option>
											<option value="$76 - $150">$76 - $150</option>
											<option value="$151 - $225">$151 - $225</option>
											<option value="$226 - $300">$226 - $300</option>
											<option value="Over $300">Over $300</option>
										</select>
								</div>
								<div class="col-12 col-md-12 col-lg-12">
									<p style="font-size: 1rem;"><strong>Do you own your home?</strong></p>
									<div class="row" style="padding-left: 15px; padding-right: 15px;">
										<div class="custom-control custom-radio col-6 col-md-6 col-lg-3">
										  <input type="radio" class="custom-control-input" name="homeowner" value="yes" checked>
										  <label class="custom-control-label" for="homeowner">Yes</label>
										</div>
										<div class="custom-control custom-radio col-6 col-md-6 col-lg-3">
										  <input type="radio" class="custom-control-input" name="homeowner" value="no">
										  <label class="custom-control-label" for="homeowner">No</label>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-12">
									<input type="text" name="full_address" class="form-control  mt-3 rounded-0 cw-75" id="full_address" placeholder="Address">
									<input type="hidden" id="street_number" name="street_number" value="" >
									<input type="hidden" id="route" name="street_name" value="">
									<input type="hidden" id="locality" name="city" value="">
									<input type="hidden" id="postal_code" name="postal_code">
									<input type="hidden" id="administrative_area_level_1">
									<input type="hidden" id="country" name="country" value="">
								</div>
							</div>
						<div class="center-block mt-3"  style="padding-left: 15px; padding-right: 15px;">
							<button class="btn btn-primary rounded-0 btn-block with-arrow" type="submit" style="margin: 0 auto; background: #5c63ad; border-color: #5c63ad">Submit</button>
						</div>
						<p class="small">*By hitting "Submit" you authorize solarpowerincentives.com and up to <a href="https://www.landpage.co/7fd0dcf4-4ed4-11eb-be96-768101ea307e" target="blank"><strong>4 solar companies</strong></a> to call and send you text messages on the phone number you submitted above using an automated dialer regarding solar products. This will override any federal or state Do Not Call lists. Consent is not a requirement of purchase. Data and text message rates will apply. <a href="/privacy-policy.php">Privacy Policy</a> | <a href="/ccpa.php">Do Not Sell</a></p>
						</div>
					</form>
				</div>
				
			</div>
		</div> 
	</section>
	<section class="main">
		<div class="container">
			<h2>Solar and You</h2>
			<div class="row justify-content-between">
				<div class="col-12 col-md-3 ficon">
					<h3>Reduce Your Monthly Bills</h3>
					<p>You can start saving immediately. Solar panels are more affordable now than they have ever been, making today the best time to make the switch. If you are looking for an environmentally friendly way to save significantly on your electricity bill solar is right for you.</p>
				</div>
				<div class="col-12 col-md-3 sicon">
					<h3>Rebates & Incentives</h3>
					<p>There are plenty of federal and state incentives for homeowners that switch to solar. Some areas quality for thousands of dollars in rebates and tax credits! Find out which incentives are eligible for if you install your solar panel system in 2021.</p>
				</div>
				<div class="col-12 col-md-3 ticon">
					<h3>Why Go Solar?</h3>
					<p>Renewable energy is the way of the future - owners of solar energy systems have financial freedom from unpredictable prices of traditional electricity while reducing their CO2 emissions, helping the environment and increase the value of their home. Instead of paying electric companies, you can lock in a low energy rate and save thousands every year.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="topbanner ways">
		<div class="container">
			<div class="row headerrow">
				<div class="col-12 col-md-7">
					<img src="images/banner-solar2.jpg" alt="">
				</div>
				<div class="col-12 col-md-5">
					<h3>How $0 Upfront Solar Works</h3>
					<p>Solar Leases or Power Purchase Agreements (PPA) allows homeowners to go solar at $0 upfront.</p>
					<div class="bannerform">
						<a class="nav-link start3" data-toggle="modal" data-target="#myModal">Get Quote</a>
					</div>
				</div>
			</div>
		</div> 
	</section>
	<section class="topbanner yoursolar">
		<div class="container">
			<div class="row headerrow">
				<div class="col-12 col-md-5">
					<h3>Solar Incentives & Rebates</h3>
					<p>There are tons of solar incentives and rebates that exists for homeowners. Most utility companies do not want you to know about them because it can cost them thousands. Let us do the research and find out which programs your zip code may qualify for.</p>
					<div class="bannerform">
						<a class="nav-link start4" data-toggle="modal" data-target="#myModal">Check Zip Code</a>
					</div>
				</div>
				<div class="col-12 col-md-7">
					<img src="images/banner-solar3.jpg" alt="">
				</div>
			</div>
		</div> 
	</section>

      
<footer class="footer"><span class="copy">© 2020 Solar  Power Incentives | <a href="/privacy-policy.php">Privacy Policy</a></span></footer>


<div id="myModal" class="modal fade custom-modal" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="scrolltoelement"></div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="container">
						<div class="progressbar">
							<!-- <div id="percents">0%</div> -->
							<div class="progress">
								<div class="complete"></div>
							</div>
						</div>
						<div class="row">
							<div class="col d-flex text-center align-items-center">
								<!-- Step 1 -->
								<div class="col form py-1 mx-auto step-1">
									<div>
										<h3 class="font-weight-light mb-5">Government Offers Rebates To Entice Residents To Go Solar</h3>
										<p class="mb-5 mt-4 custom-width">Enter Your Zip Code to See if You Qualify</p>
										<form id="step-1" class="needs-validation mx-auto"   novalidate>
											<input id="zip" class="form-control rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip" type="text">
											<div class="invalid-feedback mb-4">
												Please provide a valid zip.
											</div>
											<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
											<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
											<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
											<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
											<input type="hidden" name="url" value="<?php echo $current_link?>">
											<input type="hidden" name="step" class="step" value="step1">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
											<div class="center-block mt-5">
												<button class="btn btn-primary rounded-0 btn-block with-arrow" type="submit" style="margin: 0 auto;">Continue</button>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 2 -->
								<div class="col form pt-0 pt-lg-5 px-0 pb-0 mx-auto step-2" style="display:none;">
									<div>
									<h3 class="font-weight-light mb-5">What is your average monthly electric bill?</h3>
									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">This will help us determine how much you can save.</p>
										<form id="step-2" class="needs-validation mx-auto bill-box" novalidate>
											<div id="1" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="Under $75" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline1">Under $75</label>
											</div>
											<div id="2" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="$76 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline2">$76 - $150</label>
											</div>
											<div id="3" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="$151 - $225" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline3">$151 - $225</label>
											</div>
											<div id="4" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="$226 - $300" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline4">$226 - $300</label>
											</div>
											<div id="5" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="Over $300" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline5">Over $300</label>
											</div>
											<input type="hidden" name="step" class="step" value="step2">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
											<div class="row">
												<div class="col-6 text-left">
												
												</div>
												<div class="col-6 text-right">
													<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 3 -->
								<div class="col form pt-0 pt-lg-5 mx-auto step-3" style="display:none;">
									<div>
									<h3 class="font-weight-light mb-5">Please enter your home address.</h3>
									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">This will help us maximize solar incentives in your area.</p>
										<form id="step-3" class="needs-validation mx-auto" novalidate>
											<input type="text" name="full_address" class="form-control rounded-0 cw-75" id="full_address" placeholder="Address">
											<input type="text" id="street_number" name="street_number" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="route" name="street_name" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="locality" name="city" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="postal_code" name="postal_code" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="administrative_area_level_1" name="state_abbr" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="country" name="country" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<div class="invalid-feedback">
												Please provide a valid Address.
											</div>
											<input type="hidden" name="step" class="step" value="step3">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
											<div class="row mt-5">
												<div class="text-left col-6 ">
												</div>
												<div class="col-6 text-right">
													<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 4 -->
								<div class="col form pt-0 pt-md-3 mx-auto step-4" style="display: none;">
									<div>
										<h3 class="font-weight-light mb-5">How much shade does your home get from 9 AM - 3 PM?</h3>
										<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">This will help us determine how much solar energy your system can produce.</p>
										<form id="step-4">
											<div class="step4desk">
												<div class="row mb-5">
													<div class="col-12 col-md-4 shade " id="no-shade">
														<div class="shade-image">
															<img src="images/roof-1.png">
														</div>
														<h5 class="mt-3">No Shade</h5>
														<p>Entire roof is exposed to sunlight.</p>
														<input type="radio" class="no-shade" name="shade" value="No Shade" style="display:none">
													</div>
													<div class="col-12 col-md-4 shade" id="some-shade">
														<div class="shade-image">
															<img src="images/roof-2.png">
														</div>
														<h5 class="mt-3">Some Shade</h5>
														<p>Parts of the roof are covered at certain times/all times.</p>
														<input type="radio" class="some-shade" name="shade" value="Some Shade" style="display:none">
													</div>
													<div class="col-12 col-md-4 shade " id="severe-shade">
														<div class="shade-image">
															<img src="images/roof-3.png">
														</div>
														<h5 class="mt-3">Severe Shade</h5>
														<p>Most or all of the roof is covered at certain times/all times.</p>
														<input type="radio" class="severe-shade" name="shade" value="Severe Shade" style="display:none">
														<input type="hidden" name="step" class="step" value="step4">
														<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="text-left col-6">
												</div>
												<div class="col-6 text-right">
													<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 5 -->
								<div class="col form pt-0 pt-sm-8 mx-auto step-5" style="display:none;">
									<div>
									<h3 class="font-weight-light mb-0 mb-md-5">Please enter your contact info to see if you
qualify for solar!</h3>
									<form id="step-5" class="needs-validation row" novalidate>
										<div class="col-12">
											<div class="row m-md-auto">
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">
													<div class="invalid-feedback">
														Please provide first name.
													</div>
												</div>
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">
													<div class="invalid-feedback">
														Please provide last name.
													</div>
												</div>
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">
													<div class="invalid-feedback">
														Please provide a valid email.
													</div>
												</div>
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04" placeholder="Phone">
													<div class="invalid-feedback">
														Please provide a valid phone.
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-12 text-center my-3">
													<button class="btn btn-primary rounded-0 btn-sm final_submit" type="submit">Submit</button>
													<img src="images/loading_bubble.svg" style="display:none" class="loader_image">
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<p class="my-3 small ml-auto mr-auto">*By hitting 'Submit', you authorize Solarpowerincentives.com and up to <a href="#" class="open-modal2">4 solar companies</a> to call and send you text messages on the phone number you submitted above using an automated dialer regarding solar products. This will override any federal or state Do Not Call lists. Consent is not a requirement of purchase. Data and text message rates will apply. <a href="/privacy-policy.php">Privacy Policy</a> | <a href="/ccpa.php">Do Not Sell</a></p>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<input type="hidden" name="step" class="step" value="step5">
													<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
													<div class="row">
														<div class="text-left">
														
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
									<div class="bot-sec mt-5" style="display:none;">
										<div class="d-flex col- mx-o justify-content-between">
											<button class="btn btn-primary rounded-0" type="submit">Next</button>
										</div>
									</div>
									</div>
								</div>
								<div class="col form py-5 mx-auto step-6" style="display:none;">
									<div>
										<div class="fivdth">
											<div class="col">
												<h3 class="font-weight-light mb-3">Thank You For Your Request For A Free Solar Quote.</h3>
												<h4 class="mb-3">We’ll be calling you shortly to share a personalized quote.</h4>
												
											</div>
										</div>
									</div>
								</div>
								<div class="close" style="display:none;">
									<i class="fas fa-times"></i>
								</div>
							</div>
						</div>
						<div class="smallnotes"><i class="fas fa-lock"></i> Your information is secure</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<?php
				$solar_partners = file_get_contents('https://momentumsolar.com/assets/solar-partners.html');
				echo $solar_partners;
			?>

			</div>
		</div>
	</div>
</div>
	<!--fontawesome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>

	<script>
		function fireTaboola(){
			
				_tfa.push({notify: 'event', name: 'lead', id: 1409545});
												
		}
		WebFont.load( {
			google: {
				families: [ 'Playfair+Display:400,700,900', 'Open+Sans:300,400,600,700,800' ]
			}
		} );
	</script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style-bottom.css">
	<style>
		.pac-container {
			z-index: 9999999;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<script src="js/scrolling-nav.js"></script>
	<script src="js/app.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/lazyload.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script>
		$( '.phone_us' ).mask( '(000) 000-0000' );
		var placeSearch, autocomplete;
		var componentForm = {
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'short_name',
			country: 'long_name',
			postal_code: 'short_name'
		};
		function initAutocomplete() {
			// Create the autocomplete object, restricting the search to geographical
			// location types.
			autocomplete = new google.maps.places.Autocomplete(
				( document.getElementById( 'full_address' ) ), {
					types: [ 'geocode' ]
				} );
			// When the user selects an address from the dropdown, populate the address
			// fields in the form.
			autocomplete.addListener( 'place_changed', fillInAddress );
		}
		function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();
			for ( var component in componentForm ) {
				document.getElementById( component ).value = '';
				document.getElementById( component ).disabled = false;
			}
			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			for ( var i = 0; i < place.address_components.length; i++ ) {
				var addressType = place.address_components[ i ].types[ 0 ];
				if ( componentForm[ addressType ] ) {
					var val = place.address_components[ i ][ componentForm[ addressType ] ];
					document.getElementById( addressType ).value = val;
				}
			}
		}
		// Bias the autocomplete object to the user's geographical location,
		// as supplied by the browser's 'navigator.geolocation' object.
		function geolocate() {
			if ( navigator.geolocation ) {
				navigator.geolocation.getCurrentPosition( function ( position ) {
					var geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					var circle = new google.maps.Circle( {
						center: geolocation,
						radius: position.coords.accuracy
					} );
					autocomplete.setBounds( circle.getBounds() );
				} );
			}
		}
	</script>
	<!--Fading out "Get Free Quote" button in mobile-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete" async defer></script>
	<!-- Facebook Pixel Code -->

<script type="text/javascript">
  (function() {
      var field = 'xxTrustedFormCertUrl';
      var provideReferrer = false;
      var invertFieldSensitivity = false;
      var tf = document.createElement('script');
      tf.type = 'text/javascript'; tf.async = true;
      tf.src = 'http' + ('https:' == document.location.protocol ? 's' : '') +
        '://api.trustedform.com/trustedform.js?provide_referrer=' + escape(provideReferrer) + '&field=' + escape(field) + '&l='+new Date().getTime()+Math.random() + '&invert_field_sensitivity=' + invertFieldSensitivity;
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tf, s); }
  )();
</script>
<noscript>
    <img src="http://api.trustedform.com/ns.gif" />
</noscript>	
<script>
		jQuery( document ).ready( function ( $ ) {
			jQuery( "img.lazyload" ).lazyload();
			jQuery( '.lazy' ).lazyload()
		} );
	</script>
	
</body>
</html>