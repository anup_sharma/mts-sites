jQuery(document).ready(function($){
	   
	jQuery('#step-1').validate({			
	   rules: {
				zip: {required: true},
		   		customRadioInline1: {required: true},
		   		full_address: {required: true},
		   		first_name: {required: true},
				last_name: {required: true},
				email: {required: true,email: true},
				phone: {required: true, phoneUS: true},
		   		
			},
			messages: {
				zip: "Please enter zipcode.",
				customRadioInline1: "Please Select Electic Bill Range.",
				full_address: "Please enter address.",
				first_name: "Please enter first name.",
				last_name: "Please enter last name.",
				email: "Please enter valid email.",
				phone: "Please enter valid phone.",
			},
		submitHandler: function(form) {
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 jQuery('.topform').html('<div class="fivdth text-center"><div class="col"><h3 class="font-weight-light mb-3">Thank You For Your Request For A Free Solar Quote.</h3><h4 class="mb-3">We’ll be calling you shortly to share a personalized quote.</h4></div></div>');
				 fbq('track', 'Lead');
				 gtag('event', 'Lead', { 'event_category' : 'Leads',  'event_label' : 'Lead'}); 
			});	
			return false; 
		}
	});
	 
	jQuery("a.open-modal2").on("click", function(){
		$('#myModal2').modal('show');
		return false;
	})
	jQuery('#ccpa-form').validate({
			rules: {
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true,email: true},
				phone: {required: true},
				addres: {required: true},
				city: {required: true},
				state: {required: true},
				zip: {required: true},
				'type_request[]': {required: true}
				
			},
			messages: {
				first_name: "Please enter first name.",
				last_name: "Please enter last name.",
				email: "Please enter valid email.",
				phone: "Please enter valid phone.",
				'type_request[]': "Please select at least one"
			},
			 
		submitHandler: function(form) {
			
			
			var data = jQuery('#ccpa-form').serializeArray();
			
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax-ccpa.php",
				data: data
			})
			request.done(function( response ) {
				
				 jQuery(".havesend").html("Your request has been sent.");
				  
			});	
	
			return false; 
		}
	});
	
	 
	
})
function gotoStep(step){
	
	var current = step + 1;
	
	if(step == 5 )
		 $(".modal-content").css('margin-top','70% !important');
	
	$(".step-"+current).hide();
	$(".step-"+step).show();
	percent = step*20 - 20;

	$(".complete").css("width", percent+"%");
	$("#percents").css("left", percent+"%");
	$("#percents").html(percent+"%");
	
}
