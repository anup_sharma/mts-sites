<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176059540-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176059540-4');
</script>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Solar Power Incentives</title>
	<meta name="robots" content="noindex,nofollow">
	<meta name="description" content=""/>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--styles-->
	<link rel="stylesheet" href="assets/css/style-top.css">

</head>
<body>
<header class="fixed-top">
	<div class="container">
		<div class="logo">
			<a href="/"><img class="logo" src="images/logo.jpg" alt="logo"></a>
		</div>
		<div class="startquote">
			<form id="#step-1-form" class="needs-validation mx-auto" novalidate>
				<a class="nav-link start1">Get Quote</a>
				<input id="ziptop" class="form-control rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip" type="text">
			</form>
		</div>
	</div>
</header>
<section class="main privacy">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-12 col-md-12">
					<h2>Terms of Use</h2>
					<p>Welcome to solarpowerincentives.com! Your use of this website (the <strong>"Site"</strong>) is subject to these Terms of Use (these <strong>"Terms"</strong>). By using the Site, you agree to be bound by, and use the Site in compliance with, these Terms of Use. IF YOU DO NOT AGREE TO THESE TERMS OF USE, DO NOT USE THE SITE.</p>
					<p>We may make changes to these Terms from time to time. It is your responsibility to review these Terms frequently and to remain informed of any changes to them. You agree that your continued use of the Site after any such changes have been published to the Site shall constitute your acceptance of the Terms as revised.</p>
					<p>Special terms may apply to some services and activities offered on the Site. These terms are posted in connection with the applicable service. Any such terms are in addition to these Terms, and in the event of a conflict, prevail over these Terms.</p>
					<p><u>PLEASE NOTE: SECTION 18 OF THESE TERMS OF USE CONTAINS AN ARBITRATION CLAUSE AND CLASS ACTION WAIVER PROVISION. IT AFFECTS HOW DISPUTES ARE RESOLVED.</u></p>
					<h2>(2) LICENSE TO USE THE SITE</h2>
					<p>Subject to these Terms, we grant to you a limited, personal, non-exclusive, non-transferable license to use the Site for your personal use and not for resale or further distribution. Your right to use the Site is limited by the terms set forth in these Terms.</p>
					<p>Except for your pre-existing rights and this license granted to you, we and our licensors retain all right, title and interest in and to the Site, including all related intellectual property rights. The Site is protected by applicable intellectual property laws, including United States copyright law and international treaties.</p>
					<p>Except as otherwise explicitly provided in these Terms or as may be expressly permitted by applicable law, you will not, and will not permit or authorize third parties to: (a) reproduce, modify, translate, enhance, decompile, disassemble, reverse engineer or create derivative works of the Site; (b) rent, lease or sublicense access to the Site; nor (c) circumvent or disable any security or technological features or measures of the Site.</p>
					<h2>(3) ACCESS TO THE SITE; MODIFICATIONS TO THE SITE</h2>
					<p>We do not provide you with the equipment to access the Site. You are responsible for all fees charged by third parties to access the Site (e.g., charges by internet service providers).</p>
					<p>We reserve the right to modify or discontinue, temporarily or permanently, all or a part of the Site without notice. We will not be liable to you or to any third party for any modification, suspension, or discontinuance of the Site.</p>
					<p>We also reserve the right, in our sole discretion, to reject, refuse to post, or remove any posting (including private messages) by you, and to restrict, suspend, or terminate your access to the Site at any time, for any or no reason, with or without prior notice, and without liability.</p>
					<h2>(4) SITE RESTRICTIONS</h2>
					<p>You must comply with all applicable laws when using the Site. Except as may be expressly permitted by applicable law or authorized by us in writing, you will not, and will not permit anyone else to: (a) store, copy, modify, distribute, or resell any of the information; audio, visual, and audiovisual works; or other content made available on the Site ("Site Content) or compile or collect any Site Content as part of a database or other work; (b) use any automated tool (e.g., robots, spiders) to use the Site or store, copy, modify, distribute, or resell any Site Content; (c) rent, lease, or sublicense your access to the Site to another person; (d) use the Site or Site Content for any purpose except for your own personal use; (e) circumvent or disable any digital rights management, usage rules, or other security features of the Site; (f) use the Site in a manner that threatens the integrity, performance, or availability of the Site; or (g) remove, alter, or obscure any proprietary notices (including copyright notices) on any portion of the Site or Site Content.</p>
					<h2>(5) PRIVACY POLICY</h2>
					<p>We may collect, sell, rent or otherwise dispose of registration and other information about you through the Site. Our collection and use of this information is governed by our privacy policy.</p>
					<h2>(6) LINKING TO THIS SITE</h2>
					<p>If you link to this Site, you are permitted to link only to the home page at solarpowerincentives.com. Deep linking to internal parts of this Site, framing of this Site as part of other Web sites, and in-line linking or any other manner of incorporating parts of this Site as part of another Web site is not permitted without our prior written consent.</p>
					<h2>(7) LINKS AND THIRD PARTY CONTENT</h2>
					<p>The Site may contain links to third party products, services, and Web sites. We exercise no control over the third party products, services, and Web sites and we are not responsible for their performance, do not endorse them, and are not responsible or liable for any content, advertising, or other materials available through the third party products, services, and Web sites. We are not be responsible or liable, directly or indirectly, for any damage or loss caused to you by your use of or reliance on any goods or services available through the third party products, services, and Web sites.</p>
					<h2>(8) RESTRICTED AREAS OF THE SITE</h2>
					<p>Certain areas of the Site may be password restricted to registered users (<strong>"Password-Protected Areas"</strong>). If you have registered as an authorized user to gain access to these Password-Protected Areas, you agree that you are entirely responsible for maintaining the confidentiality of your password, and agree to notify us if the password is lost, stolen, disclosed to an unauthorized third party, or otherwise may have been compromised. You agree that you are entirely responsible for any and all activities that occur under your account, whether or not you are the individual who undertakes such activities. You agree to immediately notify us of any unauthorized use of your account or any other breach of security in relation to your password or the Site that is known to you.</p>
					<h2>(9) SUBMISSIONS</h2>
					<p>Users of the Site may have the opportunity to post information to the Site, including reviews, comments, and other information and materials. Any opinions, advice, statements, services, offers, or other information that constitutes part of the content expressed or made available by third parties on the Site are those of the respective authors or producers and not of us or our sponsors or their respective shareholders, directors, officers, employees, agents, or representatives. We do not control the content posted by third parties via the Site and do not guarantee the accuracy, integrity or quality of such content. You understand that by using the Site, you may be exposed to content that is offensive, indecent, or objectionable. Under no circumstances will we, our sponsors or their respective shareholders, directors, officers, employees, agents, or representatives be held liable for any loss or damage caused by your reliance on information obtained through the Site. It is your responsibility to evaluate the information, opinion, advice, or other content available.</p>
					<p>You agree that we are free to use any comments, information, ideas, concepts, reviews, or techniques or any other material contained in any communication you may send or provide to us (each, a <strong>"Submission"</strong>), including, without limitation, responses to questionnaires or through postings to the Site without further compensation, acknowledgement, or payment to you for any purpose whatsoever including, but not limited to, developing, manufacturing and marketing products and creating, modifying or improving the Site and our products and services.</p>
					<p>Furthermore, by posting any Submission on the Site, submitting information to us, or in responding to questionnaires or other means for providing Submissions to us, you grant us a perpetual, non-exclusive, fully paid, royalty-free, irrevocable, sublicenseable, worldwide license and right to display, use, perform, reproduce, modify, distribute and create derivative works of the Submission or information submitted in any media, software, or technology of any kind now existing or developed in the future.</p>
					<p>BY POSTING OR PROVIDING ANY SUBMISSION OR OTHER INFORMATION, YOU REPRESENT AND WARRANT THAT PUBLIC POSTING AND USE OF YOUR SUBMISSION OR INFORMATION BY US WILL NOT INFRINGE ON OR VIOLATE THE RIGHTS OF ANY THIRD PARTY.</p>
					<h2>(10) USE POLICIES</h2>
					<p>You are solely responsible for any content and other material that you submit, publish or display on the Site or transmit to other members and/or other users of the Site.</p>
					<p>You will not use the Site to: (a) upload, post, email, or otherwise transmit any Submission that contains unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libelous, invasive of another’s privacy, hateful, or racially, ethnically or otherwise objectionable; (b) harm us or third parties in any way; (c) impersonate any person or entity, or otherwise misrepresent your affiliation with a person or entity; (d) upload, post, email, or otherwise transmit any Submission that you do not have a right to transmit under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements); (e) upload, post, email or otherwise transmit any Submission that infringes any patent, trademark, trade secret, copyright, or other right of any party; (f) upload, post, email, or otherwise transmit any unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other forms of solicitation; (g) upload, post, email, or otherwise transmit any material that contains software viruses or any other computer code, files, or programs designed to interrupt, destroy, or limit the functionality of any computer software or hardware or telecommunications equipment; (h) interfere with or disrupt the Site or servers or networks connected to the Site, or disobey any requirements, procedures, policies or regulations of networks connected to the Site; (i) intentionally or unintentionally violate any applicable local, state, national or international law or regulation; (j) "stalk" or otherwise harass another; or (k) collect or store personal data about other users.</p>
					<h2>(11) FEEDBACK</h2>
					<p>We may provide you with a mechanism to provide feedback, suggestions, and ideas, if you choose, about the Site (<strong>"Feedback"</strong>). You agree that we may, in our sole discretion, use the Feedback you provide to us in any way, including in future modifications to the Site. You hereby grant to us and our assigns a perpetual, worldwide, fully transferable, irrevocable, royalty free license to use, reproduce, modify, create derivative works from, distribute, and display the Feedback in any manner any for any purpose.</p>
					<h2>(12) INFORMATIONAL DISCLAIMERS RELATED TO YOUR USE OF THE SITE</h2>
					<p>Please note that some of the services provided on the Site are designed to provide consumers an opportunity to request information regarding certain products and services, including without limitation, solar energy. As a result, please note the following:</p>
                    <ul type="disc">
                      <li>The Site is a lead generation service that matches requests from potential customers with service providers that offer products or services that may be able to meet such potential customers request. We are not involved in the actual transaction between potential customers and service providers. Consequently, we cannot ensure that a transaction will occur. Further, if a transaction does occur between a potential customer who submits their information and a service provider with whom we work, we have no control over the quality, safety or legality of the item or service sold. If we are unable to match your request to businesses within our network, we retain the right to match you to businesses outside our network through our approved partners.</li>
                      <li>The information furnished on the Site and any interactive responses are not intended to be professional advice and are not intended to replace personal consultation with a qualified home improvement service provider, solar energy specialist or other service professional. You must always seek the advice of a qualified professional for questions related to implementing and/or using energy-related products in your home. You should never disregard professional advice or delay in seeking professional advice related to your home or the implementing and/or using energy-related products in your home because of something you have read on this site. The application of information on this Site may not be applicable to your specific circumstances.</li>
                      <li>It is your responsibility to always confirm applicable licensing requirements with each service provider you talk to and the applicable state and local licensing authorities.</li>
                      <li>While information on the Site has been obtained from sources believed to be reliable, neither we nor the content providers warrant the accuracy of product content, or other data of any nature contained on this website. Neither we nor the content providers guarantee that the content on this Site covers all possible uses, directions, precautions, or adverse effects that may be associated with any energy-related product.</li>
                      <li>From time to time, we may offer visitors to this Site offers from third party marketing partners as a courtesy. Other than providing this third party offers to you, we are not associated with these third parties. Furthermore, we do not have control over the quality, safety or legality of the item or service sold via these third party offers and the inclusion of any third party offer does not imply endorsement by us. We disclaim all liability resulting from your submission of information via any third party offer provided on this Site.</li>
                      <li>YOUR USE OF THIS SITE, INCLUDING, WITHOUT LIMITATION, YOUR USE OF ANY CONTENT ACCESSIBLE THROUGH THE SERVICE AND YOUR INTERACTIONS AND DEALINGS WITH ANY SERVICE PROVIDERS, IS AT YOUR SOLE RISK. THE SITE AND ALL CONTENT AVAILABLE ON AND THROUGH THE SERVICE ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. WE AND OUR SUPPLIERS AND LICENSORS EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT. WE DO NOT WARRANT UNINTERRUPTED USE OR OPERATION OF THE SERVICE OR YOUR ACCESS TO ANY CONTENT. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM THE SERVICE WILL CREATE ANY WARRANTY REGARDING SUNRUN THAT IS NOT EXPRESSLY STATED IN THESE TERMS. SOME JURISDICTIONS MAY PROHIBIT A DISCLAIMER OF WARRANTIES AND YOU MAY HAVE OTHER RIGHTS THAT VARY FROM JURISDICTION TO JURISDICTION. WITHOUT LIMITING THIS SECTION, WE MAKE NO WARRANTIES WHATSOEVER, EXPRESS OR IMPLIED, REGARDING THE ACCURACY, COMPLETENESS, TIMELINESS, OR USEFULNESS OF ANY INFORMATION CONTAINED OR REFERENCED ON THIS SITE, INCLUDING ANY DATA. TECHNOLOGY CHANGES FREQUENTLY AND THEREFORE INFORMATION CONTAINED ON THE SERVICE MAY BE OUTDATED, INCOMPLETE OR INCORRECT. WE DO NOT ASSUME ANY RISK WHATSOEVER FOR YOUR USE OF THE SITE OR THE DATA CONTAINED ON OR RESULTING FROM THE SITE OR YOUR REQUEST FOR SERVICE PROVIDER QUOTES.</li>
                    </ul>
                    <h2>(13) USERS UNDER AGE 13</h2>
                    <p>It is our policy that visitors to the Site who are under the age of 13 should not post or provide information on the Site without the consent of their parents. The Child Online Privacy and Protection Act (COPPA) regulates online collection of information from persons under the age of 13. It is our policy to refrain from knowingly collecting or maintaining personal information relating to any person under the age of 18. If you are under the age of 18, please do not supply any personal information through the site. If you are under the age of 18 and have already provided personal information through the site, please have your parent or guardian contact us immediately, so that we can remove such information from our files. You may contact us at the address below.</p>
                    <h2>(14) SECURITY</h2>
                    <p>We use reasonable security measures that are designed to protect personal information from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. Please note, however, that no data security measures can be guaranteed to be completely effective. Consequently, we cannot ensure or warrant the security of any personal information or other information. You transmit personal information to us at your own risk.</p>
                    <h2>(15) LIMITATION OF LIABILITY</h2>
                    <p>NEITHER THE OWNERS OF THE SITE NOR ITS SUPPLIERS OR LICENSORS WILL BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA, OR OTHER INTANGIBLE LOSSES (EVEN IF WE OR ANY SUPPLIER OR LICENSOR HAS BEEN ADVISED OF THE POSSIBILITY OF THESE DAMAGES), ARISING OUT OF OR RELATING TO YOUR ACCESS TO OR USE OF, OR YOUR INABILITY TO ACCESS OR USE, THE SITE OR ANY CONTENT. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU.THE MAXIMUM TOTAL LIABILITY OF THE OWNERS OF THE SITE AND ITS SUPPLIERS AND LICENSORS TO YOU FOR ALL CLAIMS UNDER THESE TERMS, WHETHER IN CONTRACT, TORT, OR OTHERWISE, IS $100. EACH PROVISION OF THESE TERMS THAT PROVIDES FOR A LIMITATION OF LIABILITY, DISCLAIMER OF WARRANTIES, OR EXCLUSION OF DAMAGES IS TO ALLOCATE THE RISKS UNDER THESE TERMS BETWEEN THE PARTIES. THIS ALLOCATION IS AN ESSENTIAL ELEMENT OF THE BASIS OF THE BARGAIN BETWEEN THE PARTIES. EACH OF THESE PROVISIONS IS SEVERABLE AND INDEPENDENT OF ALL OTHER PROVISIONS OF THESE TERMS. THE LIMITATIONS IN THIS SECTION WILL APPLY EVEN IF ANY LIMITED REMEDY FAILS OF ITS ESSENTIAL PURPOSE.</p>
                    <h2>(16) INDEMNIFICATION</h2>
                    <p>You agree to indemnify and hold harmless the owners of this Site, its affiliates, its officers, directors, agents and employees against any asserted claims or suits made by any third party or any regulatory action brought by any entity, for any and all damages, losses, judgments, suits, claims or liabilities (including but not limited to reasonable attorney’s fees) arising from or relating to: (i) any actual or alleged breach of these terms by you and (ii) any content, data or material that you submit, post or otherwise provide to the owners of this Site.</p>
                    <h2>(17) CHOICE OF LAW &amp; LEGAL FORUM</h2>
                    <p>These Terms, Privacy Policy and your use of the Site, including all claims or causes of action (whether in contract or tort) that may be based upon or arise out of or relate to the Terms, Privacy Policy and your use of the Site, will be governed by New Jersey law, excluding its conflict and choice of law principles. The exclusive jurisdiction and venue for any claims arising out of or related to these Terms and the Privacy Policy or your use of the Site will lie in the State and Federal courts located in the State of New Jersey, and you irrevocably agree to submit to the jurisdiction of such courts. Our failure to enforce any right or provision in these Terms of the Privacy Policy will not constitute a waiver of such right or provision unless acknowledged and agreed to by us in writing. In the event that a court of competent jurisdiction finds any provision of these Terms or the Privacy Policy to be illegal, invalid, or unenforceable, the remaining provisions will remain in full force and effect.</p>
                    <h2>(18) ARBITRATION</h2>
                    <p>You agree that agree that any dispute or claim arising out of your use of the Site, including any dispute or claim as to the application, enforceability, scope, or interpretation of this agreement to arbitrate, shall be resolved by binding arbitration, rather than in court, except that you may assert claims in small claims court if your claims qualify. The Federal Arbitration Act and federal arbitration law apply to this agreement.</p>
                    <p>There is no judge or jury in arbitration, and court review of an arbitration award is limited. However, an arbitrator can award on an individual basis the same damages and relief as a court (including injunctive and declaratory relief, or statutory damages), and must follow these Terms of Use.</p>
                    <p>Arbitration shall be administered by the Judicial Arbitration and Mediation Services, Inc. ("JAMS"), pursuant to the JAMS Streamlined Arbitration Rules & Procedures effective July 1, 2014, and conducted by a single, neutral arbitrator. Arbitration shall take place by phone, unless an in-person hearing is requested by either party. In that case, the hearing shall take place in the county where you reside. To the extent this agreement to arbitrate conflicts with the JAMS Policy on Consumer Arbitrations Pursuant to Pre-Dispute Clauses Minimum Standards of Procedural Fairness (the "Minimum Standards"), the Minimum Standards in that regard will apply.</p>
                    <p>Disputes may also be referred to another arbitration organization if you and the Company agree in writing, or to an arbitrator appointed pursuant to Section 5 of the Federal Arbitration Act.</p>
                    <p>To start an arbitration proceeding, you must execute and serve a Demand for Arbitration on the Company by mailing it to insert address pursuant to the JAMS instructions. The Company will bear the cost of your initial filing fee.</p>
                    <p>We each agree that any dispute resolution proceedings will be conducted only on an individual basis and not in a class, consolidated, or representative action. If for any reason a claim proceeds in court rather than in arbitration, we each waive any right to a jury trial. We also both agree that you or we may bring suit in court to enjoin infringement or other misuse of intellectual property rights.</p>
                    <h2>(19) ENTIRE AGREEMENT</h2>
                    <p>These Terms and the Privacy Policy contain the entire agreement between the parties and supersedes all previous agreements and proposals, oral or written, and all negotiations, representations, warranties, conversations, or discussions between the parties relating to your use of the Site. You acknowledge that you have not relied on any representations or statements by the owners of this Site not included in these Terms and Privacy Policy. The Terms and the Privacy Policy may only be amended in a writing signed by the owners of this Site.</p>
                    <h2>(20) UPDATES TO THESE TERMS</h2>
                    <p>We may occasionally update the Site and these Terms. When we do, we will revise the "last updated" date on these Terms. You should check this Site and these Terms frequently to see recent changes. The then-current version of these Terms will supersede all earlier versions.</p>
                    <h2>(21) CONTACTING US</h2>
                    <p>If you have any questions or concerns about the Site or these Terms, you may contact us at  <a href="mailto:marketing@solarpowerincentives.com">marketing@solarpowerincentives.com</a>.</p>
                    <p>Last Updated June 30, 2020.</p>				
				</div>
			</div>
		</div>
	</section>
	<?php 
      function generateRandomString($length = 25) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
    }
      
      $token = generateRandomString();
    if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $protocol = 'https://';
        }
        else {
        $protocol = 'http://';
        }
        $current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         
      ?>
      
<footer class="footer"><span class="copy">© 2020 Solar  Power Incentives | <a href="/privacy-policy.php">Privacy Policy</a></span></footer>


<div id="myModal" class="modal fade custom-modal" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="scrolltoelement"></div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="container">
						<div class="progressbar">
							<!-- <div id="percents">0%</div> -->
							<div class="progress">
								<div class="complete"></div>
							</div>
						</div>
						<div class="row">
							<div class="col d-flex text-center align-items-center">
								<!-- Step 1 -->
								<div class="col form py-1 mx-auto step-1">
									<div>
										<h3 class="font-weight-light mb-5">Government Offers Rebates To Entice Residents To Go Solar</h3>
										<p class="mb-5 mt-4 custom-width">Enter Your Zip Code to See if You Qualify</p>
										<form id="step-1" class="needs-validation mx-auto" novalidate>
											<input id="zip" class="form-control rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip" type="text">
											<div class="invalid-feedback mb-4">
												Please provide a valid zip.
											</div>
											<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
											<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
											<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
											<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
											<input type="hidden" name="url" value="<?php echo $current_link?>">
											<input type="hidden" name="step" class="step" value="step1">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
											<div class="center-block mt-5">
												<button class="btn btn-primary rounded-0 btn-block with-arrow" type="submit" style="margin: 0 auto;">Continue</button>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 2 -->
								<div class="col form pt-0 pt-lg-5 px-0 pb-0 mx-auto step-2" style="display:none;">
									<div>
									<h3 class="font-weight-light mb-5">What is your average monthly electric bill?</h3>
									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">This will help us determine how much you can save.</p>
										<form id="step-2" class="needs-validation mx-auto bill-box" novalidate>
											<div id="1" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="Under $75" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline1">Under $75</label>
											</div>
											<div id="2" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="$76 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline2">$76 - $150</label>
											</div>
											<div id="3" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="$151 - $225" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline3">$151 - $225</label>
											</div>
											<div id="4" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="$226 - $300" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline4">$226 - $300</label>
											</div>
											<div id="5" class="custom-control custom-radio custom-control-inline checkbox">
												<input type="radio" value="Over $300" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">
												<label class="custom-control-label" for="customRadioInline5">Over $300</label>
											</div>
											<input type="hidden" name="step" class="step" value="step2">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
											<div class="row">
												<div class="col-6 text-left">
												
												</div>
												<div class="col-6 text-right">
													<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 3 -->
								<div class="col form pt-0 pt-lg-5 mx-auto step-3" style="display:none;">
									<div>
									<h3 class="font-weight-light mb-5">Please enter your home address.</h3>
									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">This will help us maximize solar incentives in your area.</p>
										<form id="step-3" class="needs-validation mx-auto" novalidate>
											<input type="text" name="full_address" class="form-control rounded-0 cw-75" id="full_address" placeholder="Address">
											<input type="text" id="street_number" name="street_number" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="route" name="street_name" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="locality" name="city" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="postal_code" name="postal_code" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="administrative_area_level_1" name="state_abbr" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<input type="text" id="country" name="country" value="" style="height: 0px; border: none; margin: 0; padding: 0;">
											<div class="invalid-feedback">
												Please provide a valid Address.
											</div>
											<input type="hidden" name="step" class="step" value="step3">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
											<div class="row mt-5">
												<div class="text-left col-6 ">
												</div>
												<div class="col-6 text-right">
													<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 4 -->
								<div class="col form pt-0 pt-md-3 mx-auto step-4" style="display: none;">
									<div>
										<h3 class="font-weight-light mb-5">How much shade does your home get from 9 AM - 3 PM?</h3>
										<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">This will help us determine how much solar energy your system can produce.</p>
										<form id="step-4">
											<div class="step4desk">
												<div class="row mb-5">
													<div class="col-12 col-md-4 shade " id="no-shade">
														<div class="shade-image">
															<img src="images/roof-1.png">
														</div>
														<h5 class="mt-3">No Shade</h5>
														<p>Entire roof is exposed to sunlight.</p>
														<input type="radio" class="no-shade" name="shade" value="No Shade" style="display:none">
													</div>
													<div class="col-12 col-md-4 shade" id="some-shade">
														<div class="shade-image">
															<img src="images/roof-2.png">
														</div>
														<h5 class="mt-3">Some Shade</h5>
														<p>Parts of the roof are covered at certain times/all times.</p>
														<input type="radio" class="some-shade" name="shade" value="Some Shade" style="display:none">
													</div>
													<div class="col-12 col-md-4 shade " id="severe-shade">
														<div class="shade-image">
															<img src="images/roof-3.png">
														</div>
														<h5 class="mt-3">Severe Shade</h5>
														<p>Most or all of the roof is covered at certain times/all times.</p>
														<input type="radio" class="severe-shade" name="shade" value="Severe Shade" style="display:none">
														<input type="hidden" name="step" class="step" value="step4">
														<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="text-left col-6">
												</div>
												<div class="col-6 text-right">
													<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
												</div>
											</div>
										</form>
									</div>
								</div>
								<!-- Step 5 -->
								<div class="col form pt-0 pt-sm-8 mx-auto step-5" style="display:none;">
									<div>
									<h3 class="font-weight-light mb-0 mb-md-5">Please enter your contact info to see if you
qualify for solar!</h3>
									<form id="step-5" class="needs-validation row" novalidate>
										<div class="col-12">
											<div class="row m-md-auto">
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">
													<div class="invalid-feedback">
														Please provide first name.
													</div>
												</div>
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">
													<div class="invalid-feedback">
														Please provide last name.
													</div>
												</div>
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">
													<div class="invalid-feedback">
														Please provide a valid email.
													</div>
												</div>
												<div class="col-12 col-md-12 col-lg-6">
													<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04" placeholder="Phone">
													<div class="invalid-feedback">
														Please provide a valid phone.
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-12 text-center my-3">
													<button class="btn btn-primary rounded-0 btn-sm final_submit" type="submit">Submit</button>
													<img src="images/loading_bubble.svg" style="display:none" class="loader_image">
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<p class="my-3 small ml-auto mr-auto">*By clicking 'Submit', you authorize Solar Power Incentives and up to four <a href="#" class="open-modal2">home service companies</a> to call and text you on the phone number you provided using autodialed and prerecorded calls or messages about their products and services.</p>
												</div>
											</div>
											<div class="row">
												<div class="col-12">
													<input type="hidden" name="step" class="step" value="step5">
													<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
													<div class="row">
														<div class="text-left">
														
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
									<div class="bot-sec mt-5" style="display:none;">
										<div class="d-flex col- mx-o justify-content-between">
											<button class="btn btn-primary rounded-0" type="submit">Next</button>
										</div>
									</div>
									</div>
								</div>
								<div class="col form py-5 mx-auto step-6" style="display:none;">
									<div>
										<div class="fivdth">
											<div class="col">
												<h3 class="font-weight-light mb-3">Thank You For Your Request For A Free Solar Quote.</h3>
												<h4 class="mb-3">We’ll be calling you shortly to share a personalized quote.</h4>
											</div>
										</div>
									</div>
								</div>
								<div class="close" style="display:none;">
									<i class="fas fa-times"></i>
								</div>
							</div>
						</div>
						<div class="smallnotes"><i class="fas fa-lock"></i> Your information is secure</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog modal-xl">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<p><strong>Matched solar partners included but not limited to:</strong></p>
<p style="font-size: 12px; line-height: 16px;">1 800 Solar USA, 1a Solar Systems Design, 21st Century Power Solutions, 3 Guy Solar, A &amp; R Solar, A National Electric Service Inc., A1 Certified Solar, A1A Solar Contracting, AAE Solar, ABCO Solar, Ablaze Energy, Absolute Green Energy, Absolute Solar, AC Solar, Accredited Solar, ACDC Solar, Acro Energy, Acro Energy/Energy Efficiency Solar, Ad Energy LLC, Adam Quenneville Roofing and Siding, ADR Electric, Inc, ADR Solar Solutions Inc, Advance Solar Construction, Advance Solar david beers, Advanced Air Concepts Inc, Advanced Home Services, Advanced Metrics Group, Advanced Solar Solutions, Inc, AES, AFC Electric Inc., Affordable Solar, Affordable Solar Energy, Akiva Management, Al Fresco Solar, Alder Energy Systems, All Bay Solar, All Energy Solar, All Florida Solar, All Green IT LLC, All Season Solar, All Seasons Roofing, All Sky Energy, All Solar Service, All Solar Solutions, Allegany Renewable Technologies, Allen Braden, Alltech Solar, Almost Free Energy, Alpine Solar Design, Alpine Solar Electric, Alteris Renewables, Alternate Energy Center, Alternate Energy Group, Alternative Energy Southeast, Alternative Power Solutions, AM Sun Solar, AM Sunpower, Amazing Solar Solutions, Ambassador Energy of Dallas, Ambassador Energy, Inc, Amergy Solar, American Design and Build, American Electric, American Made Solar and Wind, American Solar, American Solar Energy, American Solar Power Inc, American Solar Solution, AngelPark Renewables LLC, Aquatic Arts, Argent Solar Electric, Arise Solar, Arizona Clean Energy, Arizona Renewable Energy Solar, Arizona Solar Concepts, Arizona Solar Solutions, Arizona Solar Wave, Arosa Solar, Arosa Solar Energy Systems, Inc., Array Sunsystems, Askmeaboutsolarsc.com, Associated Community Solar Sciences, Aston Solar, Astralux, Astralux Power Systems, Astralux Solar, Astrolux Power Systems, Astrum Solar, At Your Service Power, Inc, Aten Solar, AtisSun Inc, Atlas Solar Innovations, Atomic Solar, Auric Solar, Automated Home Technologies, AWS Electrical &amp; Solar Inc., AY Solar, Aztec Solar, Aztec Solar Renewable, Barefoot Solutions/All Florida Solar, Bask Power, BCI Solar, BDS Solar, Be Utility Free, BedRock Solar, Beeson Squared Construction, Bell Solar Electric, Bella Energy, Belmont Energy, Belmont Solar, Benjamin Franklin Plumbing, Best Energy Power, Best Energy Practices, Beutler Corp, BGreenLA, Bland Family Solar and Electric, BLG Solar Solutions, Blue Chip Energy, Blue Raven Solar, Blue Ray Energy, Blue Sky Solar and Wind Power, Bola Energy, Bolt-on Solar, Bonterra Solar, Brevard Solar LLC, Bright Sky Solar, Inc, Brilliant Solar, Broward Solar Inc, Build It Green, Burnham-Beck &amp; Sun, BySolar, Calavo Builders, Cali Contractors, California Home Solar, CAM Solar, Carlson Solar, Carolina Eco Smart, Carolina Green Building, Cascade Solar &amp; Electric, Catalyst Group LLC, Catch the Wind, Cazeault Solar, CBR Engineering Contractor, Cenergy Power, Central California Solar, Central Florida Solar Inc, Centro Solar America Inc, Certified Solar, Chicago Energy Conservation, Cinnamon Solar, Clary Solar, Clean Energy Collective, Clean Energy Solar Systems, Clean Energy Solutions, Clean Energy Experts, Clean Energy USA, Clean Focus Energy Inc, Clean Power Solutions, CleanEnergyAuthority, CleanTech Energy Solutions, Clear Horizon LLC, Climatic Solar Corp, Clockwork Energies, CMI Electric, Coastal Constructors, Coastal Solar LLC, Coldwell Solar, Collegiate Builders, Collins Electric, Color New Inc, Colorado Electric Inc, Common Energy, Community Energy Inc, Compass Solar Energy, Complete Solar Solutions Inc, Concept Solar Co., Consolidated Solar Tech, Cool Solar Inc, Corbin Solar, CUC Global, Inc., Current Electric Co, Current Installations, D &amp; M Alternative Energy LLC, Dan Maher, David R. Singleton, Inc, Demco Energy, LLC, Denver Solar and Wind, Dependable Solar Products, Diablo Builders, Diamond Energy Solutions, Diez Energy Solutions, Dioko Energy, Dioko Solar, Discount Solar, Dixieline Contracting, DM Solar, Double Positive, Down to Earth Solar, Dunbarr Electric and Solar, Duramade Windows and Doors, Dynamic Integration LLC, Eclipse Solar, Eco Solar Installation, Eco Solar Solutions, Eco Solar World, Eco-merica, Inc., Ecohouse Solar, Ecological Systems, EcoMark Solar, Econogy Corp., EcoSavvy Solar, EcoSmart Solar Solutions, EcoSolar Installation, EE Solar, El Paso Green Energies, ElectriCare, Inc., Elemental Design Group, EmPower Clean Energy Solutions, EMS Contracting, Encon Solar, Endless Mountains Solar Services, Energy By Choice, Energy Concepts, Energy Conservation Solutions, Energy One LLC, Energy Savers, Energy Savings Pros, Enviro Edge, Erus Energy, Ethical Electric, Evelar, Evengreen Technology, Evergreen Sun Power LLC, Everyone Solar, Everything Solar, Evolve Solar, Evolve Solar – Bruce McAuley, Exact Solar, Excellent Solar, Fafco, FidelityHome Energy, Finlo Renewable Energy, Fire Energy, First Quality Plumbing Solar DIvision, First Sun Solar, Fitz Energy, Fl Energy, Flexera, Florida Going Solar, Florida Power Services, Florida Roofing and Solar, Flow Renewable Ventures, FLS Energy, Freedom Solar, Frontier Solar, Inc., Full Spectrum Solar, Future City Solar, Future Renovations Inc., G &amp; G Systems, Galaxy Plumbing &amp; Solar Systems Inc., Galkos Construction, GC Electric, Gene McCormick Construction, Generation Green, George J Keller &amp; Sons LLC, Georgia Enviromental, Get Solar SD, Gettysburg Stove and Solar, Global Green Energy Corp, Global Green Solar, Global Solutions Reliance, GloSolar, Go Green Energy Savings, Go Solar, Go Solar Cooperative, Inc, Go Solar Group LLC, God’s Green Earth LLC, GoGreenSolar, Golden Solar, Golden Solar Systems, Goldin Solar LLC, Granite Bay Energy, Granite Bay Energy, Green Air, Green Co Solar, Green Conception Inc, Green Contractors Inc., Green Energy Group, Green Energy LLC, Green Energy Partners, Green Energy Shield, Green Essex Solar, Green Market Solutions, Green Ox Energy Solutions, Green Planet Home Remodeling, Green Planet Solar, Green Power Energy, Green Power Solutions, Green Pride Solutions, Green Solar Systems, Green Stream Solar and Electric, Green Street Solar Power, Green With Energy, Green Worx Solar, Green-Tech Environmental Services Inc., Greenbelt Solar LLC, GreenCal Solar, Greener Solar Solutions, Greeniverse, Greenwired, Grid City Energy, Grid Freedom Inc., Griffin Mechanical, groSolar, GSC Electrical LLC, Guardian Roofing and Solar, Guardian Solar &amp; Energy, LLC, Guardian Solar LLC, H &amp; H Electric, H&amp;H Solar, H2*Sunlight, Harmon Electric, Harrimans Inc, Haverford Systems, Heating &amp; Air Technology of CA, HelioPower Inc, Herca Solar, HMR &amp; R, Ho-Bert Enterprises, Home Energy Optimizers, Home Energy Systems, Home Free Solar – SolarTEK, Home Star Solar Solutions, Homewise Improvement Solutions, Horizon Solar, Horizon Solar Power, Hot Solar, Hudson Valley Clean Energy, I Need Solar, I Need Solar of Florida, Icon Solar, IES Residential Inc., Imperial Electric, Inc, Infinity Solar, Inland Solar, Inc, Innersol Global, Innotech Engineering Inc, Innotech Solar USA, Integrated Electrical Designs, Integrated Power Sources of Virginia, Integrated Solar Solutions, Inter Faith Electric &amp; Fire Inc, Intero Energy, IQ Solar, Ire Solar, ISI Solar, Islandwide Solar, IZun Energy LLC, Jamar Power Systems, James Calderon, Jim Scott, Joule Solar Energy, Just Electric And Solar, K and K Construction, K&amp;B Factory, Kapital Electric Inc., Kasselman Solar, Kenergy Solar, Kopp Electric Company, Korling, KV Solar Supply, LA Solar Energy, Leaf Solar Power LLC, Leamy Electric Inc, Leo Sunergy, Lifetime Solar Energy, Light-Wave Solar, LightFire Partners, Lighthouse Solar – Boulder, Linear Solar, Inc, Living Solar, Local Solar Installer in Los Angeles, Long Island Power Solutions, Lowes Solar LLC, M.T. Ruhl Solar, M2M Energy, Mackey Electrical Services, Inc, Magic Sun Solar, Malachite Plumbing and Heating LLC, Martifer Solar, Maryland Solar Installer, Mass Renewables Inc, Master Solar Dealers, Match Media Group LLC, Mathers Electric, MC2 Solar, MD Mechanical, Mehr Solar, Mercury, Metal Pro Roofing, Metro Builders &amp; Remodeling, MetroTek Electrical Services, MI Solar, Midwest Roofing and Solar, Milestone Solar Consultants, Milholland Electric, Inc, Mimeos Sustainability, Miracle Solar, Miramar Consulting, Mirasol Fafco Solar Inc, Mohave Solar, Montana Solar Works, Moore Energy, Mountain View Solar and Wind, Mr Solar, Mr. Build Home Improvement, Mr. Electric West Metro, MRSSA, MSI, MSL Solar, Munro Distributing Co. Inc., Muth and Sons Plumbing, Nailman Construction LLC, Natural Power and Energy, Nature’s Energy Solutions, Navajo Solar, Neighborly Energy, Nettles Solar, New Energy Construction, Inc., New England Clean Energy, New Jersey Clean Energy, New Jersey Clean Energy Solutions, Nexamp.Inc, Next Eco Energy, Next Level Realty, Nexus Solar, Northeast Solar &amp; Wind Power LLC, Nova West Solar, NRG Home Solar, Nu Energy Solutions, OC Sunny, Ocean Seven Builders Inc, Ocean Solar LLC, Ohio Valley Solar, Old Dominion Innovations, One Energy, Onforce Solar Inc, Orange County Electric and Solar, Orange Power &amp; Light, Orpical Energy, Owens Electric and Solar, Pacific Blue Solar, Inc, Pacific Pro Solar, Pacific Solar, Pacific West Solar, Palomar Solar, Paradise Energy Solutions, Paramount Solar, Patriot Mortgage Corporation, Peak 2 Peak Solar, Peak Energy Solutions, Peak Solar LLC, PEP Solar, Perk Solar, Phat Energy, Phoenix Solar Specialists, Photon Brothers, Photovoltaic Systems, LLC, Pick My Solar, Picture City Solar, Pinnacle Solar, Plan It Solar, Platinum Remodeling, Podar Solar, Power Productions Management, Powered By Sparq, PowerGridIncSolar, Powerup Solar, Powur, PR Plumbing Heating and Air Conditioning, Inc., Premier Power Renewable Energy, Prestige Solar, Prime Solar, Protel Communications, Inc, PSL Builder Inc., Quint Solutions, RAE Solar LLC, Rayah Solar, RCL Solar, RealGoodSolar, REC Solar, Inc., Redstone Solar Group, Refine Solar, ReneHomes, Renergy LLC, Renew Energy Resources, Renewable Energy CT, Renewable Energy NW LLC, Renewable Energy Resource Asscociation, ReNu Energy Solutions, Rev Solar, Revco Solar, Revolusun, Rhino Real Estate, RightFit Home Companies, Rise Solar, River City Solar, RoncoSolar, RoofPower Inc., Ross Solar Group, Royal Solar of AZ, RS Energy, RS Promotions, LLC, Rucker Solar Power, S &amp; G Management, Salt Electric, Salt River Solar and Wind LLC, San Diego Solar Solutions, Saving Oahu’s Solar, SC Solar Energy, Scudder Solar, SDI Solar, Sea Bright Solar, Second Generation Energy, Self Reliant Energy Company, Shaughnessy Contracting, Inc., Sierra Pacific Solar, Simple Power Systems, Simplex Solar, Simply Efficient Solar, Simply Solar of Maryland, Sky Power, Skyline Savers, Skyline Solar, Smart Energy USA, Smart Key Energy, Smart Source Energy, Smart World Energy, Smiley Electric, So-Cal Solar, SoCal Solar, Sol Technologies LLC, Solair, Solaire Energy Systems, Solar &amp; More, Solar 2000, LLC, Solar Alternatives, Solar America, Solar Center, Solar City, Solar Community, Solar Crave LLC, Solar Day Design, Solar Deal USA LLC, Solar Design &amp; Construction, Solar Edison, Solar Electric Works, Solar Electrical Systems, Solar Energy Management LLC, Solar Energy NY Corp, Solar Energy of Illinois, Solar Energy Systems, Solar Energy USA, Solar Energy World, Solar Harmonics, Solar Home Energy, Solar Hot, Solar Innovations, Solar Inverted LLC, Solar Kinetics, Solar Light Roofing, Solar Maxx, Solar Now, Solar One, Solar One Shop LLC, Solar Optimum, Solar Perfect, Solar Plexus, Solar Plus, Solar Power March, Solar Power Systems, Solar R US, Solar Ray Systems, Solar Roof, Solar Sales of Michigan, Solar Sales USA, Solar Services, Solar Smart, Solar Solutions Company, Solar Solutions of Florida, Solar Source, Solar Specialist, Solar Sun Energy, Solar System Specialists, Solar Systems Engineering, Solar Systems Inter, Solar Tech, Solar Tech South, Solar Technologies, Solar Town, Solar Universe, Solar Watt Solutions Inc, Solar Zone, Solar-Fit, SolarCity – Rich Glade, Solarecity Electric, Solarfied,Inc, Solari Energy Inc, Solaris Energy, SolarMax Technology, SolarSpecialist Inc, SolarSunWorld, SolarTechnics / FJB Remodeling, Solartek Energy, Solartek Solutions of Idaho, SolarTrek, Inc, SolarTyme, SolarVolt, Solarwinds Nothernlights, SolCal Solar, Solfarm Solar CO, Solgenics, Inc, Solsource, Soltec, South Coast Energy, South Coast Solar, South Florida Pool &amp; Heating, South Texas Solar Systems, South Texas Solar Systems – Laredo, Southards Solar Energy, Southern Energy Management, Southern View Energy, Southland Solar, Spearhead Solar, Spectrum Homes Solar, SPG Solar, SRE Standard Renewable Energy, Standard Solar, Statewide Remodeling, Stealth Solar, Steele Energy Solutions, Stelcor Energy, Stellar Roofing and Solar, Stellar Solar, Straight Up Energy, Streamline Solar, Stroika Construction Solutions, Summerlin Energy, Summerwind Solar, Summit Solar, LLC, Sun 4 Solar, Sun Best Solar, Sun Blue Energy, Sun Capital Investments, Sun City Solar, Sun City Solar Energy -North TX LLC, Sun Dollar Energy LLC, Sun Electronics, Sun First, Sun Green Systems, Sun Ray, Sun Ray Systems, Sun Star Energy Inc, Sun Stuff Energy, Sun Up Zero Down, Sun-Tec, Sunworks Solar Systems, Sun’s Free Solar, SunAir Green Solutions, Sunbeam Utility, SunBug Solar, Sunburst Solar, Sunburst Solar, Suncor Solar, Inc, Sundial Solar, Sunergy Systems, Sunfusion Solar Inc., Sunfutur, Sungevity, Sunlight Solar, Sunlight Solar Systems, Sunmark Solar Systems, SunMoney Solar LLC, Sunquest Solar LLC, Sunrise Solar LLC, Sunroc Solar LLC, SunRun, SunRun Solar, Sunshine Plus, Sunshine Solar, SunStarter Solar Installations Inc, SunTalk Solar, Suntech Dealer, Suntricity Power, Sunvalley Solar Inc., Sunwize Technology Inc, Sunworks, SunWorks Solar, Supergreen Solutions Oceanside, Supreme Air Systems, Sustainable Energy Solutions, Sustainable Energy Syst, Sustainable Future, Susten Corp, Switch Renewable Energy, Sylvester &amp; Cockrum Inc, Synergy Companies, Synergy Power, Synergy Solar Energy, T.A.K Electric, Taitem Engineering, Tap Into Solar, Inc, Texas Solar and Energy Conservation, The Entech Group Inc, The Pampering Plumber, The Solar Auditor, The Solar Store, The Sun is Yours, The Sun Is Yours, Titan Solar Power, Total Concept Sales, Inc., Total Solar LLC, Total Solar Solutions, Trapsun Solar, Tri Power, Tri-State Solar and Wind Corp, Tribal Vision, Trinity Solar, True Power Solar, True South Solar, U.S. Topco Energy. Inc., United Electrical Co, United Solar Associates LLC, United Sustainable, Upstate Alternative Energy, US Power Solutions, US Renewable Energy Development, US Solar, Vaillant Solar Systems, Valley Electric, Valley Solar Electric, Vans Electric CO, Verengo – M, Veterans Energy Solutions LLC, Vibrant Solar, Vivint Solar, Watts New Under the Sun Inc, Wayne Brechtel – SolarCity, Wayne’s Solar, West Coast Solar, West Seattle Natural Energy, WireOne Electrical, Woods Electric and Solar, World Sun Technology, Xlean Energy, YellowLite Inc, Yes Solar Solutions, You Save Green Inc, Zia Suns Green Construction</p>

			</div>
		</div>
	</div>
</div>
	<!--fontawesome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
	<script>
		WebFont.load( {
			google: {
				families: [ 'Playfair+Display:400,700,900', 'Open+Sans:300,400,600,700,800' ]
			}
		} );
	</script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style-bottom.css">
	<style>
		.pac-container {
			z-index: 9999999;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<script src="js/scrolling-nav.js"></script>
	<script src="js/app.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/lazyload.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script>
		$( '.phone_us' ).mask( '(000) 000-0000' );
		var placeSearch, autocomplete;
		var componentForm = {
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'short_name',
			country: 'long_name',
			postal_code: 'short_name'
		};
		function initAutocomplete() {
			// Create the autocomplete object, restricting the search to geographical
			// location types.
			autocomplete = new google.maps.places.Autocomplete(
				( document.getElementById( 'full_address' ) ), {
					types: [ 'geocode' ]
				} );
			// When the user selects an address from the dropdown, populate the address
			// fields in the form.
			autocomplete.addListener( 'place_changed', fillInAddress );
		}
		function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();
			for ( var component in componentForm ) {
				document.getElementById( component ).value = '';
				document.getElementById( component ).disabled = false;
			}
			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			for ( var i = 0; i < place.address_components.length; i++ ) {
				var addressType = place.address_components[ i ].types[ 0 ];
				if ( componentForm[ addressType ] ) {
					var val = place.address_components[ i ][ componentForm[ addressType ] ];
					document.getElementById( addressType ).value = val;
				}
			}
		}
		// Bias the autocomplete object to the user's geographical location,
		// as supplied by the browser's 'navigator.geolocation' object.
		function geolocate() {
			if ( navigator.geolocation ) {
				navigator.geolocation.getCurrentPosition( function ( position ) {
					var geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					var circle = new google.maps.Circle( {
						center: geolocation,
						radius: position.coords.accuracy
					} );
					autocomplete.setBounds( circle.getBounds() );
				} );
			}
		}
	</script>
	<!--Fading out "Get Free Quote" button in mobile-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete" async defer></script>
	<!-- Facebook Pixel Code -->

<script type="text/javascript">
  (function() {
      var field = 'xxTrustedFormCertUrl';
      var provideReferrer = false;
      var invertFieldSensitivity = false;
      var tf = document.createElement('script');
      tf.type = 'text/javascript'; tf.async = true;
      tf.src = 'http' + ('https:' == document.location.protocol ? 's' : '') +
        '://api.trustedform.com/trustedform.js?provide_referrer=' + escape(provideReferrer) + '&field=' + escape(field) + '&l='+new Date().getTime()+Math.random() + '&invert_field_sensitivity=' + invertFieldSensitivity;
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tf, s); }
  )();
</script>
<noscript>
    <img src="http://api.trustedform.com/ns.gif" />
</noscript>	
<script>
		jQuery( document ).ready( function ( $ ) {
			jQuery( "img.lazyload" ).lazyload();
			jQuery( '.lazy' ).lazyload()
		} );
	</script>
	
</body>
</html>