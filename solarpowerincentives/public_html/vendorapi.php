<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


try {
                $url = 'https://next.leadconduit.com/flows/5ed945b7f78a2f4891f937ba/sources/5d3f65b10522c3d926ee98ec/submit';
// Takes raw data from the request
$json = file_get_contents('php://input');

$data = json_decode($json, true);

// Converts it into a PHP object
                $response = sendconvoso($url, $data);
                echo($response);


} catch (Exception $e) {
    echo $e->getMessage();
}


function sendconvoso($url, $requestdata)
{
    $data['lead_type'] = 'Solar';
    
    if (isset($requestdata['email']) && !empty($requestdata['email'])) {
        $data['email'] = $requestdata['email'];
    }

    if (isset($requestdata['fname']) && !empty($requestdata['fname'])) {
        $data['first_name'] = $requestdata['fname'];
    }

    if (isset($requestdata['lname']) && !empty($requestdata['lname'])) {
        $data['last_name'] = $requestdata['lname'];
    }


    if (isset($requestdata['phone']) && !empty($requestdata['phone'])) {
        $badchar = array("(", ")", "-");
        $onlynumbers = str_replace($badchar, "", $requestdata['phone']);
        $data['phone_1'] = $onlynumbers;
    }

    if (isset($requestdata['full_address']) && !empty($requestdata['full_address'])) {
        $data['address_1'] = $requestdata['street_number'] . ' ' . $requestdata['street_name'];
    }

    if (isset($requestdata['zip']) && !empty($requestdata['zip'])) {
        $data['postal_code'] = $requestdata['zip'];
    }

    if (isset($requestdata['city']) && !empty($requestdata['city'])) {
        $data['city'] = $requestdata['city'];
    }
    if (isset($requestdata['state_abbr']) && !empty($requestdata['state_abbr'])) {
        $data['state'] = $requestdata['state_abbr'];
    }

    if (isset($requestdata['utility_cost']) && !empty($requestdata['utility_cost'])) {
        $data['utility.electric.monthly_amount'] = $requestdata['utility_cost'];
    }

    if (isset($requestdata['roof_shading']) && !empty($requestdata['roof_shading'])) {
        $data['roof_shade_solar'] = $requestdata['roof_shading'];
    }

    if (isset($requestdata['source']) && !empty($requestdata['ource'])) {
        $data['original_source'] = $requestdata['source'];
      
    }
    if (isset($requestdata['medium']) && !empty($requestdata['medium'])) {
        $data['channel_solar'] = $requestdata['medium'];
    }

    if (isset($requestdata['campaign']) && !empty($requestdata['campaign'])) {
        $data['tracking_campaign_solar'] = $requestdata['campaign'];
    }

    if (isset($requestdata['utm_content']) && !empty($requestdata['utm_content'])) {
        $data['sub_id_solar'] = $requestdata['utm_content'];
              $data['sub_group_solar'] = $requestdata['utm_content'];
        
    }

    if (isset($requestdata['trustedform']) && !empty($requestdata['trustedform'])) {
        $data['trustedform_cert_url'] = $requestdata['trustedform'];
    }

    if (isset($requestdata['url']) && !empty($requestdata['url'])) {
        $data['url'] = $requestdata['url'];
         $data['optin_url_solar'] = $requestdata['url'];
        //Code Added for mapping of paid ad click id's
        $queryString = parse_url($requestdata['url'], PHP_URL_QUERY);
        parse_str($queryString, $output);

        if (isset($output['fbclid']) && !empty($output['fbclid'])) {
            $data['custom_id_solar'] = $output['fbclid'];
        }

        if (isset($output['gclid']) && !empty($output['gclid'])) {
            $data['click_id_solar'] = $output['gclid'];
        }

        if (isset($output['msclkid']) && !empty($output['msclkid'])) {
            $data['ref_id_solar'] = $output['msclkid'];
        }
        
                if (isset($output['utm_content']) && !empty($output['utm_content'])) {
            $data['sub_id_solar'] = $output['utm_content'];
            $data['sub_group_solar'] = $output['utm_content'];
            
        }
        
                if (isset($output['utm_source']) && !empty($output['utm_source'])) {
            $data['original_source'] = $output['utm_source'];
        
        }else{
                  $data['original_source'] = "solarpowerincentives";
        }
        
                if (isset($output['utm_medium']) && !empty($output['utm_medium'])) {
            $data['channel_solar'] = $output['utm_medium'];
        }
        
                if (isset($output['utm_campaign']) && !empty($output['utm_campaign'])) {
            $data['tracking_campaign_solar'] = $output['utm_campaign'];
     
        }
    }


    
    $data['campaign_type_solar'] = 'Solar';
     $data['lead_source_name_solar'] = "Website - Momentum Solar";
    $url = $url;
    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($data),
            'timeout' => 30,
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    return $result;

}