<!doctype html>
<html>
  <head>
    <title>MomentumSolar</title>
    <meta name="description" content="">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> 
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400&display=swap" rel="stylesheet">
    
    <!--[if IE 6]>
	<style type="text/css">
		* html .group {
			height: 1%;
		}
	</style>
  <![endif]--> 
    <!--[if IE 7]>
	<style type="text/css">
		*:first-child+html .group {
			min-height: 1px;
		}
	</style>
  <![endif]--> 
    <!--[if lt IE 9]> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script> 
  <![endif]--> 
  <style>
    .idError {
      color:red;
      font-weight: bold;

    }
    </style>
  <script src="js/jquery.validate.js"></script>
    <script src="js/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
  

  </head>
  <body>
    <div class="artboard-1">
      <header class="header">
        <div class="container">
          <div class="row match-height-bootstrap-row">
            <div class="col-md-12 col-xs-12">
              <div class="col-3">
                <img class="layer-1" src="images/momentumlogo.png" alt="">
                <img class="layer-2" src="images/takeAdvantage.png" alt="">
                <img class="layer-3" src="images/client.png" alt="">
            
              </div>
            </div>

          </div><!-- row match-height-bootstrap-row -->

        </div> <!-- container -->
      </header>
      <div class="container bottomBanner">
      </div>
      <section class="section-wrapper">
        <div class="container">

        <div class="row match-height-bootstrap-row">
        <h1 class="heroheading">Tell us about yourself</h1>
          <div class="col-md-7 col-xs-12">
              <form  method="POST" id="leadformContainer"  style = "height:auto !important;" action="ajaxAmerica.php">
                <div  >
            
                        <div class="form-error" style="display:none">Please fix the errors below.</div>
                        <div class="form-group form-group2">
                            <input type="text" id="first_name" name="first_name" class="required" value="" placeholder="First Name*" autocomplete="off" aria-required="true" required>
                            <input type="text" id="last_name" name="last_name" class="required" value="" placeholder="Last Name*" autocomplete="off" aria-required="true" required>
                            <input type="text" id="address" name="address" class="required" value="" placeholder="Street Address*"  autocomplete="off" aria-required="true" required>
                            <input type="text" id="city" name="city" cvalue="" class="required" placeholder="City*" autocomplete="off" class="city" required>
                          
                            
                            <select name="state" id="state" aria-required="true" required>
                                <option value="">Select State*</option>
                        
                                <option value="CA">California</option>
                
                                <option value="CT">Connecticut</option>

                                <option value="FL">Florida</option>
                      
                          
                                <option value="NJ">New Jersey</option>
                      
                                <option value="NY">New York</option>
                        
                                <option value="PA">Pennsylvania</option>
                                <option value="TX">Texas</option>
                              
                            </select>
                            
                            <input type="tel" id="zip" name="zip" class="required" value="" placeholder="ZIP code*"  autocomplete="off" aria-required="true" required>
                            <input type="tel" id="phone" name="phone" class="required" value="" placeholder="Phone Number*" autocomplete="off" aria-required="true" required>
                            <input type="text" id="apptDateTime" name="apptDateTime" cvalue="" placeholder="Preferred Appointment Time" autocomplete="off" >
              <input type="text" id="market_america_id" name="market_america_id" class="required associate_id" value="<?php $_GET['utm'];?>" placeholder="Market America Associate ID*(Cxxxxxxx-xxxxxxxxxxxxxxxxxxxx/Rxxxxxxx-xxxxxxxxxxxxxxxxxxxx)" autocomplete="off"  aria-required="true"  required>
                            <div class="idError">
                                Please provide Market America Assoicate ID in correct format (Cxxxxxxx-xxxxxxxxxxxxxxxxxxxx OR Rxxxxxxx-xxxxxxxxxxxxxxxxxxxx)
                            </div>
                            <input type="hidden" name="page-name" value="solar-panels-market-america">
                            <input type="hidden" name="offerid" value="3839">
                            <input type="hidden" name="affiliateid" value="2">
                            <input type="hidden" name="advertiserid" value="3409">
                            <input type="hidden" name="dynamicid" value="<?php $_GET['utm'];?>">
                        </div>
                       
                </div>
              
            </div> <!-- col-md-5 col-xs-12 -->

            <div class="col-md-5 col-xs-12">
              
                <div style = "height:auto !important;">
                 
                        <small class="title3">Preferred Appointment Type</small>
                 
                        <div class="row checkboxRow2">
                     
                        <input type="radio" name="apptType" id="virtual" class="input-hidden" value="Virtual" />
                        <label for="virtual">
                          <img src="images/virtual.png" alt="Virtual" />
                        </label>
                        <input type="radio" name="apptType" id="inperson" class="input-hidden" value="In Person"/>
                        <label for="inperson">
                          <img src="images/inperson.png" alt="In Person" />
                        </label>
                        <input type="radio" name="apptType" id="nopreference" class="input-hidden" value="No Preference" />
                        <label for="nopreference">
                          <img src="images/nopreference.png" alt="No Preference" />
                        </label>
                       </div> <!-- checkboxRow2 -->
                        <input type="submit" class="submit btn btn-style2" id="submitbtn"/>
                </div>
              </form>
            </div> <!-- col-md-5 col-xs-12 -->



          </div> <!-- row match-height-bootstrap-row -->



          <div class="row" style="margin-top:3%;">
            <div class="col-xs-12">
            <div class="columnsContainer">

              <div class="leftColumn">
              <p class="text-6">Why Go Solar With Us?</p>
                    <p class="text-7">Worry-free maintenance &amp; monitoring</p>
                    <p class="text-8">Customized system design &amp; installation</p>
                    <p class="text-9">At cost roofing program offered for any necessary repairs or replacement</p>
                    <p class="text-10">Complimentary tree removal available for qualified homes</p>



              </div>

              <div class="rightColumn">
              <p class="text-11">What's in it for you?</p>
                    <p class="text-12">Zero out-of-pocket expenses</p>
                    <p class="text-13">A lower monthly electric bill</p>
                    <p class="text-14">A potential increase in the value of your home or commercial property</p>
                    <p class="text-15">Cleaner, price-protected power</p>
                    <p class="text-16">Government incentives available for purchased systems</p>
              </div>

              </div>
          </div>
        </div>
      </section>

      <section class="footer-wrapper">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <footer class="footer">
                <p class="text-17">4 EASY STEPS TO GOING TO SOLAR</p>
                <div class="row row-5 auto-clear">
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-14" src="images/step1.png" alt="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-13" src="images/step2.png" alt="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-15" src="images/step3.png" alt="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-16" src="images/step4.png" alt="">
                  </div>
                </div>
               
              </footer>
          
            </div>
          </div>
        </div>
      </section>
      <section class="section-wrapper">
      <p class="text-18">Pro Custom Solar LLC D|B|A Momentum Solar. Terms and conditions apply, and not every homeowner or business owner will qualify. Eligibility is determined by municipal utility, roof condition and roof space, azimuth, tree location, shading and other factors. Copyright © Momentum Solar 2020.</p>
    </section>
    </div>
        <script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>

    	<script>
    	const queryString = window.location.search;
        console.log(queryString);
        const urlParams = new URLSearchParams(queryString);
	    const product = urlParams.get('utm')
        console.log(product);
        document.getElementById('market_america_id').value=product
	</script>

  <script>
    $(document).ready(function(){
      $('.idError').css("display" , "none");
        $("#apptDateTime").focus( function() {
          $(this).attr({type: 'datetime-local'});
          });
       
    });
    $("#leadformContainer").submit(function(e){
            
            firstChar = String(document.getElementById("market_america_id").value);
            
            if ((firstChar.charAt(0).toLowerCase()) == "c" || (firstChar.charAt(0).toLowerCase()) == "r" ){
              
              $('.idError').css("display" , "none");
             
            }
            else {
             
              $('.idError').css("display" , "inline");
              document.getElementById("market_america_id").value = "";
              return false;
          
            }
        });
   
    </script>
  </body>
</html>