<!doctype html>
<html>
  <head>
    <title>MomentumSolar</title>
    <meta name="description" content="">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> 
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400&display=swap" rel="stylesheet">
    
    <!--[if IE 6]>
	<style type="text/css">
		* html .group {
			height: 1%;
		}
	</style>
  <![endif]--> 
    <!--[if IE 7]>
	<style type="text/css">
		*:first-child+html .group {
			min-height: 1px;
		}
	</style>
  <![endif]--> 
    <!--[if lt IE 9]> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script> 
  <![endif]--> 
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:300,300italic,regular,italic,500,500italic,700,700italic&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin,latin-ext">
    <script src="js/match-height.js"></script> 
    <script>
		$(function () {
			$('.match-height-bootstrap-row > * > *').matchHeight();
			$('.match-height > *').matchHeight();
		})
	</script>

  </head>
  <body>
    <div class="artboard-1">
      <header class="header">
        <div class="container">
          <div class="row match-height-bootstrap-row">
            <div class="col-md-7 col-xs-12">
              <div class="col-3">
                <img class="layer-1" src="images/layer_1.png" alt="">
                <img class="layer-2" src="images/layer_2.png" alt="">
                <img class="layer-3" src="images/layer_3.png" alt="">
                <img class="layer-4" src="images/layer_4.png" alt="">
              </div>
            </div>
            <div class="col-md-5 col-xs-12">
              <form class="form-form" method="POST" id="leadformContainer" >
                <div class="form">
                  <center>
                        <small class="title2">Let’s Get Started!</small></center>
                        <div class="form-error" style="display:none">Please fix the errors below.</div>
                        <div class="form-group form-group2">
                            <input type="text" id="first_name" name="first_name" class="required" value="" placeholder="First Name*" autocomplete="off" aria-required="true" required>
                            <input type="text" id="last_name" name="last_name" class="required" value="" placeholder="Last Name*" autocomplete="off" aria-required="true" required>
                            <input type="text" id="email" name="email" class="required" value="" placeholder="Email Address*" autocomplete="off" aria-required="true" required>
                            <input type="tel" id="zip" name="zip" class="required" value="" placeholder="ZIP code*"  autocomplete="off" aria-required="true" required>
                            <input type="tel" id="phone" name="phone" class="required" value="" placeholder="Phone Number*" autocomplete="off" aria-required="true" required>
               <input type="text" id="address" name="address" class="required" value="" placeholder="Address*"  autocomplete="off" aria-required="true" required>
              <select name="state" id="state" aria-required="true" required>
                  <option value="">--Select State--</option>
          
                  <option value="CA">California</option>
  
                  <option value="CT">Connecticut</option>

                  <option value="FL">Florida</option>
        
            
                  <option value="NJ">New Jersey</option>
        
                  <option value="NY">New York</option>
          
                  <option value="PA">Pennsylvania</option>
                  <option value="TX">Texas</option>
                
              </select>
              <input type="text" id="market_america_id" name="market_america_id" class="required" value="<?php $_GET['utm'];?>" placeholder="Market America Associate ID*" autocomplete="off" aria-required="true" required>
                            <input type="hidden" name="page-name" value="solar-panels-market-america">
                            <input type="hidden" name="offerid" value="3839">
                            <input type="hidden" name="affiliateid" value="2">
                            <input type="hidden" name="advertiserid" value="3409">
                            <input type="hidden" name="dynamicid" value="<?php $_GET['utm'];?>">
                        </div>
                        <button type="submit" class="submit btn btn-style2" id="submitbtn">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </header>
      <section class="section-wrapper">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <section class="section">
                <div class="text-4">
                  <p>SPEAK TO A SOLAR SPECIALIST IN YOUR AREA TODAY</p>
                </div>
                <div class="text-5">
                  <p>888-255-8000</p>
                </div>
                <img class="layer-9" src="images/layer_9.png" alt="">
                <div class="wrapper">
                  <div class="layer-10"></div>
                  <img class="layer-11" src="images/layer_9.png" alt="">
                  <div class="col-4">
                    <p class="text-6">Why Go Solar With Us?</p>
                    <p class="text-7">Worry-free maintenance &amp; monitoring</p>
                    <p class="text-8">Customized system design &amp; installation</p>
                    <p class="text-9">At cost roofing program offered for any necessary repairs or replacement</p>
                    <p class="text-10">Complimentary tree removal available for qualified homes</p>
                  </div>
                  <div class="right-2">
                    <p class="text-11">What's in it for you?</p>
                    <p class="text-12">Zero out-of-pocket expenses</p>
                    <p class="text-13">A lower monthly electric bill</p>
                    <p class="text-14">A potential increase in the value of your home or commercial property</p>
                    <p class="text-15">Cleaner, price-protected power</p>
                    <p class="text-16">Government incentives available for purchased systems</p>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="footer-wrapper">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <footer class="footer">
                <p class="text-17">4 EASY STEPS TO GOING TO SOLAR</p>
                <div class="row row-5 auto-clear">
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-14" src="images/layer_14.png" alt="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-13" src="images/layer_13.png" alt="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-15" src="images/layer_15.png" alt="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <img class="layer-16" src="images/layer_16.png" alt="">
                  </div>
                </div>
                <p class="text-18">Pro Custom Solar LLC D|B|A Momentum Solar. Terms and conditions apply, and not every homeowner or business owner will qualify. Eligibility is determined by municipal utility, roof condition and roof space, azimuth, tree location, shading and other factors. Copyright © Momentum Solar 2020.</p>
              </footer>
            </div>
          </div>
        </div>
      </section>
    </div>
        <script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script  src="js/jscriptAmerica.js"></script>
    <script  src="js/masketinput.js"></script>
    	<script>
    	const queryString = window.location.search;
        console.log(queryString);
        const urlParams = new URLSearchParams(queryString);
	    const product = urlParams.get('utm')
        console.log(product);
        document.getElementById('market_america_id').value=product
	</script>
  </body>
</html>