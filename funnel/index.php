<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Less Css -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <title>Funel Form</title>
  </head>
  <body>
    <section>
        <div class="container">
		
 <?php 
	  function generateRandomString($length = 10) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
	}
	  
	  $token = generateRandomString();
	  
	  ?>
            <div class="row">
                <div class="col d-flex text-center align-items-center" style="height:100vh;">
                    <!-- Step 1 -->
                    <div class="col form py-5 mx-auto step-1">
                        <h3 class="font-weight-light">Where will this project take place?</h3>
                        <p class="mb-5 mt-4">Your zip code will provide us with information on the amount of tax credit you are eligible to receive.</p>
                        <form id="step-1" class="needs-validation col-12 col-sm-10 mx-auto" novalidate>
                             <input id="zip" class="form-control mb-3 rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip" type="text">
                            <div class="invalid-feedback mb-4">
                                Please provide a valid zip.
                            </div>
							<input type="hidden" name="step" class="step" value="step1">
                            <input type="hidden" name="token" value="<?php echo $token;?>">
                            <button class="btn btn-primary rounded-0 btn-block" type="submit">Next</button>
                        </form>
                    </div>
                    
                    <!-- Step 2 -->
                    <div class="col form py-5 mx-auto step-2" style="display:none;">
                        <h3 class="font-weight-light mb-4">What is your average monthly electric bill?</h3>
                        <form id="step-2" class="needs-validation col-12 mx-auto" novalidate>
                            <div class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="Under $50" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline1">Under $50</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="$50 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline2">$50 - $150</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="$150 - $350" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline3">$150 - $350</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="$300 - $450" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline4">$300 - $450</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="Over $450" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline5">Over $450</label>
                            </div>
							<input type="hidden" name="step" class="step" value="step2">
                            <input type="hidden" name="token" value="<?php echo $token;?>">
                        </form>
                    </div>
                    
                    <!-- Step 3 -->
                    <div class="col form py-5 mx-auto step-3" style="display:none;">
                        <h3 class="font-weight-light">Please enter your home address.</h3>
                        <p class="mt-4 mb-5">This will allow our team to gather more information about your roof. We will not sell, trade or rent your personal information to others without your permission.</p>
                        <form id="step-3" class="needs-validation col-12 col-sm-10 mx-auto" novalidate>
                            <input type="text" name="address" class="form-control rounded-0" id="address" placeholder="Address">
                            <div class="invalid-feedback">
                                Please provide a valid Address.
                            </div>
							<input type="hidden" name="step" class="step" value="step3">
                            <input type="hidden" name="token" value="<?php echo $token;?>">
							<button class="btn btn-primary rounded-0 btn-block" type="submit">Next</button>
                        </form>
                    </div>

                    <!-- Step 4 -->
                    <div class="col py-5 form mx-auto step-4" style="display: none;">
                        <h3 class="font-weight-light mb-5">How much shade does your roof get from 9 AM - 3 PM?</h3>
						<form id="step-4">
                        <div class="row">
                            <div class="col-12 col-md-4 shade" id="no-shade">
                                <img src="images/roof-1.png">
                                <h5 class="mt-3">No Shade</h5>
                                <p>Entire roof is exposed to sunlight.</p>
								<input type="radio" class="no-shade" name="shade" value="No Shade">
                            </div>
                            <div class="col-12 col-md-4 shade" id="some-shade">
                                <img src="images/roof-2.png">
                                <h5 class="mt-3">Some Shade</h5>
                                <p>Parts of the roof are covered at certain times/all times.</p>
								<input type="radio" class="some-shade" name="shade" value="Some Shade">
                            </div>
                            <div class="col-12 col-md-4 shade" id="severe-shade">
                                <img src="images/roof-3.png">
                                <h5 class="mt-3">Severe Shade</h5>
                                <p>Most or all of the roof is covered at certain times/all times.</p>
								<input type="radio" class="severe-shade" name="shade" value="Severe Shade">
								<input type="hidden" name="step" class="step" value="step3">
                                <input type="hidden" name="token" value="<?php echo $token;?>">
								
                            </div>
                        </div>
						</form>
                    </div>

                    <!-- Step 5 -->
                    <div class="col py-5 form mx-auto step-5" style="display:none;">
                        <h3 class="font-weight-light mb-5">Please enter your contact information to receive a <b>no-obligation</b> detailed quote from our solar professionals.</h3>
                        <form class="needs-validation row" novalidate>
                            <div class="col-12 col-md-6">
                                <input type="text" class="form-control mb-3 rounded-0" id="validationCustom01" placeholder="First name" value="" required>
                                <div class="invalid-feedback">
                                    Please provide a valid Address.
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <input type="text" class="form-control mb-3 rounded-0" id="validationCustom02" placeholder="Last name" value="" required>
                                <div class="invalid-feedback">
                                    Please provide a valid Address.
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <input type="text" class="form-control mb-3 rounded-0" id="validationCustom03" placeholder="Email" value="" required>
                                <div class="invalid-feedback">
                                    Please provide a valid Address.
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <input type="text" class="form-control mb-3 rounded-0" id="validationCustom04" placeholder="Phone" value="" required>
                                <div class="invalid-feedback">
                                    Please provide a valid Address.
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-primary rounded-0" type="submit">Submit</button>
                            </div>
                            <div class="col">
                                <p class="mt-5">*By clicking the "Submit" button you agree to our Terms and Privacy Policy and authorize Momentum Solar to reach out to you with a no-obligation quote or to ask for additional information in order to provide an accurate quote.</p>
                            </div>
                        </form>
                        <div class="bot-sec mt-5" style="display:none;">
                            <div class="d-flex col- mx-o justify-content-between">
                                <p class="back d-flex align-items-center"><span><i class="fas fa-chevron-left"></i></span>Back</p>
                                <button class="btn btn-primary rounded-0" type="submit">Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="close">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="js/validation.js"></script>
    <script src="js/jquery.validate.js"></script>
  </body>
</html>