jQuery(document).ready(function(){
	
	jQuery('#step-1').validate({
			rules: {
				zip: {required: true},
			},
			messages: {
				zip: "Please enter zipcode.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-1").hide();
				 $(".step-2").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	
	jQuery(".electicbill_radio").on('change',function(){
		var data = jQuery('#step-2').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-2").hide();
				 $(".step-3").fadeIn( 2000 );
				  
			});	
	})
	
	
	
	jQuery('#step-3').validate({
			rules: {
				address: {required: true},
			},
			messages: {
				address: "Please enter address.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#step-3').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-3").hide();
				 $(".step-4").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	
	
	jQuery(".shade").on('click',function(){
		
		var divid = jQuery(this).attr('id');
		$("."+divid).prop('checked',true);
		var data = jQuery('#step-4').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-4").hide();
				 $(".step-5").fadeIn( 2000 );
				  
			});	
	})
	
	
	
	jQuery('#solar_quote_basic_2_utility').validate({
			rules: {
				utility_cost: {required: true},
			},
			messages: {
				utility_cost: "Please select utility_cost.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_2_utility').serializeArray();
		 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step4").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	
	 
	jQuery('#solar_quote_basic_utility_form').validate({
			rules: {
				selectOther: {required: true},
			},
			messages: {
				selectOther: "Please select utility provider.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_utility_form').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step5").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_enter_address_form').validate({
			rules: {
				full_address: {required: true},
			},
			messages: {
				full_address: "Please enter full address.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_enter_address_form').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step7").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_4_manual_address').validate({
			rules: {
				street_number: {required: true},
				street_name: {required: true},
			},
			messages: {
				street_number: "Please select utility street number.",
				street_name: "Please select utility street name.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_4_manual_address').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				initialize();
				$(".step7").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_markerlocation').validate({
			rules: {
				latitude: {required: true},
				longitude: {required: true},
			},
			messages: {
				latitude: "Please select utility street number.",
				longitude: "Please select utility street name.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_markerlocation').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step8").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_roof_type').validate({
			rules: {
				roof_type: {required: true},
				 
			},
			messages: {
				roof_type: "Please select roof type.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_roof_type').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step9").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_roof_shading').validate({
			rules: {
				roof_shading: {required: true},
				 
			},
			messages: {
				roof_shading: "Please select roof shades.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_roof_shading').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step10").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_roof_pitch').validate({
			rules: {
				roof_pitch: {required: true},
				 
			},
			messages: {
				roof_pitch: "Please select roof pitch.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_roof_pitch').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step12").fadeIn( 2000 );
				setTimeout(function(){
					$("#nameBeforeSpinner").hide();
					$("#nameAfterSpinner").show();
				
				}, 2000);
				  
			});	
			return false; 
		}
	});
	
	 
	
	jQuery('#solar_quote_basic_full_name').validate({
			rules: {
				fname: {required: true},
				lname: {required: true},
				 
			},
			messages: {
				fname: "Please enter fname.",
				lname: "Please enter lname.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_full_name').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step13").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_email').validate({
			rules: {
				email: {required: true,email: true},
			},
			messages: {
				email: "Please enter email.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_email').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step14").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	jQuery('#solar_quote_basic_phone').validate({
			rules: {
				phone: {required: true},
			},
			messages: {
				phone: "Please enter phone.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_phone').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step18").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	 
	
	jQuery('#solar_quote_basic_payment_option').validate({
			rules: {
				payment_option: {required: true},
			},
			messages: {
				payment_option: "Please select installer number.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_payment_option').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				$(".step19").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_credit_score').validate({
			rules: {
				credit_score: {required: true},
			},
			messages: {
				credit_score: "Please select installer number.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_credit_score').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				window.location.href = "https://momentumsolar.com/solarenergyquote/thankyou.html";	
				//$(".basic-slider-calc").hide();
				//$(".step20").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
	
	jQuery('#solar_quote_basic_best_time').validate({
			rules: {
				best_day: {required: true},
				best_time: {required: true},
			},
			messages: {
				best_day: "Please select best day.",
				best_time: "Please select best time.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#solar_quote_basic_best_time').serializeArray();
			 
			request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				$(".basic-slider-calc").hide();
				window.location.href = "https://momentumsolar.com/solarenergyquote/thankyou.html";	
				  
			});	
			return false; 
		}
	});
	
	 
	 
	
	 
	
})
