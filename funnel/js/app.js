jQuery(document).ready(function(){
	
	jQuery('#step-1').validate({
			rules: {
				zip: {required: true},
			},
			messages: {
				zip: "Please enter zipcode.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-1").hide();
				 $(".step-2").fadeIn( 2000 );
				  
			});	
			return false; 
		}
	});
 
})
