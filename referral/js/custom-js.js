
    jQuery(function ($) {
        // init the state from the input
        $(".image-checkbox-ac").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-checked');
            }
            else {
                $(this).removeClass('image-checkbox-ac-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-ac ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-ac-checked')) {
                $(this).removeClass('image-checkbox-ac-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-ac-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });


        $(".image-checkbox-fridge").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-fridge-checked');
            }
            else {
                $(this).removeClass('image-checkbox-fridge-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-fridge ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-fridge-checked')) {
                $(this).removeClass('image-checkbox-fridge-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-fridge-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });

        $(".image-checkbox-lighting").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-lighting-checked');
            }
            else {
                $(this).removeClass('image-checkbox-lighting-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-lighting ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-lighting-checked')) {
                $(this).removeClass('image-checkbox-lighting-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-lighting-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });

        $(".image-checkbox-microwave").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-microwave-checked');
            }
            else {
                $(this).removeClass('image-checkbox-microwave-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-microwave ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-microwave-checked')) {
                $(this).removeClass('image-checkbox-microwave-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-microwave-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });

        $(".image-checkbox-internet").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-internet-checked');
            }
            else {
                $(this).removeClass('image-checkbox-internet-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-internet").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-internet-checked')) {
                $(this).removeClass('image-checkbox-internet-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-internet-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });

        $(".image-checkbox-dishwasher").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-dishwasher-checked');
               
            }
            else {
                $(this).removeClass('image-checkbox-dishwasher-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-dishwasher ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-dishwasher-checked')) {
                $(this).removeClass('image-checkbox-dishwasher-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-dishwasher-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });

        $(".image-checkbox-tv").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-tv-checked');
            }
            else {
                $(this).removeClass('image-checkbox-ac-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-tv ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-tv-checked')) {
                $(this).removeClass('image-checkbox-tv-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-tv-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });
        $(".image-checkbox-washingmachine").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-washingmachine-checked');
            }
            else {
                $(this).removeClass('image-checkbox-washingmachine-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-washingmachine ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-washingmachine-checked')) {
                $(this).removeClass('image-checkbox-washingmachine-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-washingmachine-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });
        $(".image-checkbox-coffeemaker").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-coffeemaker-checked');
            }
            else {
                $(this).removeClass('image-checkbox-coffeemaker-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-coffeemaker ").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-coffeemaker-checked')) {
                $(this).removeClass('image-checkbox-coffeemaker-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-coffeemaker-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });

        $(".image-checkbox-waterheater").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-waterheater-checked');
            }
            else {
                $(this).removeClass('image-checkbox-waterheater-checked');
            }
        });

        // sync the state to the input
        $(".image-checkbox-waterheater").on("click", function (e) {
            if ($(this).hasClass('image-checkbox-waterheater-checked')) {
                $(this).removeClass('image-checkbox-waterheater-checked');
                $(this).find('input[type="checkbox"]').first().removeAttr("checked");
            }
            else {
                $(this).addClass('image-checkbox-waterheater-checked');
                $(this).find('input[type="checkbox"]').first().attr("checked", "checked");
            }

            e.preventDefault();
        });

    });
