<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<title>Momentum Solar</title>
<style>
  body {
    text-align: center;
    margin: auto;
    margin-top: 8%;
    font-size: 28px;
    padding: 5%;
  }
  </style>
<script>
function toValidDate(datestring){
    return datestring.replace(/(\d{2})(\/)(\d{2})/, "$3$2$1");   
}
var query = window.location.search.substring(1);
var qs = parse_query_string(query);
var currentdatetime = new Date().toLocaleString();

console.log(qs.event_type_name); // event name utmcamp, utmsource is calendly
console.log(qs.event_start_time); // start time choosen
console.log(qs.event_end_time); // end time choosen
console.log(qs.event_type_uuid); // calendly uid for event type

console.log(qs.invitee_first_name);
console.log(qs.invitee_last_name);
console.log(qs.invitee_email);
console.log(qs.answer_1); //phone

console.log(qs.answer_2); // address
console.log(qs.answer_3); // zip


console.log(qs.event_start_time.toLocaleString()); // current date and time
var utmsource ="";
var utmmedium="";
var utmcampaign="";
var utmcontent="";
var clickid ="";
var leadvendorsubgroup="";
var utmurl = window.location.href; 
var optinurl = "Website Virtual Appt Calendly";
var leadvendoraffiliatesource = "Calendly Virtual Appointment";
console.log(currentdatetime); // current date and time
if (window.location.href.indexOf('&utm_source=') > 0) {
    console.log(qs.utm_source); //utm source
    utmsource=qs.utm_source;
    optinurl = $.cookie("optinurl");
    
}
if (window.location.href.indexOf('&utm_medium=') > 0) {
    console.log(qs.utm_medium); //utm medium
    utmmedium=qs.utm_medium;
}
if (window.location.href.indexOf('&utm_campaign=') > 0) {
    console.log(qs.utm_campaign); //utm campaign
    utmcampaign=qs.utm_campaign;
}
if (window.location.href.indexOf('&utm_content=') > 0) {
    console.log(qs.utm_content); //utm content
    utmcontent=qs.utm_content;
    leadvendorsubgroup = qs.utm_content;
}


console.log (optinurl);
var date = new Date(toValidDate(qs.event_start_time));
var dateStr =
  ("00" + (date.getMonth() + 1)).slice(-2) + "/" +
  ("00" + date.getDate()).slice(-2) + "/" +
  date.getFullYear() + " " +
  ("00" + date.getHours()).slice(-2) + ":" +
  ("00" + date.getMinutes()).slice(-2) + ":" +
  ("00" + date.getSeconds()).slice(-2);

var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");

//var raw = JSON.stringify({"locationurl":"online","phone":qs.answer_1,"monthlyelectricbill":"","starttime":dateStr, "createdtime":currentdatetime,"altphone":"","address":qs.answer_2,"product":"Solar","city":"New York","fname":qs.invitee_first_name,"state":"New York","zipcode":qs.answer_3,"lname":qs.invitee_last_name,"utility":"Momentum Energy","comments":"VIRTUAL APPT – ASSIGN juanrodriguez@momentumsolar.com or agyrla@momentumsolar.com","shade":"","utmcamp":utmcampaign,"utmsource":utmsource,"utmchannel":utmmedium,"clickid":clickid,"subid":utmcontent,"repemail":"","email":qs.invitee_email, "leadvendorsubgroup":leadvendorsubgroup, "optinurl":optinurl, "leadvendoraffiliatesource": leadvendoraffiliatesource });
var raw = JSON.stringify({"phone":qs.answer_1,"demo_date_time":dateStr, "address":qs.answer_2,"city":"New York","fname":qs.invitee_first_name,"state":"New York","lname":qs.invitee_last_name });


var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

fetch("https://sjgk4x31xl.execute-api.us-east-1.amazonaws.com/api/addcrmleadappdev", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));


function parse_query_string(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
 
    } else if (typeof query_string[key] === "string") {
      var arr = [query_string[key], decodeURIComponent(value)];
      query_string[key] = arr;
    
    } else {
      query_string[key].push(decodeURIComponent(value));
    }
  }
  return query_string;
}


  </script>
</head>
<body>

<h1>Thank you! Your appointment has been confirmed.</h1>
You can now visit Momentum Solar home page by clicking <a href="https://dev.momentumsolar.com/wp/">here</a>.


</body>
</html>
