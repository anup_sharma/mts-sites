@app.route('/addcrmleadapp', methods=['POST'])
def addcrmleadapp():
    print("WebsiteLead")
    token = crmtoken()
    request_body = app.current_request.json_body
    print(request_body)
    try:
        loglead(request_body)
    except Exception as e:
        print(e)

    fname = request_body['fname']
    lname = request_body['lname']
    name = "Website Appt {} {}".format(fname, lname)
    phone = request_body['phone']
    address = request_body['address']
    product = "Solar"
    city = request_body['city']
    state = request_body['state']
    comments = "Momentumsolar.com appointments lead "
    canvasser = "website@momentumsolar.com"

    crmurl = 'https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    if 'demo_date_time' in request_body:
        demodatetime = request_body['demo_date_time']
        crmbody['mts_appointmentstarttime'] = demodatetime
    if 'cb_date_time' in request_body:
        demodatetime = request_body['cb_date_time']
        crmbody['mts_appointmentstarttime'] = demodatetime

    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_city'] = city
    crmbody['mts_firstname'] = fname
    crmbody['mts_state'] = state
    crmbody['mts_lastname'] = lname
    crmbody['mts_leadadditionalcomments'] = comments
    crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(27d5be09-6826-e911-a970-000d3a32c8b8)"
    crmbody['mts_returnleadsource'] = 'Momentum Solar Website'

    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverify(canvasser, phone)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}