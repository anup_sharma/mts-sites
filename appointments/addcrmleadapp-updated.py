@app.route('/addcrmleadapp', methods=['POST'])
def addcrmleadapp():
    token = crmtoken()
    request_body = app.current_request.json_body
    print(request_body)
    print("Calendly")
    fname = request_body['fname']
    lname = request_body['lname']
    locationurl = request_body['locationurl']
    phone = request_body['phone']
    altphone = request_body['altphone']
    email = request_body['email']
    repemail = request_body['repemail']
    address = request_body['address']
    product = "Solar"
    canvasser = "website@momentumsolar.com"
    utmcamp = request_body['utmcamp']
    utmsource = request_body['utmsource']
    utmchannel = request_body['utmchannel']
    clickid = request_body['clickid']
    subid = request_body['subid']
    comments = request_body['comments']
    leadvendorsubgroup = request_body['leadvendorsubgroup']
    optinurl = request_body['optinurl']
    leadvendoraffiliatesource = request_body['leadvendoraffiliatesource']

    name = "Website Virtual Appt {} {}".format(fname, lname)
    crmurl = 'https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    crmbody['mts_email'] = email
    crmbody['mts_locationurl'] = locationurl
    crmbody['mts_altphone1'] = altphone
    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_assigneeemail'] = repemail
    if 'demo_date_time' in request_body:
        demodatetime = request_body['demo_date_time']
        crmbody['mts_appointmenttimeind'] = demodatetime


    if 'cb_date_time' in request_body:
        demodatetime = request_body['cb_date_time']
        crmbody['mts_appointmentstarttime'] = demodatetime
    if 'city' in request_body:
        city = request_body['city']
        crmbody['mts_city'] = city
    if 'state' in request_body:
        state = request_body['state']
        crmbody['mts_state'] = state
    if "zip" in request_body:
        crmbody['mts_zipcode'] = request_body['zip']
    crmbody['mts_firstname'] = fname
    crmbody['mts_lastname'] = lname
    crmbody['mts_leadvendortrackingcampaign'] = utmcamp
    crmbody['mts_leadvendorsource'] = utmsource
    crmbody['mts_leadvendorchannel'] = utmchannel
    crmbody['mts_leadvendorclickid'] = clickid
    crmbody['mts_leadvendorsubid'] = subid
    crmbody['mts_leadvendoroptinurl'] = optinurl
    crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(e82ed355-a664-eb11-a812-000d3a3baf24)"
    crmbody['mts_returnleadsource'] = 'Website Calendly'
    crmbody['mts_leadadditionalcomments'] = comments
    crmbody['mts_demoappointmenttype'] = 962080000
    crmbody['mts_leadvendoraffiliatesource'] = leadvendoraffiliatesource
    if "leadvendorsubgroup" in request_body:
        crmbody['mts_leadvendorsubgroup'] = leadvendorsubgroup

    if len(name) < 1:
        return {'success': 'failed'}

    if len(phone) < 1:
        return {'success': 'failed'}

    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverifydevb(repemail, phone)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}