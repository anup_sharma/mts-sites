   function startQuote() {
	  		ga('set', 'page', '/quote/zip/');
			ga('send', 'pageview');
			fbq('track', 'Started Quote');
	}

jQuery(document).ready(function($){
   /* $("#myCarousel").swiperight(function() {
      $(this).carousel('prev');
    });
   $("#myCarousel").swipeleft(function() {
      $(this).carousel('next');
   }); */
   


   
   
   jQuery('#step-1-form').validate({
			rules: {
				zip2: {required: true},
			},
			messages: {
				zip2: "Please enter zipcode.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			//ga('set', 'page', '/quote/zip/');
			//ga('send', 'pageview');
			ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-1-form').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			jQuery("form#step-1 #zip").val(jQuery('#step-1-form #zip2').val());
			request.done(function( response ) {
				$('#myModal').modal('show');
				 $("#myModal .step-1").hide();
				 $("#myModal .step-2").fadeIn( 1000 );
				  
			});	
			return false; 
		}
	});
   

   
	jQuery('#step-1').validate({
			rules: {
				zip: {required: true},
			},
			messages: {
				zip: "Please enter zipcode.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			ga('set', 'page', '/quote/bill/');
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-1').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-1").hide();
				 $(".step-2").fadeIn( 1000 );
				  
			});	
			return false; 
		}
	});
	
	
	
	jQuery('#step-2').validate({
			rules: {
				customRadioInline1: {required: true},
			},
			messages: {
				customRadioInline1: "Please Select Electic Bill Range.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			ga('set', 'page', '/quote/address/');
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-2').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-2").hide();
				 $(".step-3").fadeIn( 1000 );
				  
			});	
			return false; 
		}
	});
	
	
	jQuery(".custom-radio").on('click',function(){
		$(".custom-control").css({'background':'#FFFFFF','color':'#000000'});
		var checkedid = $(this).attr('id');
		$('#customRadioInline'+checkedid).prop('checked', true);
		$("#"+checkedid).css({'background':'#009fdf','color':'#FFFFFF'});
	})
	
	 
	
	
	jQuery('#step-3').validate({
			rules: {
				full_address: {required: true},
			},
			messages: {
				full_address: "Please enter address.",
			},
		submitHandler: function(form) {
			//ga('send', 'pageview', '/quote/address/');
			ga('set', 'page', '/quote/shadow/');
			ga('send', 'pageview');
			// Hide the Message holder incase it was visible in the last term.
			var data = jQuery('#step-3').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-3").hide();
				 $(".step-4").fadeIn( 1000 );
				  
			});	
			return false; 
		}
	});
	
	
	jQuery(".shade").on('click', function() {
		var divid = jQuery(this).attr('id');

		$(".shade-image").css('border-color', 'transparent'); //removes blue color when out of focus
		$(".shade-image").removeClass('joomdev');
		$(".step-4 H5 .fas").css('color','#414141');
		// $(".shade-image:after").css('background', 'none');

		
		$("#"+divid+" .shade-image").css('border-color','#00aeef'); //add blue color when in focus
		$("#"+divid+" .shade-image").css('background','rgba(0,0,0,0.2)');
		$("#"+divid+" .shade-image").css('background','rgba(0,0,0,0.2)');
		$("#"+divid+" H5 .fas").css('color','#00aeef');

		// $("#"+divid+" .shade-image").css('border-radius', '60px');
		

		$("."+divid).prop('checked', true);

	 });
	 
	// jQuery(".shade").on('click', function() {
	// 		var divid = jQuery(this).attr('id');
	// 		$("#"+divid+" .shade-image").css('border-radius', '60px');
	// 	}).on('focusout', function(){
	// 		var divid = jQuery(this).attr('id');
	// 		$("#"+divid+" .shade-image").css('border-radius', '10px');
	// });

	jQuery(".custom-radio1").on('click',function(){
		$(".custom-control-shade").css({'background':'#FFFFFF','color':'#000000'});
		var checkedid = $(this).attr('id');
		$('#customRadioInline'+checkedid).prop('checked', true);
		$("#"+checkedid).css({'background':'#009fdf','color':'#FFFFFF'});
	})
	
	
	
	jQuery('#step-4').validate({
			rules: {
				shade: {required: true},
			},
			messages: {
				shade: "Please Select Home shade.",
			},
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			ga('set', 'page', '/quote/contact/');
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/zip/');
			fbq('track', 'Started Quote');
			var data = jQuery('#step-4').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-4").hide();
				 $(".step-5").fadeIn( 1000 );
				  $target = $('#scrolltoelement');
				  $('.modal-dialog').animate({
					scrollTop: 0
				  }, 'show');
						  
			});	
			return false; 
		}
	});
	
	
	 jQuery('#step-5').validate({

			rules: {
				first_name: {required: true},
				last_name: {required: true},
				email: {required: true,email: true},
				phone: {required: true},
				
			},
			messages: {
				first_name: "Please enter first name.",
				last_name: "Please enter last name.",
				email: "Please enter valid email.",
				phone: "Please enter valid phone.",
			},
			 
		submitHandler: function(form) {
			// Hide the Message holder incase it was visible in the last term.
			$(".final_submit").hide();
			$(".loader_image").show();
			var data = jQuery('#step-5').serializeArray();
			ga('set', 'page', '/quote/thanks/');
			ga('send', 'pageview');
			//ga('send', 'pageview', '/quote/thanks/');
			gtag('event', 'conversion', {'send_to': 'AW-922148957/0agnCJjG9JMBEN3A27cD'});
			fbq('track', 'Lead');
			window.uetq = window.uetq || []; 
			window.uetq.push('event', 'Lead', {});  
			window.uetq.push({ 'ec': 'Lead', 'ea': 'Lead', 'el': 'Lead', 'ev': 3 });
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				 $(".step-5").hide();
				 $(".step-6").fadeIn( 1000 );
				  
			});	
			return false; 
		}
	});
	 
	 
	
	 
	
})

function gotoStep(step){
	
	var current = step + 1;
	
	if(step == 5 )
		 $(".modal-content").css('margin-top','70% !important');
	
	$(".step-"+current).hide();
	$(".step-"+step).show();
	
}
