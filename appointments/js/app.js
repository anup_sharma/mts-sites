jQuery(document).ready(function($){
	
	$('.phone_us').mask('(000) 000-0000');
	
	jQuery('.schedule_call').validate({
			rules: {
				fname: {required: true},
				lname: {required: true},
				phone: {required: true},
				date: {required: true},
				time: {required: true},
			},
			messages: {
				fname: "Please enter first name.",
				lname: "Please enter last name.",
				phone: "Please enter valid phone.",
				date: "Please enter date.",
				time: "Please enter time.",
			},
		submitHandler: function(form) {
			jQuery(".btn-primary").hide();
			jQuery(".loader_image").show();
			var data = jQuery('.schedule_call').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
				  snaptr('track','SIGN_UP');
				  fbq('track', 'APPT Scheduled Call');
				  location.href = "/appointments/thank-you.php";
				  //jQuery(".loader_image").hide();
				  //jQuery(".scheduledacallForm").hide();
				  //jQuery(".response_div").show();
			});	
			return false; 
		}
	});
	
	jQuery('.book_appointment').validate({
		
			rules: {
				fname: {required: true},
				lname: {required: true},
				phone: {required: true},
				full_address: {required: true},
				zip: {required: true},
				date: {required: true},
				time: {required: true},
			},
			messages: {
				fname: "Please enter your first name.",
				lname: "Please enter your last name.",
				phone: "Please enter valid phone.",
				full_address: "Please enter full address.",
				zip: "Please enter zip.",
				date: "Please enter date.",
				time: "Please enter time.",
			},
		submitHandler: function(form) {
			jQuery(".btn-primary").hide();
			jQuery(".loader_image").show();
			var data = jQuery('.book_appointment').serializeArray();
		 	request = jQuery.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			request.done(function( response ) {
					snaptr('track','SIGN_UP');
					//fbq('track', 'APPT Demo Booked');
					//$('#myModal_b_app').hide();
			        
					$('#myModal_b_app').modal('hide'); 
					$('#myModal-Calendly').modal('show');
					
					//$('#myModal_b_calendly').show();
					//console.log ('test response below');
					//console.log(response);
					//location.href = "/appointments-dev/thank-you.php";
				   //jQuery(".bookappointmentForm").hide();
				   //jQuery(".loader_image").hide();
				   //jQuery(".response_div").show();
			});	
			return false; 
		}
	});
	
})
 
