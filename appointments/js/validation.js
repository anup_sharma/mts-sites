// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }else{
			   event.preventDefault();
               event.stopPropagation();
			fbq('track', 'Started Quote');  
			var data = $('#step-1-form').serializeArray();
		 	$.ajax({
				method: "POST",
				url: "ajax.php",
				data: data
			})
			.done(function( response ) {
				var customformtoken = $(".customformtoken").val();
				$('#myModal').modal('show');
				$('#frame').contents().find('.step-1').hide(); 
				$('#frame').contents().find('.step-2').show(); 
				$('#frame').contents().find('input[name="token"]').val(customformtoken);
				
			});	
		
			   
		  }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
	
	/*  $(".button-go-text").on('click',function(){
	  $('#myModal').modal('show');
  }) */
	
  })();