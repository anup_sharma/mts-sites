<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.0.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
<title>Momentum Solar</title>
<style>
  body {
    text-align: center;
    margin: auto;
    margin-top: 8%;
    font-size: 28px;
    padding: 5%;
  }
  </style>
<script>

  
function toValidDate(datestring){
    return datestring.replace(/(\d{2})(\/)(\d{2})/, "$3$2$1");   
}
var query = window.location.search.substring(1);
var qs = parse_query_string(query);
var currentdatetime = new Date().toLocaleString();
var date = new Date(toValidDate(qs.event_start_time));
//alert (qs.event_start_time);
////alert (date);

var dateStr =
  ("00" + (date.getMonth() + 1)).slice(-2) + "/" +
  ("00" + date.getDate()).slice(-2) + "/" +
  date.getFullYear() + " " +
  ("00" + date.getHours()).slice(-2) + ":" +
  ("00" + date.getMinutes()).slice(-2) + ":" +
  ("00" + date.getSeconds()).slice(-2);
//set date time value in cookie
//alert (dateStr);
function parse_query_string(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
 
    } else if (typeof query_string[key] === "string") {
      var arr = [query_string[key], decodeURIComponent(value)];
      query_string[key] = arr;
    
    } else {
      query_string[key].push(decodeURIComponent(value));
    }
  }
  return query_string;
}
  </script>
</head>
<body>
<?php
//show php errors
require_once 'unirest-php/src/Unirest.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

  $data['fname']	 	= $_GET['invitee_first_name'];
  $data['lname']	 	= $_GET['invitee_last_name'];
  $data['city']	 	= "";
  $data['state']	 	= "";
  
  $data['email']	 	= $_GET['invitee_email'];
  $data['phone']	 	= $_GET['answer_1'];
  $data['address']	 	= $_GET['answer_2'];
  $data['zip']	 	= $_GET['answer_3'];

  //echo "existing date value " . $_GET['event_start_time'] . "<br/>"; 
 

 $array = explode('T', $_GET['event_start_time']);
 $timearray = explode('-', $array[1]);
 //echo "timearray value" , $timearray [0] . "<br/>";
 $newdate2 =  ($array[0] . ' ' . $timearray [0]);
 //echo "newdate2 value" . $newdate2 . "<br/>";
  $sec = strtotime($newdate2);  
  $newdate = date ("m/d/Y H:i", $sec);  
 $newdate = $newdate . ":00"; 

  $data['demo_date_time']	 	=$newdate;
 // echo "converted new date value = " . $data['demo_date_time'];
  
  $data['repemail']	 	= "";
  $data['locationurl']	 	= "online";
  $data['altphone']	 	= "";
  $data['utmcamp']	 	= "";
  $data['utmsource']	 	= "";
  $data['utmchannel']	 	= "";
  $data['utmcontent']	 	= "";
  $data['utmcampaign']	 	= "";
  $data['clickid']	 	= "";
  $data['subid']	 	= "";
  $data['comments']	 	= "VIRTUAL APPT – ASSIGN juanrodriguez@momentumsolar.com or agyrla@momentumsolar.com";
  $data['leadvendorsubgroup']	 	= "";
  $data['optinurl']	 	= "Website Virtual Appt Calendly";
  $data['leadvendoraffiliatesource']	 	= "Calendly Virtual Appointment";
  
  if(isset($_GET['utm_source']))
  {
    $data['utmsource'] = $_GET['utm_source'];
    if(isset($_COOKIE['optinurl'])){
      $data['optinurl']	= $_COOKIE['optinurl'];
    }
  }
  if(isset($_GET['utm_content']))
  {
    $data['subid'] = $_GET['utm_content'];
    $data['leadvendorsubgroup'] = $_GET['utm_content'];
  }
  if(isset($_GET['utm_campaign']))
  {
    $data['utmcamp'] = $_GET['utm_campaign'];
  }
  if(isset($_GET['utm_medium']))
  {
    $data['utmchannel'] = $_GET['utm_medium'];
  }
$headers = array('Accept' => 'application/json',  'Content-Type' => 'application/json');
//echo $data['optinurl'];

$body = Unirest\Request\Body::json($data);

$response = Unirest\Request::post('https://sjgk4x31xl.execute-api.us-east-1.amazonaws.com/api/addcrmleadapp', $headers, $body);

//echo json_encode($response->body);
?>
<h1>Thank you! Your appointment has been confirmed.</h1>
You can now visit Virtual Appointment page by clicking <a href="https://momentumsolar.com/appointments/">here</a>.
<script>
         setTimeout(function(){
            window.location.href = 'https://momentumsolar.com/appointments/';
         }, 5000);
      </script>


</body>
</html>
