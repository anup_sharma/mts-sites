<!-- <?php

$phone = "(844) 592-8056";

if($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_PA") {

    $phone = "(844) 592-8056";

} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Remarketing") {

    $phone = "(844) 249-0102";

} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_PA") {

    $phone = "(855) 997-7495";

} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_CT") {

    $phone = "(855) 338-0690";

} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_CT") {

    $phone = "(855) 782-0757";

} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_FL") {

    $phone = "(866) 426-9323";

} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_FL") {

    $phone = "(855) 965-1765";

} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_TX") {

    $phone = "(844) 731-0129";

} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_TX") {

    $phone = "(844) 790-2124";

} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_NY") {

    $phone = "(855) 997-3041";

} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_NY") {

    $phone = "(844) 247-3551";

} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_PA") {

    $phone = "(844) 263-0006";

} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_FL") {

    $phone = "(844) 249-0568";

} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_CT") {

    $phone = "(844) 259-1553";

} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_TX") {

    $phone = "(844) 264-4824";

} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_NY") {

    $phone = "(844) 261-9076";

}



?> -->





<!DOCTYPE html>

<html lang="en">



<head>

	<meta charset="utf-8">

	<meta name="robots" content="noindex" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="">

	<meta name="author" content="">

	<title>Momentum Solar</title>

	<!-- Bootstrap core CSS -->

	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<link href="css/bootstrap-modal-ios.css" rel="stylesheet">

	<!--Font Awsome-->

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"

	crossorigin="anonymous">

	<!-- Custom styles for this template -->

	<link href="css/new.css" rel="stylesheet">

	<!-- Fonts -->

	<link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,600" rel="stylesheet">



</head>



<body id="page-top">

	<div class="phonetop">

		<a href="tel:<?php echo $phone; ?>"><img src="images/phone-black.png" alt="<?php echo $phone; ?>"></a>

	</div>

	<div class="fixed-bottom d-block d-flex justify-content-between d-sm-none px-5">

		<a class="btn btn-primary rounded-0 my-3 text-white pull-left" type="submit" href="tel:<?php echo $phone; ?>">Call Us</a>

		<a class="btn btn-primary rounded-0 my-3 text-white pull-right" type="submit" data-toggle="modal" data-target="#myModal_schedule_call">Schedule A Call</a>

	</div>

	<!-- Navigation -->

	<nav class="navbar navbar-expand-lg bg-white navbar-light fixed-top p-0" id="mainNav">

		<div class="logo-container">

			<a class="navbar-brand js-scroll-trigger pl-lg-5 pl-0 py-3 py-lg-0 mr-0">

				<img src="images/logo.png" alt="logo" class="text-center sitelogo">

				<!--button class="btn btn-primary rounded-0 btn-sm d-none text-center my-3 top-cta" type="submit" data-toggle="modal"

					data-target="#myModal">Free Quote</button-->

				</a>

			</div>

			<div class="container-fluid p-0 d-flex justify-content-center">

			<!--button class="navbar-toggler mr-3 hamburger-icon" type="button" data-toggle="collapse" data-target="#navbarResponsive"

			 aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

				<span class="navbar-toggler-icon"></span>

			</button-->

			<div class="collapse navbar-collapse" id="navbarResponsive">

				<ul class="navbar-nav ml-auto">

					<li class="nav-item phonenav">

						<!--a class="nav-link js-scroll-trigger phonenumber" href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone; ?></a-->

						<a class="nav-link js-scroll-trigger phonenumber" href="tel:1-888-666-36886"><i class="fa fa-phone" aria-hidden="true"></i> 1 (888) MOMENTUM</a>

						

					</li>

					<li class="nav-item free-quote">

						<a class="nav-link js-scroll-trigger text-white px-5 free-quote-text" OnClick="startQuote()" style="cursor:pointer" data-toggle="modal"

						data-target="#myModal_schedule_call">Schedule A Call</a>

					</li>

				</ul>

			</div>

		</div>

	</nav>

	<?php 

	function generateRandomString($length = 10) {

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		$charactersLength = strlen($characters);

		$randomString = '';

		for ($i = 0; $i < $length; $i++) {

			$randomString .= $characters[rand(0, $charactersLength - 1)];

		}

		return $randomString;

	}

	

	$token = generateRandomString();

	if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {

		$protocol = 'https://';

	}

	else {

		$protocol = 'http://';

	}



	$current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

	

	?>

	<section class="banner text-center pt-5"><div class="container">

		<div class="fivdth">

			<div class="col">

				<h3 class="font-weight-light mb-3" style="color: #414141;">Thank You For Your Request For A Free Solar Quote.</h3>

			</div>	

		</div>

		<div class="col colv2">

			<h5 class="mb-3" style="color: #414141;">Here are the next steps to get your no obligation quote:</h5>

			<div class="row mb-3">

				<div class="col-12 col-md-4 shade " id="no-shade">

					<div class="d-sm-block">

						<img src="/landing/images/step-icon-1.jpg">

					</div>

					<p class="d-sm-block mb-0" style="font-size: 14px;color: #414141;">A Momentum solar specialist will call you to verify your information. Make sure to answer!</p>

				</div>

				<div class="col-12 col-md-4 shade" id="some-shade">

					<div class="d-sm-block">

						<img src="/landing/images/step-icon-2.jpg">

					</div>

					<p class="d-sm-block mb-0" style="font-size: 14px;color: #414141;">We will ask for a few more details to provide an accurate quote.</p>

				</div>

				<div class="col-12 col-md-4 shade " id="severe-shade">

					<div class="d-sm-block">

						<img src="/landing/images/step-icon-4.jpg">

					</div>

					<p class="d-sm-block mb-0" style="font-size: 14px;color: #414141;">You can choose to schedule an onsite assessment with our solar expert to see if your home is fit for solar.</p>

				</div>

			</div>

		</div></div>

	</section>



	<!-- Footer -->

	<footer class="footer-section">

		<div class="container-fluid">

			<div class="row no-gutters">

				<div class="col-lg-3 align-self-center text-center col-md-12">

					<img src="images/logo-footer.jpg" alt="Footer Logo">

				</div>

				<div class="col-lg-6 col-md-9 align-self-center py-4">

					<p class="m-0">© 2019 Momentum Solar. All Rights Reserved by Pro Custom Solar LLC D|B|A Momentum Solar | <a href="https://momentumsolar.com/privacy-policy" target="_blank">Privacy Policy</a></p>

				</div>

				<!-- <div class="col-lg-3 col-md-3 text-md-right align-self-center text-center">

					<div class="social-icons">

						<a href="https://twitter.com/momentumsolar" class="social-icon" target="_blank">

							<i class="fab fa-twitter twitter-icon"></i>

						</a>

						<a href="https://www.facebook.com/momentumsolar" class="social-icon" target="_blank">

							<i class="fab fa-facebook-f facebook-icon"></i>

						</a>

						<a href="https://www.instagram.com/momentumsolar" class="social-icon" target="_blank">

							<i class="fab fa-instagram insta-icon"></i>

						</a>

						<a href="https://www.linkedin.com/company/momentum-solar/" class="social-icon" target="_blank">

							<i class="fab fa-linkedin-in linked-icon"></i>

						</a>

					</div>

				</div> -->

			</div>

		</div>

		<!-- /.container -->

	</footer>



	<div id="myModal" class="modal fade custom-modal" role="dialog">

		<div class="modal-dialog modal-xl">

			<!-- Modal content-->

			<div class="modal-content">

				<div class="modal-body">

					<div id="scrolltoelement"></div>

					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<div class="container">

						<div class="progressbar"><!-- <div id="percents">0%</div> --><div class="progress"><div class="complete"></div></div></div>

						<div class="row">

							<div class="col d-flex text-center align-items-center">

								<!-- Step 1 -->

								<div class="col form py-1 mx-auto step-1">

									<h3 class="font-weight-light mb-5">What is your zip code?</h3>

									<p class="mb-5 mt-4 custom-width">Your zip code will allow Momentum Solar to calculate state and utility

									incentives to maximize your tax benefits.</p>

									<form id="step-1" class="needs-validation mx-auto" novalidate>

										<input id="zip" class="form-control rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip"

										type="text">

										<div class="invalid-feedback mb-4">

											Please provide a valid zip.

										</div>

										<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">

										<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">

										<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">

										<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">

										<input type="hidden" name="url" value="<?php echo $current_link?>">

										<input type="hidden" name="step" class="step" value="step1">

										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">

										<div class="center-block mt-5">

											<button class="btn btn-primary rounded-0 btn-block" type="submit">Next</button>

										</div>

									</form>

								</div>



								<!-- Step 2 -->

								<div class="col form pt-0 pt-lg-5 px-0 pb-0 mx-auto step-2" style="display:none;">

									<h3 class="font-weight-light mb-5">What is your average monthly electric bill?</h3>

									<form id="step-2" class="needs-validation mx-auto bill-box" novalidate>

										<div id="1" class="custom-control custom-radio custom-control-inline checkbox">

											<input type="radio" value="Under $50" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">

											<label class="custom-control-label" for="customRadioInline1">Under $50</label>

										</div>

										<div id="2" class="custom-control custom-radio custom-control-inline checkbox">

											<input type="radio" value="$50 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">

											<label class="custom-control-label" for="customRadioInline2">$50 - $150</label>

										</div>

										<div id="3" class="custom-control custom-radio custom-control-inline checkbox">

											<input type="radio" value="$150 - $350" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">

											<label class="custom-control-label" for="customRadioInline3">$150 - $350</label>

										</div>

										<div id="4" class="custom-control custom-radio custom-control-inline checkbox">

											<input type="radio" value="$300 - $450" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">

											<label class="custom-control-label" for="customRadioInline4">$300 - $450</label>

										</div>

										<div id="5" class="custom-control custom-radio custom-control-inline checkbox">

											<input type="radio" value="Over $450" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">

											<label class="custom-control-label" for="customRadioInline5">Over $450</label>

										</div>

										<input type="hidden" name="step" class="step" value="step2">

										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">

										<div class="row">

											<div class="col-6 text-left">

												<button class="btn btn-default text-left btn-link rounded-0 btn-sm" onclick="gotoStep(1)" type="button">

													<i class="fas fa-chevron-left"></i>Back</button>

												</div>

												<div class="col-6 text-right">

													<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>

												</div>

											</div>

										</form>

									</div>



									<!-- Step 3 -->

									<div class="col form pt-0 pt-lg-5 mx-auto step-3" style="display:none;">

										<h3 class="font-weight-light mb-5">Please enter your home address.</h3>

										<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">We will use this information to estimate how much energy your roof can produce.</p>

										<form id="step-3" class="needs-validation mx-auto" novalidate>

											<input type="text" name="full_address" class="form-control rounded-0 cw-75" id="full_address" placeholder="Address">



											<input type="hidden" id="street_number" name="street_number" value="">

											<input type="hidden" id="route" name="street_name" value="">

											<input type="hidden" id="locality" name="city" value="">

											<input type="hidden" id="postal_code" name="postal_code" value="11201">

											<input type="hidden" id="administrative_area_level_1" name="state_abbr" value="">

											<input type="hidden" id="country" name="country" value="">



											<div class="invalid-feedback">

												Please provide a valid Address.

											</div>

											<input type="hidden" name="step" class="step" value="step3">

											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">

											<div class="row mt-5">

												<div class="text-left col-6 ">

													<button class="btn btn-default btn-link rounded-0 btn-sm text-left" onclick="gotoStep(2)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>

													<div class="col-6 text-right">

														<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>

													</div>

												</div>

											</form>

										</div>



										<!-- Step 4 -->

										<div class="col form pt-0 pt-md-3 mx-auto step-4" style="display: none;">

											<h3 class="font-weight-light mb-5">How much shade does your roof get from 9 AM - 3 PM?</h3>

											<form id="step-4">

												<div class="step4mobile">

													<div id="6" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">

														<input type="radio" value="No Shade" id="customRadioInline6" name="shade" class="custom-control-input shade_radio">

														<label class="custom-control-label" for="customRadioInline6">No Shade</label>

													</div>

													<div id="7" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">

														<input type="radio" value="Some Shade" id="customRadioInline7" name="shade" class="custom-control-input shade_radio">

														<label class="custom-control-label" for="customRadioInline7">Some Shade</label>

													</div>

													<div id="8" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">

														<input type="radio" value="Severe Shade" id="customRadioInline8" name="shade" class="custom-control-input shade_radio">

														<label class="custom-control-label" for="customRadioInline8">Severe Shade</label>

													</div>

												</div>

												<div class="step4desk">

													<div class="row mb-5">

														<div class="col-12 col-md-6 shade " id="no-shade">

															<div class="shade-image d-none d-sm-block">

																<img src="images/roof-1.png">

															</div>

															<h5 class="mt-3 "><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> No Shade</h5>

															<p class="d-none d-sm-block">Entire roof is exposed to sunlight.</p>

															<input type="radio" class="no-shade" name="shade" value="No Shade" style="display:none">

														</div>

														<div class="col-12 col-md-6 shade" id="some-shade">

															<div class="shade-image d-none d-sm-block">

																<img src="images/roof-2.png">

															</div>

															<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Some Shade</h5>

															<p class="d-none d-sm-block">Parts of the roof are covered at certain times/all times.</p>

															<input type="radio" class="some-shade" name="shade" value="Some Shade" style="display:none">

														</div>

														<div class="col-12 col-md-6 shade " id="severe-shade">

															<div class="shade-image d-none d-sm-block">	

																<img src="images/roof-3.png">

															</div>

															<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Severe Shade</h5>

															<p class="d-none d-sm-block">Most or all of the roof is covered at certain times/all times.</p>

															<input type="radio" class="severe-shade" name="shade" value="Severe Shade" style="display:none">

															<input type="hidden" name="step" class="step" value="step4">

															<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">

														</div>

													</div>

												</div>



												<div class="row">

													<div class="text-left col-6">

														<button class="btn text-left btn-default rounded-0 btn-link btn-sm" onclick="gotoStep(3)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>



														<div class="col-6 text-right">

															<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>

														</div>

													</div>



												</form>

											</div>



											<!-- Step 5 -->

											<div class="col form pt-0 pt-sm-8 mx-auto step-5" style="display:none;">

												

												<h3 class="font-weight-light mb-0 mb-md-5">Please enter your contact information to find out if you qualify for solar.</h3>

												<form id="step-5" class="needs-validation row" novalidate>

													<div class="col-12">

														<div class="row m-md-auto">

															<div class="col-12 col-md-12 col-lg-6">

																<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">

																<div class="invalid-feedback">

																	Please provide first name.

																</div>

															</div>

															<div class="col-12 col-md-12 col-lg-6">

																<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">

																<div class="invalid-feedback">

																	Please provide last name.

																</div>

															</div>

															<div class="col-12 col-md-12 col-lg-6">

																<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">

																<div class="invalid-feedback">

																	Please provide a valid email.

																</div>

															</div>

															<div class="col-12 col-md-12 col-lg-6">

																<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04"

																placeholder="Phone">

																<div class="invalid-feedback">

																	Please provide a valid phone.

																</div>

															</div>

														</div>

														<div class="row">

															<div class="col-12 text-center my-3">

																<button class="btn btn-primary rounded-0 btn-sm final_submit" type="submit">Submit</button>

																<img src="/landing/images/loading_bubble.svg" style="display:none" class="loader_image">

															</div>

														</div>

														<div class="row">

															<div class="col-12">

																<p class="my-3 small w-75 ml-auto mr-auto">*By clicking the "Submit" button you agree to our Terms and Privacy Policy and authorize Momentum Solar to reach out to you by phone and/or email.</p>

															</div>

														</div>



														<div class="row">

															<div class="col-12">

																<input type="hidden" name="step" class="step" value="step5">

																<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">

																<div class="row">

																	<div class="text-left">

																		<button class="btn btn-default rounded-0 btn-sm btn-link" onclick="gotoStep(4)" type="button">

																			<i class="fas fa-chevron-left"></i>Back</button>

																		</div>

																	</div>

																</div>

															</div>

														</div>

													</form>

													<div class="bot-sec mt-5" style="display:none;">

														<div class="d-flex col- mx-o justify-content-between">



															<p class="back d-flex align-items-center"><span><i class="fas fa-chevron-left"></i></span>Back</p>

															<button class="btn btn-primary rounded-0" type="submit">Next</button>

														</div>

													</div>

												</div>

												<div class="col form py-5 mx-auto step-6" style="display:none;">



													<h3 class="text-success text-center">

														<span class="d-block text-center">

															<i class="fas fa-check-circle mb-3 fa-2x"></i></span>Thank you for your interest in Momentum Solar. Our team will get back to you within 24-48 hours.

														</h3>

													</div>

													<div class="close" style="display:none;">

														<i class="fas fa-times"></i>

													</div>

												</div>

											</div>

											<div class="smallnotes"><i class="fas fa-lock"></i> Your information is secure</div>

										</div>

									</div>

								</div>

							</div>

						</div>



						<div id="myModal_schedule_call" class="modal fade custom-modal" role="dialog">

							<div class="modal-dialog">

								<!-- Modal content-->

								<div class="modal-content">

									<div class="modal-body">

										<div id="scrolltoelement"></div>

										<button type="button" class="close" data-dismiss="modal">&times;</button>

										<div class="container">

											<div class="row">

												<p class="mb-2 mt-2 custom-width text-center">Schedule a no-obligation phone call with a solar expert.</p>

												<div class="col d-flex text-center align-items-center">

													<form class="needs-validation mx-auto formmodal form" action="" method="post" enctype="multipart/form-data">

														<div class="row">

															<div class="col-12 mb-4">

																<input type="text" name="fname" class="form-control rounded" placeholder="First Name *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="text" name="lname" class="form-control rounded" placeholder="Last Name *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="text" name="phone" class="form-control rounded" placeholder="Phone *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="date" name="date" class="form-control rounded" value="yyyy-mm-dd" style="min-height: auto;color: #9c9c9c;">

															</div>

															<div class="col-12 mb-4">

																<select name="time" class="form-control rounded" style="color: #9c9c9c;">

																	<option disabled="">Time</option>

																	<option value="9:00 AM">9:00 AM</option>

																	<option value="9:15 AM">9:15 AM</option>

																	<option value="9:30 AM">9:30 AM</option>

																	<option value="9:45 AM">9:45 AM</option>

																	<option value="10:00 AM">10:00 AM</option>

																	<option value="10:15 AM">10:15 AM</option>

																	<option value="10:30 AM">10:30 AM</option>

																	<option value="10:45 AM">10:45 AM</option>

																	<option value="11:00 AM">11:00 AM</option>

																	<option value="11:15 AM">11:15 AM</option>

																	<option value="11:30 AM">11:30 AM</option>

																	<option value="11:45 AM">11:45 AM</option>

																	<option value="12:00 AM">12:00 AM</option>

																	<option value="12:15 AM">12:15 AM</option>

																	<option value="12:30 AM">12:30 AM</option>

																	<option value="12:45 AM">12:45 AM</option>

																	<option value="1:00 PM">1:00 PM</option>

																	<option value="1:15 PM">1:15 PM</option>

																	<option value="1:30 PM">1:30 PM</option>

																	<option value="1:45 PM">1:45 PM</option>

																	<option value="2:00 PM">2:00 PM</option>

																	<option value="2:15 PM">2:15 PM</option>

																	<option value="2:30 PM">2:30 PM</option>

																	<option value="2:45 PM">2:45 PM</option>

																	<option value="3:00 PM">3:00 PM</option>

																	<option value="3:15 PM">3:15 PM</option>

																	<option value="3:30 PM">3:30 PM</option>

																	<option value="3:45 PM">3:45 PM</option>

																	<option value="4:00 PM">4:00 PM</option>

																	<option value="4:15 PM">4:15 PM</option>

																	<option value="4:30 PM">4:30 PM</option>

																	<option value="4:45 PM">4:45 PM</option>

																	<option value="5:00 PM">5:00 PM</option>

																	<option value="5:15 PM">5:15 PM</option>

																	<option value="5:30 PM">5:30 PM</option>

																	<option value="5:45 PM">5:45 PM</option>

																	<option value="6:00 PM">6:00 PM</option>



																</select>

															</div>

															<div class="col-12">

																<input id="submit" type="submit" class="btn btn-primary form-control rounded" value="Submit" style="min-height: auto;cursor: pointer;line-height: 20px;">

															</div>

														</div>

													</form>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

						<div id="myModal_b_app" class="modal fade custom-modal" role="dialog">

							<div class="modal-dialog">

								<!-- Modal content-->

								<div class="modal-content">

									<div class="modal-body">

										<div id="scrolltoelement"></div>

										<button type="button" class="close" data-dismiss="modal">&times;</button>

										<div class="container">

											<div class="row">

												<p class="mb-2 mt-2 custom-width text-center">Schedule a no-obligation on-site assessment with a solar expert.</p>

												<div class="col d-flex text-center align-items-center">

													<form class="needs-validation mx-auto formmodal form" action="" method="post" enctype="multipart/form-data">

														<div class="row">

															<div class="col-12 mb-4">

																<input type="text" name="fname" class="form-control rounded" placeholder="First Name *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="text" name="lname" class="form-control rounded" placeholder="Last Name *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="text" name="phone" class="form-control rounded" placeholder="Phone *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="text" name="full_address" class="form-control rounded" id="full_address" placeholder="Address *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="text" name="zip" class="form-control rounded" placeholder="Zip Code *" required="">

															</div>

															<div class="col-12 mb-4">

																<input type="date" name="date" class="form-control rounded" value="yyyy-mm-dd" style="min-height: auto;color: #9c9c9c;">

															</div>

															<div class="col-12 mb-4">

																<select name="time" class="form-control rounded" style="color: #9c9c9c;">

																	<option disabled="">Time</option>

																	<option value="9:00 AM">9:00 AM</option>

																	<option value="9:30 AM">9:30 AM</option>

																	<option value="10:00 AM">10:00 AM</option>

																	<option value="10:30 AM">10:30 AM</option>

																	<option value="11:00 AM">11:00 AM</option>

																	<option value="11:30 AM">11:30 AM</option>

																	<option value="12:00 AM">12:00 AM</option>

																	<option value="12:30 AM">12:30 AM</option>

																	<option value="1:00 PM">1:00 PM</option>

																	<option value="1:30 PM">1:30 PM</option>

																	<option value="2:00 PM">2:00 PM</option>

																	<option value="2:30 PM">2:30 PM</option>

																	<option value="3:00 PM">3:00 PM</option>

																	<option value="3:30 PM">3:30 PM</option>

																	<option value="4:00 PM">4:00 PM</option>

																	<option value="4:30 PM">4:30 PM</option>

																	<option value="5:00 PM">5:00 PM</option>

																	<option value="5:30 PM">5:30 PM</option>

																	<option value="6:00 PM">6:00 PM</option>



																</select>

															</div>

															<div class="col-12">

																<input id="submit" type="submit" class="btn btn-primary form-control rounded" value="Submit" style="min-height: auto;cursor: pointer;line-height: 20px;">

															</div>

														</div>

													</form>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>



						<!-- Bootstrap core JavaScript -->

						<script src="vendor/jquery/jquery.min.js"></script>

						<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>



						<!-- Plugin JavaScript -->

						<script src="vendor/jquery-easing/jquery.easing.min.js"></script>



						<!-- Custom JavaScript for this theme -->

						<script src="js/scrolling-nav.js"></script>

						<script src="js/app.js"></script>

						<script src="js/jquery.validate.js"></script>

						<script src="js/additional-methods.min.js"></script>

						<script src="js/jquery.mask.js"></script>

						<script>

							$('.phone_us').mask('(000) 000-0000');

							var placeSearch, autocomplete;

							var componentForm = {

								street_number: 'short_name',

								route: 'long_name',

								locality: 'long_name',

								administrative_area_level_1: 'short_name',

								country: 'long_name',

								postal_code: 'short_name'

							};



							function initAutocomplete() {

			// Create the autocomplete object, restricting the search to geographical

			// location types.

			autocomplete = new google.maps.places.Autocomplete(

				(document.getElementById('full_address')), {

					types: ['geocode']

				});



			// When the user selects an address from the dropdown, populate the address

			// fields in the form.

			autocomplete.addListener('place_changed', fillInAddress);

		}



		function fillInAddress() {

			// Get the place details from the autocomplete object.

			var place = autocomplete.getPlace();



			for (var component in componentForm) {

				document.getElementById(component).value = '';

				document.getElementById(component).disabled = false;

			}



			// Get each component of the address from the place details

			// and fill the corresponding field on the form.

			for (var i = 0; i < place.address_components.length; i++) {

				var addressType = place.address_components[i].types[0];

				if (componentForm[addressType]) {

					var val = place.address_components[i][componentForm[addressType]];

					document.getElementById(addressType).value = val;

				}

			}

		}



		// Bias the autocomplete object to the user's geographical location,

		// as supplied by the browser's 'navigator.geolocation' object.

		function geolocate() {

			if (navigator.geolocation) {

				navigator.geolocation.getCurrentPosition(function (position) {

					var geolocation = {

						lat: position.coords.latitude,

						lng: position.coords.longitude

					};

					var circle = new google.maps.Circle({

						center: geolocation,

						radius: position.coords.accuracy

					});

					autocomplete.setBounds(circle.getBounds());

				});

			}

		}

	</script>

	<!--Fading out "Get Free Quote" button in mobile-->

	<script>

		var windowsize = $(window).width();

		if(windowsize <= 425){

			$('.fixed-bottom').css('position','unset');

			$('.banner ').css('margin-top','0');

			var preScroll = 0;

			$(window).scroll(function(){

				var currentScroll = $(this).scrollTop();

				if (currentScroll > 350){

					$('.fixed-bottom').fadeIn(500).css('position','fixed');

//                    $('.banner ').css('margin-top','48');

} else {

	$('.fixed-bottom').fadeOut(500).css('position','unset');

}

preScroll = currentScroll;

});

		}



	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete"

	async defer></script>

	<!-- Facebook Pixel Code -->

	<script>

		! function (f, b, e, v, n, t, s) {

			if (f.fbq) return;

			n = f.fbq = function () {

				n.callMethod ?

				n.callMethod.apply(n, arguments) : n.queue.push(arguments)

			};

			if (!f._fbq) f._fbq = n;

			n.push = n;

			n.loaded = !0;

			n.version = '2.0';

			n.queue = [];

			t = b.createElement(e);

			t.async = !0;

			t.src = v;

			s = b.getElementsByTagName(e)[0];

			s.parentNode.insertBefore(t, s)

		}(window, document, 'script',

			'https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '252884438399772');

		fbq('track', 'PageView');

	</script>

	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=696369663874492&ev=PageView&noscript=1" />

	</noscript>

	<!-- End Facebook Pixel Code -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75829764-1"></script>

	<script>

		window.dataLayer = window.dataLayer || [];



		function gtag() {

			dataLayer.push(arguments);

		}

		gtag('js', new Date());

		gtag('config', 'UA-75829764-1');

		gtag('config', 'AW-922148957');

	</script>

	<script>

		(function (i, s, o, g, r, a, m) {

			i['GoogleAnalyticsObject'] = r;

			i[r] = i[r] || function () {

				(i[r].q = i[r].q || []).push(arguments)

			}, i[r].l = 1 * new Date();

			a = s.createElement(o),

			m = s.getElementsByTagName(o)[0];

			a.async = 1;

			a.src = g;

			m.parentNode.insertBefore(a, m)

		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-75829764-1', 'momentumsolar.com');

		ga('set', 'page', '/appointments/thank-you.php');

		ga('send', 'pageview');

	</script>

	<script>

		gtag('config', 'AW-922148957/sK1kCKaBr5QBEN3A27cD', {

			'phone_conversion_number': '<?php echo $phone;?>'

		});

	</script>

	<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"26038347"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>





	<!-- Taboola Pixel Code -->

	<script type='text/javascript'>

		window._tfa = window._tfa || [];

		window._tfa.push({notify: 'event', name: 'page_view', id: 1145705});

		!function (t, f, a, x) {

			if (!document.getElementById(x)) {

				t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);

			}

		}(document.createElement('script'),

			document.getElementsByTagName('script')[0],

			'//cdn.taboola.com/libtrc/unip/1145705/tfa.js',

			'tb_tfa_script');

		</script>

		<noscript>

			<img src='//trc.taboola.com/1145705/log/3/unip?en=page_view'

			width='0' height='0' style='display:none'/>

		</noscript>

		<!-- End of Taboola Pixel Code -->



		<script data-obct type="text/javascript">

			/** DO NOT MODIFY THIS CODE**/

			!function(_window, _document) {

				var OB_ADV_ID='004f7faeddae738de0c5c31bb62c5e99e5';

				if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}

				var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);

				obApi('track', 'PAGE_VIEW');

			</script><!-- Hotjar Tracking Code for https://momentumsolar.com --><script>    (function(h,o,t,j,a,r){        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};        h._hjSettings={hjid:1275020,hjsv:6};        a=o.getElementsByTagName('head')[0];        r=o.createElement('script');r.async=1;        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;        a.appendChild(r);    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');</script>

			<script async type="text/javascript" src="//static.klaviyo.com/onsite/js/klaviyo.js?company_id=QGKQnL"></script>

<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','o2rlz');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->			

			<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10081644'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>

<script>
(function(document, tag) {
    var script = document.createElement(tag);
    var element = document.getElementsByTagName('body')[0];
    script.src = 'https://acsbap.com/apps/app/assets/js/acsb.js'; script.async = true; script.defer = true;
    (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script);
    script.onload = function() {
        acsbJS.init({
            statementLink     : '',
            feedbackLink      : '',
            footerHtml        : '',
            hideMobile        : false,
            hideTrigger       : false,
            language          : 'en',
            position          : 'left',
            leadColor         : '#146ff8',
            triggerColor      : '#146ff8',
            triggerRadius     : '50%',
            triggerPositionX  : 'left',
            triggerPositionY  : 'bottom',
            triggerIcon       : 'default',
            triggerSize       : 'medium',
            triggerOffsetX    : 20,
            triggerOffsetY    : 20,
            mobile            : {
                triggerSize       : 'small',
                triggerPositionX  : 'left',
                triggerPositionY  : 'center',
                triggerOffsetX    : 0,
                triggerOffsetY    : 0,
                triggerRadius     : '0'
            }
        });
    };
}(document, 'script'));
</script>			

		</body>



		</html>