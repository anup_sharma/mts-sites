from chalice import Chalice
import pymongo
import json
from bson.regex import Regex
from bson.json_util import dumps
import requests
import collections
import datetime
from datetime import timedelta
from json2html import *
app = Chalice(app_name='mrelaycrm')

conn = pymongo.MongoClient('mongodb://rkamtamts:Solar1234!@3.216.192.59:1433/sales')
db = conn['sales']

@app.route('/')
def index():
    return {'hello': 'world'}

def sendEmail(jbody):
    url = "https://veeyieqt20.execute-api.us-east-1.amazonaws.com/api/sendmailbulkhome"

    jsonb = {'from': 'HomeLead@momentumsolar.app',
               'body': json2html.convert(json = jbody)}

    response = requests.request("POST", url, json=jsonb)
    print(response)



def stingifyjson(jsonstr):
    y = json.loads(jsonstr)
    z = {}
    for x in y:
        key = x
        val = str(y[x])
        z[key] = val or ""
    return json.dumps(z)

def loglead(jbody):
    print(jbody)
    connection = pymongo.MongoClient('mongodb://root:6A4jYIj9AWx6@34.198.183.205:1433/?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-256')
    mdb = connection['sales']
    collection = mdb['leads']
    try:
        collection.insert(jbody)
    except Exception as e:
        print(e)
    finally:
        connection .close()

def crmtoken():
    coll = db['crmtokenaccess']
    query = {}
    query["updateid"] = 101
    sort = [(u"timestamp", -1)]
    cursor = coll.find(query, sort = sort, limit = 1)
    try:
        for doc in cursor:
            print(doc['crmtoken'])
            return doc['crmtoken']
    except Exception as e:
        print(e)
        conn.close()

def crmverify(email, phone):
    token = crmtoken()
    crmurl = "https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads?$top=1&$select=mts_appointmentstarttime,mts_firstname,mts_lastname,mts_canvasser,mts_city,mts_leadnumber,mts_name,mts_phone,mts_addressline1&$filter=mts_canvasser eq '{}' and  mts_phone eq '{}'".format(email, phone)
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    resp = requests.request("GET", crmurl, headers=headers)
    print(crmurl)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp.content)
        data = resp.json()
        return data["value"]
    else:
        print(resp)
        return {'success': 'false'}

def crmverifydev(email, phone):
    token = crmtoken()
    crmurl = "https://procustomsolarllcdev.crm.dynamics.com/api/data/v9.1/mts_leads?$top=1&$select=mts_appointmentstarttime,mts_firstname,mts_lastname,mts_canvasser,mts_city,mts_leadnumber,mts_name,mts_phone,mts_addressline1&$filter=mts_assigneeemail eq '{}' and  mts_phone eq '{}'".format(email, phone)
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    resp = requests.request("GET", crmurl, headers=headers)
    print(crmurl)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp.content)
        data = resp.json()
        return data["value"]
    else:
        print(resp)
        return {'success': 'false'}

def crmexist(email, phone):
    token = crmtoken()
    crmurl = "https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads?$top=1&$select=mts_appointmentstarttime,mts_firstname,mts_lastname,mts_canvasser,mts_city,mts_leadnumber,mts_name,mts_phone,mts_addressline1&$filter=mts_canvasser eq '{}' and  mts_phone eq '{}'".format(email, phone)
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    resp = requests.request("GET", crmurl, headers=headers)
    print(crmurl)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp.content)
        data = resp.json()
        if len(data["value"]) > 0:
            return data["value"]
        else:
            return False
    else:
        print(resp)
        return False

@app.route('/addcrmlead', methods=['POST'])
def addcrmlead():
    token = crmtoken()
    request_body = app.current_request.json_body
    try:
        loglead(request_body)
    except Exception as e:
        print(e)

    phone = request_body['phone']
    email = request_body['email']
    demodatetime = request_body['demodatetime']
    altphone = request_body['altphone']
    address = request_body['address']
    product = request_body['product']
    city = request_body['city']
    fname = request_body['fname']
    state = request_body['state']
    lname = request_body['lname']
    utility = request_body['utility']
    comments = request_body['comments']
    roofage = request_body['roofage']
    rooftype = request_body['rooftype']
    canvasser = request_body['canvasseremail']
    name = "{} {}".format(fname, lname)
    if crmexist(canvasser,phone):
        print("EXISTS!")
        return crmexist(canvasser,phone)
    crmurl = 'https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    crmbody['mts_email'] = email
    crmbody['mts_appointmentstarttime'] = demodatetime
    crmbody['mts_appointmenttimeind'] = demodatetime
    crmbody['mts_altphone1'] = altphone
    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_city'] = city
    crmbody['mts_firstname'] = fname
    crmbody['mts_state'] = state
    crmbody['mts_lastname'] = lname
    crmbody['mts_utility'] = utility
    crmbody['mts_leadadditionalcomments'] = comments
    crmbody['mts_roofage'] = roofage
    crmbody['mts_rooftype'] = rooftype
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_leadvendoroptinurl'] = 'MRelay.app'
    if product == 'Home':
        crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(e02cbd27-55fe-e811-a96b-000d3a32c8b8)"
        crmbody['mts_returnleadsource'] = 'Canvassing Home'
        print("Home")
    if product == 'Solar':
        crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(612e176b-681b-e611-812c-c4346bac1a4c)"
        crmbody['mts_returnleadsource'] = 'Canvassing Solar'
        print("Solar")
    if "zip" in request_body:
        crmbody['mts_zipcode'] = request_body['zip']
    if len(name) < 1:
        return {'success': 'failed'}

    if len(phone) < 1:
        return {'success': 'failed'}

    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverify(canvasser,phone)
        if product == 'Home':
            print("Home")
            try:
                print(data)
                sendEmail(data)
            except Exception as e:
                print(e)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}

@app.route('/myleads', methods=['POST'])
def myleads():
    request_body = app.current_request.json_body
    then = datetime.datetime.now()
    twodaysago = then - timedelta(days=1)
    tdstr = twodaysago.strftime("%Y-%m-%-d")
    token = crmtoken()
    email = request_body["email"]
    crmurl = "https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads?$select=mts_leadadditionalcomments,mts_leadnumber,mts_name,mts_phone,mts_addressline1&$filter=createdon gt {} and mts_canvasser eq '{}'".format(tdstr ,email)
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    resp = requests.request("GET", crmurl, headers=headers)
    print(crmurl)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp.content)
        data = resp.json()
        return data["value"]
    else:
        print(resp)
        return {'success': 'false'}

@app.route('/addcrmleadapp', methods=['POST'])
def addcrmleadapp():
    token = crmtoken()
    request_body = app.current_request.json_body
    try:
        loglead(request_body)
    except Exception as e:
        print(e)

    fname = request_body['fname']
    lname = request_body['lname']
    name = "Website Appt {} {}".format(fname, lname)
    phone = request_body['phone']
    address = request_body['address']
    product = "Solar"
    city = request_body['city']
    state = request_body['state']
    comments = "Momentumsolar.com appointments lead "
    canvasser = "website@momentumsolar.com"

    crmurl = 'https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    if 'demo_date_time' in request_body:
        demodatetime = request_body['demo_date_time']
        crmbody['mts_appointmentstarttime'] = demodatetime
    if 'cb_date_time' in request_body:
        demodatetime = request_body['cb_date_time']
        crmbody['mts_appointmentstarttime'] = demodatetime

    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_city'] = city
    crmbody['mts_firstname'] = fname
    crmbody['mts_state'] = state
    crmbody['mts_lastname'] = lname
    crmbody['mts_leadadditionalcomments'] = comments
    crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(27d5be09-6826-e911-a970-000d3a32c8b8)"
    crmbody['mts_returnleadsource'] = 'Momentum Solar Website'


    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverify(canvasser,phone)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}

@app.route('/addcrmleaddelta', methods=['POST'])
def addcrmleaddelta():
    token = crmtoken()
    request_body = app.current_request.json_body
    try:
        loglead(request_body)
    except Exception as e:
        print(e)

    fname = request_body['fname']
    lname = request_body['lname']
    name = "Delta Lead {} {}".format(fname, lname)
    phone = request_body['phone']
    address = request_body['address']
    product = "Solar"
    city = request_body['city']
    state = request_body['state']
    salesrep = request_body['salesrep']
    referredbyphone = request_body['referredbyphone']
    referredbydate = request_body['referredbydate']
    comments = "Refferal Form - SalesRep - {} referredbyphone-{} referredbydate-{} Com-{} ".format(salesrep,referredbyphone,referredbydate ,request_body['comments'])
    canvasser = request_body['canvasseremail']

    crmurl = 'https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    if 'demo_date_time' in request_body:
        demodatetime = request_body['demo_date_time']
        crmbody['mts_appointmentstarttime'] = demodatetime
    if 'demodatetime' in request_body:
        demodatetime = request_body['demodatetime']
        crmbody['mts_appointmentstarttime'] = demodatetime
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_city'] = city
    crmbody['mts_firstname'] = fname
    crmbody['mts_state'] = state
    crmbody['mts_lastname'] = lname
    crmbody['mts_leadadditionalcomments'] = comments


    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        return crmverify(canvasser,phone)
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}


@app.route('/searchleadsource', methods=['POST'], cors=True)
def searchleadsource():
    token = crmtoken()
    request_body = app.current_request.json_body
    searchterm = request_body["searchterm"]
    crmurl = "https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/iis_leadsources?$select=iis_leadsource,iis_leadsourceid&$filter=contains(iis_leadsource,'{}')".format(searchterm)
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    resp = requests.request("GET", crmurl, headers=headers)
    print(crmurl)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp.content)
        data = resp.json()
        return data["value"]
    else:
        print(resp)
        return {'success': 'false'}


@app.route('/addcanvaslead', methods=['POST'], cors=True)
def addcanvaslead():
    token = crmtoken()
    request_body = app.current_request.json_body
    try:
        loglead(request_body)
    except Exception as e:
        print(e)
    print("CanvasLead")
    phone = request_body['phone']
    email = request_body['email']
    demodatetime = request_body['demodatetime']
    address = request_body['address']
    product = request_body['product']
    city = request_body['city']
    fname = request_body['fname']
    state = request_body['state']
    lname = request_body['lname']
    utility = request_body['utility']
    comments = request_body['comments']
    canvasser = request_body['canvasseremail']
    name = "{} {}".format(fname, lname)
    crmurl = 'https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    crmbody['mts_email'] = email
    crmbody['mts_appointmentstarttime'] = demodatetime
    crmbody['mts_appointmenttimeind'] = demodatetime
    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_city'] = city
    crmbody['mts_firstname'] = fname
    crmbody['mts_state'] = state
    crmbody['mts_lastname'] = lname
    crmbody['mts_utility'] = utility
    crmbody['mts_leadadditionalcomments'] = comments
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_leadvendoroptinurl'] = 'CallInLead.manual'
    if product == 'Home':
        crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(e02cbd27-55fe-e811-a96b-000d3a32c8b8)"
        crmbody['mts_returnleadsource'] = 'Canvassing Home'
        print("Home")
    if product == 'Solar':
        crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(612e176b-681b-e611-812c-c4346bac1a4c)"
        crmbody['mts_returnleadsource'] = 'Canvassing Solar'
        print("Solar")
    if "zip" in request_body:
        crmbody['mts_zipcode'] = request_body['zip']
    if len(name) < 1:
        return {'success': 'failed'}

    if len(phone) < 1:
        return {'success': 'failed'}

    if "confirmed" in request_body:
        if request_body['confirmed']:
            crmbody['mts_ConfirmationStatus@odata.bind'] = "/mts_demoteamstatusreasons(c2ecdfee-6f82-ea11-a812-000d3a5a1cac)"

    if "qualified" in request_body:
        if request_body['qualified']:
            crmbody['mts_LatestQualificationStatus@odata.bind'] = "/mts_demoteamstatusreasons(2dccfb0d-3683-ea11-a812-000d3a5a1cac)"


    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverify(canvasser,phone)
        if product == 'Home':
            print("Home")
            try:
                print(data)
                sendEmail(data)
            except Exception as e:
                print(e)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}

@app.route('/utilitybyzip', methods=['POST'], cors=True)
def utilitybyzip():
    request_body = app.current_request.json_body
    zip = request_body["zip"]
    headers = {
        'x-api-key': '41fe3626-130a-4c7d-a982-ad6e40175c3c',
        'Content-Type': 'application/json'
    }
    inserturl = "select zip , utility_name as 'name',state from UtilityByZip where zip = {}".format(zip)
    r = requests.request("POST", 'https://hscabuwbfe.execute-api.us-east-1.amazonaws.com/latest/sql',
                         headers=headers, json={"query" : inserturl})
    if r.status_code >= 200 and r.status_code <= 299:
        data = r.json()
        dataobj = data["data"]
        if len(dataobj) > 0:
            print("yes")
            return r.json()
        else:
            print("no")
            return {"data": "Failed"}

    else:
        print(r.status_code)
        print(r.content)

@app.route('/leadsourcesubcategories', methods=['POST'], cors=True)
def leadsourcesubcategories():
    token = crmtoken()
    crmurl = "https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leadsourcesubcategories?$select=mts_name&$filter=contains(mts_name,%27BJ%27)"
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    resp = requests.request("GET", crmurl, headers=headers)
    print(crmurl)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp.content)
        data = resp.json()
        return data["value"]
    else:
        print(resp)
        return {'success': 'false'}

@app.route('/addretail', methods=['POST'], cors=True)
def addretail():
    token = crmtoken()
    request_body = json.loads(app.current_request.json_body)
    print(request_body)
    try:
        loglead(request_body)
    except Exception as e:
        print(e)
    print("RetailLead")
    print(request_body)
    territory = request_body['leadsub']
    phone = request_body['phone']
    email = request_body['email']
    demodatetime = request_body['preferredTime']
    address = request_body['address']
    product = "Solar"
    city = request_body['city']
    fname = request_body['fname']
    state = request_body['state']
    lname = request_body['lname']
    utility = request_body['UtilityProvider']
    comments = request_body['notes']
    canvasser = request_body['user']
    name = "{} {}".format(fname, lname)
    crmurl = 'https://procustomsolarllc755.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    crmbody['mts_email'] = email
    crmbody['mts_appointmentstarttime'] = demodatetime
    crmbody['mts_appointmenttimeind'] = demodatetime
    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_city'] = city
    crmbody['mts_firstname'] = fname
    crmbody['mts_state'] = str(state).upper()
    crmbody['mts_lastname'] = lname
    crmbody['mts_utility'] = utility
    crmbody['mts_leadadditionalcomments'] = comments
    crmbody['mts_canvasser'] = canvasser
    crmbody['mts_leadvendoroptinurl'] = 'MRelayRetail'
    crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(f8511427-122b-e911-a96d-000d3a310e39)"
    crmbody['mts_LeadSourceSubcategory@odata.bind'] = "/mts_leadsourcesubcategories({})".format(territory)
    crmbody['mts_returnleadsource'] = 'Canvassing Solar'

    if "zip" in request_body:
        crmbody['mts_zipcode'] = request_body['zip']
    if len(name) < 1:
        return {'success': 'failed'}

    if len(phone) < 1:
        return {'success': 'failed'}

    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverify(canvasser,phone)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}

@app.route('/addcallead', methods=['POST'], cors=True)
def addcallead():
    token = crmtoken()
    request_body = app.current_request.json_body
    print(request_body)
    print("Calendly")
    fname = request_body['fname']
    lname = request_body['lname']
    locationurl = request_body['locationurl']
    phone = request_body['phone']
    altphone = request_body['altphone']
    email = request_body['email']
    repemail = request_body['repemail']
    starttime = request_body['starttime']
    createdtime = request_body['createdtime']
    address = request_body['address']
    product = "Solar"
    city = request_body['city']
    state = request_body['state']
    zipcode = request_body['zipcode']
    monthlyelectricbill = request_body['monthlyelectricbill']
    shade = request_body['shade']
    utmcamp = request_body['utmcamp']
    utmsource = request_body['utmsource']
    utmchannel = request_body['utmchannel']
    clickid = request_body['clickid']
    subid = request_body['subid']

    name = "{} {}".format(fname, lname)
    crmurl = 'https://procustomsolarllcdev.crm.dynamics.com/api/data/v9.1/mts_leads'
    headers = {
        'OData-Version': "4.0",
        'OData-MaxVersion': "4.0",
        'Content-Type': "application/json; charset=utf-8",
        'Accept': "application/json,*/*",
        'Authorization': "Bearer {}".format(token),
    }
    crmbody = {}
    crmbody['mts_name'] = name
    crmbody['mts_phone'] = phone
    crmbody['mts_email'] = email
    crmbody['mts_zipcode'] = zipcode
    crmbody['mts_locationurl'] = locationurl
    crmbody['mts_altphone1'] = altphone
    crmbody['mts_addressline1'] = address
    crmbody['mts_leadproducts'] = product
    crmbody['mts_assigneeemail'] = repemail
    crmbody['mts_appointmenttimeind'] = starttime
    crmbody['mts_leadreceiveddate'] = createdtime
    crmbody['mts_monthlyelectricbill'] = monthlyelectricbill
    crmbody['mts_roofshade'] = shade
    crmbody['mts_city'] = city
    crmbody['mts_firstname'] = fname
    crmbody['mts_state'] = str(state).upper()
    crmbody['mts_lastname'] = lname
    crmbody['mts_leadvendortrackingcampaign'] = utmcamp
    crmbody['mts_leadvendorsource'] = utmsource
    crmbody['mts_leadvendorchannel'] = utmchannel
    crmbody['mts_leadvendorclickid'] = clickid
    crmbody['mts_leadvendorsubid'] = subid
    crmbody['mts_leadvendoroptinurl'] = 'urlfromsource.com'
    crmbody['mts_LeadSource@odata.bind'] = "/iis_leadsources(fb5f949b-a664-eb11-a812-000d3a3ba9b8)"
    crmbody['mts_returnleadsource'] = 'Website Calendly'

    if "zip" in request_body:
        crmbody['mts_zipcode'] = request_body['zip']
    if len(name) < 1:
        return {'success': 'failed'}

    if len(phone) < 1:
        return {'success': 'failed'}

    json_data = json.dumps(crmbody)
    resp = requests.request("POST", crmurl, data=json_data, headers=headers)
    if resp.status_code >= 200 and resp.status_code <= 299:
        print(resp)
        print(resp.content)
        data = crmverifydev(repemail,phone)
        return data
    else:
        print(resp)
        print(resp.content)
        return {'success': str(resp.content)}