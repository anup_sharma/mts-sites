<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Momentum Solar</title>
	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-modal-ios.css" rel="stylesheet">
	<!--Font Awsome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	 crossorigin="anonymous">
	<!-- Custom styles for this template -->
	<link href="css/new.css" rel="stylesheet">
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,600" rel="stylesheet">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLXNWPG');</script>
<!-- End Google Tag Manager -->
</head>
<?php 
$phone = "(844) 592-8056";
if($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_PA") {
    $phone = "(844) 592-8056";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Remarketing") {
    $phone = "(844) 249-0102";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_PA") {
    $phone = "(855) 997-7495";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_CT") {
    $phone = "(855) 338-0690";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_CT") {
    $phone = "(855) 782-0757";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_FL") {
    $phone = "(866) 426-9323";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_FL") {
    $phone = "(855) 965-1765";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_TX") {
    $phone = "(844) 731-0129";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_TX") {
    $phone = "(844) 790-2124";
} elseif ($_GET['utm_medium'] == "AdWords" && $_GET['utm_campaign'] == "Search_NY") {
    $phone = "(855) 997-3041";
} elseif ($_GET['utm_medium'] == "Bing" && $_GET['utm_campaign'] == "Search_NY") {
    $phone = "(844) 247-3551";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_PA") {
    $phone = "(844) 263-0006";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_FL") {
    $phone = "(844) 249-0568";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_CT") {
    $phone = "(844) 259-1553";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_TX") {
    $phone = "(844) 264-4824";
} elseif ($_GET['utm_medium'] == "Facebook" && $_GET['utm_campaign'] == "Prospecting_NY") {
    $phone = "(844) 261-9076";
}

?>
<body id="page-top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLXNWPG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="covid19" style="background: #00aeef;text-align: center;padding: 10px 5px;color: #fff;width: 100%;flex: 1; top: 0px;position: fixed; height: 51px; vertical-align: middle; z-index: 1030;">
Momentum Solar &amp; COVID 19 Safety Information <a href="/covid19/" style="color: #fff; text-decoration: underline;">Learn More</a>
</div>
<style>
	.fixed-top{
			top: 51px !important;
		}
	.banner{
		padding-top: 56px;
	}
	@media (max-width: 991.98px){
		.phonetop{
			    top: 43px !important;
		}
		.covid19{
			height: 36px !important;
			font-size: 11px;
			line-height: 12px;
		}
		.fixed-top{
			top: 36px !important;
		}
		.banner{
			padding-top: 36px;
		}
		
	}
</style>
<div class="phonetop">
	<a href="tel:<?php echo $phone; ?>"><img src="images/phone-black.png" alt="<?php echo $phone; ?>"></a>
</div>
<div class="fixed-bottom d-block d-flex justify-content-between d-sm-none px-5">
    <a class="btn btn-primary rounded-0 my-3 text-white pull-left" type="submit" href="tel:<?php echo $phone; ?>">Call Us</a>
        <a class="btn btn-primary rounded-0 my-3 text-white pull-right" type="submit" data-toggle="modal" data-target="#myModal_schedule_call">Schedule A Call</a>
</div>
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg bg-white navbar-light fixed-top p-0" id="mainNav">
		<div class="logo-container">
			<a class="navbar-brand js-scroll-trigger pl-lg-5 pl-0 py-3 py-lg-0 mr-0">
				<img src="images/logo.png" alt="logo" class="text-center sitelogo">
				<!--button class="btn btn-primary rounded-0 btn-sm d-none text-center my-3 top-cta" type="submit" data-toggle="modal"
				 data-target="#myModal">Free Quote</button-->
			</a>
		</div>
		<div class="container-fluid p-0 d-flex justify-content-center">
			<!--button class="navbar-toggler mr-3 hamburger-icon" type="button" data-toggle="collapse" data-target="#navbarResponsive"
			 aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button-->
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item phonenav">
						<!--a class="nav-link js-scroll-trigger phonenumber" href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone; ?></a-->
						<a class="nav-link js-scroll-trigger phonenumber" href="tel:1-888-666-36886"><i class="fa fa-phone" aria-hidden="true"></i> 1 (888) MOMENTUM</a>
						
					</li>
					<li class="nav-item free-quote">
						<a class="nav-link js-scroll-trigger text-white px-5 free-quote-text" style="cursor:pointer" data-toggle="modal"
						 data-target="#myModal_schedule_call">Schedule A Call</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<?php 
	  function generateRandomString($length = 10) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
	}
	  
	  $token = generateRandomString();
	if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
		$protocol = 'https://';
		}
		else {
		$protocol = 'http://';
		}

		$current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		 
	  ?>
	  <section class="banner">
	  	<div class="row">
	  		<div class="col-12 col-md-6 bannerbox bnnerl text-center">
		  		<div class="container">
		  			<h2>Free</h2>
		  			<h3>Phone Consultation</h3>
		  			<p>Have questions? Schedule a call with our solar experts so they can answer any questions or concerns that you may have.</p>
		  			<a class="btn btn-primary btn-lg gradient-btn rounded-pill contact-button" style="cursor:pointer" data-toggle="modal" data-target="#myModal_schedule_call"><i class="fa fa-phone" aria-hidden="true" style="transform: rotate(90deg);"></i> Schedule A Call</a>
	  			</div>
	  		</div>
	  		<div class="col-12 col-md-6 bannerbox bnnerr text-center">
	  			<div class="container">
	  				<h2>Free</h2>
	  				<h3>Online Consultation</h3>
	  				<p>Review your FREE no-obligation quote with our solar experts online, at a time that works for you! </p>
	  				<a class="btn btn-primary btn-lg gradient-btn rounded-pill contact-button" style="cursor:pointer" data-toggle="modal" data-target="#myModal_b_app"><i class="far fa-calendar-alt"></i> Book An Appointement</a>
	  			</div>
	  		</div>
	  	</div>
	  </section>

	<section id="section-2" class="py-5 my-3">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-md-4">
					<h2 class="text-uppercase center-on-small">Save up to <span class="font-weight-bold" style="color:#00aeef">30% OFF</span>
						your electric bill <span style="color:#ff9504; letter-spacing:9px;">immediately.</span></h2>
				</div>
				<div class="col-12 col-md-8">
					<ul class="pb-3">
						<li>$0 to enroll</li>
						<li>No hidden fees</li>
						<li>Upfront pricing with no obligation</li>
					</ul>
					<p class="font-italic pr-md-5">Momentum solar offers many programs that allow residents to go solar with <strong>zero</strong> upfront costs and none of the hidden fees that other companies charge.</p>
				</div>
			</div>
		</div>
	</section>

	<section id="section-3" class="bg-light py-4 featured-in-section">
		<div class="container">
			<div class="row">
				<div class="col">
					<ul class="d-lg-flex align-items-center justify-content-between text-center">
						<li class="m-0">
							<h5 class="mb-4 border-dark bt-sm bb-sm" style="letter-spacing:1px;">Featured In</h5>
						</li>
						<li>
							<img src="images/client-1.png" alt="forbes" class="my-2">
						</li>
						<li>
							<img src="images/client-2.png" alt="Inc" class="my-2">
						</li>
						<li>
							<img src="images/client-3.png" alt="Angie's List" class="my-2">
						</li>
						<li>
							<img src="images/client-4.png" alt="accredited business" class="my-2">
						</li>
						<li>
							<img src="images/client-5.png" alt="Deloitte" class="my-2">
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section id="whychooseus" class="why-momentum-solar-container">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-6 why-momentum-solar py-5">
					<h2 class="pb-4">WHY MOMENTUM SOLAR?</h2>
					<p><span class="font-weight-bold text-dark">Flexible Payment Options</span> - We offer customized payment plans
						that can include $0 upfront cost so you can save up to <span class="font-weight-bold text-dark">30% OFF your
							electric bill</span> immediately.</p>
					<p><span class="font-weight-bold text-dark">Long Term Price Protection</span> - We provide upfront rates for your electricity for the next 25 years and can guarantee that our prices will be lower than your electric company based on the program you choose.</p>
					<p><span class="font-weight-bold text-dark">Worry-Free Maintenance & Monitoring </span>- We provide complimentary system monitoring and maintenance to ensure your system is always functioning optimally as part of many of our programs.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- FAQ Section -->
	<section id="faq">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="frequentlyaq-text pt-5 pb-4">Compare Bills</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<p>This customer’s bill is estimated to be $150 lower than the previous bill.</p>
					<div class="row comparemage"><img src="images/sws-utility-bill.png" alt="" /></div>
					<h5 class="step-text py-3 font-weight-bold mb-0 text-center">What you can do next:</h5>
					<p>Discover a new way to save energy.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section -->
	<section class="testimonial py-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="row">
						<p class="quote-heading center-on-small"><span>WHAT OUR CUSTOMERS SAY</span></p>
					</div>
				</div>
				<div class="col-lg-8">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
							<li data-target="#myCarousel" data-slide-to="4"></li>
							<li data-target="#myCarousel" data-slide-to="5"></li>
							<li data-target="#myCarousel" data-slide-to="6"></li>
							<li data-target="#myCarousel" data-slide-to="7"></li>
						</ol>
						<!-- Wrapper for carousel items -->
						<div class="carousel-inner">
							<div class="item carousel-item active">
								<p class="testimonial">We’re growing more trees because of us going solar. I’m really happy because we’re
									saving more money and helping the environment by helping more trees grow.</p>
								<p class="person-name">- The Veiga Family from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Coming from a background in construction, I wanted every wire to be perfect, and
									everything squared, conduit straight. Momentum nailed every facet of that.</p>
								<p class="person-name">- Zach from Texas</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">We wanted to save money and do something that’s good for the environment. I went with
									Momentum because of the products and services they offer, the pricing and ultimately our salesman.</p>
								<p class="person-name">- Michael from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Thanks to Momentum and the savings I’m getting, I can spend more money on things I’d put
									off for later. I don’t know exactly what but I know it’ll be good things for my house.</p>
								<p class="person-name">- Austria from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">We just get our first bill and it was only $30. That’s your money or your children’s
									money to spend as you wish. We really can’t tell you how happy we are.</p>
								<p class="person-name">- The Wdowiaks from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">Customer experience I had at Momentum Solar was exceptional. Everyone I’ve talked to has
									been very helpful and if I had a question, they would seek to answer my question and I was very happy with
									them!</p>
								<p class="person-name">- John from New Jersey</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">The reputation of the company had a lot to do with it, the 25 yr warranty with service
									included was really important to me because I didn’t want to have to start paying for labor down the road.</p>
								<p class="person-name">- Bob from Florida</p>
							</div>
							<div class="item carousel-item">
								<p class="testimonial">I’ll be saving about $70 per month. Well, $70 a month gives me the opportunity to really
									spoil my grandchildren!</p>
								<p class="person-name">- Tom from New Jersey</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Contact Section -->
	<!--section class="call-us">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center contact-number py-6">
					<p class="phone-number m-0"><img src="images/phone.png"> CALL US AT <a class="text-white" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></p>
					<p class="phone-number my-3">OR</p>
					<button type="button" class="btn btn-primary btn-lg gradient-btn rounded-pill contact-button" data-toggle="modal"
					 data-target="#myModal" OnClick="startQuote()">GET FREE QUOTE ONLINE</button>
				</div>
			</div>
		</div>
	</section-->
	<!-- Footer -->
	<footer class="footer-section">
		<div class="container-fluid">
			<div class="row no-gutters">
				<div class="col-lg-3 align-self-center text-center col-md-12">
					<img src="images/logo-footer.jpg" alt="Footer Logo">
					<p><a href="/contractor-information/">Contractor Information</a></p>
				</div>x
				<div class="col-lg-6 col-md-9 align-self-center py-4">
					<p class="m-0">© 2019 Momentum Solar. All Rights Reserved by Pro Custom Solar LLC D|B|A Momentum Solar | <a href="https://momentumsolar.com/privacy-policy" target="_blank">Privacy Policy</a></p>
				</div>
				<!-- <div class="col-lg-3 col-md-3 text-md-right align-self-center text-center">
					<div class="social-icons">
						<a href="https://twitter.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-twitter twitter-icon"></i>
						</a>
						<a href="https://www.facebook.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-facebook-f facebook-icon"></i>
						</a>
						<a href="https://www.instagram.com/momentumsolar" class="social-icon" target="_blank">
							<i class="fab fa-instagram insta-icon"></i>
						</a>
						<a href="https://www.linkedin.com/company/momentum-solar/" class="social-icon" target="_blank">
							<i class="fab fa-linkedin-in linked-icon"></i>
						</a>
					</div>
				</div> -->
			</div>
		</div>
		<!-- /.container -->
	</footer>

	<div id="myModal" class="modal fade custom-modal" role="dialog">
		<div class="modal-dialog modal-xl">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="scrolltoelement"></div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="container">
						<div class="progressbar"><!-- <div id="percents">0%</div> --><div class="progress"><div class="complete"></div></div></div>
						<div class="row">
							<div class="col d-flex text-center align-items-center">
								<!-- Step 1 -->
								<div class="col form py-1 mx-auto step-1">
									<h3 class="font-weight-light mb-5">What is your zip code?</h3>
									<p class="mb-5 mt-4 custom-width">Your zip code will allow Momentum Solar to calculate state and utility
										incentives to maximize your tax benefits.</p>
									<form id="step-1" class="needs-validation mx-auto" novalidate>
										<input id="zip" class="form-control rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip"
										 type="text">
										<div class="invalid-feedback mb-4">
											Please provide a valid zip.
										</div>
										<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
										<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
										<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
										<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
										<input type="hidden" name="url" value="<?php echo $current_link?>">
										<input type="hidden" name="step" class="step" value="step1">
										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
										<div class="center-block mt-5">
											<button class="btn btn-primary rounded-0 btn-block" type="submit">Next</button>
										</div>
									</form>
								</div>

								<!-- Step 2 -->
								<div class="col form pt-0 pt-lg-5 px-0 pb-0 mx-auto step-2" style="display:none;">
									<h3 class="font-weight-light mb-5">What is your average monthly electric bill?</h3>
									<form id="step-2" class="needs-validation mx-auto bill-box" autocomplete="off" novalidate>
										<div id="1" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="Under $50" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline1">Under $50</label>
										</div>
										<div id="2" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="$50 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline2">$50 - $150</label>
										</div>
										<div id="3" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="$150 - $350" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline3">$150 - $350</label>
										</div>
										<div id="4" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="$300 - $450" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline4">$300 - $450</label>
										</div>
										<div id="5" class="custom-control custom-radio custom-control-inline checkbox">
											<input type="radio" value="Over $450" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">
											<label class="custom-control-label" for="customRadioInline5">Over $450</label>
										</div>
										<input type="hidden" name="step" class="step" value="step2">
										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
										<div class="row">
											<div class="col-6 text-left">
												<button class="btn btn-default text-left btn-link rounded-0 btn-sm" onclick="gotoStep(1)" type="button">
													<i class="fas fa-chevron-left"></i>Back</button>
											</div>
											<div class="col-6 text-right">
												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
											</div>
										</div>
									</form>
								</div>

								<!-- Step 3 -->
								<div class="col form pt-0 pt-lg-5 mx-auto step-3" style="display:none;">
									<h3 class="font-weight-light mb-5">Please enter your home address.</h3>
									<p class="mt-4 mb-5 ml-auto mr-auto mb-5 px-0 px-lg-5 px-md-0">We will use this information to estimate how much energy your roof can produce.</p>
									<form id="step-3" class="needs-validation mx-auto" novalidate>
										<input type="text" name="fulladdress" class="form-control rounded-0 cw-75" id="fulladdress" placeholder="Address" autocomplete="none">

										<!--<input type="hidden" id="street_number" name="street_number" value="">
										<input type="hidden" id="route" name="street_name" value="">
										<input type="hidden" id="locality" name="city" value="">
										<input type="hidden" id="postal_code" name="postal_code" value="11201">
										<input type="hidden" id="administrative_area_level_1" name="state_abbr" value="">
										<input type="hidden" id="country" name="country" value="">-->

										<div class="invalid-feedback">
											Please provide a valid Address.
										</div>
										<input type="hidden" name="step" class="step" value="step3">
										<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
										<div class="row mt-5">
											<div class="text-left col-6 ">
												<button class="btn btn-default btn-link rounded-0 btn-sm text-left" onclick="gotoStep(2)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>
											<div class="col-6 text-right">
												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
											</div>
										</div>
									</form>
								</div>

								<!-- Step 4 -->
								<div class="col form pt-0 pt-md-3 mx-auto step-4" style="display: none;">
									<h3 class="font-weight-light mb-5">How much shade does your roof get from 9 AM - 3 PM?</h3>
									<form id="step-4">
										<div class="step4mobile">
											<div id="6" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">
												<input type="radio" value="No Shade" id="customRadioInline6" name="shade" class="custom-control-input shade_radio">
												<label class="custom-control-label" for="customRadioInline6">No Shade</label>
											</div>
											<div id="7" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">
												<input type="radio" value="Some Shade" id="customRadioInline7" name="shade" class="custom-control-input shade_radio">
												<label class="custom-control-label" for="customRadioInline7">Some Shade</label>
											</div>
											<div id="8" class="custom-control custom-control-shade custom-radio1 custom-control-inline checkbox">
												<input type="radio" value="Severe Shade" id="customRadioInline8" name="shade" class="custom-control-input shade_radio">
												<label class="custom-control-label" for="customRadioInline8">Severe Shade</label>
											</div>
										</div>
										<div class="step4desk">
											<div class="row mb-5">
												<div class="col-12 col-md-6 shade " id="no-shade">
													<div class="shade-image d-none d-sm-block">
														<img src="images/roof-1.png">
													</div>
													<h5 class="mt-3 "><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> No Shade</h5>
													<p class="d-none d-sm-block">Entire roof is exposed to sunlight.</p>
													<input type="radio" class="no-shade" name="shade" value="No Shade" style="display:none">
												</div>
												<div class="col-12 col-md-6 shade" id="some-shade">
													<div class="shade-image d-none d-sm-block">
														<img src="images/roof-2.png">
													</div>
													<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Some Shade</h5>
													<p class="d-none d-sm-block">Parts of the roof are covered at certain times/all times.</p>
													<input type="radio" class="some-shade" name="shade" value="Some Shade" style="display:none">
												</div>
												<div class="col-12 col-md-6 shade " id="severe-shade">
													<div class="shade-image d-none d-sm-block">	
														<img src="images/roof-3.png">
													</div>
													<h5 class="mt-3"><i class="fas fa-check-square d-block d-sm-none mt-1 mr-2"></i> Severe Shade</h5>
													<p class="d-none d-sm-block">Most or all of the roof is covered at certain times/all times.</p>
													<input type="radio" class="severe-shade" name="shade" value="Severe Shade" style="display:none">
													<input type="hidden" name="step" class="step" value="step4">
													<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
												</div>
											</div>
										</div>

										<div class="row">
											<div class="text-left col-6">
												<button class="btn text-left btn-default rounded-0 btn-link btn-sm" onclick="gotoStep(3)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>

											<div class="col-6 text-right">
												<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
											</div>
										</div>

									</form>
								</div>

								<!-- Step 5 -->
								<div class="col form pt-0 pt-sm-8 mx-auto step-5" style="display:none;">
								
									<h3 class="font-weight-light mb-0 mb-md-5">Please enter your contact information to find out if you qualify for solar.</h3>
									<form id="step-5" class="needs-validation row" novalidate>
										<div class="col-12">
										<div class="row m-md-auto">
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" placeholder="First name">
												<div class="invalid-feedback">
													Please provide first name.
												</div>
											</div>
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" placeholder="Last name">
												<div class="invalid-feedback">
													Please provide last name.
												</div>
											</div>
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" placeholder="Email">
												<div class="invalid-feedback">
													Please provide a valid email.
												</div>
											</div>
											<div class="col-12 col-md-12 col-lg-6">
												<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" id="validationCustom04"
													placeholder="Phone">
												<div class="invalid-feedback">
													Please provide a valid phone.
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-12 text-center my-3">
												<button class="btn btn-primary rounded-0 btn-sm final_submit" type="submit">Submit</button>
												<img src="/landing/images/loading_bubble.svg" style="display:none" class="loader_image">
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<p class="my-3 small w-75 ml-auto mr-auto">*By clicking the "Submit" button you agree to our Terms and Privacy Policy and authorize Momentum Solar to reach out to you by phone and/or email.</p>
											</div>
										</div>

										<div class="row">
											<div class="col-12">
												<input type="hidden" name="step" class="step" value="step5">
												<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
												<div class="row">
													<div class="text-left">
														<button class="btn btn-default rounded-0 btn-sm btn-link" onclick="gotoStep(4)" type="button">
															<i class="fas fa-chevron-left"></i>Back</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									</form>
									<div class="bot-sec mt-5" style="display:none;">
										<div class="d-flex col- mx-o justify-content-between">

											<p class="back d-flex align-items-center"><span><i class="fas fa-chevron-left"></i></span>Back</p>
											<button class="btn btn-primary rounded-0" type="submit">Next</button>
										</div>
									</div>
								</div>
								<div class="col form py-5 mx-auto step-6" style="display:none;">

								<h3 class="text-success text-center">
									<span class="d-block text-center">
									<i class="fas fa-check-circle mb-3 fa-2x"></i></span>Thank you for your interest in Momentum Solar. Our team will get back to you within 24-48 hours.
								</h3>
								</div>
								<div class="close" style="display:none;">
									<i class="fas fa-times"></i>
								</div>
							</div>
						</div>
						<div class="smallnotes"><i class="fas fa-lock"></i> Your information is secure</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="myModal_schedule_call" class="modal fade custom-modal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="scrolltoelement"></div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="container">
						<div class="row scheduledacallForm">
							<p class="mb-2 mt-2 custom-width text-center">Schedule a no-obligation phone call with a solar expert.</p>
							<div class="col d-flex text-center align-items-center">
								<form class="needs-validation mx-auto formmodal form schedule_call" method="post">
									<div class="row">
										<div class="col-12 mb-4">
											<input type="text" name="fname" class="form-control rounded" placeholder="First Name *" required="">
										</div>
										<div class="col-12 mb-4">
											<input type="text" name="lname" class="form-control rounded" placeholder="Last Name *" required="">
										</div>
										<div class="col-12 mb-4">
											<input type="text" name="phone" class="form-control rounded phone_us" placeholder="Phone *" required="">
										</div>
										<div class="col-12 mb-4">
											<input type="date" name="date" class="form-control rounded" placeholder="Select Date:&nbsp;" style="min-height: auto;color: #9c9c9c; padding-left: 136px;">
										</div>
										<div class="col-12 mb-4">
											<select name="time" class="form-control rounded" style="color: #9c9c9c;">
												<option disabled="">Time</option>
												<option value="9:00 AM">9:00 AM</option>
												<option value="9:15 AM">9:15 AM</option>
												<option value="9:30 AM">9:30 AM</option>
												<option value="9:45 AM">9:45 AM</option>
												<option value="10:00 AM">10:00 AM</option>
												<option value="10:15 AM">10:15 AM</option>
												<option value="10:30 AM">10:30 AM</option>
												<option value="10:45 AM">10:45 AM</option>
												<option value="11:00 AM">11:00 AM</option>
												<option value="11:15 AM">11:15 AM</option>
												<option value="11:30 AM">11:30 AM</option>
												<option value="11:45 AM">11:45 AM</option>
												<option value="12:00 AM">12:00 AM</option>
												<option value="12:15 AM">12:15 AM</option>
												<option value="12:30 AM">12:30 AM</option>
												<option value="12:45 AM">12:45 AM</option>
												<option value="1:00 PM">1:00 PM</option>
												<option value="1:15 PM">1:15 PM</option>
												<option value="1:30 PM">1:30 PM</option>
												<option value="1:45 PM">1:45 PM</option>
												<option value="2:00 PM">2:00 PM</option>
												<option value="2:15 PM">2:15 PM</option>
												<option value="2:30 PM">2:30 PM</option>
												<option value="2:45 PM">2:45 PM</option>
												<option value="3:00 PM">3:00 PM</option>
												<option value="3:15 PM">3:15 PM</option>
												<option value="3:30 PM">3:30 PM</option>
												<option value="3:45 PM">3:45 PM</option>
												<option value="4:00 PM">4:00 PM</option>
												<option value="4:15 PM">4:15 PM</option>
												<option value="4:30 PM">4:30 PM</option>
												<option value="4:45 PM">4:45 PM</option>
												<option value="5:00 PM">5:00 PM</option>
												<option value="5:15 PM">5:15 PM</option>
												<option value="5:30 PM">5:30 PM</option>
												<option value="5:45 PM">5:45 PM</option>
												<option value="6:00 PM">6:00 PM</option>

											</select>
										</div>
										<div class="col-12">
											<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
											<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
											<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
											<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
											<input type="hidden" name="url" value="<?php echo $current_link?>">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
										    <input type="hidden" name="schedule_a_call" value="1">
											<input id="submit" type="submit" class="btn btn-primary form-control rounded" value="Submit" style="min-height: auto;cursor: pointer;line-height: 20px;">
											<img src="/landing/images/loading_bubble.svg" style="display:none" class="loader_image">
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="response_div" style="display:none;"><h4>Thank you for your request , Our team will get back to you within 24-48 hours.</h4></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="myModal_b_app" class="modal fade custom-modal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<div id="scrolltoelement"></div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="container">
						<div class="row bookappointmentForm">
							<p class="mb-2 mt-2 custom-width text-center">Schedule a no-obligation online appointment with a solar expert.</p>
							<div class="col d-flex text-center align-items-center">
								<form class="needs-validation mx-auto formmodal form book_appointment"  method="post" autcomplete="off">
									<div class="row">
										<div class="col-12 mb-4">
											<input type="text" name="fname" class="form-control rounded" placeholder="First Name *" required="">
										</div>
										<div class="col-12 mb-4">
											<input type="text" name="lname" class="form-control rounded" placeholder="Last Name *" required="">
										</div>
										<div class="col-12 mb-4">
											<input type="text" name="phone" class="form-control rounded phone_us" placeholder="Phone *" required="">
										</div>
										<div class="col-12 mb-4">
											<input type="text" name="full_address" class="form-control rounded" id="full_address" placeholder="Address *" required=""  >
											
											<input type="hidden" id="street_number" name="street_number" value="">
											<input type="hidden" id="route" name="street_name" value="">
											<input type="hidden" id="locality" name="city" value="">
											<input type="hidden" id="postal_code" name="postal_code" value="11201">
											<input type="hidden" id="administrative_area_level_1" name="state_abbr" value="">
											<input type="hidden" id="country" name="country" value="">
											
											
										</div>
										<div class="col-12 mb-4">
											<input type="text" name="zip" class="form-control rounded" placeholder="Zip Code *" required="">
										</div>
										
										<div class="col-12">
										    <input type="hidden" name="book_appointment" value="1">
											<input type="hidden" name="utm_source" value="<?php echo $_REQUEST['utm_source']?>">
											<input type="hidden" name="utm_medium" value="<?php echo $_REQUEST['utm_medium']?>">
											<input type="hidden" name="utm_campaign" value="<?php echo $_REQUEST['utm_campaign']?>">
											<input type="hidden" name="utm_content" value="<?php echo $_REQUEST['utm_content']?>">
											<input type="hidden" name="url" value="<?php echo $current_link?>">
											<input type="hidden" name="token" class="freetoken" value="<?php echo $token;?>">
											<input id="submit-bookappt" type="button" class="btn btn-primary form-control rounded" value="Next" style="min-height: auto;cursor: pointer;line-height: 20px;">
											<img src="/landing/images/loading_bubble.svg" style="display:none" class="loader_image">
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="response_div" style="display:none;"><h4>Thank you for your request , Our team will get back to you within 24-48 hours.</h4></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom JavaScript for this theme -->
	<script src="js/jquery.validate.js"></script>
	<script src="js/app.js"></script>
	<script src="js/jquery.mask.js"></script>
    <!--Fading out "Get Free Quote" button in mobile-->
	
		<script>


		var placeSearch, autocomplete;

		var componentForm = {

			street_number: 'short_name',

			route: 'long_name',

			locality: 'long_name',

			administrative_area_level_1: 'short_name',

			country: 'long_name',

			postal_code: 'short_name'

		}; 

		function initAutocomplete() {

			// Create the autocomplete object, restricting the search to geographical

			// location types.

			autocomplete = new google.maps.places.Autocomplete(

				(document.getElementById('full_address')), {

					types: ['geocode']

				});

			// When the user selects an address from the dropdown, populate the address

			// fields in the form.

			autocomplete.addListener('place_changed', fillInAddress);

		}

		function fillInAddress() {

			// Get the place details from the autocomplete object.

			var place = autocomplete.getPlace();

			for (var component in componentForm) {

				document.getElementById(component).value = '';

				document.getElementById(component).disabled = false;

			}

			// Get each component of the address from the place details

			// and fill the corresponding field on the form.

			for (var i = 0; i < place.address_components.length; i++) {

				var addressType = place.address_components[i].types[0];

				if (componentForm[addressType]) {

					var val = place.address_components[i][componentForm[addressType]];

					document.getElementById(addressType).value = val;

				}

			}

		}
		
		function geolocate() {

			if (navigator.geolocation) {

				navigator.geolocation.getCurrentPosition(function (position) {

					var geolocation = {

						lat: position.coords.latitude,

						lng: position.coords.longitude

					};

					var circle = new google.maps.Circle({

						center: geolocation,

						radius: position.coords.accuracy

					});

					autocomplete.setBounds(circle.getBounds());

				});

			}

		}

		 

	</script>
	
	
	
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete"

	 async defer></script>
	
    <script>
        var windowsize = $(window).width();
        if(windowsize <= 425){
            $('.fixed-bottom').css('position','unset');
            $('.banner ').css('margin-top','0');
            var preScroll = 0;
            $(window).scroll(function(){
                var currentScroll = $(this).scrollTop();
                if (currentScroll > 350){
                    $('.fixed-bottom').fadeIn(500).css('position','fixed');
//                    $('.banner ').css('margin-top','48');
                } else {
                    $('.fixed-bottom').fadeOut(500).css('position','unset');
                }
                preScroll = currentScroll;
            });
        }

    </script>
	<!-- Facebook Pixel Code -->
	<script>
		! function (f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function () {
				n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '252884438399772');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=696369663874492&ev=PageView&noscript=1" />
	</noscript>
	<!-- End Facebook Pixel Code -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75829764-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'UA-75829764-1');
		gtag('config', 'AW-922148957');
	</script>
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-75829764-1', 'momentumsolar.com');
		ga('set', 'page', '/appointments/');
		ga('send', 'pageview');
	</script>
<script>
	  gtag('config', 'AW-922148957/sK1kCKaBr5QBEN3A27cD', {
    'phone_conversion_number': '<?php echo $phone;?>'
  });
</script>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js" data-processed="true"></script>
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"26038347"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>


<!-- Taboola Pixel Code -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1145705});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1145705/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='//trc.taboola.com/1145705/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript>
<!-- End of Taboola Pixel Code -->

<script data-obct type="text/javascript">
  /** DO NOT MODIFY THIS CODE**/
  !function(_window, _document) {
    var OB_ADV_ID='004f7faeddae738de0c5c31bb62c5e99e5';
    if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
    var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
obApi('track', 'PAGE_VIEW');
</script><!-- Hotjar Tracking Code for https://momentumsolar.com --><script>    (function(h,o,t,j,a,r){        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};        h._hjSettings={hjid:1275020,hjsv:6};        a=o.getElementsByTagName('head')[0];        r=o.createElement('script');r.async=1;        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;        a.appendChild(r);    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');</script>
<script async type="text/javascript" src="//static.klaviyo.com/onsite/js/klaviyo.js?company_id=QGKQnL"></script>
<!-- Snap Pixel Code --> <script type='text/javascript'> (function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function() {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)}; a.queue=[];var s='script';r=t.createElement(s);r.async=!0; r.src=n;var u=t.getElementsByTagName(s)[0]; u.parentNode.insertBefore(r,u);})(window,document, 'https://sc-static.net/scevent.min.js'); snaptr('init', 'aef0a08a-ac06-4142-b18d-531729c6cfa6', { 'user_email': '__INSERT_USER_EMAIL__' }); snaptr('track', 'PAGE_VIEW'); </script> <!-- End Snap Pixel Code -->
<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','o2rlz');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10081644'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
<script type="text/javascript">
    adroll_adv_id = "6GSI7DKO2ZAUFHVY3S2HXR";
    adroll_pix_id = "KXJUCWTBX5EYDF7NKT2JUM";
    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());
</script>
<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"> </script> <script type="text/javascript"> window.criteo_q = window.criteo_q || []; var deviceType = /iPad/.test(navigator.userAgent) ? "t" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? "m" : "d"; window.criteo_q.push( { event: "setAccount", account: 71272 }, { event: "setSiteType", type: deviceType }, { event: "viewItem", item: "1" } ); </script>
<script>
(function(document, tag) {
    var script = document.createElement(tag);
    var element = document.getElementsByTagName('body')[0];
    script.src = 'https://acsbap.com/apps/app/assets/js/acsb.js'; script.async = true; script.defer = true;
    (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script);
    script.onload = function() {
        acsbJS.init({
            statementLink     : '',
            feedbackLink      : '',
            footerHtml        : '',
            hideMobile        : false,
            hideTrigger       : false,
            language          : 'en',
            position          : 'left',
            leadColor         : '#146ff8',
            triggerColor      : '#146ff8',
            triggerRadius     : '50%',
            triggerPositionX  : 'left',
            triggerPositionY  : 'bottom',
            triggerIcon       : 'default',
            triggerSize       : 'medium',
            triggerOffsetX    : 20,
            triggerOffsetY    : 20,
            mobile            : {
                triggerSize       : 'small',
                triggerPositionX  : 'left',
                triggerPositionY  : 'center',
                triggerOffsetX    : 0,
                triggerOffsetY    : 0,
                triggerRadius     : '0'
            }
        });
    };
}(document, 'script'));

$(document).ready(function() {
	$("#full_address").attr('autocomplete', 'none');
});

$("form#step-5").submit(function(e) {
					
					e.preventDefault();    
					/*
					$('#radio_button').attr("checked", "checked");
					var vfirstName = jQuery('input[name="first_name2"]').val();
					var vlastName = jQuery('input[name="last_name2"]').val();
					var vemail = jQuery('input[name="email2"]').val();
					var vphone = "+1" + jQuery('input[name="phone2"]').val();
					var vzip = jQuery('input[name="zip2"]').val();
					var vaddress = jQuery('input[name="full_address2"]').val();
					var vcity = jQuery('input[name="city2"]').val();
					var vstate = $( "#state option:selected" ).text();
				*/
               
					Calendly.initInlineWidget({
					url: 'https://calendly.com/virtual-appointment-leads/test_team_followup-appt',
					parentElement: document.getElementById('booking'),
					prefill: {
					firstName: vfirstName,
					lastName: vlastName,
					email: vemail,
					
					
					customAnswers: {
						a1:vzip,
						a2:billchecked,
						a3:vaddress,
						a4:vcity,
						a5:vstate,
						a7: vphone

					}
					},
					utm: {
						utmCampaign: utmcampaign,
						utmSource: utmsource,
						utmMedium: utmmedium,
						utmContent: utmcontent

					}
					});

				});
</script>
<style>
	input[type='date']:before {
   color: #495057;
    content: attr(placeholder);
    position: absolute;
    left: 35px;
}
</style>
</body>

</html>