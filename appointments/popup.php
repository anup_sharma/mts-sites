<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Less Css -->
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <title>Funel Form</title>
  </head>
  <body>
    <section>
        <div class="container">
		
 <?php 
	  function generateRandomString($length = 10) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
	}
	  
	  $token = generateRandomString();
	  
	  ?>
            <div class="row">
                <div class="col d-flex text-center align-items-center" style="height:100vh;">
                    <!-- Step 1 -->
                    <div class="col form py-1 mx-auto step-1">
                        <h3 class="font-weight-light">What is your zip code?</h3>
                        <p class="mb-5 mt-4">Your zip code will allow Momentum Solar to calculate state and utility incentives to maximize your tax benefits.</p>
                        <form id="step-1" class="needs-validation mx-auto" novalidate>
                             <input id="zip" class="form-control mb-3 rounded-0" placeholder="Enter ZIP Code" autocomplete="off" name="zip" type="text">
                            <div class="invalid-feedback mb-4">
                                Please provide a valid zip.
                            </div>
							<input type="hidden" name="step" class="step" value="step1">
                            <input type="hidden" name="token" value="<?php echo $token;?>">
							<div class="center-block">
								<button class="btn btn-primary rounded-0 btn-block" type="submit">Next</button>
							</div>
                        </form>
                    </div>
                    
                    <!-- Step 2 -->
                    <div class="col form pt-5 pb-0 mb-0 mb-xl-0 mx-auto step-2" style="display:none;">
                        <h3 class="font-weight-light mb-4">What is your average monthly electric bill?</h3>
                        <form id="step-2" class="needs-validation mx-auto bill-box" novalidate>
                            <div id="1" class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="Under $50" id="customRadioInline1" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline1">Under $50</label>
                            </div>
                            <div id="2" class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="$50 - $150" id="customRadioInline2" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline2">$50 - $150</label>
                            </div>
                            <div id="3" class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="$150 - $350" id="customRadioInline3" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline3">$150 - $350</label>
                            </div>
                            <div id="4" class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="$300 - $450" id="customRadioInline4" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline4">$300 - $450</label>
                            </div>
                            <div id="5" class="custom-control custom-radio custom-control-inline checkbox">
                                <input type="radio" value="Over $450" id="customRadioInline5" name="customRadioInline1" class="custom-control-input electicbill_radio">
                                <label class="custom-control-label" for="customRadioInline5">Over $450</label>
                            </div>
							<input type="hidden" name="step" class="step" value="step2">
                            <input type="hidden" name="token" value="<?php echo $token;?>">
							<div class="row">
								  <div class="text-left col-6">
									  <button class="btn btn-default rounded-0 btn-sm" onclick="gotoStep(1)" type="button">
									  <i class="fas fa-chevron-left"></i>Back</button>
								  </div>
								  <div class="col-6">
									<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
								  </div>
						   </div>
                        </form>
                    </div>
                    
                    <!-- Step 3 -->
                    <div class="col form py-5 mx-auto step-3" style="display:none;">
                        <h3 class="font-weight-light">Please enter your home address.</h3>
                        <p class="mt-4 mb-5">This will allow our team to gather more information about your roof. We will not sell, trade or rent your personal information to others without your permission.</p>
                        <form id="step-3" class="needs-validation col-sm-10 mx-auto" novalidate>
                            <input type="text" name="full_address" class="form-control mb-3 rounded-0" id="full_address" placeholder="Address">
							
							  <input type="hidden" id="street_number"
                                          name="street_number"
                                          value="">
                                       <input type="hidden" id="route"
                                          name="street_name"
                                          value="">
                                       <input type="hidden" id="locality"
                                          name="city"
                                          value="">
                                       <input type="hidden" id="postal_code"
                                          name="postal_code"
                                          value="11201">
                                       <input type="hidden" id="administrative_area_level_1"
                                          name="state_abbr"
                                          value="">
                                       <input type="hidden" id="country"
                                          name="country"
                                          value="">
							
                            <div class="invalid-feedback">
                                Please provide a valid Address.
                            </div>
							<input type="hidden" name="step" class="step" value="step3">
                            <input type="hidden" name="token" value="<?php echo $token;?>">
							<div class="row">
								  <div class="text-left col-6 ">
									<button class="btn btn-default rounded-0 btn-sm text-left" onclick="gotoStep(2)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>
								  <div class="col-6">
								
									<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
								  </div>
								</div>
                        </form>
                    </div>

                    <!-- Step 4 -->
                    <div class="col pt-5 form mx-auto step-4" style="display: none;">
                        <h3 class="font-weight-light mb-3">How much shade does your roof get from 9 AM - 3 PM?</h3>
						<form id="step-4">
                        <div class="row">
                            <div class="col-4 col-md-4 shade" id="no-shade">
                                <img src="images/roof-1.png">
                                <h5 class="mt-3">No Shade</h5>
                                <p>Entire roof is exposed to sunlight.</p>
								<input type="radio" class="no-shade" name="shade" value="No Shade" style="display:none">
                            </div>
                            <div class="col-4 col-md-4 shade" id="some-shade">
                                <img src="images/roof-2.png">
                                <h5 class="mt-3">Some Shade</h5>
                                <p>Parts of the roof are covered at certain times/all times.</p>
								<input type="radio" class="some-shade" name="shade" value="Some Shade" style="display:none">
                            </div>
                            <div class="col-4 col-md-4 shade" id="severe-shade">
                                <img src="images/roof-3.png">
                                <h5 class="mt-3">Severe Shade</h5>
                                <p>Most or all of the roof is covered at certain times/all times.</p>
								<input type="radio" class="severe-shade" name="shade" value="Severe Shade" style="display:none">
								<input type="hidden" name="step" class="step" value="step4">
                                <input type="hidden" name="token" value="<?php echo $token;?>">
                            </div>
							
                        </div>
						
						<div class="row">
								  <div class="text-left col-6">
									<button class="btn btn-default rounded-0 btn-sm" onclick="gotoStep(3)" type="button"><i class="fas fa-chevron-left"></i>Back</button></div>
								  
								  <div class="col-6">
									<button class="btn btn-primary rounded-0 btn-sm" type="submit">Next</button>
								  </div>
								</div>
						
						</form>
                    </div>

                    <!-- Step 5 -->
                    <div class="col pt-1 pt-sm-8 form mx-auto step-5" style="display:none;">
                        <h3 class="font-weight-light mb-3">Please enter your contact information to receive a <b>no-obligation</b> detailed quote from our solar professionals.</h3>
                        <form id="step-5" class="needs-validation row" novalidate>
                            <div class="col-12 col-md-6">
                                <input type="text" name="first_name" class="form-control mb-3 rounded-0" id="validationCustom01" placeholder="First name" >
                                <div class="invalid-feedback">
                                    Please provide first name.
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <input type="text"  name="last_name"class="form-control mb-3 rounded-0" id="validationCustom02" placeholder="Last name">
                                <div class="invalid-feedback">
                                    Please provide last name.
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <input type="text" name="email" class="form-control mb-3 rounded-0" id="validationCustom03" placeholder="Email" >
                                <div class="invalid-feedback">
                                    Please provide a valid email.
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <input type="text"  name="phone" class="form-control mb-3 rounded-0 phone_us" maxlength="14" id="validationCustom04" placeholder="Phone">
                                <div class="invalid-feedback">
                                    Please provide a valid phone.
                                </div>
                            </div>
                            <div class="col-12">
								<input type="hidden" name="step" class="step" value="step5">
                                <input type="hidden" name="token" value="<?php echo $token;?>">
                                <div class="row">
								  <div class="text-left col-6">
									<button class="btn btn-default rounded-0 btn-sm" onclick="gotoStep(4)" type="button">
									<i class="fas fa-chevron-left"></i>Back</button>
								  </div>
								  <div class="col-6">
									<button class="btn btn-primary rounded-0 btn-sm" type="submit">Submit</button>
								  </div>
								</div>
                            </div>
                            <div class="col">
                                <p class="mt-3 small">*By clicking the "Submit" button you agree to our Terms and Privacy Policy and authorize Momentum Solar to reach out to you with a no-obligation quote or to ask for additional information in order to provide an accurate quote.</p>
                            </div>
                        </form>
                        <div class="bot-sec mt-5" style="display:none;">
                            <div class="d-flex col- mx-o justify-content-between">
								
                                <p class="back d-flex align-items-center"><span><i class="fas fa-chevron-left"></i></span>Back</p>
							    <button class="btn btn-primary rounded-0" type="submit">Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="col py-5 form mx-auto step-6" style="display:none;">
						<h3>Thank you for your interest in Momentum Solar. Our team will get back to you within 24-48 hours.</h3>
					</div>
                    <div class="close" style="display:none;">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/jquery.mask.js"></script>
	<script>
		$('.phone_us').mask('(000) 000-0000');
         var placeSearch, autocomplete;
         var componentForm = {
           street_number: 'short_name',
           route: 'long_name',
           locality: 'long_name',
           administrative_area_level_1: 'short_name',
           country: 'long_name',
           postal_code: 'short_name'
         };
         
         function initAutocomplete() {
           // Create the autocomplete object, restricting the search to geographical
           // location types.
           autocomplete = new google.maps.places.Autocomplete(
               (document.getElementById('full_address')),
               {types: ['geocode']});
         
           // When the user selects an address from the dropdown, populate the address
           // fields in the form.
           autocomplete.addListener('place_changed', fillInAddress);
         }
         
         function fillInAddress() {
           // Get the place details from the autocomplete object.
           var place = autocomplete.getPlace();
         
           for (var component in componentForm) {
             document.getElementById(component).value = '';
             document.getElementById(component).disabled = false;
           }
         
           // Get each component of the address from the place details
           // and fill the corresponding field on the form.
           for (var i = 0; i < place.address_components.length; i++) {
             var addressType = place.address_components[i].types[0];
             if (componentForm[addressType]) {
               var val = place.address_components[i][componentForm[addressType]];
               document.getElementById(addressType).value = val;
             }
           }
         } 
         
         // Bias the autocomplete object to the user's geographical location,
         // as supplied by the browser's 'navigator.geolocation' object.
         function geolocate() {
           if (navigator.geolocation) {
             navigator.geolocation.getCurrentPosition(function(position) {
               var geolocation = {
                 lat: position.coords.latitude,
                 lng: position.coords.longitude
               };
               var circle = new google.maps.Circle({
                 center: geolocation,
                 radius: position.coords.accuracy
               });
               autocomplete.setBounds(circle.getBounds());
             });
           }
         }
         
      </script>
	  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete"
         async defer></script>
		 	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '252884438399772');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=696369663874492&ev=PageView&noscript=1"
	/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-75829764-1"></script>
	<script>  window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(	arguments);}  
		gtag('js', new Date());
		//gtag('config', 'UA-75829764-1');
		gtag('config', 'AW-922148957');
	</script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-75829764-1', 'momentumsolar.com');
			ga('set', 'page', '/quote/');
			ga('send', 'pageview');

		</script>
  </body>
</html>