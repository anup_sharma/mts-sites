<?php 

include('header.php');

?>

<title>Home Solar Incentives</title>



<!--<script type="text/javascript" src="script/form.js"></script>

-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T9FDNR7');</script>
<!-- End Google Tag Manager -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">

  #user_form fieldset:not(:first-of-type) {

    display: none;

  }


section.topbanner {
    background: url(https://homesolarincentives.com/affiliate/images/bg.png);
    background-size: cover;
    border-radius: 20px;
    padding-bottom: 50px;
    padding-top: 3%;
    box-shadow: rgb(156 152 152) 0px 0px 5px;
}

.progress {
    width: 99%;
    border-radius: 15px;
	margin-top: 3%;
}



h2 {

    text-align: center;

    font-weight: 600;

}



.next-btn {
    background: none !important;
    border: none;
    color: #2B2828;
    font-weight: 700;
    font-size: 20px;
    padding-top: 5px !important;
    padding-bottom: 5px !important;
    padding-left: 5px !important;
    padding-right: 5px !important;
    border: none;
    font-weight: 400;
    margin: auto;
    margin-left: 88%;
}

p{

	text-align:center;

}

.btn-outline-dark{

    background-color: white !important;


}

.btn-group-vertical {
    min-width: 72%;
    padding: 2%;
    margin-top: 2%;
}

.btn-group-vertical>.btn+.btn, .btn-group-vertical>.btn+.btn-group, .btn-group-vertical>.btn-group+.btn, .btn-group-vertical>.btn-group+.btn-group {

    margin-top: 5%;

    margin-left: 0;

}

label.btn.btn-outline-dark.active {

    background-color: rgb(125 186 147) !important;

	color: white !important;

}
label.btn.btn-outline-dark.inactive {

background-color: white !important;

color: #2B2828 !important;
border: 2px solid red !important;

}

.btn-group-vertical>.btn:first-child:not(:last-child) {

    border-top-right-radius: 0;

    border-bottom-right-radius: 0;

    border-bottom-left-radius: 0;

}

fieldset {

    max-width: 95%;

    padding-left: 2%;

}

.h3, h3 {

    text-align: center;

}

label.btn.btn-outline-dark {

    padding: 4%;

}
.form-group {
    margin-bottom: 9%;
}
.alert.alert-success {
    background: none;
    color: #FF0000;
    border: none;
    text-align: center;
    margin-top: -6%;
    margin-bottom: 9%;
}



#preloader {

    

    top: 0;

    left: 0;

    width: 100%;

    height: 250px;

}

#preloader2 {

    

    top: 0;

    left: 0;

    width: 100%;

    height: auto;

	background:white;

}



#loader {

    display: block;

    position: relative;

    left: 50%;

    top: 50%;

    width: 150px;

    height: 150px;

    margin: -75px 0 0 -75px;

    border-radius: 50%;

    border: 3px solid transparent;

    border-top-color: #9370DB;

    -webkit-animation: spin 2s linear infinite;

    animation: spin 2s linear infinite;

}

#loader:before {

    content: "";

    position: absolute;

    top: 5px;

    left: 5px;

    right: 5px;

    bottom: 5px;

    border-radius: 50%;

    border: 3px solid transparent;

    border-top-color: #BA55D3;

    -webkit-animation: spin 3s linear infinite;

    animation: spin 3s linear infinite;

}

#loader:after {

    content: "";

    position: absolute;

    top: 15px;

    left: 15px;

    right: 15px;

    bottom: 15px;

    border-radius: 50%;

    border: 3px solid transparent;

    border-top-color: #FF00FF;

    -webkit-animation: spin 1.5s linear infinite;

    animation: spin 1.5s linear infinite;

}

@-webkit-keyframes spin {

    0%   {

        -webkit-transform: rotate(0deg);

        -ms-transform: rotate(0deg);

        transform: rotate(0deg);

    }

    100% {

        -webkit-transform: rotate(360deg);

        -ms-transform: rotate(360deg);

        transform: rotate(360deg);

    }

}

@keyframes spin {

    0%   {

        -webkit-transform: rotate(0deg);

        -ms-transform: rotate(0deg);

        transform: rotate(0deg);

    }

    100% {

        -webkit-transform: rotate(360deg);

        -ms-transform: rotate(360deg);

        transform: rotate(360deg);

    }

}




img.logo {
    padding: 7%;
}

body {
    font-family: "lato";
    
}


.btn-group-vertical>.btn:first-child:not(:last-child) {
	background-color: white;
    border: 1px solid #D3D3D3;
    color: #2B2828;
    padding: 70px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 60%;
    margin-right: 9%;
    margin-bottom: 2%;
}
.btn-group-vertical>.btn, .btn-group-vertical>.btn-group, .btn-group-vertical>.btn-group>.btn {
    width: auto;
}
label.btn.btn-outline-dark.active {
    background-color: #99E766 !important;
    color: white !important;
	border:none !important;
}
label.btn.nobtn.active {
    background-color: #d3d3d3 !important;
    color: white !important;
	
}
.btn.active, .btn:active {
 
    box-shadow: none;
    /* border: none; */
}
.btn-group-vertical>.btn:last-child:not(:first-child) {
	background-color: white;
    border: 1px solid #D3D3D3;
    color: #2B2828;
    padding: 70px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 60%;
    margin-right: 9%;
    margin-bottom: 2%;
}

.arrow {
    border: solid #99E766;
    border-width: 0 5px 5px 0;
    display: inline-block;
    padding: 10px;
    vertical-align: middle;
}

.right {
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
}
.btn-info:hover {
    color: #2B2828;
}
.btn-danger, .btn-default, .btn-info, .btn-primary, .btn-success, .btn-warning {
    text-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.form-control {
    display: block;
    width: 100%;
    height: auto;
    padding: 6px 12px;
    font-size: 17px;
    line-height: 1.42857143;
    color: #555;
    text-align: center;
    background-color: #fff;
    background-image: none;
    border: 1px solid #99e766;
    border-radius: 21px;
    margin-bottom: 8% !important;
    
}
.alert {
    text-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}
label.btn.btn-outline-dark2.inactive {
    background-color: white !important;
    color: white !important;
    border-radius: 65%;
    width: 15%;
    border: 2px solid red;
    height: 15vh;
    padding-top: 2%;
}
label.btn.btn-outline-dark2.active {
    background-color: white !important;
    color: white !important;
    border-radius: 65%;
    width: 15%;
    border: 2px solid #99E766;
    height: 15vh;
    padding-top: 2%;
}


label.btn.btn-outline-dark2 {
    margin-right: 3%;
    margin-top: 5%;
}
div#slider {
    background: #f4f4f4;
    border: none;
    border-radius: 15px;
}
span.ui-slider-handle.ui-corner-all.ui-state-default {
    height: 20px;
    background: #99E766;
    border: none;
    border-radius: 65%;
    width: 20px;
}
.ui-slider .ui-slider-handle {
  
    cursor: pointer;
}
#slider-value {
  margin-bottom: 2%;
  font-size: 34px;
  color: #99E766;
}

select#provider-select {
    margin-top: 11%;
    width: 54%;
    text-align-last: center;
    margin-left: 22%;
}
.error {
	border: 2px solid red !important;
}
</style>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>



$(document).ready(function() {


	$("#slider").slider(
{
            value:100,
            min: 0,
            max: 700,   
            slide: function( event, ui ) {
				$( "#slider-value" ).html( "$" + ui.value );
				if (ui.value > 200){
					
					$("span.ui-slider-handle.ui-corner-all.ui-state-default").css('background', '#F5E739 !important');
					$("#slider-value").css('color', '#F5E739 !important');
				}
				if (ui.value > 600){
					
					$("span.ui-slider-handle.ui-corner-all.ui-state-default").css('background', '#FF0000 !important');
					$("#slider-value").css('color', '#FF0000 !important');
				}
				if (ui.value < 200){
					
					$("span.ui-slider-handle.ui-corner-all.ui-state-default").css('background', '#99E766 !important');
					$("#slider-value").css('color', '#99E766 !important');
				}
				
            }
}
);

$( "#slider-value" ).html(  $('#slider').slider('value') );

		function checkZip(value) {

       		 return (/(^\d{5}$)|(^\d{5}-\d{4}$)/).test(value);

   		 }

});

</script>

<?php include('container.php');?>


<h2>Get Your Free Solar Quotes </h2>
<p>

Take this survey to find out if you qualify. Only takes 60 seconds!
</p>

<section class="topbanner">

<div class="container">



		

	

	

	<!-- form starts from here -->

	<form id="user_form"  data-toggle="validator" method="post">	


<!-- Are you a homeowner section  -->
	<fieldset id ="homeset" class="text-center"> 

	<h2> Are your a homeowner?</h2>



	<div class="form-group">

			<div class="btn-group-vertical" data-toggle="buttons">

				<label class="btn btn-outline-dark" >

					<input type="radio" name="options-homerent" id="option1"  > Yes!

				</label>

				<label class="btn btn btn-outline-dark nobtn" >

					<input type="radio" name="options-homerent" id="option2" > No

				</label>


			</div>

	</div>
	<div class="alert alert-success hide"></div>	

	<input type="button" name="next" class="nexthomerent btn btn-info next-btn" value="Next" > <i class="arrow right"></i></input>

	</fieldset>

<!-- Zip Code section -->
	<fieldset style="text-align:center;">

			<h3>What is your zip code?</h3>
			<input type="zip" class="form-control" required id="zip" name="zip" placeholder="Zip" style="width:60%; margin:auto; margin-top: 7%; display:inline;margin-bottom: -3%; !important"> 

			<div class="alert alert-success hide"></div>	

			<input type="button"  name="next" class="nextZip btn btn-info next-btn" value="Next"   > <i class="arrow right"></i></input>

	</fieldset>
<!--  Utility Provider Section -->
	<fieldset style="text-align:center;">

			<h3>Who’s your electric utility provider?</h3>
			
			<select name="utility_provider" type="text" id="provider-select" class="form-control custom-select" required="" data-tf-value="" >
			<option value="" disabled selected>Select a Provider</option>
			<option value="City of Plattsburgh">City of Plattsburgh</option><option value="City of Salamanca">City of Salamanca</option><option value="Jamestown Board of Public Utilities">Jamestown Board of Public Utilities</option><option value="Lake Placid Village">Lake Placid Village</option><option value="Long Island Power Authority">Long Island Power Authority</option><option value="Steuben Rural Electric">Steuben Rural Electric</option><option value="Town of Massena">Town of Massena</option><option value="Village of Arcade">Village of Arcade</option><option value="Village of Fairport">Village of Fairport</option><option value="Village of Freeport">Village of Freeport</option><option value="Village of Rockville Centre">Village of Rockville Centre</option><option value="Village of Rouses Point">Village of Rouses Point</option><option value="Village of Solvay">Village of Solvay</option><option value="OTHER PROVIDER">OTHER PROVIDER</option></select>
			<div class="alert alert-success hide"></div>	

			<input type="button"  name="next" class="nextutilityprovider btn btn-info next-btn" value="Next"   > <i class="arrow right"></i></input>

	</fieldset>

<!-- Shade Section   -->
	<fieldset class="text-center">

	<h2> How sunny is your roof area?</h2>

	

	<div class="form-group">

			<div  data-toggle="buttons">

				<label class="btn btn-outline-dark2" >

					<input type="radio" name="options-shade" id="option1" value="Full Sun"> 
					<img src="https://homesolarincentives.com/affiliate/images/fullsun.png" alt="Full Sun" />
				</label>

				<label class="btn btn btn-outline-dark2" >

					<input type="radio" name="options-shade" id="option2"  value="Partial Sun"> 
					<img src="https://homesolarincentives.com/affiliate/images/partialsun.png" alt="Partial" />
				</label>

				<label class="btn btn-outline-dark2" >

					<input type="radio" name="options-shade" id="option3" value="A lot of Shade" > 
					<img src="https://homesolarincentives.com/affiliate/images/lotofshade.png" alt="A Lot of Shade" />

				</label>

				<label class="btn btn btn-outline-dark2" >

					<input type="radio" name="options-shade" id="option4" value="Uncertain">
					<img src="https://homesolarincentives.com/affiliate/images/uncertain.png" alt="Uncertain" />

				</label>

			

			</div>

	</div>
	<div class="alert alert-success hide"></div>
	<input type="button"  name="next" class="nextShade btn btn-info next-btn" value="Next"   > <i class="arrow right"></i></input>

	</fieldset>


	<!-- Utility Bill Section  -->
	<fieldset class="text-center">

	<h2>How much is your average monthly electric bill?</h2>

	

	<div class="form-group">

			<div class="btn-group-vertical" data-toggle="buttons">

			<p><span id="slider-value"></span></p>
			<div id="slider"></div>


			

			</div>

	</div>
	<div class="alert alert-success hide"></div>
	<input type="button"  name="next" class="nextZip btn btn-info next-btn" value="Next"   > <i class="arrow right"></i></input>


	</fieldset>

<!-- Email Address Section  -->
	<fieldset id="emailSet" style="text-align:center;">

			<h3>What email address should your results be sent to?</h3>
			<input type="zip" class="form-control" required id="email" name="email" placeholder="Email Address" style="width:60%; margin:auto; margin-top: 7%; display:inline;margin-bottom: -5%;"> 

			<div class="alert alert-success hide"></div>	

			<input type="button"  name="next" class="nextEmail btn btn-info next-btn" value="Next"   > <i class="arrow right"></i></input>

	</fieldset>

<!-- Name Section  -->

	<fieldset  class="text-center">
		<h3>What is your name? </h3>
		<div class="form-group" style="text-align:left; margin-top: 11%; margin-left: 16%; margin-right: 15%;">
		<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" style="width: 44%; float: left;">
		<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" style="width: 47%;float: right;">
		</div>
		<div class="alert alert-success hide"></div>

		<input type="button" name="next" class="nextName btn btn-info next-btn" value="Next"> <i class="arrow right"></i></input>



</fieldset>
<!-- Address Section  -->
	

	<fieldset class="text-center">

		<h3>Where do you want the solar panels installed (street address)?</h3>

		<div class="form-group">

		<input class="form-control" name="address" placeholder="Street Address"  id = "address" style="margin-top: 7%; width: 55%; margin-left: 22%; "></textarea>
		</div>
		<div class="alert alert-success hide"></div>
		<input type="button" name="next" class="nextAddress btn btn-info next-btn" value="Next" > <i class="arrow right"></i></input>


	</fieldset>

<!-- Phone Section -->

	<fieldset class="text-center">

		<h3>VWhat phone number can we reach you at?</h3>

		<div class="form-group">

			<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" style=" margin-top: 11%; width: 47%; margin-left: 27%;">

		</div>
		<div class="alert alert-success hide"></div>
	<!--	<input type="submit" name="submit" class="nextPhone btn btn-success next-btn" value="Submit" id ="submitBtn" /><br/><br/>-->
		<input type="button" name="next" class="nextPhone btn btn-info next-btn" value="Next" > <i class="arrow right"></i></input>

		

	</fieldset>

<!-- Congrat Secton -->

	<fieldset id="resultSet" class="text-center">

	<div id="preloader2">

	   <img src="/landing/images/solarloader2.gif" style="width: 59%; margin:auto; margin-top: -2%;"/>

	</div>

		<div id="congratSection">

				<h2><span style ="color:#99E766;">CONGRATULATIONS!</span></h2>

				<h3>Your solar quotes are on the way!</h3>
				

		</div>

	</fieldset>


	</form>	<!-- form ends from here -->

</div>	

</section>

<div class="progress">

	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 20%;background: #99E766;"></div>

</div>

<script>


function showhomeset(){

	document.getElementById('solarset').style.display = "none";

	document.getElementById('homeset').style.display = "block";

}

function showemailSet(){

	document.getElementById('resultSet').style.display = "none";

	document.getElementById('emailSet').style.display = "block";

}

</script>



<script>

$(document).ready(function(){  
  var form_count = 1, form_count_form, next_form, total_forms;

  total_forms = $("fieldset").length;  

  //when homeowner section is completed
$(".nexthomerent").click(function(){		
	var error_message = "" ;

	if ($("input[name='options-homerent']").is(':checked')) {
		
		$('.btn.btn-outline-dark').removeClass('inactive');
		$('.alert-success').addClass('hide');
		form_count_form = $(this).parent();

		next_form = $(this).parent().next();

		next_form.show();

		form_count_form.hide();

		setProgressBar(++form_count);
		} else {
			error_message +="*Please select an option.";
			$('.alert-success').removeClass('hide').html(error_message);
			$('.btn.btn-outline-dark').addClass('inactive');
			return false;

		exit;
	}

});  

// when Zip code is selected
  $(".nextZip").click(function(){
	var zipvalue = $('#zip').val();

    var error_message = "" ;

	if(!zipvalue) {

		error_message +="*Please enter your zip code.";		

	}

	if(error_message) {
		$('.form-control').addClass('error');
		$('.alert-success').removeClass('hide').html(error_message);
        $(".form-control").css("border", "2px solid red !important");
		return false;
		exit;

	} else {
		$('.form-control').removeClass('error');
		$('.alert-success').addClass('hide');
		form_count_form = $(this).parent();
		next_form = $(this).parent().next();
		next_form.show();
		form_count_form.hide();
		setProgressBar(++form_count);

  } 

    

  });  

// when Utility Provider is selected
$(".nextutilityprovider").click(function(){
	var error_message = "" ;
	if($('#provider-select').val() == null ){
		error_message +="*Please enter your utility provider.";		
		$('.alert-success').removeClass('hide').html(error_message);
		$('.form-control').addClass('error');
        $(".form-control").css("border", "1px solid red !important");
		return false;
		exit;
	} else {
		$('.form-control').removeClass('error');
		$('.alert-success').addClass('hide');
		form_count_form = $(this).parent();
		next_form = $(this).parent().next();
		next_form.show();
		form_count_form.hide();
		setProgressBar(++form_count);
	}

  });  

// when shade is selected
$(".nextShade").click(function(){
		var error_message = "" ;
		if ($("input[name='options-shade']").is(':checked')) {
			
			$('.btn.btn-outline-dark2').removeClass('inactive');
			$('.alert-success').addClass('hide');
			form_count_form = $(this).parent();

			next_form = $(this).parent().next();

			next_form.show();

			form_count_form.hide();

			setProgressBar(++form_count);
			} else {
				error_message +="*Please select an option.";
				$('.alert-success').removeClass('hide').html(error_message);
				$('.btn.btn-outline-dark2').addClass('inactive');
				return false;

			exit;
		}

});  

// when email is selected
$(".nextEmail").click(function(){

	var emailvalue = $('#email').val();
	var error_message = "" ;
	if(!emailvalue) {
		error_message +="<br>*Please fill in your email.";
	}
	if(error_message) {
		$('.form-control').addClass('error');
		$('.alert-success').removeClass('hide').html(error_message);
		return false;
		exit;

	} else {
		$('.form-control').removeClass('error');
		$('.alert-success').addClass('hide').html(error_message);
		$('.alert-success').removeClass('show').html(error_message);
		form_count_form = $(this).parent();
		next_form = $(this).parent().next();
		next_form.show();
		form_count_form.hide();
		setProgressBar(++form_count);
	} 

});  

// when name is entered
  $(".nextName").click(function(){
	  var error_message = "" ;
	  var firstname = $('#first_name').val();
	  var lastname = $('#last_name').val();
	  error_message="";
	  if(!firstname || !lastname) {
		  error_message +="<br>*Please enter your name.";

	  }

	  if(error_message) {
		  $('.form-control').addClass('error');
		  $('.alert-success').addClass('show').html(error_message);
		  return false;
			exit;
	  } else {
		   $('.form-control').removeClass('error');
			$('.alert-success').addClass('hide').html(error_message);
			$('.alert-success').removeClass('show').html(error_message);
		  form_count_form = $(this).parent();
		  next_form = $(this).parent().next();
		  next_form.show();
		  form_count_form.hide();
		  setProgressBar(++form_count);

	} 
	});  


// when address is entered
	$(".nextAddress").click(function(){

	  var addressvalue = $('#address').val();
	  var error_message = "" ;
	  if(!addressvalue) {
		  error_message +="<br>*Please enter your street address.";
	  }

	  if(error_message) {
		$('.form-control').addClass('error');
		  $('.alert-success').removeClass('hide').html(error_message);
		  return false;
		  exit;
	  } else {
		$('.form-control').removeClass('error');
		$('.alert-success').addClass('hide').html(error_message);
		  form_count_form = $(this).parent();
		  next_form = $(this).parent().next();
		  next_form.show();
		  form_count_form.hide();
		  setProgressBar(++form_count);

	} 

	});  


// when phone screen is selected and form is submitted
    $(".nextPhone").click(function(){
       
	 var mobilevalue = $('#mobile').val();
	  var error_message = "" ;
	  if(!mobilevalue) {
		 error_message+="<br>*Please enter a valid phone number.";
	  }

	  if(error_message) {
		$('.form-control').addClass('error');
		  $('.alert-success').removeClass('hide').html(error_message);
		  return false;
		  exit;
	  } else {
	
		$('.form-control').removeClass('error');
		$('.alert-success').addClass('hide').html(error_message);
		form_count_form = $(this).parent();
		next_form = $(this).parent().next();
		next_form.show();
		form_count_form.hide();
		setProgressBar(++form_count);
		$("#congratSection").hide();
		$("#preloader2").show();
		setTimeout(function() { $("#preloader2").hide(); }, 9000);
		
		setTimeout(function() { $("#congratSection").show(); }, 9000);
		alert ('end');

	} 
	});







  $(".next").click(function(){

    

	$('.alert-success').addClass('hide').html(error_message);

    form_count_form = $(this).parent();

    next_form = $(this).parent().next();

    next_form.show();

    form_count_form.hide();

    setProgressBar(++form_count);

  });  



  setProgressBar(form_count);  
  function setProgressBar(curStep){

    var percent = parseFloat(100 / total_forms) * curStep;

    percent = percent.toFixed();

    $(".progress-bar")

      .css("width",percent+"%")

      .html(percent+"%");   

  } 

});

</script>

<?php include('footer.php');?> 