<?php 

include('header.php');

?>

<title>Home Solar Incentives</title>



<!--<script type="text/javascript" src="script/form.js"></script>

-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T9FDNR7');</script>
<!-- End Google Tag Manager -->

<style type="text/css">

  #user_form fieldset:not(:first-of-type) {

    display: none;

  }


section.topbanner {
    background: url(https://homesolarincentives.com/affiliate/images/bg.png);
    background-size: cover;
    border-radius: 20px;
    padding-bottom: 50px;
    padding-top: 3%;
    box-shadow: rgb(156 152 152) 0px 0px 5px;
}

.progress {
    width: 99%;
    border-radius: 15px;
	margin-top: 3%;
}



h2 {

    text-align: center;

    font-weight: 600;

}



.next-btn {
    background: none !important;
    border: none;
    color: #2B2828;
    font-weight: 700;
    font-size: 20px;
    padding-top: 5px !important;
    padding-bottom: 5px !important;
    padding-left: 5px !important;
    padding-right: 5px !important;
    border: none;
    font-weight: 400;
    margin: auto;
    margin-left: 88%;
}

p{

	text-align:center;

}

.btn-outline-dark{

    background-color: white !important;


}

.btn-group-vertical {
    min-width: 72%;
    padding: 2%;
    margin-top: 2%;
}

.btn-group-vertical>.btn+.btn, .btn-group-vertical>.btn+.btn-group, .btn-group-vertical>.btn-group+.btn, .btn-group-vertical>.btn-group+.btn-group {

    margin-top: 5%;

    margin-left: 0;

}

label.btn.btn-outline-dark.active {

    background-color: rgb(125 186 147) !important;

	color: white !important;

}

.btn-group-vertical>.btn:first-child:not(:last-child) {

    border-top-right-radius: 0;

    border-bottom-right-radius: 0;

    border-bottom-left-radius: 0;

}

fieldset {

    max-width: 95%;

    padding-left: 2%;

}

.h3, h3 {

    text-align: center;

}

label.btn.btn-outline-dark {

    padding: 4%;

}

.alert.alert-success {
    background: white;
    color: #FF0000;
    border: none;
    width: 54%;
    text-align: center;
}



#preloader {

    

    top: 0;

    left: 0;

    width: 100%;

    height: 250px;

}

#preloader2 {

    

    top: 0;

    left: 0;

    width: 100%;

    height: auto;

	background:white;

}



#loader {

    display: block;

    position: relative;

    left: 50%;

    top: 50%;

    width: 150px;

    height: 150px;

    margin: -75px 0 0 -75px;

    border-radius: 50%;

    border: 3px solid transparent;

    border-top-color: #9370DB;

    -webkit-animation: spin 2s linear infinite;

    animation: spin 2s linear infinite;

}

#loader:before {

    content: "";

    position: absolute;

    top: 5px;

    left: 5px;

    right: 5px;

    bottom: 5px;

    border-radius: 50%;

    border: 3px solid transparent;

    border-top-color: #BA55D3;

    -webkit-animation: spin 3s linear infinite;

    animation: spin 3s linear infinite;

}

#loader:after {

    content: "";

    position: absolute;

    top: 15px;

    left: 15px;

    right: 15px;

    bottom: 15px;

    border-radius: 50%;

    border: 3px solid transparent;

    border-top-color: #FF00FF;

    -webkit-animation: spin 1.5s linear infinite;

    animation: spin 1.5s linear infinite;

}

@-webkit-keyframes spin {

    0%   {

        -webkit-transform: rotate(0deg);

        -ms-transform: rotate(0deg);

        transform: rotate(0deg);

    }

    100% {

        -webkit-transform: rotate(360deg);

        -ms-transform: rotate(360deg);

        transform: rotate(360deg);

    }

}

@keyframes spin {

    0%   {

        -webkit-transform: rotate(0deg);

        -ms-transform: rotate(0deg);

        transform: rotate(0deg);

    }

    100% {

        -webkit-transform: rotate(360deg);

        -ms-transform: rotate(360deg);

        transform: rotate(360deg);

    }

}




img.logo {
    padding: 7%;
}

body {
    font-family: "lato";
    
}


.btn-group-vertical>.btn:first-child:not(:last-child) {
	background-color: white;
    border: 1px solid #D3D3D3;
    color: #2B2828;
    padding: 70px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 60%;
    margin-right: 9%;
    margin-bottom: 2%;
}
.btn-group-vertical>.btn, .btn-group-vertical>.btn-group, .btn-group-vertical>.btn-group>.btn {
    width: auto;
}
label.btn.btn-outline-dark.active {
    background-color: #99E766 !important;
    color: white !important;
	border:none !important;
}
label.btn.nobtn.active {
    background-color: #d3d3d3 !important;
    color: white !important;
	
}
.btn.active, .btn:active {
 
    box-shadow: none;
    /* border: none; */
}
.btn-group-vertical>.btn:last-child:not(:first-child) {
	background-color: white;
    border: 1px solid #D3D3D3;
    color: #2B2828;
    padding: 70px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    border-radius: 60%;
    margin-right: 9%;
    margin-bottom: 2%;
}

.arrow {
    border: solid #99E766;
    border-width: 0 5px 5px 0;
    display: inline-block;
    padding: 10px;
    vertical-align: middle;
}

.right {
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
}
.btn-info:hover {
    color: #2B2828;
}
.btn-danger, .btn-default, .btn-info, .btn-primary, .btn-success, .btn-warning {
    text-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.form-control {
    display: block;
    width: 100%;
    height: auto;
    padding: 6px 12px;
    font-size: 17px;
    line-height: 1.42857143;
    color: #555;
    text-align: center;
    background-color: #fff;
    background-image: none;
    border: 1px solid #99e766;
    border-radius: 21px;
    margin-bottom: 8% !important;
    
}
.alert {
    text-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}
</style>

<script>



$(document).ready(function() {



// Hide the div



	



		function checkZip(value) {

       		 return (/(^\d{5}$)|(^\d{5}-\d{4}$)/).test(value);

   		 }


});

</script>

<?php include('container.php');?>


<h2>Get Your Free Solar Quotes </h2>
<p>

Take this survey to find out if you qualify. Only takes 60 seconds!
</p>

<section class="topbanner">

<div class="container">



		

	

	

	<!-- form starts from here -->

	<form id="user_form"  data-toggle="validator" method="post">	



	<fieldset id ="homeset" class="text-center"> 

	<h2> Are your a homeowner?</h2>



	<div class="form-group">

			<div class="btn-group-vertical" data-toggle="buttons">

				<label class="btn btn-outline-dark" >

					<input type="radio" name="options-homerent" id="option1"  > Yes!

				</label>

				<label class="btn btn btn-outline-dark nobtn" >

					<input type="radio" name="options-homerent" id="option2" > No

				</label>

			

			</div>

	</div>

	<input type="button" name="next" class="next btn btn-info next-btn" value="Next" > <i class="arrow right"></i></input>

	</fieldset>


	<fieldset style="text-align:center;">

			<h3>What is your zip code?</h3>
			<input type="zip" class="form-control" required id="zip" name="zip" placeholder="Zip" style="width:60%; margin:auto; margin-top: 7%; display:inline;margin-bottom: -5%;"> 

			<div class="alert alert-success hide"></div>	

			<input type="button"  name="next" class="nextZip btn btn-info next-btn" value="Next"   > <i class="arrow right"></i></input>

	</fieldset>

	

	<fieldset id ="solarset" class="text-center"> 

	<div id="preloader">

	

	<h3> Checking for Solar programs in <span class="zipvar" style="display:inline;"></span> ...</h3>

		<span id="loader"></span>

	</div>

	<div id="solarincent">

			<h2> Solar incentives found for <span class="cityvar" style="display:inline;"></span>:</h2>

			<p>

			<h3>Now let's qualify for rebates, calculate your extensive savings report, and see how much you can save. <br/>

				How much is your average monthly power bill?</h3>

			</p>

			<div class="form-group">

				<table class="table">

				<tbody>

					<tr>

						<td>

							<div class="btn-group-vertical" data-toggle="buttons">

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option1" > $0-99

							</label>

							<label class="btn btn btn-outline-dark">

								<input type="radio" name="options" id="option2"> $151-200

							</label>

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option3" >  $301-400

							</label>

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option4" > $501-600

							</label>

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option5" > $701-800

							</label>

						</div>

						</td>

						<td>

							<div class="btn-group-vertical" data-toggle="buttons">

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option6" > $100-150

							</label>

							<label class="btn btn btn-outline-dark">

								<input type="radio" name="options" id="option7"> $201-300

							</label>

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option8" > $401-500

							</label>

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option9" > $601-700

							</label>

							<label class="btn btn-outline-dark">

								<input type="radio" name="options" id="option10" > $801+

							</label>

						</div>

						</td>

						</tr>

						</tbody>

						</table>



			

			</div>

			<input type="button" name="next" class="next btn btn-info next-btn" value="Next" onclick="showhomeset()"/>

		</div>

	</fieldset>









	<fieldset class="text-center">

	<h2> How much shade does your roof get from 10am - 2pm?</h2>

	

	<div class="form-group">

			<div class="btn-group-vertical" data-toggle="buttons">

				<label class="btn btn-outline-dark" >

					<input type="radio" name="options-shade" id="option1"> No Shade

				</label>

				<label class="btn btn btn-outline-dark" >

					<input type="radio" name="options-shade" id="option2" > A Little Shade

				</label>

				<label class="btn btn-outline-dark" >

					<input type="radio" name="options-shade" id="option3" > A Lot of Shade

				</label>

				<label class="btn btn btn-outline-dark" >

					<input type="radio" name="options-shade" id="option4" > Not Sure

				</label>

			

			</div>

	</div>

	<input type="button" name="next" class="next btn btn-info next-btn" value="Next" />

	</fieldset>



	<fieldset class="text-center">

	<h2> How's your credit score?

</h2>

	

	<div class="form-group">

			<div class="btn-group-vertical" data-toggle="buttons">

				<label class="btn btn-outline-dark" >

					<input type="radio" name="options-score" id="option1" > Excellent (750+)

				</label>

				<label class="btn btn btn-outline-dark" >

					<input type="radio" name="options-score" id="option2" > Good (700-750)

					</label>

				<label class="btn btn-outline-dark" >

					<input type="radio" name="options-score" id="option3" > Fair (650-700)

				</label>

				<label class="btn btn btn-outline-dark" >

					<input type="radio" name="options-score" id="option4" > Poor (Less Than 650)

				</label>

			

			</div>

	</div>

	<input type="button" name="next" class="next btn btn-info next-btn" value="Next" onclick="showCongrat()"/>

	</fieldset>

	

	<fieldset id="resultSet" class="text-center">



	<div id="preloader2">

	   <img src="/landing/images/solarloader2.gif" style="width: 59%; margin:auto; margin-top: -2%;"/>

	</div>

	<div id="congratSection">

			<h2><span style ="color:#f39c12;">Congrats!</span></h2>

			<h3>You've qualified for <span style ="color:#f39c12;">2</span> active solar incentives for homeowners in <span class="zipvar" style="display:inline; color:#f39c12;"></span></h3>

			<p>

				

				<br/><br/>

				What's your name? <br/>

				

			(so we know who to address your custom saving report to)

			</p>



			<div class="form-group" style="text-align:left;">

			<label for="first_name">First Name</label>

			<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">

			</div>

			<div class="form-group" style="text-align:left;">

			<label for="last_name">Last Name</label>

			<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">

			</div>

			

			<input type="button" name="next" class="nextCongrat btn btn-info next-btn" value="Next" />

	</div>

	</fieldset>

	

	<fieldset id="emailSet" class="text-center">

	<h2>What's your email?</h2>

	<p>

		

	(so we can send your custom solar savings report)

	</p>



	<div class="form-group" style="text-align:left;">

		<label for="email">Email address*</label>

		<input type="email" class="form-control" required id="email" id ="email" name="email" placeholder="Email">

	</div>

	

	<input type="button" name="next" class="nextEmail btn btn-info next-btn" value="Next" />

	</fieldset>



	<fieldset class="text-center">

		<h2>What's your address?</h2>

		<p>

			

		(so we can better calculate your savings - this information isn't used for anything else)

		</p>



		<div class="form-group">

		

		<input class="form-control" name="address" placeholder="Address"  id = "address" style="margin:auto; "></textarea>

		</div>

		

		<input type="button" name="next" class="nextAddress btn btn-info next-btn" value="Next" />

	</fieldset>



	<fieldset class="text-center">

		<h2>Verify your phone number</h2>

		<p>

			

		Verify that you are a real person to qualify

		</p>



		<div class="form-group">

	

	<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" style="margin-top:6%;">

	</div>

		<input type="submit" name="submit" class="submit btn btn-success next-btn" value="Submit" id ="submitBtn"/><br/><br/>

		<p class="my-3 small ml-auto mr-auto">*By hitting 'See How Much You Can Save', you authorize Homesolarincentives.com and up to <a href="#" class="open-modal2 pum-trigger" style="cursor: pointer;">4 solar companies</a> to call and send you text messages on the phone number you submitted above using an automated dialer regarding solar products. This will override any federal or state Do Not Call lists. Consent is not a requirement of purchase. Data and text message rates will apply. <a href="/privacy-policy/">Privacy Policy</a> | <a href="/ccpa/">Do Not Sell</a></p>

		

	</fieldset>

    



	</form>	<!-- form ends from here -->



	

</div>	

</section>

<div class="progress">

	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 20%;background: #99E766;"></div>

</div>

<script>

function getZip(){

	

	var zipvalue = $('#zip').val();

	



	$('.zipvar').html(zipvalue);

	$("#preloader").show();

		setTimeout(function() { $("#preloader").hide(); }, 4000);

		$("#solarincent").hide();

		setTimeout(function() { $("#solarincent").show(); }, 4000);

	$.ajax({

      url: "https://zip.getziptastic.com/v2/US/" + zipvalue,

      cache: false,

      dataType: "json",

      type: "GET",

      success: function(result, success) {

        

        var placeName = result.city + ", " + result.state;

        $(".cityvar").html(placeName);

		



      }



    });



}

function showhomeset(){

	document.getElementById('solarset').style.display = "none";

	document.getElementById('homeset').style.display = "block";

}

function showemailSet(){

	document.getElementById('resultSet').style.display = "none";

	document.getElementById('emailSet').style.display = "block";

}

function showCongrat(){

		$("#preloader2").show();

		setTimeout(function() { $("#preloader2").hide(); }, 9000);

		$("#congratSection").hide();

		setTimeout(function() { $("#congratSection").show(); }, 9000);

		$(".topbanner").css({"background":"white"});

		

}



</script>



<script>

$(document).ready(function(){  

  

  var form_count = 1, form_count_form, next_form, total_forms;

  total_forms = $("fieldset").length;  

  $(".nextZip").click(function(){

	  

	var zipvalue = $('#zip').val();

    var error_message = "" ;

	

	if(!zipvalue) {

		error_message +="*Please enter your zip code.";
		

	}

	

	if(error_message) {

		$('.alert-success').removeClass('hide').html(error_message);
        $(".form-control").css("border", "1px solid red !important");
		return false;

		exit;

	} else {

		

		getZip();

		form_count_form = $(this).parent();

		next_form = $(this).parent().next();

		next_form.show();

		form_count_form.hide();

		setProgressBar(++form_count);

  } 

    

  });  

  var error_message = "" ;

  $(".nextCongrat").click(function(){

	  

	  

	  var firstname = $('#first_name').val();

	  var lastname = $('#last_name').val();

	 

	  error_message="";

	  if(!firstname) {

		  error_message +="<br>Please Fill First Name.";

	  }

	  if(!lastname) {

		  error_message +="<br>Please Fill Last Name.";

	  }

	  

	  

	  if(error_message) {

		  $('.alert-success').addClass('show').html(error_message);

		  

		 return;

		

	  } else {

		  

		$('.alert-success').addClass('hide').html(error_message);

		$('.alert-success').removeClass('show').html(error_message);

		  showemailSet();

		  form_count_form = $(this).parent();

		  next_form = $(this).parent().next();

		  next_form.show();

		  form_count_form.hide();

		  setProgressBar(++form_count);

	} 

	  

	});  



	$(".nextEmail").click(function(){

	  

	  var emailvalue = $('#email').val();

	  var error_message = "" ;

	  

	  if(!emailvalue) {

		  error_message +="<br>Please Fill In Your Email.";

	  }

	  

	  if(error_message) {

		  $('.alert-success').removeClass('hide').html(error_message);

		  return false;

		  exit;

	  } else {

		  

		$('.alert-success').addClass('hide').html(error_message);

		$('.alert-success').removeClass('show').html(error_message);

		  form_count_form = $(this).parent();

		  next_form = $(this).parent().next();

		  next_form.show();

		  form_count_form.hide();

		  setProgressBar(++form_count);

	} 

	  

	});  



	$(".nextAddress").click(function(){

	  

	  var addressvalue = $('#address').val();

	  var error_message = "" ;

	  

	  if(!addressvalue) {

		  error_message +="<br>Please Fill In Your Address.";

	  }

	  

	  if(error_message) {

		  $('.alert-success').removeClass('hide').html(error_message);

		  return false;

		  exit;

	  } else {

		$('.alert-success').addClass('hide').html(error_message);

	

		

		  form_count_form = $(this).parent();

		  next_form = $(this).parent().next();

		  next_form.show();

		  form_count_form.hide();

		  setProgressBar(++form_count);

	} 

	  

	});  



    $("#submitBtn").click(function(){

    

    

    

	if(!$("#mobile").val()) {

		error_message+="<br>Please Fill Mobile Number";

	}

	if(error_message) {

		$('.alert-success').removeClass('hide').html(error_message);

		return false;

	} else {

		$('.alert-success').addClass('hide').html(error_message);

		alert ('submit');

		return true;    

	}    





	});







  $(".next").click(function(){

    

	$('.alert-success').addClass('hide').html(error_message);

    form_count_form = $(this).parent();

    next_form = $(this).parent().next();

    next_form.show();

    form_count_form.hide();

    setProgressBar(++form_count);

  });  



  setProgressBar(form_count);  

  function setProgressBar(curStep){

    var percent = parseFloat(100 / total_forms) * curStep;

    percent = percent.toFixed();

    $(".progress-bar")

      .css("width",percent+"%")

      .html(percent+"%");   

  } 



 

});





</script>

<?php include('footer.php');?> 