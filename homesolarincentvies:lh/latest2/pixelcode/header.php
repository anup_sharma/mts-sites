<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?> class="no-js">

    <head>
        <script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10100514'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KMRKCRS');</script>
<!-- End Google Tag Manager -->
	
<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10100514'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>
		
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php endif; ?>
        

        <?php wp_head(); ?>

<meta name="google-site-verification" content="g9fs3LePwi0BZGMij2ZdrHPiU8Dl_8XxzKfi1BExRvg" />		
		
<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','o3df8');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T9FDNR7');</script>
<!-- End Google Tag Manager -->	

<!-- TikTok Pixel Code Start --> <script> !function (w, d, t) {   w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};   ttq.load('C1VF31IAIIHA8PLC3ME0');   ttq.page(); }(window, document, 'ttq'); </script> <!-- TikTok Pixel Code End -->
    </head>

    <body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMRKCRS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9FDNR7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <div id="page" class="site">
            <div class="main-header">
                <div class="bottom-header">
                    <div class="site-inner">
                        <header id="masthead" class="site-header" role="banner">
                            <div class="site-header-main">
                                <div class="site-branding">
                                    <?php twentysixteen_the_custom_logo(); ?>

                                    <?php if ( is_front_page() && is_home() ) : ?>
                                    <h1 class="site-title">
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                            <?php bloginfo( 'name' ); ?>
                                        </a>
                                    </h1>
                                    <?php else : ?>
                                    <p class="site-title">
                                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                            <?php bloginfo( 'name' ); ?>
                                        </a>
                                    </p>
                                    <?php endif;

								$description = get_bloginfo( 'description', 'display' );
								if ( $description || is_customize_preview() ) : ?>
                                    <p class="site-description">
                                        <?php echo $description; ?>
                                    </p>
                                    <?php endif; ?>
                                </div>
                                <!-- .site-branding -->
                                <?php if(!is_page(array(576, 588))){ ?>
									<?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>
									<button id="menu-toggle" class="menu-toggle"><i class="fa fa-fw fa-bars" aria-hidden="true"></i></button>

									<div id="site-header-menu" class="site-header-menu">
										<?php if ( has_nav_menu( 'primary' ) ) : ?>
										<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
											<?php
													wp_nav_menu( array(
														'theme_location' => 'primary',
														'menu_class'     => 'primary-menu',
													 ) );
												?>
										</nav>
										<!-- .main-navigation -->
										<?php endif; ?>
										<?php if ( has_nav_menu( 'social' ) ) : ?>
										<nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentysixteen' ); ?>">
											<?php
													wp_nav_menu( array(
														'theme_location' => 'social',
														'menu_class'     => 'social-links-menu',
														'depth'          => 1,
														'link_before'    => '<span class="screen-reader-text">',
														'link_after'     => '</span>',
													) );
												?>
										</nav>
										<!-- .social-navigation -->
										<?php endif; ?>
									</div>
									<!-- .site-header-menu -->
									<?php endif; ?>
                               	<?php } ?>
                            </div>
                            <!-- .site-header-main -->

                            <?php if ( get_header_image() ) : ?>
                            <?php
								/**
								 * Filter the default twentysixteen custom header sizes attribute.
								 *
								 * @since Twenty Sixteen 1.0
								 *
								 * @param string $custom_header_sizes sizes attribute
								 * for Custom Header. Default '(max-width: 709px) 85vw,
								 * (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px'.
								 */
								$custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
							?>
                                <div class="header-image">
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<img src="<?php header_image(); ?>" srcset="<?php echo esc_attr( wp_get_attachment_image_srcset( get_custom_header()->attachment_id ) ); ?>" sizes="<?php echo esc_attr( $custom_header_sizes ); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
								</a>
                                </div>
                                <!-- .header-image -->
                                <?php endif; // End header image check. ?>
                        </header>
                        <!-- .site-header -->
                    </div>
                    <div class="header-overly"></div>
                </div>
            </div>
            <div class="content-body">
                <div class="site-inner">
                    <a class="skip-link screen-reader-text" href="#content">
                        <?php _e( 'Skip to content', 'twentysixteen' ); ?>
                    </a>


                    <div id="content" class="site-content">
