<?php 
include('header.php');
?>
<title>Home Solar Incentives</title>
<script type="text/javascript" src="script/form.js"></script>

<style type="text/css">
  #user_form fieldset:not(:first-of-type) {
    display: none;
  }
 body {
    
    background: url(https://homesolarincentives.com/home-new/images/banner.v2.jpg) no-repeat center center;
    background-size: cover;
    
}
section.topbanner {
    background-color: rgba(245, 245, 245, 0.9);
    border-width: 2px;
    border-color: rgb(0, 0, 0);
    border-radius: 20px;
    padding-bottom: 50px;
    padding-top: 3%;
    box-shadow: rgb(0, 0, 0) 0px 0px 15px;
}
.progress {
    width: 99%;
    border: 1px solid #ddd;
}
h2 {
    margin-left: 29%;
    font-weight: 600;
}


</style>
<?php include('container.php');?>
<div class="progress">
	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 20%;background: #7dba93;"></div>
	</div>
<section class="topbanner">
<div class="container">

		
	
	<div class="alert alert-success hide"></div>	
	<form id="user_form" novalidate action="form_action.php"  method="post">	
	<fieldset>
	
	<div class="form-group">
			<img alt="" height="103" src="https://static.leadshook.io/upload/asm/solar%20icon-1587057839037-1587478826986.png" width="115" class="img-responsive" imageonload="" style="margin-left:40%;"><br/>
			<h2>New York State Solar Program</h2>	
			<h4 color="#3D3D3D" style="text-align: center;" ><br>
					In <strong>3</strong><strong>0 seconds</strong>, see if you qualify to go solar for $0 down and estimate your savings with the&nbsp;New York State Solar Program.<br>
					<br>
					This Federal and State-backed&nbsp;program&nbsp;helps&nbsp;you switch to solar energy for<strong>&nbsp;$0 out of pocket.</strong><br>
					<br>
					&nbsp;
			</h4>

	
	</div>

	<input type="button" class="next btn btn-info" style= "background: rgba(12,176,0,1); font-family: Arial; font-weight: 700; font-size: 20px; border:none; width: 30%; margin-left: 30%;" value="See If You Qualify" />
	</fieldset>	
	<fieldset>
	<h2> Enter your zip code to see if your home is in an approved area:</h2>
		<div class="form-group">
		<label for="email">Email address*</label>
		<input type="email" class="form-control" required id="email" name="email" placeholder="Email">
		<label for="password">Password*</label>
		<input type="password" class="form-control" name="password" id="password" placeholder="Password">
	</div>
	<div class="form-group">
	<label for="first_name">First Name</label>
	<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
	</div>
	<div class="form-group">
	<label for="last_name">Last Name</label>
	<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
	</div>
	<input type="button" name="previous" class="previous btn btn-default" value="Previous" />
	<input type="button" name="next" class="next btn btn-info" value="Next" />
	</fieldset>

	<fieldset>
	<h2> Step 3: New Add Personal Details</h2>
	<div class="form-group">
	<label for="first_name">First Name</label>
	<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
	</div>
	<div class="form-group">
	<label for="last_name">Last Name</label>
	<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
	</div>
	<input type="button" name="previous" class="previous btn btn-default" value="Previous" />
	<input type="button" name="next" class="next btn btn-info" value="Next" />
	</fieldset>

	<fieldset>
	<h2> Step 4: New Add Personal Details</h2>
	<div class="form-group">
	<label for="first_name">First Name</label>
	<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
	</div>
	<div class="form-group">
	<label for="last_name">Last Name</label>
	<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
	</div>
	<input type="button" name="previous" class="previous btn btn-default" value="Previous" />
	<input type="button" name="next" class="next btn btn-info" value="Next" />
	</fieldset>


	
	<fieldset>
	<h2>Step 5: Add Contact Information</h2>
	<div class="form-group">
	<label for="mobile">Mobile*</label>
	<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number">
	</div>
	<div class="form-group">
	<label for="address">Address</label>
	<textarea  class="form-control" name="address" placeholder="Communication Address"></textarea>
	</div>
	<input type="button" name="previous" class="previous btn btn-default" value="Previous" />
	<input type="submit" name="submit" class="submit btn btn-success" value="Submit" />
	</fieldset>
	




	</form>
		
		
		
		
</div>	
</section>
<?php include('footer.php');?> 