<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176059540-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176059540-4');
</script>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Home Solar Incentives</title>
	<meta name="robots" content="noindex,nofollow">
	<meta name="description" content=""/>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--styles-->
	<link rel="stylesheet" href="assets/css/style-top.css">

</head>
<body>
<header class="fixed-top">
	<div class="container">
		<div class="logo">
			<a href="/"><img class="logo" src="images/logo.jpg" alt="logo"></a>
		</div>
		<div class="startquote">
		</div>
	</div>
</header>
	<section class="main privacy">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-12 col-md-12">
					<div class="row m-md-auto">
						<form id="ccpa-form" class="needs-validation row" novalidate>
						<div class="col-12 col-md-12 col-lg-12 mt-3">
							<label><strong>Are you making this request on behalf of yourself or someone else?</strong></label>
							<select name="request" class="form-control rounded-0" required><option value="">---</option><option value="Myself">Myself</option><option value="Someone Else">Someone Else</option></select>
						</div>
						<div class="col-12 col-md-12 col-lg-12  mt-3">
							<label><strong>What type of request would you like to make? (Select at least one)</strong></label>
						</div>
						<div class="col-12 col-md-12 col-lg-12 mb-3 checkbox-select" style="position: relative">
							<label><input type="checkbox" class="my_check" name="type_request[]" value="Please provide access to my personal information"> <span>Please provide access to my personal information</span></label>
							<label><input type="checkbox" class="my_check" name="type_request[]" value="Please delete my personal information"> <span>Please delete my personal information</span></label>
							<label><input type="checkbox" class="my_check" name="type_request[]" value="Please do not sell my personal information"> <span>Please do not sell my personal information</span></label>
							<label><input type="checkbox" class="my_check" name="type_request[]" value="Ask Momentum Solar a question related to my personal information and/or the CCPA"> <span>Ask Momentum Solar a question related to my personal information and/or the CCPA</span></label>
						</div>
						<div class="col-12 col-md-12 col-lg-12  mt-3">
							<strong>In order for us to process your request, please provide the following information</strong><br>(please try to use the information previously provided to Momentum Solar to help us identify you)
						</div>
						<div class="col-12 col-md-12 col-lg-6 mt-3">
							<input type="text" name="first_name" class="form-control mt-3 rounded-0" id="validationCustom01" required placeholder="First name">
						</div>
						<div class="col-12 col-md-12 col-lg-6 mt-3">
							<input type="text" name="last_name" class="form-control mt-3 rounded-0" id="validationCustom02" required placeholder="Last name">
						</div>
						<div class="col-12 col-md-12 col-lg-6 mt-3">
							<input type="text" name="email" class="form-control mt-3 rounded-0" id="validationCustom03" required placeholder="Email">
						</div>
						<div class="col-12 col-md-12 col-lg-6 mt-3">
							<input type="text" name="phone" class="form-control mt-3 rounded-0 phone_us" maxlength="14" required id="validationCustom04" placeholder="Phone">
						</div>
						<div class="col-12 col-md-12 col-lg-12 mt-3">
							<input type="text" name="addres" class="form-control mt-3 rounded-0" maxlength="14" required id="validationCustom05" placeholder="Street Address">
						</div>
						<div class="col-12 col-md-12 col-lg-6 mt-3">
							<input type="text" name="city" class="form-control mt-3 rounded-0" id="validationCustom06" required placeholder="City">
						</div>
						<div class="col-12 col-md-12 col-lg-6 mt-3">
							<input type="text" name="state" class="form-control mt-3 rounded-0" id="validationCustom07" required placeholder="State">
						</div>
						<div class="col-12 col-md-12 col-lg-6 mt-3">
							<input type="text" name="zip" class="form-control mt-3 rounded-0" id="validationCustom08" required placeholder="Zip">
						</div>
						<div class="col-12 col-md-12 col-lg-12 mt-3">
							<label><strong>Please provide any additional information that may help us to confirm your identity or ask us a question</strong></label>
							<input type="text" name="question" class="form-control rounded-0" maxlength="14" id="validationCustom09" >
						</div>
						<div class="col-12 col-md-12 col-lg-12 mt-3">
							<input type="submit" value="Send">
						</div>
						<div class="havesend mt-3 mb-3"></div>
						</form>
					</div>				
				</div>
			</div>
		</div>
	</section>
	<?php 
      function generateRandomString($length = 25) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
    }
      
      $token = generateRandomString();
    if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||  isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
        $protocol = 'https://';
        }
        else {
        $protocol = 'http://';
        }
        $current_link = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
         
      ?>
      
<footer class="footer"><span class="copy">© 2021 Home Solar Incentives | <a href="/privacy-policy/">Privacy Policy</a></span></footer>



	<!--fontawesome-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
	<script>
		WebFont.load( {
			google: {
				families: [ 'Playfair+Display:400,700,900', 'Open+Sans:300,400,600,700,800' ]
			}
		} );
	</script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style-bottom.css">
	<style>
		.pac-container {
			z-index: 9999999;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<script src="js/scrolling-nav.js"></script>
	<script src="js/app.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/additional-methods.min.js"></script>
	<script src="js/lazyload.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script>
		$( '.phone_us' ).mask( '(000) 000-0000' );
		var placeSearch, autocomplete;
		var componentForm = {
			street_number: 'short_name',
			route: 'long_name',
			locality: 'long_name',
			administrative_area_level_1: 'short_name',
			country: 'long_name',
			postal_code: 'short_name'
		};
		function initAutocomplete() {
			// Create the autocomplete object, restricting the search to geographical
			// location types.
			autocomplete = new google.maps.places.Autocomplete(
				( document.getElementById( 'full_address' ) ), {
					types: [ 'geocode' ]
				} );
			// When the user selects an address from the dropdown, populate the address
			// fields in the form.
			autocomplete.addListener( 'place_changed', fillInAddress );
		}
		function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();
			for ( var component in componentForm ) {
				document.getElementById( component ).value = '';
				document.getElementById( component ).disabled = false;
			}
			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			for ( var i = 0; i < place.address_components.length; i++ ) {
				var addressType = place.address_components[ i ].types[ 0 ];
				if ( componentForm[ addressType ] ) {
					var val = place.address_components[ i ][ componentForm[ addressType ] ];
					document.getElementById( addressType ).value = val;
				}
			}
		}
		// Bias the autocomplete object to the user's geographical location,
		// as supplied by the browser's 'navigator.geolocation' object.
		function geolocate() {
			if ( navigator.geolocation ) {
				navigator.geolocation.getCurrentPosition( function ( position ) {
					var geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					var circle = new google.maps.Circle( {
						center: geolocation,
						radius: position.coords.accuracy
					} );
					autocomplete.setBounds( circle.getBounds() );
				} );
			}
		}
	</script>
	<!--Fading out "Get Free Quote" button in mobile-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete" async defer></script>
	<!-- Facebook Pixel Code -->

<script type="text/javascript">
  (function() {
      var field = 'xxTrustedFormCertUrl';
      var provideReferrer = false;
      var invertFieldSensitivity = false;
      var tf = document.createElement('script');
      tf.type = 'text/javascript'; tf.async = true;
      tf.src = 'http' + ('https:' == document.location.protocol ? 's' : '') +
        '://api.trustedform.com/trustedform.js?provide_referrer=' + escape(provideReferrer) + '&field=' + escape(field) + '&l='+new Date().getTime()+Math.random() + '&invert_field_sensitivity=' + invertFieldSensitivity;
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tf, s); }
  )();
</script>
<noscript>
    <img src="http://api.trustedform.com/ns.gif" />
</noscript>	
<script>
		jQuery( document ).ready( function ( $ ) {
			jQuery( "img.lazyload" ).lazyload();
			jQuery( '.lazy' ).lazyload()
		} );
	</script>
<style>
	label {
		width: 100%;
	}
	.checkbox-select label.error{
		position: absolute;
		bottom: -20px;
	}
	.havesend{
		color: #28a745;
		font-size: 18px;
	}
</style>	
</body>
</html>