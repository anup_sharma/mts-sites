<?php 
include('header.php');
?>
<title>Home Solar Incentives</title>

<!--<script type="text/javascript" src="script/form.js"></script>
-->

<style type="text/css">
  #user_form fieldset:not(:first-of-type) {
    display: none;
  }
  .navbar.navbar-default {
    background: white;
}
section.topbanner {
    background-color: rgba(245, 245, 245, 0.9);
    border-width: 2px;
    border-color: rgb(0, 0, 0);
    border-radius: 20px;
    padding-bottom: 50px;
    padding-top: 3%;
    box-shadow: rgb(0, 0, 0) 0px 0px 15px;
}
.progress {
    width: 99%;
    border: 1px solid #ddd;
}

h2 {
    text-align: center;
    font-weight: 600;
}

.next-btn {
	background: rgba(235,169,0,1);
    font-family: Arial;
    font-weight: 700;
    font-size: 23px;
    width: 250px;
    padding-top: 5px !important;
   
    padding-bottom: 5px !important;
    padding-left: 5px !important;
    padding-right: 5px !important;
    border: none;
    margin:auto;
}
p{
	text-align:center;
}
.btn-outline-dark{
    background-color: rgba(204,204,204,1) !important;
    color: #000000 !important;
    font-family: Arial;
    border-width: 1px !important;
    border-style: solid !important;
    border-color: #000000 !important;
    margin-top: 1%;
    padding: 4%;
    font-size: 18px;
}
.btn-group-vertical {
    width: 68%;
    padding: 2%;
}
.btn-group-vertical>.btn+.btn, .btn-group-vertical>.btn+.btn-group, .btn-group-vertical>.btn-group+.btn, .btn-group-vertical>.btn-group+.btn-group {
    margin-top: 5%;
    margin-left: 0;
}
label.btn.btn-outline-dark.active {
    background-color: rgb(125 186 147) !important;
	color: white !important;
}
.btn-group-vertical>.btn:first-child:not(:last-child) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
}
fieldset {
    max-width: 95%;
    padding-left: 2%;
}
.h3, h3 {
    text-align: center;
}
label.btn.btn-outline-dark {
    padding: 4%;
}
.alert.alert-success {
    background: #336699;
    color: white;
    border: none;
    width: 97%;
}

#preloader {
    
    top: 0;
    left: 0;
    width: 100%;
    height: 250px;
}
#preloader2 {
    
    top: 0;
    left: 0;
    width: 100%;
    height: auto;
	background:white;
}

#loader {
    display: block;
    position: relative;
    left: 50%;
    top: 50%;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #9370DB;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
}
#loader:before {
    content: "";
    position: absolute;
    top: 5px;
    left: 5px;
    right: 5px;
    bottom: 5px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #BA55D3;
    -webkit-animation: spin 3s linear infinite;
    animation: spin 3s linear infinite;
}
#loader:after {
    content: "";
    position: absolute;
    top: 15px;
    left: 15px;
    right: 15px;
    bottom: 15px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #FF00FF;
    -webkit-animation: spin 1.5s linear infinite;
    animation: spin 1.5s linear infinite;
}
@-webkit-keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
    }
}
@keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    100% {
        -webkit-transform: rotate(360deg);
        -ms-transform: rotate(360deg);
        transform: rotate(360deg);
    }
}

div#congratSection {
    display: none;
}
</style>
<script>

$(document).ready(function() {
	$("#congratSection").hide();
// Hide the div

$(".nextPhone").click(function(){
       
	$("#user_form").submit();
	$("#congratSection").show();
	   
  
	  
	  });
  
	  $('#user_form').on('submit', function (e) {
		 // alert ('forSubmitted');
		document.getElementById('phoneSet').style.display = "none";
			e.preventDefault();
			
			$.ajax({
			  type: 'post',
			  url: 'ajax.php',
			  data: $('form').serialize(),
			  success: function (data) {
			   //alert(data);
			  }
			});
  
		  });

		function checkZip(value) {
       		 return (/(^\d{5}$)|(^\d{5}-\d{4}$)/).test(value);
   		 }

			$('#zip').on('keyup', function (e) {
				e.preventDefault;
				var value = $('#zip').val();
				if (checkZip(value)) {
					$('.validateZipcode').html("Valid Zip Code");
				} else {
					$('.validateZipcode').html("Invalid Zip Code");        }
			});




});
</script>

<script type="text/javascript">
  function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('address')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
  }

  function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

  }
</script>


<?php include('container.php');?>
<div class="progress">
	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 20%;background: #7dba93;"></div>
	</div>
<section class="topbanner">
<div class="container">

		
	
	<div class="alert alert-success hide"></div>	
	<!-- form starts from here -->
	<form id="user_form"  data-toggle="validator" method="post">	
	<fieldset class="text-center">
	
	<div class="form-group text-center">
			<img alt="" height="103" src="https://static.leadshook.io/upload/asm/solar%20icon-1587057839037-1587478826986.png" width="115" class="img-responsive" imageonload="" style="margin:auto;"><br/>
			
			<h4 color="#3D3D3D" style="text-align: center;" ><br>
					In <strong>3</strong><strong>0 seconds</strong>, see if you qualify to go solar for $0 down and estimate your savings with federal & state solar programs.<br>
					<br>
					This Federal and State-backed&nbsp;program&nbsp;helps&nbsp;you switch to solar energy for<strong>&nbsp;$0 out of pocket.</strong><br>
					<br>
					&nbsp;
			</h4>

	
	</div>

	<input type="button" class="next btn btn-info" style= "background: rgba(12,176,0,1); font-family: Arial; font-weight: 700; font-size: 20px; border:none; " value="See If You Qualify" />
	</fieldset>	
	<fieldset style="text-align:center;">
	<p><h3>Enter your zip code to see if your home is in an approved area:</h3>
		

		<input type="zip" class="form-control" required id="zip" name="zip" placeholder="Zip" style="width:auto; margin:auto; margin-top: 2%; display:inline;"> <div class="validateZipcode" style="font-size:14px; margin-left: 1%; margin-top: 1%; color:green;"></div>
	
</p>


	<input type="button"  name="next" class="nextZip btn btn-info next-btn" value="Next >"  style="margin:auto; margin-top:2%;" />
	</fieldset>
	
	<fieldset id ="solarset" class="text-center"> 
	<div id="preloader">
	
	<h3> Checking for Solar programs in <span class="zipvar" style="display:inline;"></span> ...</h3>
		<span id="loader"></span>
	</div>
	<div id="solarincent">


			<h2> Solar incentives found for <span class="cityvar" style="display:inline;"></span>:</h2>
			<p>
			<h3>Now let's qualify for rebates, calculate your extensive savings report, and see how much you can save. <br/>
				How much is your average monthly power bill?</h3>
			</p>
			<div class="form-group">
				<table class="table">
				<tbody>
					<tr>
						<td>
							<div class="btn-group-vertical" data-toggle="buttons">
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option1" value="$0-99" > $0-99
							</label>
							<label class="btn btn btn-outline-dark">
								<input type="radio" name="options" id="option2" value="$151-200"> $151-200
							</label>
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option3" value="$301-400" >  $301-400
							</label>
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option4" value="$501-600"> $501-600
							</label>
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option5" value="$701-800" > $701-800
							</label>
						</div>
						</td>
						<td>
							<div class="btn-group-vertical" data-toggle="buttons">
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option6" > $100-150
							</label>
							<label class="btn btn btn-outline-dark">
								<input type="radio" name="options" id="option7"> $201-300
							</label>
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option8" > $401-500
							</label>
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option9" > $601-700
							</label>
							<label class="btn btn-outline-dark">
								<input type="radio" name="options" id="option10" > $801+
							</label>
						</div>
						</td>
						</tr>
						</tbody>
						</table>

			
			</div>
			<input type="button" name="next" class="next btn btn-info next-btn" value="Next" onclick="showhomeset()"/>
		</div>
	</fieldset>


	<fieldset id ="homeset" class="text-center"> 
	<h2> Do you own your home or rent?</h2>
	<p>
	Sorry! Homeowneres only
	</p>
	<div class="form-group">
			<div class="btn-group-vertical" data-toggle="buttons">
				<label class="btn btn-outline-dark" >
					<input type="radio" name="options-homerent" id="option1" value="Yes" > I Own My Home
				</label>
				<label class="btn btn btn-outline-dark" >
					<input type="radio" name="options-homerent" id="option2" value="No" > I Rent My Home
				</label>
			
			</div>
	</div>
	<input type="button" name="next" class="next btn btn-info next-btn" value="Next" />
	</fieldset>


	<fieldset class="text-center">
	<h2> How much shade does your roof get from 10am - 2pm?</h2>
	
	<div class="form-group">
			<div class="btn-group-vertical" data-toggle="buttons">
				<label class="btn btn-outline-dark" >
					<input type="radio" name="options-shade" id="option1" value="No Shade"> No Shade
				</label>
				<label class="btn btn btn-outline-dark" >
					<input type="radio" name="options-shade" id="option2" value="A Little Shade" > A Little Shade
				</label>
				<label class="btn btn-outline-dark" >
					<input type="radio" name="options-shade" id="option3" value="A Lot of Shade" > A Lot of Shade
				</label>
				<label class="btn btn btn-outline-dark" >
					<input type="radio" name="options-shade" id="option4" value="Not sure" > Not Sure
				</label>
			
			</div>
	</div>
	<input type="button" name="next" class="next btn btn-info next-btn" value="Next" />
	</fieldset>

	<fieldset class="text-center">
	<h2> How's your credit score?
</h2>
	
	<div class="form-group">
			<div class="btn-group-vertical" data-toggle="buttons">
				<label class="btn btn-outline-dark" >
					<input type="radio" name="options-score" id="option1" value="Excellent (750+)"> Excellent (750+)
				</label>
				<label class="btn btn btn-outline-dark" >
					<input type="radio" name="options-score" id="option2"  value="Good (700-750)"> Good (700-750)
					</label>
				<label class="btn btn-outline-dark" >
					<input type="radio" name="options-score" id="option3" value="Fair (650-700)" > Fair (650-700)
				</label>
				<label class="btn btn btn-outline-dark" >
					<input type="radio" name="options-score" id="option4" value="Poor (Less Than 650)"> Poor (Less Than 650)
				</label>
			
			</div>
	</div>
	<input type="button" name="next" class="next btn btn-info next-btn" value="Next" onclick="shownameSection()"/>
	</fieldset>
	
	<fieldset id="resultSet" class="text-center">

	<div id="preloader2">
	   <img src="/landing/images/solarloader2.gif" style="width: 59%; margin:auto; margin-top: -2%;"/>
	</div>
	<div id="nameSection">
	
				What's your name? <br/>
				
			(so we know who to address your custom saving report to)
			</p>

			<div class="form-group">
			<label for="first_name">First Name</label>
			<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
			</div>
			<div class="form-group">
			<label for="last_name">Last Name</label>
			<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
			</div>
			
			<input type="button" name="next" class="next btn btn-info next-btn" value="Next"  onclick="showemailSet()"/>
	</div>
	</fieldset>
	
	<fieldset id="emailSet" class="text-center">
	<h2>What's your email?</h2>
	<p>
		
	(so we can send your custom solar savings report)
	</p>

	<div class="form-group" style="text-align:left;">
		<label for="email">Email address*</label>
		<input type="email" class="form-control" required id="email" id ="email" name="email" placeholder="Email">
	</div>
	
	<input type="button" name="next" class="nextEmail btn btn-info next-btn" value="Next" />
	</fieldset>

	<fieldset class="text-center">
		<h2>What's your address?</h2>
		<p>
			
		(so we can better calculate your savings - this information isn't used for anything else)
		</p>

		<div class="form-group">
		
		<input class="form-control" name="address" placeholder="Address"  id = "address" style="margin:auto; "></textarea>
		</div>
		
		<input type="button" name="next" class="nextAddress btn btn-info next-btn" value="Next" />
	</fieldset>

	<fieldset class="text-center" id="phoneSet">
		<h2>Verify your phone number</h2>
		<p>
			
		Verify that you are a real person to qualify
		</p>

		<div class="form-group">
	
	<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number" style="margin-top:6%;">
	<input type="hidden" name="utm_source" id="utm_source" value="<?php echo $_GET['utm_source'];?>">
                              <input type="hidden" name="utm_medium" id="utm_medium" value="<?php echo $_GET['utm_medium'];?>">
                              <input type="hidden" name="utm_campaign" id="utm_campaign" value="<?php echo $_GET['utm_campaign'];?>">
                              <input type="hidden" name="utm_content" id="utm_content" value="<?php echo $_GET['utm_content'];?>">
 <!-- get optin url-->
 <input type="hidden" name="optinurl" id="optinurl" value="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;?>" />
	</div>
		<input type="button" name="next" class="nextPhone btn btn-success next-btn" value="Next" > </input>

		
	</fieldset>
    

	</form>	<!-- form ends from here -->
	<div id="congratSection">
			<h2><span style ="color:#f39c12;">Congrats!</span></h2>
			<h3>You've qualified for active solar incentives for homeowners in <span class="zipvar" style="display:inline; color:#f39c12;"></span>. <br/>Someone will contact you soon. Thank you!!!</h3>
			

			
	</div>
	
</div>	
</section>
<script>
function getZip(){
	
	var zipvalue = $('#zip').val();
	

	$('.zipvar').html(zipvalue);
	$("#preloader").show();
		setTimeout(function() { $("#preloader").hide(); }, 4000);
		$("#solarincent").hide();
		setTimeout(function() { $("#solarincent").show(); }, 4000);
	$.ajax({
      url: "https://zip.getziptastic.com/v2/US/" + zipvalue,
      cache: false,
      dataType: "json",
      type: "GET",
      success: function(result, success) {
        
        var placeName = result.city + ", " + result.state;
        $(".cityvar").html(placeName);
		

      }

    });

}
function showhomeset(){
	document.getElementById('solarset').style.display = "none";
	document.getElementById('homeset').style.display = "block";
}

function shownameSection(){
	$("#preloader2").show();
	$("#nameSection").hide();
	setTimeout(function() { $("#preloader2").hide(); }, 9000);
	setTimeout(function() { $("#nameSection").show(); }, 9000);
//	document.getElementById('emailSet').style.display = "block";
}
function showemailSet(){
	$("#emailSet").show();

}
function showCongrat(){
		$("#preloader2").show();
		setTimeout(function() { $("#preloader2").hide(); }, 9000);
		$("#congratSection").hide();
		setTimeout(function() { $("#congratSection").show(); }, 9000);
		$(".topbanner").css({"background":"white"});
		
}

</script>

<script>
$(document).ready(function(){  
  
  var form_count = 1, form_count_form, next_form, total_forms;
  total_forms = $("fieldset").length;  
  $(".nextZip").click(function(){
	  
	var zipvalue = $('#zip').val();
    var error_message = "" ;
	
	if(!zipvalue) {
		error_message +="<br>Please Fill Zip code.";
	}
	
	if(error_message) {
		$('.alert-success').removeClass('hide').html(error_message);
		return false;
		exit;
	} else {
		
		getZip();
		form_count_form = $(this).parent();
		next_form = $(this).parent().next();
		next_form.show();
		form_count_form.hide();
		setProgressBar(++form_count);
  } 
    
  });  
  var error_message = "" ;
  $(".nextCongrat").click(function(){
	  
	  
	  var firstname = $('#first_name').val();
	  var lastname = $('#last_name').val();
	 
	  error_message="";
	  if(!firstname) {
		  error_message +="<br>Please Fill First Name.";
	  }
	  if(!lastname) {
		  error_message +="<br>Please Fill Last Name.";
	  }
	  
	  
	  if(error_message) {
		  $('.alert-success').addClass('show').html(error_message);
		  
		 return;
		
	  } else {
		  
		$('.alert-success').addClass('hide').html(error_message);
		$('.alert-success').removeClass('show').html(error_message);
		//  showemailSet();
		  form_count_form = $(this).parent();
		  next_form = $(this).parent().next();
		  next_form.show();
		  form_count_form.hide();
		  setProgressBar(++form_count);
	} 
	  
	});  

	$(".nextEmail").click(function(){
	  
	  var emailvalue = $('#email').val();
	  var error_message = "" ;
	  
	  if(!emailvalue) {
		  error_message +="<br>Please Fill In Your Email.";
	  }
	  
	  if(error_message) {
		  $('.alert-success').removeClass('hide').html(error_message);
		  return false;
		  exit;
	  } else {
		  
		$('.alert-success').addClass('hide').html(error_message);
		$('.alert-success').removeClass('show').html(error_message);
		  form_count_form = $(this).parent();
		  next_form = $(this).parent().next();
		  next_form.show();
		  form_count_form.hide();
		  setProgressBar(++form_count);
	} 
	  
	});  

	$(".nextAddress").click(function(){
	  
	  var addressvalue = $('#address').val();
	  var error_message = "" ;
	  
	  if(!addressvalue) {
		  error_message +="<br>Please Fill In Your Address.";
	  }
	  
	  if(error_message) {
		  $('.alert-success').removeClass('hide').html(error_message);
		  return false;
		  exit;
	  } else {
		$('.alert-success').addClass('hide').html(error_message);
	
		
		  form_count_form = $(this).parent();
		  next_form = $(this).parent().next();
		  next_form.show();
		  form_count_form.hide();
		  setProgressBar(++form_count);
	} 
	  
	});  

    $("#submitBtn").click(function(){
    
    
    
	if(!$("#mobile").val()) {
		error_message+="<br>Please Fill Mobile Number";
	}
	if(error_message) {
		$('.alert-success').removeClass('hide').html(error_message);
		return false;
	} else {
		$('.alert-success').addClass('hide').html(error_message);
		
	}    


	});



  $(".next").click(function(){
    
	$('.alert-success').addClass('hide').html(error_message);
    form_count_form = $(this).parent();
    next_form = $(this).parent().next();
    next_form.show();
    form_count_form.hide();
    setProgressBar(++form_count);
  });  

  setProgressBar(form_count);  
  function setProgressBar(curStep){
    var percent = parseFloat(100 / total_forms) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")
      .html(percent+"%");   
  } 

 
});


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXhjcUXPc7k2JwT0EI8MuiHkn7OJDIYCk&libraries=places&callback=initAutocomplete" async defer></script>

<?php include('footer.php');?> 